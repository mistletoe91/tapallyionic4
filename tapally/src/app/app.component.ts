import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
//import { SQLiteDatabaseConfig } from '@ionic-native/sqlite';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private sqlite: SQLite
  ) {


    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
        db.executeSql('create table danceMoves(name VARCHAR(32))', [])
          .then(() => console.log('Executed SQL'))
          .catch(e => console.log(e));
    })
    .catch(e => console.log(e));



    //this.router.navigate(['/enterphonenumber']);

    //this.deleteTable ();//for testing

    //Check if its not first time
    /*
    this.sqlite.create({
      name: 'tapally.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
        console.log ("Ok got the connection");
        db.executeSql('create table cnf(key VARCHAR(32), val VARCHAR(32))', [])
          .then(() => {
              console.log ("user came first time");
              //this.router.navigate(['/enterphonenumber']);
          })
          .catch(e => {
              console.log ("seems like user already exists");
              //this.router.navigate(['/enterphonenumber']);//dajs
          });
        })
    .catch(e => console.log(e));
    */

    this.initializeApp();
  }

  deleteTable (){
    this.sqlite.create({
      name: 'tapally.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
        db.executeSql('drop table cnf', [])
          .then(() => console.log('Droped cnf'))
          .catch(e => console.log(e));
        })
    .catch(e => console.log(e));
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
