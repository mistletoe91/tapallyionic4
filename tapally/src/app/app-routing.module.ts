import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

/*
const routes: Routes = [
  { path: '',  loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'enterphonenumber', loadChildren: './enterphonenumber/enterphonenumber.module#EnterphonenumberPageModule' },
  { path: 'otp', loadChildren: './otp/otp.module#OtpPageModule' }
];
*/

const routes: Routes = [
  { path: '',  loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'enterphonenumber', loadChildren: './enterphonenumber/enterphonenumber.module#EnterphonenumberPageModule' },
  { path: 'otp', loadChildren: './otp/otp.module#OtpPageModule' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
