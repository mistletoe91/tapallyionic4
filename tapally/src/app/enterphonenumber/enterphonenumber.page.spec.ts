import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnterphonenumberPage } from './enterphonenumber.page';

describe('EnterphonenumberPage', () => {
  let component: EnterphonenumberPage;
  let fixture: ComponentFixture<EnterphonenumberPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnterphonenumberPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnterphonenumberPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
