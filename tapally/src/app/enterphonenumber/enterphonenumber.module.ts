import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes,Router, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EnterphonenumberPage } from './enterphonenumber.page';
 
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { SQLiteDatabaseConfig } from '@ionic-native/sqlite';

const routes: Routes = [
  {
    path: '',
    component: EnterphonenumberPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EnterphonenumberPage]
})
export class EnterphonenumberPageModule {

  constructor(
    private router: Router,
    private sqlite: SQLite
  ) {
  }

}
