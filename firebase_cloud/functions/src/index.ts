
const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();


/*
Housekeeping : on chatusers/<id>/* update, update other places too. e.g.
- friends/friend_id/<id>/*
*/
exports.duplicateUserDetailsToOtherNodes = functions.database.ref('/chatusers/{nodeId}')
    .onUpdate((change, context) => {

      var countryCode = change.after.child('countryCode').val();
      var deviceToken = change.after.child('deviceToken').val();
      var disc = change.after.child('disc').val();
      var displayName = change.after.child('displayName').val();
      var mobile = change.after.child('mobile').val();
      var photoURL = change.after.child('photoURL').val();
      var timestamp = change.after.child('timestamp').val();
      var uid = change.after.child('uid').val();
      var useremail = change.after.child('useremail').val();

      admin.database().ref('/friends/'+uid).once('value')
      .then(my_snapshot => {
          my_snapshot.forEach(function(myFriendSnapshot) {
              var myFriendKey = myFriendSnapshot.key;

              //Now go to each friend one by one
              admin.database().ref('/friends/').child(myFriendKey).child(context.params.nodeId).update({
                 countryCode : countryCode,
                 deviceToken : deviceToken,
                 disc : disc,
                 displayName : displayName,
                 mobile : mobile,
                 photoURL : photoURL,
                 timestamp : timestamp,
                 uid : uid,
                 useremail : useremail,
               });

          });
      })
      .catch(error => {});

      return 1; // IT SHOULD RETURN NON-NULL VALUE

    });

/*

Algo :
Update Contact List

1. Get the string and explode it in for loop
2. For each phone# check if "phones" record exist and get uid
3. Make friend record with INPUT uid
4. Update "phones" with in_contact_list_of with INPUT uid


https://us-central1-tapallyionic3.cloudfunctions.net/updateContactList?inputuid=m4Fs8izONXhTixz72Xit4aVA6L83&inputStr=6472047581,6477782850,1231231234

*/

/*
https://us-central1-tapallyionic3.cloudfunctions.net/updateContactList?inputuid=m4Fs8izONXhTixz72Xit4aVA6L83&inputStr=undefssined,2886,8294,1450717210,6863399999,8872970638,9052766674,411,6476480230,9057827000,9057993334,4163035990,6476485254,4162387515,6478282115,6478359672,8001028182,4167482526,4212996386,9059653031,6478361421,6479575793,9052799229,9872970415,8699339338,4165775128,5735535211,6473911092,6479266520,4163019347,6474581357,6479613375,9888377307,6044465841,4166378000,6476386468,4163017503,6476244599,6477662013,6476288406,4169879853,6472411326,4163425002,6476223049,4168760144,6473892206,4163600688,4167315477,9231546717,4165351876,6473000873,9057897247,4169097194,4169998778,4165670945,4167404000,4168716100,8872510072,2505352172,6476277995,6472731419,6477828185,6472444786,6478853793,4168631800,4168470114,6474052083,6472971608,7446099054,4168794141,6472946933,9054612515,6475010697,4165295300,8884722072,6476806014,9575252227,4168245182,4168189194,6472699567,6477038351,9052770577,8776045267,4166480252,9054511258,6473271500,9052792849,9052792848,1433969406,8666076301,4167825900,4168897168,4169078200,4164838000,4166068337,4169171410,6472839828,6477487663,4167977260,9054532274,9463036599,4168934234,9053050505,9058745101,8882422100,4163383115,4169857298,6472922286,4163579361,4168309723,6479654092,4372289867,8009598281,4165587588,9057897744,9056043130,6478070078,9646874105,4169531030,6472212233,7057158262,9175789069,4163011314,4165430810,9990301160,4168474960,6475275057,6472044113,9055682121,9058589393,9056722800,6477846521,6477941365,9055950912,6474924886,6473440691,4168776125,4163174462,9054610321,8883973342,9053060223,2892017809,9057835848,9057935001,3072454142,8054576575,6472892025,4169699089,3471523379,4168474987,4163201367,2128977660,6132651742,6476182763,4163008467,7206597489,9988345923,4169878760,4169003585,6478524884,9914001048,4169096590,8879944479,6478986432,6476319652,8003187888,9878662661,6478776100,5875806600,4163018227,9781518181,4166766100,4165006827,4165697454,6476770831,6477414211,6472302255,1481715080,6473812456,4169097231,6479686845,9646125111,9646111719,9464550077,6475052339,4167164402,4169949444,4164148488,4169701890,4168932478,4163623636,4163058920,6477464475,4168358999,4169498902,6478342308,3657884440,6477690408,6476216869,4164098567,8884266840,8554536951,6473081650,8054312964,4166427735,9053386686,4168209346,4164412888,4168271835,6472618184,4168090751,4163893615,4169029293,6477452440,9988999373,4164643329,6477850986,6479870954,6476408935,6478985296,4165455048,4162024649,6472240550,4163098986,6477796429,6138944819,4169709288,7837519494,4164854448,3069991220,6472994817,9052168758,6475397855,6478910143,6473021189,6473824104,2893120455,9054612415,6476087526,6474027649,9052323646,9872995634,4379221262,9055861200,9815501200,4165612616,8699998695,1433300529,6472962408,123123123,6476406666,6477011440,9057548000,6479198049,4167357082,4165335593,6478559861,4167207073,9057960909,6473091307,6476189791,6473556213,4167284663,4167600600,6472610707,4165589369,9056957888,4164901177,4165684575,2269795925,4168310547,2062958822,6472977668,6472302722,6476973060,6472944717,9055951700,9058724130,9876450820,4168561294,9053303786,4165224167,4163425037,4164893434,4169306730,8308767189,8289885316,4162747663,4166942499,4168693600,6475348008,6479492344,6472287887,4168579259,9052762676,6479139722,9056712001,7696762086,6477782850,9876674054,7837498860,4163173487,6472047581,6479706151,2896545096,4167325867,4167049111,6479229455,6477181358,9052734092,9780663273,8019155691,8427224841,6479285665,9058030942,6472193045,6477006652,4165610592,6476259250,6479817981,4168351070,9058400505,6477102670,6479823313,9054078642,8007326868,4168579196,1551729827,4164974111,6474005200,9997751336,4162940001,4168248533,6475042251,6477223669,9052901380,4169333853,6478013595,3063512995,9058460400,6478283132,4169339440,4165662150,6136005539,6476186767,9057936124,6479900029,6479360826,4169700920,9055070910,4168341602,7479313013,4168786614,4164730336,6477079411,4169642011,9058828249,9855592219,4167108821,4163994441,9057999222,4168454964,4162036636,4168796202,6475005028,6472178844,6479235500,6475884323,8699339336,9501978237,6472730960,6477854985,4167666444,4168738140,6476319582,9059153738,9764374247,8208654559,2895415867,1431749890,6473490725,6474054243,6472697386,9419031673,6472191414,6475301982,3657786594,4372274127,9833899799,6477709292,7837243735,9056298981,9814054811,8555880999,4085121281,6473814426,9041121192,9057832623,6479273247,4162025668,6178037455,6477138435,9052721176,4162946192,4165099501,8773559007,9041072002,7986067773,8146708227,4163080143,3069145178,6475248040,2063971299,9095022443,6479431257,6479431260,9052752640,4162294454,4169902313,6472625054,7788231084,4163176597,5148666411,8667338612,7986308983,1433952700,0433952700,6476560039,9054543984,6472183330,4163155676,6473811112,9057908557,4165420041,4166664751,4163425054,8667970007,4165516473,6478828092,1112223334,4164582766,9057922874,6475246633,4167317542,4169660300,98,4168013168,4169338665,9056818300,9056034325,6472882060,4182645588,4183178466,416497,9059490049,8872477724,6476257346,6472745668,6472956430,4165715922,9780170159,4163990750,3062160098,6472120272,6478224464,1433907210,7347271499,4169894570,5053060756,5153060756,9057905400,4164892121,4164537645,6474489950,4162698333,4164188396,7804478839,8427776848,6475502229,4168064233,8146427511,4167359260,2897953420,4164794488,4159157487,2896601617,4165786743,6473893427
*/
exports.updateContactList = functions.https.onRequest((req, res) => {
  res.set('Access-Control-Allow-Origin', '*');
  if(!req.query.inputuid){
      res.send ("Not allowed");
  }
  if(!req.query.inputStr){
      res.send ("Not allowed");
  }
  var tmpV = req.query.inputStr;
  var inputArr = tmpV.split(",");

  //inputArr.length
  var pushNotificationCounter = 0; //how many push notifications are send back to customer
  for(let x=0;x<inputArr.length;x++){

    //Get Numbers from string
    inputArr[x] = inputArr[x].replace( /^\D+/g, '');
    if(!inputArr[x] || inputArr[x].length<=0){
        continue;
    }

    admin.database().ref('/phones/'+inputArr[x]).once('value').then(function(snapshot) {
         let val = snapshot.val();
         //res.write ("VAL");
         //res.write (val);
         if(val && val.uid && val.uid!=req.query.inputuid){
            //This guy is already a member of Tapally : so lets make them both friends
            //res.write ( "GOT SNAPSHOT UID "+val.uid);
            admin.database().ref('/chatusers/'+req.query.inputuid).once('value').then(function(usera) {
                let vali = usera.val();
                if(vali && vali.displayName && vali.photoURL){
                       admin.database().ref('/friends/'+val.uid+'/'+req.query.inputuid).set( {"isActive" : 1,"isBlock" : false,"uid" : vali.uid,"mobile":vali.mobile,"displayName":vali.displayName ,"deviceToken":vali.deviceToken,"photoURL" : vali.photoURL});//.catch(e1 => {})


                       admin.database().ref('/chatusers/'+val.uid).once('value').then(function(userb) {
                           let buddy = userb.val();
                           if(buddy && buddy.displayName && buddy.photoURL){
                                  admin.database().ref('/friends/'+req.query.inputuid+'/'+val.uid).set( {"isActive" : 1,"isBlock" : false,"uid" : buddy.uid,"displayName":buddy.displayName,"deviceToken":buddy.deviceToken,"mobile":buddy.mobile ,"photoURL" : buddy.photoURL} );//.catch(e1 => {})

                                  //only send one push notification
                                  if (pushNotificationCounter++<1){

                                    //send notification after 2 minutes;give time to script to fully finish
                                    setTimeout(function(){
                                        var payload = {
                                                      "notification": {
                                                          "title": buddy.displayName+' joined you on TapAlly',
                                                          "body": 'Others are joining soon, check it out!',
                                                          "forceStart": "1",
                                                          "sound": "default",
                                                          "icon": "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/common%2Ficon.png?alt=media&token=35726d75-69f6-4f50-bfb2-07079e28d67a"
                                                      }
                                                 };
                                        admin.messaging().sendToDevice(vali.deviceToken, payload);
                                    }, 120000);


                                  }//endif


                           }//endif
                       }).catch(e => { });

                }//endif
            }).catch(e => { });




         } else {
           //record does not exists . This person is not in Tapally's network
           //admin.database().ref('/phones/'+inputArr[x]).set({ in_contact_list_of: req.query.inputuid }).catch(e1 => {/*res.write ( "Error 213"+e1);*/ });
         }
         return 1; // IT SHOULD RETURN NON-NULL VALUE
    }).catch(e => {
         //record does not exists . This person is not in Tapally's network
         //admin.database().ref('/phones/'+inputArr[x]).set({ in_contact_list_of: req.query.inputuid }).catch(e1 => {/*res.write ( "Error 382"+e1);*/ });
    });

  }//end for

  if(req.query.inputuid && req.query.inputStr){
    res.send ("Done");
  }



  //admin.database().ref('/friends/'+{userId}).child('-LcllK2yW0lBWtOvE33F').update({ displayName: displayName_after });

});


//exports.duplicateUserDetailsToOtherNodes = functions.database.ref('/tapallyionic3/chatusers/{userId}/original')
/*

    admin.database().ref('/phones/').child(inputArr[x]).on('value', (snapshot) => {
         let val = snapshot.val();
         if(val){
            response += "GOT SNAPSHOT UID "+val.uid;
         } else {
           //record does not exists so add a new record
           admin.database().ref('/phones/').child(inputArr[x]).set({ in_contact_list_of: inputUID });
           response += "ADDED A NEW CHILD";
         }
         return 1; // IT SHOULD RETURN NON-NULL VALUE
    }, err => {
         //record does not exists so add a new record
         admin.database().ref('/phones/').child(inputArr[x]).set({ in_contact_list_of: inputUID });
         response += "ADDED A NEW CHILD";
    });


exports.duplicateUserDetailsToOtherNodes = functions.database.ref('/chatusers/{userId}/displayName')
    .onUpdate((change) => {
      const displayName_after = change.after  // DataSnapshot after the change
      admin.database().ref('/friends/'+{userId}).child('-LcllK2yW0lBWtOvE33F').update({ displayName: displayName_after });
    });
    */

/* ON CREATE - This Works
exports.duplicateUserDetailsToOtherNodes = functions.database.ref('/chatusers/{userId}/original')
    .onCreate((snapshot, context) => {
      admin.database().ref('/friends/1bqhG3I2U5e6tMzkhTrIw8Ehozb2').child('-LcllK2yW0lBWtOvE33F').update({ displayNameX: "xxx" });
    });
*/
