webpackJsonp([24],{

/***/ 1057:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilepicPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_imagehandler_imagehandler__ = __webpack_require__(499);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_user_user__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_loading_loading__ = __webpack_require__(240);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_requests_requests__ = __webpack_require__(495);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_groups_groups__ = __webpack_require__(496);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_firebase__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ionic_img_viewer__ = __webpack_require__(501);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_app_angularfireconfig__ = __webpack_require__(142);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Angular core

// Ionic-angular

// Providers





// Firebase

// Ionic image viewer


var ProfilepicPage = (function () {
    function ProfilepicPage(navCtrl, navParams, imgservice, zone, events, loadingProvider, userservice, loadingCtrl, modalCtrl, alertCtrl, imageViewerCtrl, viewCtrl, platform, requestservice, groupservice) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.imgservice = imgservice;
        this.zone = zone;
        this.events = events;
        this.loadingProvider = loadingProvider;
        this.userservice = userservice;
        this.loadingCtrl = loadingCtrl;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.imageViewerCtrl = imageViewerCtrl;
        this.viewCtrl = viewCtrl;
        this.platform = platform;
        this.requestservice = requestservice;
        this.groupservice = groupservice;
        this.imgurl = __WEBPACK_IMPORTED_MODULE_9__app_app_angularfireconfig__["c" /* env */].userPic;
        this.isDisabled = false;
        this.profileData = {
            username: '',
            disc: '',
            imgurl: __WEBPACK_IMPORTED_MODULE_9__app_app_angularfireconfig__["c" /* env */].userPic
        };
        this.usernamePlaceholder = "Enter your name";
        this.discPlaceholder = 'About yourself';
        this.name = navParams.get('name');
        this.img = navParams.get('img');
        this._imageViewerCtrl = imageViewerCtrl;
    }
    ProfilepicPage.prototype.presentImage = function (myImage) {
        var imageViewer = this._imageViewerCtrl.create(myImage);
        imageViewer.present();
    };
    ProfilepicPage.prototype.ionViewWillEnter = function () {
        this.loadingProvider.presentLoading();
        this.loaduserdetails();
    };
    ProfilepicPage.prototype.loaduserdetails = function () {
        var _this = this;
        var userId;
        if (__WEBPACK_IMPORTED_MODULE_7_firebase___default.a.auth().currentUser != null) {
            userId = __WEBPACK_IMPORTED_MODULE_7_firebase___default.a.auth().currentUser.uid;
        }
        else {
            userId = localStorage.getItem('userUID');
        }
        if (userId != undefined) {
            this.isDisabled = true;
            this.userservice.getuserdetails().then(function (res) {
                _this.loadingProvider.dismissMyLoading();
                if (res) {
                    var desc = res['disc'].replace(new RegExp('\n', 'g'), "<br />");
                    if (res['displayName'] != undefined) {
                        _this.profileData.username = res['displayName'];
                    }
                    if (res['photoURL'] != undefined) {
                        _this.profileData.imgurl = res['photoURL'];
                    }
                    if (desc != undefined) {
                        _this.profileData.disc = desc;
                    }
                }
            }).catch(function (err) {
            });
        }
        else {
            this.loadingProvider.dismissMyLoading();
            this.profileData = {
                username: '',
                disc: '',
                imgurl: __WEBPACK_IMPORTED_MODULE_9__app_app_angularfireconfig__["c" /* env */].userPic
            };
        }
    };
    ProfilepicPage.prototype.choseImage = function () {
        var _this = this;
        this.imgservice.uploadimage().then(function (uploadedurl) {
            _this.loadingProvider.dismissMyLoading();
            _this.zone.run(function () {
                _this.profileData.imgurl = uploadedurl;
                _this.updateproceed(_this.profileData.imgurl);
            });
        }).catch(function (err) {
            if (err == 20) {
                _this.navCtrl.pop();
            }
            _this.loadingProvider.dismissMyLoading();
        });
    };
    ProfilepicPage.prototype.back = function () {
        this.navCtrl.pop();
    };
    ProfilepicPage.prototype.updateproceed = function (imgurl) {
        this.userservice.updateimage(imgurl).then(function (res) {
            if (res.success) {
            }
            else {
                alert(res);
            }
        });
    };
    ProfilepicPage.prototype.proceed = function () {
        var _this = this;
        this.userservice.directLogin().then(function (data) {
            if (data.success) {
                _this.navCtrl.setRoot('ChatsPage');
            }
        });
    };
    ProfilepicPage.prototype.username = function (username) {
        var _this = this;
        var profileModal = this.modalCtrl.create("UsernamePage", { userName: username });
        profileModal.onDidDismiss(function (data) {
            if (data != undefined) {
                _this.profileData.username = data.username;
                _this.userservice.updatedisplayname(data.username).then(function (res) {
                    if (res.success) {
                    }
                });
            }
            else {
            }
        });
        profileModal.present();
    };
    ProfilepicPage.prototype.discription = function (disc) {
        var _this = this;
        var profileModal = this.modalCtrl.create("DiscPage", { disc: disc });
        profileModal.onDidDismiss(function (data) {
            if (data != undefined) {
                data.disc = data.disc.replace(new RegExp('\n', 'g'), "<br />");
                _this.profileData.disc = data.disc;
                _this.userservice.updatedisc(data.disc).then(function (res) {
                    if (res.success) {
                    }
                });
            }
            else {
            }
        });
        profileModal.present();
    };
    ProfilepicPage.prototype.deleteProfile = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Delete User',
            message: 'Do you want to delete this Account?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                    }
                },
                {
                    text: 'Ok',
                    handler: function () {
                        _this.userservice.deleteUser().then(function (data) {
                            if (data.success) {
                                _this.navCtrl.setRoot('PhonePage');
                            }
                            else {
                            }
                        }).catch(function (err) {
                        });
                    }
                }
            ]
        });
        alert.present();
    };
    ProfilepicPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-profilepic',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/profilepic/profilepic.html"*/'<ion-header>\n\n  <ion-navbar color="darkblue" >\n    <div class="header-wrap">\n      <!-- <ion-buttons class="left-arrow" left *ngIf="isDisabled">\n        <button ion-button icon-only (click)="back()">\n          <ion-icon ios="md-arrow-round-back" md="md-arrow-round-back"></ion-icon>\n        </button>\n      </ion-buttons> -->\n      <div class="header-title">Profile</div>\n    </div>\n    <!-- <ion-title text-left>Profile</ion-title>\n\n      <ion-buttons left *ngIf="isDisabled">\n          <button ion-button icon-only (click)="back()">\n            <ion-icon ios="md-arrow-round-back" md="md-arrow-round-back"></ion-icon>\n          </button>\n        </ion-buttons> -->\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content class="bg-grey">\n  <div class="bg-profile-wrap">\n    <div [style.background]="\'url(\'+ profileData.imgurl + \')\'" class="bg-profile"></div>\n    <div class="profile-image">\n      <img src="{{profileData.imgurl}}" #myImage (click)="presentImage(myImage)" />\n      <button ion-button round outline color="btn-white" (click)="choseImage()" style="padding: 0 1.2rem;">\n        <ion-icon name="camera"></ion-icon>\n      </button>\n\n    </div>\n  </div>\n  <div class="bg-grey" padding-vertical>\n    <ion-card>\n      <ion-card-content>\n        <ion-list>\n          <ion-item (click)="username(profileData.username)">\n            <ion-label>\n              <p *ngIf="profileData.username != \'\'">{{profileData.username}}</p>\n              <p *ngIf="profileData.username == \'\'">{{usernamePlaceholder}}</p>\n            </ion-label>\n          </ion-item>\n\n          <ion-item class="textarea-lbl" (click)="discription(profileData.disc)">\n            <div>\n              <p *ngIf="profileData.disc != \'\'" [innerHTML]="profileData.disc"></p>\n              <p *ngIf="profileData.disc == \'\'" [innerHTML]="discPlaceholder"></p>\n            </div>\n          </ion-item>\n          <button ion-button class="button-secondary" (click)="proceed()" [disabled]="profileData.username == \'\'">Update</button>\n          <button ion-button class="button-secondary" (click)="deleteProfile()" [disabled]="profileData.username == \'\'">Delete</button>\n          </ion-list>\n      </ion-card-content>\n    </ion-card>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/profilepic/profilepic.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_imagehandler_imagehandler__["a" /* ImagehandlerProvider */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_4__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_8_ionic_img_viewer__["a" /* ImageViewerController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["B" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_5__providers_requests_requests__["a" /* RequestsProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_groups_groups__["a" /* GroupsProvider */]])
    ], ProfilepicPage);
    return ProfilepicPage;
}());

//# sourceMappingURL=profilepic.js.map

/***/ }),

/***/ 986:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilepicPageModule", function() { return ProfilepicPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__profilepic__ = __webpack_require__(1057);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ProfilepicPageModule = (function () {
    function ProfilepicPageModule() {
    }
    ProfilepicPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__profilepic__["a" /* ProfilepicPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__profilepic__["a" /* ProfilepicPage */]),
            ],
        })
    ], ProfilepicPageModule);
    return ProfilepicPageModule;
}());

//# sourceMappingURL=profilepic.module.js.map

/***/ })

});
//# sourceMappingURL=24.js.map