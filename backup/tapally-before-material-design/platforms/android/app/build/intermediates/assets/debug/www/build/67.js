webpackJsonp([67],{

/***/ 1013:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AllcontactsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_contact_contact__ = __webpack_require__(498);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_groups_groups__ = __webpack_require__(496);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_imagehandler_imagehandler__ = __webpack_require__(499);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_user_user__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_loading_loading__ = __webpack_require__(240);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var AllcontactsPage = (function () {
    function AllcontactsPage(navCtrl, navParams, contact, imgstore, groupservice, userservice, loading, platform) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.contact = contact;
        this.imgstore = imgstore;
        this.groupservice = groupservice;
        this.userservice = userservice;
        this.loading = loading;
        this.platform = platform;
        this.selectedMember = [];
        this.myFriends = [];
        this.loginuserInfo = [];
        this.msgstatus = false;
        this.tmpmyfriends = [];
    }
    AllcontactsPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.gpname = this.navParams.get('gpname');
        this.userservice.getuserdetails().then(function (res) {
            _this.loginuserInfo = res;
        }).catch(function (err) {
        });
        this.contact.getSimJsonContacts().then(function (res) {
            _this.myFriends = res["contacts"];
            var myFriends = res["contacts"];
            _this.tmpmyfriends = myFriends.sort(function (a, b) {
                if (a.displayName != undefined) {
                    var nameA = a.displayName.toUpperCase(); // ignore upper and lowercase
                }
                if (b.displayName != undefined) {
                    var nameB = b.displayName.toUpperCase(); // ignore upper and lowercase
                }
                if (nameA < nameB) {
                    return -1;
                }
                if (nameA > nameB) {
                    return 1;
                }
                return 0;
            });
            _this.myFriends = _this.tmpmyfriends;
        });
    };
    AllcontactsPage.prototype.initializeItems = function () {
        this.myFriends = this.tmpmyfriends;
    };
    AllcontactsPage.prototype.addContact = function (contact) {
        if (this.selectedMember.length == 0) {
            this.selectedMember.push(contact);
        }
        else {
            var flagofbuddy = false;
            var posionofbuddy = 0;
            for (var i = 0; i < this.selectedMember.length; i++) {
                if (this.selectedMember[i]._id == contact._id) {
                    flagofbuddy = true;
                    posionofbuddy = i;
                    break;
                }
                else {
                    flagofbuddy = false;
                }
            }
            if (flagofbuddy == true) {
                this.selectedMember.splice(posionofbuddy, 1);
            }
            else if (this.selectedMember.length == 6) {
                this.loading.presentToast("Maximum six contacts allowed");
            }
            else {
                this.selectedMember.push(contact);
            }
        }
    };
    AllcontactsPage.prototype.searchuser = function (ev) {
        this.initializeItems();
        var val = ev.target.value;
        if (val && val.trim() != '') {
            this.myFriends = this.tmpmyfriends;
            this.myFriends = this.myFriends.filter(function (item) {
                return (item.displayName.toLowerCase().indexOf(val.toLowerCase().trim()) > -1);
            });
        }
    };
    AllcontactsPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    AllcontactsPage.prototype.goNext = function () {
        var _this = this;
        this.loading.presentLoading();
        this.groupservice.addgroupmsgmultiple(this.selectedMember, this.loginuserInfo, 'contact', this.gpname).then(function () {
            _this.selectedMember = [];
            _this.msgstatus = false;
            _this.loading.dismissMyLoading();
            _this.navCtrl.pop();
        });
    };
    AllcontactsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-allcontacts',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/allcontacts/allcontacts.html"*/'\n<ion-header>\n  <ion-navbar >\n      <div class="header-wrap">\n          <!-- <ion-buttons class="left-arrow" left>\n              <button ion-button icon-only (click)="goBack()">\n                <ion-icon ios="md-arrow-round-back" md="md-arrow-round-back"></ion-icon>\n              </button>\n          </ion-buttons> -->\n          <div class="header-title" text-left>All Contacts</div> \n      </div>\n  </ion-navbar>\n  <ion-searchbar placeholder="Search" class="searchbar" (ionInput)="searchuser($event)"></ion-searchbar>\n  <ng-container *ngIf="selectedMember.length > 0">\n  <ion-list  class="group-wrap">\n    <ion-item *ngFor="let item of selectedMember">\n      <ion-avatar item-left><img src="assets/imgs/user.png"></ion-avatar>\n      <p>{{item.displayName}}</p>\n    </ion-item>\n  </ion-list>\n</ng-container>\n</ion-header>\n\n\n<ion-content padding [ngClass]="selectedMember.length > 0?\'group-wrap-active\':\'group-wrap-no-active\'">\n \n   <ion-list *ngIf="myFriends.length>0">\n    <ng-container *ngFor="let key of myFriends">\n      <ion-item (click)="addContact(key)">\n        <ion-avatar item-left><img src="assets/imgs/user.png"></ion-avatar> \n        <h2>{{key.displayName}}</h2>\n      </ion-item>  \n    </ng-container>\n  </ion-list> \n\n  \n  <ion-fab bottom right>\n    <button ion-fab [disabled]="selectedMember.length == 0" (click)="goNext()"><ion-icon ios="ios-arrow-round-forward" md="md-arrow-round-forward"></ion-icon></button>\n  </ion-fab>\n\n  <div *ngIf="myFriends.length==0" padding text-center>No contacts found!</div>\n\n</ion-content>'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/allcontacts/allcontacts.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_contact_contact__["a" /* ContactProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_imagehandler_imagehandler__["a" /* ImagehandlerProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_groups_groups__["a" /* GroupsProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */]])
    ], AllcontactsPage);
    return AllcontactsPage;
}());

//# sourceMappingURL=allcontacts.js.map

/***/ }),

/***/ 942:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AllcontactsPageModule", function() { return AllcontactsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__allcontacts__ = __webpack_require__(1013);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AllcontactsPageModule = (function () {
    function AllcontactsPageModule() {
    }
    AllcontactsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__allcontacts__["a" /* AllcontactsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__allcontacts__["a" /* AllcontactsPage */]),
            ],
        })
    ], AllcontactsPageModule);
    return AllcontactsPageModule;
}());

//# sourceMappingURL=allcontacts.module.js.map

/***/ })

});
//# sourceMappingURL=67.js.map