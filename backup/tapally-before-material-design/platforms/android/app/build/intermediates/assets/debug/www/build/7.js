webpackJsonp([7],{

/***/ 1004:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShowContactPageModule", function() { return ShowContactPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__show_contact__ = __webpack_require__(1075);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ShowContactPageModule = (function () {
    function ShowContactPageModule() {
    }
    ShowContactPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__show_contact__["a" /* ShowContactPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__show_contact__["a" /* ShowContactPage */]),
            ],
        })
    ], ShowContactPageModule);
    return ShowContactPageModule;
}());

//# sourceMappingURL=show-contact.module.js.map

/***/ }),

/***/ 1075:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShowContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_contact_contact__ = __webpack_require__(498);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ShowContactPage = (function () {
    function ShowContactPage(navCtrl, navParams, modalCtrl, platform, contactProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.contactProvider = contactProvider;
        this.contacts = [];
    }
    ShowContactPage.prototype.ionViewDidLoad = function () {
        this.getAllDeviceContacts();
    };
    ShowContactPage.prototype.getAllDeviceContacts = function () {
        var _this = this;
        var deviceContacts = [];
        var tmpcontacts = [];
        tmpcontacts = this.navParams.get('contacts');
        this.contactProvider.getSimJsonContacts().then(function (res) {
            deviceContacts = res['contacts'];
            deviceContacts.forEach(function (deviceelement) {
                tmpcontacts.forEach(function (elements) {
                    if (deviceelement['mobile'] == elements['mobile']) {
                        elements['displayName'] = deviceelement['displayName'];
                        elements.isSave = true;
                    }
                });
            });
            _this.contacts = tmpcontacts;
        });
    };
    ShowContactPage.prototype.addContacts = function (contacts, myEvent) {
        var _this = this;
        var profileModal = this.modalCtrl.create("AddContactPage", { contacts: contacts });
        profileModal.present({
            ev: myEvent
        });
        profileModal.onDidDismiss(function (pages) {
            _this.ionViewDidLoad();
        });
    };
    ShowContactPage.prototype.backButtonClick = function () {
        this.navCtrl.pop();
    };
    ShowContactPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-show-contact',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/show-contact/show-contact.html"*/'<ion-header>\n\n  <ion-navbar >\n      <div class="header-wrap">\n            <div class="header-title" *ngIf="contacts.length == 1">Contact</div>\n            <div class="header-title" *ngIf="contacts.length >1">Contacts</div>\n      </div>\n    \n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n    <ion-list *ngIf="contacts.length>0" >\n        <ion-item *ngFor="let item of contacts">\n            <ion-avatar item-start>\n                <img src="assets/imgs/user.png">\n              </ion-avatar>\n         <h2 >{{item.displayName}}</h2> \n      <button *ngIf="!item.isSave"ion-button outline item-end (click)="addContacts(item,$event)">ADD</button>\n  </ion-item> \n  </ion-list>\n\n</ion-content>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/show-contact/show-contact.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__providers_contact_contact__["a" /* ContactProvider */]])
    ], ShowContactPage);
    return ShowContactPage;
}());

//# sourceMappingURL=show-contact.js.map

/***/ })

});
//# sourceMappingURL=7.js.map