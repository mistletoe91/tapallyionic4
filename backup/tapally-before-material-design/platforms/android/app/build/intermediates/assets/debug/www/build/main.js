webpackJsonp([70],{

/***/ 141:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FcmProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_firebase__ = __webpack_require__(345);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_firestore__ = __webpack_require__(346);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_user_user__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_firebase__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_app_angularfireconfig__ = __webpack_require__(142);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








var FcmProvider = (function () {
    function FcmProvider(userservice, http, firebaseNative, afs, platform) {
        this.userservice = userservice;
        this.http = http;
        this.firebaseNative = firebaseNative;
        this.afs = afs;
        this.platform = platform;
        this.fireuserStatus = __WEBPACK_IMPORTED_MODULE_6_firebase___default.a.database().ref('/userstatus');
    }
    // Get permission from the user
    FcmProvider.prototype.getToken = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var userID, token;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        userID = localStorage.getItem('userUID');
                        if (userID != undefined) {
                            this.userId = userID;
                        }
                        else {
                            this.userId = __WEBPACK_IMPORTED_MODULE_6_firebase___default.a.auth().currentUser.uid;
                        }
                        if (!this.platform.is('android')) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.firebaseNative.getToken()];
                    case 1:
                        token = _a.sent();
                        this.firebaseNative.onTokenRefresh()
                            .subscribe(function (token) {
                            _this.userservice.updateDeviceToken(token, _this.userId);
                        });
                        _a.label = 2;
                    case 2:
                        if (!this.platform.is('ios')) return [3 /*break*/, 6];
                        return [4 /*yield*/, this.firebaseNative.getToken()];
                    case 3:
                        token = _a.sent();
                        return [4 /*yield*/, this.firebaseNative.grantPermission()];
                    case 4:
                        _a.sent();
                        return [4 /*yield*/, this.firebaseNative.onTokenRefresh()
                                .subscribe(function (token) {
                                _this.userservice.updateDeviceToken(token, _this.userId);
                            })];
                    case 5:
                        _a.sent();
                        _a.label = 6;
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    // Listen to incoming FCM messages
    FcmProvider.prototype.listenToNotifications = function () {
        this.firebaseNative.grantPermission();
        return this.firebaseNative.onNotificationOpen();
    };
    FcmProvider.prototype.setstatusUser = function () {
        var _this = this;
        var time = this.formatAMPM(new Date());
        var date = this.formatDate(new Date());
        var promise = new Promise(function (resolve, reject) {
            _this.fireuserStatus.child(_this.userId).set({
                status: 1,
                data: 'online',
                timestamp: date + ' at ' + time
            }).then(function () {
                resolve(true);
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    FcmProvider.prototype.setStatusOffline = function () {
        var _this = this;
        var time = this.formatAMPM(new Date());
        var date = this.formatDate(new Date());
        var promise = new Promise(function (resolve, reject) {
            _this.fireuserStatus.child(_this.userId).update({
                status: 0,
                data: 'offline',
                timestamp: date + ' at ' + time
            }).then(function () {
                resolve(true);
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    FcmProvider.prototype.formatAMPM = function (date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    };
    FcmProvider.prototype.formatDate = function (date) {
        var dd = date.getDate();
        var mm = date.getMonth() + 1;
        var yyyy = date.getFullYear();
        if (dd < 10)
            dd = '0' + dd;
        if (mm < 10)
            mm = '0' + mm;
        return dd + '/' + mm + '/' + yyyy;
    };
    FcmProvider.prototype.sendNotification = function (buddy, msg, pagename) {
        var body = {};
        if (pagename == 'chatpage') {
            body = {
                "notification": {
                    "title": buddy.displayName,
                    "body": msg,
                    "sound": "default",
                    "forceStart": "1",
                    "icon": "https://s3.envato.com/files/244032374/ICON.jpg"
                },
                "to": buddy.deviceToken,
                "priority": "high"
            };
        }
        else if (pagename == 'Grouppage') {
            body = {
                "notification": {
                    "title": buddy.displayName,
                    "body": msg,
                    "sound": "default",
                    "forceStart": "1",
                    "icon": "https://s3.envato.com/files/244032374/ICON.jpg"
                },
                "to": buddy.deviceToken,
                "priority": "high"
            };
        }
        else if (pagename == 'sendreq') {
            body = {
                "notification": {
                    "title": '',
                    "body": msg,
                    "forceStart": "1",
                    "sound": "default",
                    "icon": "https://s3.envato.com/files/244032374/ICON.jpg"
                },
                "to": buddy.deviceToken,
                "priority": "high"
            };
        }
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Headers */]({ 'Authorization': 'key=' + __WEBPACK_IMPORTED_MODULE_7__app_app_angularfireconfig__["d" /* server_key */].key, 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["f" /* RequestOptions */]({ headers: headers });
        this.http.post("https://fcm.googleapis.com/fcm/send", body, options).subscribe(function (res) {
        }, function (error) {
        });
    };
    FcmProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Http */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_firebase__["a" /* Firebase */],
            __WEBPACK_IMPORTED_MODULE_4_angularfire2_firestore__["a" /* AngularFirestore */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */]])
    ], FcmProvider);
    return FcmProvider;
}());

//# sourceMappingURL=fcm.js.map

/***/ }),

/***/ 142:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return config; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return server_key; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return apiKey; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return env; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return userPicArr; });
//FirebaseChatNew johnnyharper44@gmail.com
var config = {
    apiKey: "AIzaSyB9ZN4YPIdFZgLfHWo5HbsBp2gOiJeWKCI",
    authDomain: "tapallyionic3.firebaseapp.com",
    databaseURL: "https://tapallyionic3.firebaseio.com",
    projectId: "tapallyionic3",
    storageBucket: "tapallyionic3.appspot.com",
    messagingSenderId: "392933828452"
};
// Push Notification
var server_key = {
    key: "AIzaSyAFOrrJM2tyCI_HS3OshvFSXviNBu7qra8"
};
// Twilio API key
var apiKey = "TKgzw8rHwnYloYWUQqhJDPhTVMpQMIk4";
//default image of profile image
var env = {
    userPic: "https://firebasestorage.googleapis.com/v0/b/fir-chat-1f142.appspot.com/o/notouch%2Fuser.png?alt=media&token=79404d85-679a-48d3-9f73-edc7e53b4c2f"
};
//Animal Pics for icons
var userPicArr = {
    0: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F0.png?alt=media&token=2c3594e5-a0ce-42a8-83af-c98a7a7827b9",
    1: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F1.png?alt=media&token=d718091f-5d10-4edc-80e1-a2a10ef574ff",
    2: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F2.png?alt=media&token=425fbd27-514c-486b-962e-0c1648897e60",
    3: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F3.png?alt=media&token=dba66456-cca8-404f-ad6f-22d85b5c87ab",
    4: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F4.png?alt=media&token=aa684de9-c64e-4f0f-ae44-6b816ac346ff",
};
//Object length
//console.log (Object.keys(userPicArr).length);
//# sourceMappingURL=app.angularfireconfig.js.map

/***/ }),

/***/ 240:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoadingProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LoadingProvider = (function () {
    function LoadingProvider(toastCtrl, loadingCtrl) {
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
    }
    // For present loading
    LoadingProvider.prototype.presentLoading = function () {
        /*
            this.loading = this.loadingCtrl.create({
                content: 'Please wait...'
            });

            this.loading.present().then(()=>{
               //setTimeout(()=>{
                  this.loading.dismiss();
               //},300);
            });
            */
    };
    LoadingProvider.prototype.dismissMyLoading = function () {
        //this.loading.dismiss();
    };
    // Toast message
    LoadingProvider.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    LoadingProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["z" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* LoadingController */]])
    ], LoadingProvider);
    return LoadingProvider;
}());

//# sourceMappingURL=loading.js.map

/***/ }),

/***/ 254:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 254;

/***/ }),

/***/ 296:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/add-contact/add-contact.module": [
		941,
		69
	],
	"../pages/allbuddy-contacts/allbuddy-contacts.module": [
		944,
		68
	],
	"../pages/allcontacts/allcontacts.module": [
		942,
		67
	],
	"../pages/allimg/allimg.module": [
		943,
		66
	],
	"../pages/attachments/attachments.module": [
		945,
		65
	],
	"../pages/block-user/block-user.module": [
		947,
		64
	],
	"../pages/broadcast-submitted/broadcast-submitted.module": [
		948,
		63
	],
	"../pages/broadcast/broadcast.module": [
		946,
		62
	],
	"../pages/buddies/buddies.module": [
		949,
		61
	],
	"../pages/buddychat/buddychat.module": [
		953,
		60
	],
	"../pages/chats/chats.module": [
		950,
		59
	],
	"../pages/contact/contact.module": [
		951,
		58
	],
	"../pages/country-list/country-list.module": [
		952,
		57
	],
	"../pages/disc/disc.module": [
		954,
		56
	],
	"../pages/displayname/displayname.module": [
		956,
		55
	],
	"../pages/earnings/earnings.module": [
		955,
		54
	],
	"../pages/extra/extra.module": [
		957,
		53
	],
	"../pages/friend-asking-referral/friend-asking-referral.module": [
		958,
		52
	],
	"../pages/friend-list/friend-list.module": [
		959,
		51
	],
	"../pages/gpattachments/gpattachments.module": [
		960,
		50
	],
	"../pages/group-chat-popover/group-chat-popover.module": [
		961,
		49
	],
	"../pages/groupbuddies/groupbuddies.module": [
		962,
		48
	],
	"../pages/groupbuddiesadd/groupbuddiesadd.module": [
		963,
		47
	],
	"../pages/groupchat/groupchat.module": [
		964,
		46
	],
	"../pages/groupinfo/groupinfo.module": [
		965,
		45
	],
	"../pages/groupmembers/groupmembers.module": [
		966,
		44
	],
	"../pages/groups/groups.module": [
		968,
		43
	],
	"../pages/home-after-i-need-today/home-after-i-need-today.module": [
		967,
		42
	],
	"../pages/home/home.module": [
		969,
		41
	],
	"../pages/importp/importp.module": [
		970,
		40
	],
	"../pages/invite-friend/invite-friend.module": [
		971,
		39
	],
	"../pages/invitemanually/invitemanually.module": [
		972,
		38
	],
	"../pages/invitepeople/invitepeople.module": [
		973,
		37
	],
	"../pages/map/map.module": [
		974,
		36
	],
	"../pages/menu-block/menu-block.module": [
		976,
		35
	],
	"../pages/menu/menu.module": [
		975,
		34
	],
	"../pages/notification/notification.module": [
		977,
		33
	],
	"../pages/outreach-for-business/outreach-for-business.module": [
		978,
		32
	],
	"../pages/phone/phone.module": [
		980,
		31
	],
	"../pages/phoneverfy/phoneverfy.module": [
		979,
		30
	],
	"../pages/pick-contact-execute/pick-contact-execute.module": [
		981,
		29
	],
	"../pages/pick-contact-from/pick-contact-from.module": [
		982,
		28
	],
	"../pages/pick-contact-to/pick-contact-to.module": [
		983,
		27
	],
	"../pages/popover-chat/popover-chat.module": [
		984,
		26
	],
	"../pages/profile-view/profile-view.module": [
		985,
		25
	],
	"../pages/profilepic/profilepic.module": [
		986,
		24
	],
	"../pages/referral-infopage/referral-infopage.module": [
		987,
		23
	],
	"../pages/referral/referral.module": [
		988,
		22
	],
	"../pages/registerbusiness/registerbusiness.module": [
		989,
		21
	],
	"../pages/request-after-selected/request-after-selected.module": [
		990,
		20
	],
	"../pages/request-status/request-status.module": [
		991,
		19
	],
	"../pages/request-submitted/request-submitted.module": [
		992,
		18
	],
	"../pages/request/request.module": [
		993,
		17
	],
	"../pages/search-chats/search-chats.module": [
		994,
		16
	],
	"../pages/search-group-chat/search-group-chat.module": [
		995,
		15
	],
	"../pages/search-next-send/search-next-send.module": [
		997,
		14
	],
	"../pages/search-next/search-next.module": [
		996,
		13
	],
	"../pages/send-referral-backward/send-referral-backward.module": [
		998,
		12
	],
	"../pages/send-referral-filter/send-referral-filter.module": [
		1000,
		11
	],
	"../pages/send-referral-forward/send-referral-forward.module": [
		999,
		10
	],
	"../pages/send-referral-pick/send-referral-pick.module": [
		1001,
		9
	],
	"../pages/setting/setting.module": [
		1002,
		8
	],
	"../pages/show-contact/show-contact.module": [
		1004,
		7
	],
	"../pages/stats/stats.module": [
		1003,
		6
	],
	"../pages/status/status.module": [
		1005,
		1
	],
	"../pages/submit-receipt/submit-receipt.module": [
		1007,
		5
	],
	"../pages/tabs/tabs.module": [
		1006,
		4
	],
	"../pages/tutorial/tutorial.module": [
		1008,
		0
	],
	"../pages/username/username.module": [
		1010,
		3
	],
	"../pages/view-buddy/view-buddy.module": [
		1009,
		2
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 296;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 495:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RequestsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_firebase__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__user_user__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(102);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var RequestsProvider = (function () {
    function RequestsProvider(userservice, events, storage) {
        this.userservice = userservice;
        this.events = events;
        this.storage = storage;
        this.firebuddychats = __WEBPACK_IMPORTED_MODULE_1_firebase___default.a.database().ref('/buddychats');
        this.firedata = __WEBPACK_IMPORTED_MODULE_1_firebase___default.a.database().ref('/chatusers');
        this.firereq = __WEBPACK_IMPORTED_MODULE_1_firebase___default.a.database().ref('/requests');
        this.firefriends = __WEBPACK_IMPORTED_MODULE_1_firebase___default.a.database().ref('/friends');
        this.blockRequest = [];
        var userID = localStorage.getItem('userUID');
        if (userID != undefined) {
            this.userId = userID;
        }
        else {
            this.userId = __WEBPACK_IMPORTED_MODULE_1_firebase___default.a.auth().currentUser.uid;
        }
    }
    RequestsProvider.prototype.sendrequest = function (req) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.firereq.child(req.recipient).push({ sender: req.sender, isBlock: false }).then(function () {
                resolve({ success: true });
            });
        });
        return promise;
    };
    RequestsProvider.prototype.getmyrequests = function () {
        var _this = this;
        var allmyrequests;
        var myrequests = [];
        this.firereq.child(this.userId).on('value', function (snapshot) {
            allmyrequests = snapshot.val();
            myrequests = [];
            for (var i in allmyrequests) {
                myrequests.push(allmyrequests[i]);
            }
            _this.userservice.getallusers().then(function (res) {
                var allusers = res;
                _this.userdetails = [];
                for (var j in myrequests)
                    for (var key in allusers) {
                        if (myrequests[j].sender === allusers[key].uid && myrequests[j].isBlock == false) {
                            _this.userdetails.push(allusers[key]);
                        }
                    }
                _this.events.publish('gotrequests');
            });
        });
    };
    RequestsProvider.prototype.acceptrequest = function (buddy) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.firefriends.child(_this.userId).push({
                uid: buddy.uid,
                isActive: 1,
                isBlock: false
            }).then(function () {
                _this.firefriends.child(buddy.uid).push({
                    uid: _this.userId,
                    isActive: 1,
                    isBlock: false
                }).then(function () {
                    _this.deleterequest(buddy).then(function () {
                        resolve(true);
                    });
                });
            });
        });
        return promise;
    };
    RequestsProvider.prototype.deleterequest = function (buddy) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.firereq.child(_this.userId).orderByChild('sender').equalTo(buddy.uid).once('value', function (snapshot) {
                var somekey;
                for (var key in snapshot.val())
                    somekey = key;
                _this.firereq.child(_this.userId).child(somekey).remove().then(function () {
                    resolve(true);
                });
            })
                .then(function () {
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    RequestsProvider.prototype.getmyfriends = function () {
        var _this = this;
        var friendsuid = [];
        this.firefriends.child(this.userId).on('value', function (snapshot) {
            var allfriends = snapshot.val();
            if (allfriends != null) {
                for (var i in allfriends) {
                    friendsuid.push(allfriends[i]);
                }
            }
            _this.userservice.getallusers().then(function (users) {
                _this.myfriends = [];
                var counter = 0;
                if (friendsuid.length > 0) {
                    for (var j in friendsuid) {
                        var _loop_1 = function () {
                            if (friendsuid[j].uid === users[key].uid) {
                                users[key].isActive = friendsuid[j].isActive;
                                users[key].userprio = friendsuid[j].userprio ? friendsuid[j].userprio : null;
                                var userKey_1;
                                userKey_1 = users[key];
                                var flag1_1 = false;
                                var flag2_1 = false;
                                var flag3_1 = false;
                                _this.userservice.getuserblock(users[key]).then(function (res) {
                                    userKey_1.isBlock = res;
                                    flag1_1 = true;
                                });
                                _this.get_last_unreadmessage(users[key]).then(function (responce) {
                                    userKey_1.unreadmessage = responce;
                                    flag2_1 = true;
                                });
                                _this.get_last_msg(users[key]).then(function (resp) {
                                    userKey_1.lastMessage = resp;
                                    flag3_1 = true;
                                    _this.myfriends.push(userKey_1);
                                    counter++;
                                    if (counter == friendsuid.length) {
                                        _this.events.publish('friends');
                                    }
                                });
                            }
                        };
                        for (var key in users) {
                            _loop_1();
                        }
                    }
                }
                else {
                    if (friendsuid.length == 0) {
                        _this.events.publish('friends');
                    }
                }
            }).catch(function (err) {
            });
        });
    };
    RequestsProvider.prototype.get_last_msg = function (buddy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firebuddychats.child(_this.userId).child(buddy.uid).orderByChild('uid').once('value', function (snapshot) {
                var allmsg = snapshot.val();
                if (allmsg != null) {
                    var lastmsg = allmsg[Object.keys(allmsg)[Object.keys(allmsg).length - 1]];
                    resolve(lastmsg);
                }
                else {
                    resolve('');
                }
            });
        });
    };
    RequestsProvider.prototype.get_last_unreadmessage = function (buddy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var allunreadmessage = [];
            _this.firebuddychats.child(buddy.uid).child(_this.userId).orderByChild('uid').once('value', function (snapshot) {
                var allmsg = snapshot.val();
                if (allmsg != null) {
                    for (var tmpkey in allmsg) {
                        if (allmsg[tmpkey].isRead == false) {
                            allunreadmessage.push(allmsg);
                        }
                    }
                }
                if (allunreadmessage.length > 0) {
                    resolve(allunreadmessage.length);
                }
                else {
                    resolve(allunreadmessage.length);
                }
            });
        });
    };
    RequestsProvider.prototype.pendingetmyrequests = function (userd) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var allmyrequests;
            var myrequests = [];
            _this.firereq.child(userd).on('value', function (snapshot) {
                allmyrequests = snapshot.val();
                myrequests = [];
                for (var i in allmyrequests) {
                    myrequests.push(allmyrequests[i].sender);
                }
                _this.userservice.getallusers().then(function (res) {
                    var allusers = res;
                    var pendinguserdetails = [];
                    for (var j in myrequests)
                        for (var key in allusers) {
                            if (myrequests[j] == allusers[key].uid) {
                                pendinguserdetails.push(allusers[key]);
                            }
                        }
                    resolve(pendinguserdetails);
                });
            });
        });
    };
    RequestsProvider.prototype.blockuserprof = function (buddy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firereq.child(_this.userId).orderByChild('uid').once('value', function (snapshot) {
                var requestFriends = snapshot.val();
                for (var tmpkey in requestFriends) {
                    if (requestFriends[tmpkey].sender == buddy.uid) {
                        _this.firereq.child(_this.userId).child(tmpkey).update({ isBlock: true });
                        resolve(true);
                    }
                }
            });
        });
    };
    RequestsProvider.prototype.getAllBlockRequest = function () {
        var _this = this;
        this.firereq.child(this.userId).orderByChild('uid').once('value', function (snapshot) {
            var allrequest = snapshot.val();
            var blockuser = [];
            for (var key in allrequest) {
                if (allrequest[key].isBlock) {
                    _this.firedata.child(allrequest[key].sender).once('value', function (snapsho) {
                        blockuser.push(snapsho.val());
                    }).catch(function (err) {
                    });
                }
                _this.blockRequest = [];
                _this.blockRequest = blockuser;
            }
            _this.events.publish('block-request');
        });
    };
    RequestsProvider.prototype.unblockRequest = function (buddy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firereq.child(_this.userId).orderByChild('uid').once('value', function (snapshot) {
                var allfriends = snapshot.val();
                for (var key in allfriends) {
                    if (allfriends[key].sender == buddy.uid) {
                        _this.firereq.child(_this.userId).child(key).update({ isBlock: false });
                        resolve(true);
                    }
                }
            });
        });
    };
    RequestsProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */]])
    ], RequestsProvider);
    return RequestsProvider;
}());

//# sourceMappingURL=requests.js.map

/***/ }),

/***/ 496:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__user_user__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__fcm_fcm__ = __webpack_require__(141);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var GroupsProvider = (function () {
    function GroupsProvider(http, events, userservice, fcm) {
        this.http = http;
        this.events = events;
        this.userservice = userservice;
        this.fcm = fcm;
        this.firedata = __WEBPACK_IMPORTED_MODULE_3_firebase___default.a.database().ref('/chatusers');
        this.firegroup = __WEBPACK_IMPORTED_MODULE_3_firebase___default.a.database().ref('/groups');
        this.fireuserStatus = __WEBPACK_IMPORTED_MODULE_3_firebase___default.a.database().ref('/userstatus');
        this.mygroups = [];
        this.currentgroup = [];
        this.flag = 0;
        this.groupmemeberStatus = [];
        var userID = localStorage.getItem('userUID');
        if (userID != undefined) {
            this.userId = userID;
        }
        else {
            this.userId = __WEBPACK_IMPORTED_MODULE_3_firebase___default.a.auth().currentUser.uid;
        }
    }
    GroupsProvider.prototype.formatAMPM = function (date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    };
    GroupsProvider.prototype.formatDate = function (date) {
        var dd = date.getDate();
        var mm = date.getMonth() + 1;
        var yyyy = date.getFullYear();
        if (dd < 10)
            dd = '0' + dd;
        if (mm < 10)
            mm = '0' + mm;
        return dd + '/' + mm + '/' + yyyy;
    };
    GroupsProvider.prototype.getmygroups = function () {
        var _this = this;
        this.firegroup.child(this.userId).once('value', function (snapshot) {
            _this.mygroups = [];
            if (snapshot.val() != null) {
                var temp = snapshot.val();
                for (var key in temp) {
                    var newgroup = {
                        groupName: key,
                        groupimage: temp[key].groupimage,
                        timestamp: temp[key].timestamp,
                        userprio: temp[key].userprio ? temp[key].userprio : null,
                        lastmsg: temp[key].lastmsg,
                        dateofmsg: temp[key].dateofmsg,
                        timeofmsg: temp[key].timeofmsg,
                    };
                    _this.mygroups.push(newgroup);
                }
            }
            _this.events.publish('allmygroups');
        });
    };
    GroupsProvider.prototype.deletegroup = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firegroup.child(_this.userId).child(_this.currentgroupname).child('members').once('value', function (snapshot) {
                var tempmembers = snapshot.val();
                for (var key in tempmembers) {
                    _this.firegroup.child(tempmembers[key].uid).child(_this.currentgroupname).remove();
                }
                _this.firegroup.child(_this.userId).child(_this.currentgroupname).remove().then(function () {
                    resolve(true);
                }).catch(function (err) {
                    reject(err);
                });
            });
        });
    };
    GroupsProvider.prototype.addMeaasge = function (newmessage, loginUser, tempowner, type, groupname) {
        var _this = this;
        var date = this.formatDate(new Date());
        var timeofmsg = this.formatAMPM(new Date());
        return new Promise(function (resolve) {
            _this.firegroup.child(tempowner).child(groupname).child('msgboard').push({
                sentby: _this.userId,
                displayName: loginUser.displayName,
                photoURL: loginUser.photoURL,
                message: newmessage,
                timestamp: __WEBPACK_IMPORTED_MODULE_3_firebase___default.a.database.ServerValue.TIMESTAMP,
                multiMessage: false,
                type: type,
                messageId: '',
                timeofmsg: timeofmsg,
                dateofmsg: date,
                userprio: '',
            }).then(function (res) {
                _this.firegroup.child(tempowner).child(groupname).child('msgboard').child(res.key).update({ messageId: res.key, userprio: new Date() });
                var notifyMessage = 'You have received a message in ' + groupname + ' by ' + loginUser.displayName;
                if (_this.groupmemeberStatus[tempowner] != _this.userId) {
                    if (_this.groupmemeberStatus[tempowner].status == 'offline') {
                        _this.userservice.getbuddydetails(tempowner).then(function (res) {
                            var buddydata = res;
                            _this.fcm.sendNotification(buddydata, notifyMessage, 'Grouppage');
                        });
                    }
                }
                resolve(true);
            });
        });
    };
    GroupsProvider.prototype.addMultiMeaasge = function (newmessage, loginUser, tempowner, type, cgroupname) {
        var _this = this;
        var timeofmsg = this.formatAMPM(new Date());
        var date = this.formatDate(new Date());
        return new Promise(function (resolve) {
            _this.firegroup.child(tempowner).child(cgroupname).child('msgboard').push({
                sentby: _this.userId,
                displayName: loginUser.displayName,
                photoURL: loginUser.photoURL,
                message: newmessage,
                messageId: '',
                timestamp: __WEBPACK_IMPORTED_MODULE_3_firebase___default.a.database.ServerValue.TIMESTAMP,
                multiMessage: true,
                type: type,
                timeofmsg: timeofmsg,
                dateofmsg: date,
                userprio: '',
            }).then(function (res) {
                _this.firegroup.child(tempowner).child(cgroupname).child('msgboard').child(res.key).update({ messageId: res.key, userprio: new Date() });
                var notifyMessage = 'You have received a message in ' + _this.currentgroupname + ' by ' + loginUser.displayName;
                if (_this.groupmemeberStatus[tempowner] != _this.userId) {
                    if (_this.groupmemeberStatus[tempowner].status == 'offline') {
                        _this.userservice.getbuddydetails(tempowner).then(function (res) {
                            var buddydata = res;
                            _this.fcm.sendNotification(buddydata, notifyMessage, 'Grouppage');
                        });
                    }
                }
                resolve(true);
            });
        });
    };
    GroupsProvider.prototype.addgroupmsg = function (newmessage, loginUser, type, groupname) {
        var _this = this;
        var time = this.formatAMPM(new Date());
        var date = this.formatDate(new Date());
        var cgroupname = '';
        if (this.currentgroupname == undefined || this.currentgroupname == null) {
            cgroupname = groupname;
        }
        else {
            cgroupname = this.currentgroupname;
        }
        return new Promise(function (resolve) {
            _this.firegroup.child(_this.userId).child(cgroupname).child('owner').once('value', function (snapshots) {
                var own = _this.converanobj(snapshots.val());
                _this.firegroup.child(own[0].uid).child(cgroupname).child('members').once('value', function (snapshot) {
                    var tempowners = _this.converanobj(snapshot.val());
                    var flag = 0;
                    var _loop_1 = function (i) {
                        _this.addMeaasge(newmessage, loginUser, tempowners[i].uid, type, cgroupname).then(function (res) {
                            var msgtype = type;
                            var msg = newmessage;
                            _this.firegroup.child(tempowners[i].uid).child(cgroupname).update({ userprio: new Date() });
                            _this.firegroup.child(tempowners[i].uid).child(cgroupname).update({ dateofmsg: date });
                            _this.firegroup.child(tempowners[i].uid).child(cgroupname).update({ timeofmsg: time });
                            if (msgtype == 'message') {
                                _this.firegroup.child(tempowners[i].uid).child(cgroupname).update({ lastmsg: msg });
                            }
                            else {
                                _this.firegroup.child(tempowners[i].uid).child(cgroupname).update({ lastmsg: msgtype });
                            }
                            flag++;
                            if (tempowners.length == flag) {
                                resolve(true);
                            }
                        });
                    };
                    for (var i = 0; i < tempowners.length; i++) {
                        _loop_1(i);
                    }
                });
            });
        });
    };
    GroupsProvider.prototype.deleteMessages = function (item) {
        var _this = this;
        var cgroupname = this.currentgroupname;
        return new Promise(function (resolve, reject) {
            for (var i = 0; i < item.length; i++) {
                _this.firegroup.child(_this.userId).child(cgroupname).child('msgboard').child(item[i].messageId).remove().then(function (res) {
                    _this.firegroup.child(_this.userId).child(cgroupname).child('msgboard').once('value', function (snapshot) {
                        var tempData = _this.converanobj(snapshot.val());
                        if (tempData.length > 0) {
                            var lastMsg = tempData[tempData.length - 1];
                            _this.firegroup.child(_this.userId).child(cgroupname).update({
                                timeofmsg: lastMsg.timeofmsg,
                                timestamp: lastMsg.timestamp,
                                userprio: lastMsg.userprio,
                                dateofmsg: lastMsg.dateofmsg,
                            });
                            if (lastMsg.type == 'message') {
                                _this.firegroup.child(_this.userId).child(cgroupname).update({ lastmsg: lastMsg.message });
                            }
                            else {
                                _this.firegroup.child(_this.userId).child(cgroupname).update({ lastmsg: lastMsg.type });
                            }
                        }
                        else {
                            _this.firegroup.child(_this.userId).child(cgroupname).update({
                                timeofmsg: '',
                                timestamp: '',
                                userprio: '',
                                lastmsg: '',
                                dateofmsg: '',
                            });
                        }
                    }).then(function (res) {
                        resolve(true);
                    }, function (err) {
                        reject(err);
                    });
                }, function (err) {
                    reject(err);
                });
            }
        });
    };
    GroupsProvider.prototype.addgroupmsgmultiple = function (newmessage, loginUser, type, groupname) {
        var _this = this;
        var time = this.formatAMPM(new Date());
        var date = this.formatDate(new Date());
        var cgroupname = '';
        if (this.currentgroupname == undefined || this.currentgroupname == null) {
            cgroupname = groupname;
        }
        else {
            cgroupname = this.currentgroupname;
        }
        return new Promise(function (resolve) {
            _this.firegroup.child(_this.userId).child(cgroupname).child('owner').once('value', function (snapshots) {
                var own = _this.converanobj(snapshots.val());
                _this.firegroup.child(_this.userId).child(cgroupname).child('msgboard').once('value', function (snapshots) {
                });
                _this.firegroup.child(own[0].uid).child(cgroupname).child('members').once('value', function (snapshot) {
                    var tempowners = _this.converanobj(snapshot.val());
                    var flag = 0;
                    for (var i = 0; i < tempowners.length; i++) {
                        _this.firegroup.child(tempowners[i].uid).child(cgroupname).update({ userprio: new Date() });
                        _this.firegroup.child(tempowners[i].uid).child(cgroupname).update({ dateofmsg: date });
                        _this.firegroup.child(tempowners[i].uid).child(cgroupname).update({ timeofmsg: time });
                        if (type == 'message') {
                            _this.firegroup.child(tempowners[i].uid).child(cgroupname).update({ lastmsg: newmessage });
                        }
                        else {
                            _this.firegroup.child(tempowners[i].uid).child(cgroupname).update({ lastmsg: type });
                        }
                        _this.addMultiMeaasge(newmessage, loginUser, tempowners[i].uid, type, cgroupname).then(function (res) {
                            if (res) {
                                flag++;
                            }
                            if (tempowners.length == flag) {
                                resolve(true);
                            }
                        });
                    }
                });
            });
        });
    };
    GroupsProvider.prototype.postmsgs = function (member, msg, cb, loginUser) {
        this.firegroup.child(member.uid).child(this.currentgroupname).child('msgboard').push({
            sentby: this.userId,
            displayName: loginUser.displayName,
            photoURL: loginUser.photoURL,
            message: msg,
            timestamp: __WEBPACK_IMPORTED_MODULE_3_firebase___default.a.database.ServerValue.TIMESTAMP
        }).then(function () {
            cb();
        });
    };
    GroupsProvider.prototype.converanobj = function (obj) {
        var tmp = [];
        for (var key in obj) {
            tmp.push(obj[key]);
        }
        return tmp;
    };
    GroupsProvider.prototype.getownership = function (groupname) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.firegroup.child(_this.userId).child(groupname).once('value', function (snapshot) {
                var temp = _this.converanobj(snapshot.val().owner);
                if (temp.length > 0) {
                    var flag = 0;
                    for (var i = 0; i < temp.length; i++) {
                        if (temp[i].uid == _this.userId) {
                            flag = 1;
                            break;
                        }
                    }
                    if (flag == 1) {
                        resolve(true);
                    }
                    else {
                        resolve(false);
                    }
                }
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    GroupsProvider.prototype.getgroupimage = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firegroup.child(_this.userId).child(_this.currentgroupname).once('value', function (snapshot) {
                if (snapshot.val() != null) {
                    _this.grouppic = snapshot.val().groupimage;
                    resolve(true);
                }
                else {
                    resolve(true);
                }
            });
        });
    };
    GroupsProvider.prototype.getintogroup = function (groupname) {
        var _this = this;
        if (groupname != null) {
            this.currentgroupname = groupname;
            this.firegroup.child(this.userId).child(groupname).once('value', function (snapshot) {
                if (snapshot.val() != null) {
                    var temp = snapshot.val().members;
                    _this.currentgroup = [];
                    for (var key in temp) {
                        _this.currentgroup.push(temp[key]);
                    }
                    _this.currentgroupname = groupname;
                    _this.events.publish('gotintogroup');
                }
            });
        }
        else {
            this.currentgroupname = undefined;
        }
    };
    GroupsProvider.prototype.getgroupmember = function (gpname) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firegroup.child(_this.userId).child(gpname).once('value', function (snap) {
                var groupqwner = _this.converanobj(snap.val().owner);
                _this.firegroup.child(groupqwner[0].uid).child(gpname).once('value', function (snapshot) {
                    if (snapshot.val() != null) {
                        var temp = snapshot.val().members;
                        var currentgroup = [];
                        for (var key in temp) {
                            currentgroup.push(temp[key]);
                        }
                        resolve(currentgroup);
                    }
                });
            });
        });
    };
    GroupsProvider.prototype.addgroup = function (newGroup, members) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.userservice.getbuddydetails(_this.userId).then(function (owendetail) {
                _this.firegroup.child(_this.userId).child(newGroup.groupName).set({
                    timestamp: __WEBPACK_IMPORTED_MODULE_3_firebase___default.a.database.ServerValue.TIMESTAMP,
                    groupimage: newGroup.groupPic,
                    msgboard: '',
                    owner: '',
                }).then(function () {
                    _this.firegroup.child(_this.userId).child(newGroup.groupName).child('owner').push(owendetail);
                    _this.tempAddMember(members, newGroup).then(function (res) {
                        if (res == true) {
                            _this.firegroup.child(_this.userId).child(newGroup.groupName).child('members').push(owendetail);
                            resolve(true);
                        }
                    });
                }).catch(function (err) {
                    reject(err);
                });
            });
        });
    };
    GroupsProvider.prototype.tempAddMember = function (members, newGroup) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.flag = 0;
            for (var i = 0; i < members.length; i++) {
                _this.addmember(members[i], newGroup.groupName).then(function (res) {
                    if (res == true) {
                        _this.flag++;
                        if (_this.flag == members.length) {
                            resolve(true);
                        }
                    }
                });
            }
        });
    };
    GroupsProvider.prototype.tempAddMembers = function (members, groupName) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.flag = 0;
            for (var i = 0; i < members.length; i++) {
                _this.tempaddmember(members[i], groupName).then(function (res) {
                    if (res == true) {
                        _this.flag++;
                        if (_this.flag == members.length) {
                            resolve(true);
                        }
                    }
                });
            }
        });
    };
    GroupsProvider.prototype.tempaddmember = function (newmember, groupName) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firegroup.child(_this.userId).child(groupName).child('owner').once('value', function (snapshot) {
                var owners = snapshot.val();
                for (var key in owners) {
                    _this.firegroup.child(owners[key].uid).child(groupName).child('members').push(newmember).then(function () {
                        _this.getgroupimage().then(function () {
                            _this.firegroup.child(newmember.uid).child(groupName).set({
                                timestamp: __WEBPACK_IMPORTED_MODULE_3_firebase___default.a.database.ServerValue.TIMESTAMP,
                                groupimage: _this.grouppic,
                                owner: owners,
                                msgboard: ''
                            }).then(function () {
                                resolve(true);
                            }).catch(function (err) {
                                resolve(false);
                            });
                        });
                    });
                }
            });
        });
    };
    GroupsProvider.prototype.addmember = function (newmember, groupName) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.currentgroupname = groupName;
            _this.userservice.getbuddydetails(_this.userId).then(function (owendetail) {
                _this.firegroup.child(_this.userId).child(groupName).child('members').push(newmember).then(function () {
                    _this.getgroupimage().then(function () {
                        _this.firegroup.child(newmember.uid).child(groupName).set({
                            timestamp: __WEBPACK_IMPORTED_MODULE_3_firebase___default.a.database.ServerValue.TIMESTAMP,
                            groupimage: _this.grouppic,
                            owner: '',
                            msgboard: ''
                        }).then(function () {
                            _this.firegroup.child(newmember.uid).child(groupName).child('owner').push(owendetail);
                            resolve(true);
                        }).catch(function (err) {
                            resolve(false);
                        });
                    });
                });
            });
        });
    };
    GroupsProvider.prototype.deletemember = function (member) {
        var _this = this;
        this.firegroup.child(this.userId).child(this.currentgroupname).child('owner').once('value', function (snapshot) {
            var owners = snapshot.val();
            var ownersmember = [];
            for (var keys in owners) {
                ownersmember.push(owners[keys]);
            }
            var flag = 0;
            var _loop_2 = function (i) {
                _this.firegroup.child(ownersmember[i].uid).child(_this.currentgroupname).child('members').once('value', function (snapshot) {
                    var buddy = snapshot.val();
                    for (var kk in buddy) {
                        if (buddy[kk].uid == member.uid) {
                            _this.firegroup.child(ownersmember[i].uid).child(_this.currentgroupname).child('members').child(kk).remove();
                            flag++;
                            if (flag == ownersmember.length) {
                                _this.firegroup.child(member.uid).child(_this.currentgroupname).remove().then(function () {
                                    _this.getintogroup(_this.currentgroupname);
                                    _this.getgroupmembers();
                                });
                            }
                        }
                    }
                });
            };
            for (var i = 0; i < ownersmember.length; i++) {
                _loop_2(i);
            }
        });
    };
    GroupsProvider.prototype.getgroupmsgs = function (groupname) {
        var _this = this;
        this.firegroup.child(this.userId).child(groupname).child('msgboard').on('value', function (snapshot) {
            var tempmsgholder = snapshot.val();
            _this.groupmsgs = [];
            for (var key in tempmsgholder)
                _this.groupmsgs.push(tempmsgholder[key]);
            _this.events.publish('newgroupmsg');
        });
    };
    GroupsProvider.prototype.getgroupmembers = function () {
        var _this = this;
        this.firegroup.child(this.userId).child(this.currentgroupname).once('value', function (snapshots) {
            var tempdata = _this.converanobj(snapshots.val().owner);
            _this.firegroup.child(tempdata[0].uid).child(_this.currentgroupname).child('members').once('value', function (snapshot) {
                var tempvar = snapshot.val();
                _this.currentgroup = [];
                for (var key in tempvar) {
                    _this.currentgroup.push(tempvar[key]);
                }
                _this.events.publish('gotmembers');
            });
        });
    };
    GroupsProvider.prototype.getgroupmemebrstatus = function (member) {
        var _this = this;
        var _loop_3 = function (i) {
            var tmpStatus;
            this_1.fireuserStatus.child(member[i].uid).on('value', function (statuss) {
                tmpStatus = statuss.val();
                if (tmpStatus.status == 1) {
                    _this.groupmemeberStatus[member[i].uid] = {
                        uid: member[i].uid,
                        status: tmpStatus.data,
                        sdate: tmpStatus.timestamp
                    };
                }
                else {
                    _this.groupmemeberStatus[member[i].uid] = {
                        uid: member[i].uid,
                        status: tmpStatus.data,
                        sdate: tmpStatus.timestamp
                    };
                }
            });
        };
        var this_1 = this;
        for (var i = 0; i < member.length; i++) {
            _loop_3(i);
        }
        this.events.publish('groupmemberstatus');
    };
    GroupsProvider.prototype.leavegroup = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firegroup.child(_this.userId).child(_this.currentgroupname).once('value', function (snapshot) {
                var tempowner = _this.converanobj(snapshot.val().owner);
                _this.firegroup.child(tempowner[0].uid).child(_this.currentgroupname).child('members').once('value', function (snapshots) {
                    var cuser = snapshots.val();
                    for (var key in cuser) {
                        if (cuser[key].uid == _this.userId) {
                            _this.firegroup.child(tempowner[0].uid).child(_this.currentgroupname).child('members').child(key).remove();
                        }
                    }
                    _this.firegroup.child(_this.userId).child(_this.currentgroupname).remove().then(function () {
                        resolve(true);
                    }).catch(function (err) {
                        reject(err);
                    });
                });
            });
        });
    };
    GroupsProvider.prototype.getgroupInfo = function (groupname) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firegroup.child(_this.userId).child(groupname).once('value', function (snapshot) {
                resolve(snapshot.val());
            });
        });
    };
    GroupsProvider.prototype.updategroupname = function (oldgpname, newgpname) {
        var _this = this;
        var _loop_4 = function (i) {
            setTimeout(function () {
                var child = _this.firegroup.child(_this.currentgroup[i].uid).child(oldgpname);
                _this.firegroup.child(_this.currentgroup[i].uid).child(oldgpname).once('value', function (snapshot) {
                    _this.firegroup.child(_this.currentgroup[i].uid).child(newgpname).set(snapshot.val());
                    child.remove();
                });
            }, 1000);
        };
        for (var i = 0; i < this.currentgroup.length; i++) {
            _loop_4(i);
        }
    };
    GroupsProvider.prototype.updategroupImage = function (res, gpname) {
        for (var i = 0; i < this.currentgroup.length; i++) {
            this.firegroup.child(this.currentgroup[i].uid).child(gpname).update({ groupimage: res });
        }
        this.firegroup.child(this.userId).child(gpname).update({ groupimage: res });
    };
    GroupsProvider.prototype.addGroupAdmin = function (buddy, gpname) {
        var _this = this;
        for (var i = 0; i < this.currentgroup.length; i++) {
            this.firegroup.child(this.currentgroup[i].uid).child(gpname).child('owner').push(buddy);
        }
        this.firegroup.child(this.userId).child(gpname).child('members').once('value', function (snapshot) {
            _this.firegroup.child(buddy.uid).child(gpname).child('members').set(snapshot.val());
        });
    };
    GroupsProvider.prototype.removefromAdmin = function (member) {
        var _this = this;
        this.firegroup.child(member.uid).child(this.currentgroupname).child('members').once('value', function (snapshots) {
            var members = snapshots.val();
            var allmember = [];
            for (var keys in members) {
                allmember.push(members[keys]);
            }
            if (allmember.length > 1) {
                var flag_1 = 0;
                var _loop_5 = function (i) {
                    _this.firegroup.child(allmember[i].uid).child(_this.currentgroupname).child('owner').once('value', function (snapshot) {
                        var owner = snapshot.val();
                        flag_1++;
                        var keyofmember;
                        for (var keyss in owner) {
                            if (owner[keyss].uid == member.uid) {
                                keyofmember = keyss;
                            }
                        }
                        _this.firegroup.child(allmember[i].uid).child(_this.currentgroupname).child('owner').child(keyofmember).remove();
                        if (flag_1 == allmember.length) {
                            _this.firegroup.child(member.uid).child(_this.currentgroupname).child('members').remove();
                            _this.getgroupmembers();
                        }
                    });
                };
                for (var i = 0; i < allmember.length; i++) {
                    _loop_5(i);
                }
            }
        });
    };
    GroupsProvider.prototype.removeAdmin = function (member) {
        this.deletemember(member);
        this.removefromAdmin(member);
    };
    GroupsProvider.prototype.deletegroups = function (groupname) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firegroup.child(_this.userId).child(groupname).child('members').once('value', function (snapshot) {
                var tempmembers = snapshot.val();
                for (var key in tempmembers) {
                    _this.firegroup.child(tempmembers[key].uid).child(groupname).remove();
                }
                _this.firegroup.child(_this.userId).child(groupname).remove().then(function () {
                    resolve(true);
                }).catch(function (err) {
                    reject(err);
                });
            });
        });
    };
    GroupsProvider.prototype.leavegroups = function (groupname) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firegroup.child(_this.userId).child(groupname).once('value', function (snapshot) {
                var tempowner = _this.converanobj(snapshot.val().owner);
                _this.firegroup.child(tempowner[0].uid).child(groupname).child('members').once('value', function (snapshots) {
                    var cuser = snapshots.val();
                    for (var key in cuser) {
                        if (cuser[key].uid == _this.userId) {
                            _this.firegroup.child(tempowner[0].uid).child(groupname).child('members').child(key).remove();
                        }
                    }
                    _this.firegroup.child(_this.userId).child(groupname).remove().then(function () {
                        resolve(true);
                    }).catch(function (err) {
                        reject(err);
                    });
                });
            });
        });
    };
    GroupsProvider.prototype.leavegroupsowner = function (groupname) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firegroup.child(_this.userId).child(groupname).once('value', function (snapshot) {
                _this.firegroup.child(_this.userId).child(groupname).child('members').once('value', function (snapshot) {
                    var members = snapshot.val();
                    var allmember = [];
                    for (var keys in members) {
                        allmember.push(members[keys]);
                    }
                    if (allmember.length > 1) {
                        var flag_2 = 0;
                        var _loop_6 = function (i) {
                            _this.firegroup.child(allmember[i].uid).child(groupname).child('owner').once('value', function (snapshot) {
                                var owner = snapshot.val();
                                flag_2++;
                                var keyofmember;
                                for (var keyss in owner) {
                                    if (owner[keyss].uid == _this.userId) {
                                        keyofmember = keyss;
                                    }
                                }
                                _this.firegroup.child(allmember[i].uid).child(groupname).child('owner').child(keyofmember).remove();
                                if (flag_2 == allmember.length) {
                                    _this.firegroup.child(_this.userId).child(groupname).child('members').remove();
                                }
                            });
                            _this.firegroup.child(allmember[i].uid).child(groupname).child('members').once('value', function (snapshots) {
                                var cuser = snapshots.val();
                                for (var key in cuser) {
                                    if (cuser[key].uid == _this.userId) {
                                        _this.firegroup.child(allmember[i].uid).child(groupname).child('members').child(key).remove();
                                    }
                                }
                            });
                            _this.firegroup.child(_this.userId).child(groupname).remove().then(function () {
                                resolve(true);
                            }).catch(function (err) {
                                reject(err);
                            });
                        };
                        for (var i = 0; i < allmember.length; i++) {
                            _loop_6(i);
                        }
                    }
                });
            });
        });
    };
    GroupsProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_4__user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_5__fcm_fcm__["a" /* FcmProvider */]])
    ], GroupsProvider);
    return GroupsProvider;
}());

//# sourceMappingURL=groups.js.map

/***/ }),

/***/ 497:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__user_user__ = __webpack_require__(70);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ChatProvider = (function () {
    function ChatProvider(events, storage, userservice) {
        this.events = events;
        this.storage = storage;
        this.userservice = userservice;
        this.firedata = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/chatusers');
        this.firebuddychats = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/buddychats');
        this.firebuddymessagecounter = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/buddychats');
        this.fireuserStatus = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/userstatus');
        this.fireStar = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/starredmessage');
        this.firefriends = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/friends');
        this.buddymessages = [];
        this.msgcount = 0;
        var userID = localStorage.getItem('userUID');
        if (userID != undefined) {
            this.userId = userID;
        }
        else {
            this.userId = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid;
        }
    }
    ChatProvider.prototype.buddymessageRead = function () {
        var _this = this;
        this.firebuddychats.child(this.buddy.uid).child(this.userId)
            .orderByChild('uid').once('value', function (snapshot) {
            var allmessahes = snapshot.val();
            for (var key in allmessahes) {
                if (allmessahes[key].isRead == false) {
                    _this.firebuddychats.child(_this.buddy.uid).child(_this.userId).child(key).update({ isRead: true });
                }
            }
        });
    };
    ChatProvider.prototype.initializebuddy = function (buddy) {
        this.buddy = buddy;
        this.buddymessageRead();
    };
    ChatProvider.prototype.formatAMPM = function (date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    };
    ChatProvider.prototype.formatDate = function (date) {
        var dd = date.getDate();
        var mm = date.getMonth() + 1;
        var yyyy = date.getFullYear();
        if (dd < 10)
            dd = '0' + dd;
        if (mm < 10)
            mm = '0' + mm;
        return dd + '/' + mm + '/' + yyyy;
    };
    ChatProvider.prototype.addnewmessageBroadcast = function (msg, type, buddy, selectCatId) {
        var _this = this;
        var time = this.formatAMPM(new Date());
        var date = this.formatDate(new Date());
        if (buddy) {
            var promise = new Promise(function (resolve, reject) {
                _this.firebuddychats.child(_this.userId).child(buddy.uid).push({
                    sentby: _this.userId,
                    message: msg,
                    type: type,
                    timestamp: __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database.ServerValue.TIMESTAMP,
                    timeofmsg: time,
                    dateofmsg: date,
                    messageId: '',
                    selectCatId: selectCatId,
                    multiMessage: false,
                    isStarred: false,
                    userprio: ''
                })
                    .then(function (item) {
                    _this.getfirendlist(_this.userId).then(function (res) {
                        var friends = res;
                        for (var tmpkey in friends) {
                            if (friends[tmpkey].uid == buddy.uid) {
                                _this.firefriends.child(_this.userId).child(tmpkey).update({ isActive: 1, userprio: new Date() });
                                resolve(true);
                            }
                        }
                    });
                    _this.firebuddychats.child(_this.userId).child(buddy.uid).child(item.key).update({ messageId: item.key, userprio: new Date() });
                    _this.userservice.getstatusblock(buddy).then(function (res) {
                        if (res == false) {
                            //mytodo : check if buddystatus is online
                            //if (buddystatus == "online") {
                            if (false) {
                                //this.firebuddychats.child(this.userId).child(buddy.uid).child(item.key).update({ isRead: true });
                            }
                            else {
                                _this.firebuddychats.child(_this.userId).child(buddy.uid).child(item.key).update({ isRead: false });
                            }
                        }
                        else {
                            _this.firebuddychats.child(_this.userId).child(buddy.uid).child(item.key).update({ isRead: false });
                        }
                    });
                    /*
                    //mytodo : why following code does not work : it create 4-6 additional chat messages which i dont understand why
                    this.userservice.getstatusblock(buddy).then((res) => {
                      if (res == false) {
                        this.firebuddychats.child(buddy.uid).child(this.userId).push({
                          sentby: this.userId,
                          message: msg,
                          type: type,
                          timestamp: firebase.database.ServerValue.TIMESTAMP,
                          timeofmsg: time,
                          dateofmsg: date,
                          messageId: '',
                          isRead: true,
                          isStarred: false,
                          userprio:''
                        }).then((items) => {
                          this.firebuddychats.child(buddy.uid).child(this.userId).child(items.key).update({ messageId: items.key , userprio:new Date()}).then(() => {
        
                            this.getfirendlist(buddy.uid).then((res) => {
                              let friends = res;
                              for (var tmpkey in friends) {
                                if (friends[tmpkey].uid == this.userId) {
                                  this.firefriends.child(buddy.uid).child(tmpkey).update({ isActive: 1, userprio: new Date() });
                                  resolve(true);
                                }
                              }
                            })
                          })
                        })
                      } else {
                        resolve(true);
                      }
                    })
                    */
                });
            });
            return promise;
        }
    };
    ChatProvider.prototype.addnewmessage = function (msg, type, buddystatus) {
        var _this = this;
        var time = this.formatAMPM(new Date());
        var date = this.formatDate(new Date());
        if (this.buddy) {
            var promise = new Promise(function (resolve, reject) {
                _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).push({
                    sentby: _this.userId,
                    message: msg,
                    type: type,
                    timestamp: __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database.ServerValue.TIMESTAMP,
                    timeofmsg: time,
                    dateofmsg: date,
                    messageId: '',
                    multiMessage: false,
                    isStarred: false,
                    userprio: ''
                })
                    .then(function (item) {
                    _this.getfirendlist(_this.userId).then(function (res) {
                        var friends = res;
                        for (var tmpkey in friends) {
                            if (friends[tmpkey].uid == _this.buddy.uid) {
                                _this.firefriends.child(_this.userId).child(tmpkey).update({ isActive: 1, userprio: new Date() });
                                resolve(true);
                            }
                        }
                    });
                    _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).child(item.key).update({ messageId: item.key, userprio: new Date() });
                    _this.userservice.getstatusblock(_this.buddy).then(function (res) {
                        if (res == false) {
                            if (buddystatus == "online") {
                                _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).child(item.key).update({ isRead: true });
                            }
                            else {
                                _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).child(item.key).update({ isRead: false });
                            }
                        }
                        else {
                            _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).child(item.key).update({ isRead: false });
                        }
                    });
                    _this.userservice.getstatusblock(_this.buddy).then(function (res) {
                        if (res == false) {
                            _this.firebuddychats.child(_this.buddy.uid).child(_this.userId).push({
                                sentby: _this.userId,
                                message: msg,
                                type: type,
                                timestamp: __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database.ServerValue.TIMESTAMP,
                                timeofmsg: time,
                                dateofmsg: date,
                                messageId: '',
                                isRead: true,
                                isStarred: false,
                                userprio: ''
                            }).then(function (items) {
                                _this.firebuddychats.child(_this.buddy.uid).child(_this.userId).child(items.key).update({ messageId: items.key, userprio: new Date() }).then(function () {
                                    _this.getfirendlist(_this.buddy.uid).then(function (res) {
                                        var friends = res;
                                        for (var tmpkey in friends) {
                                            if (friends[tmpkey].uid == _this.userId) {
                                                _this.firefriends.child(_this.buddy.uid).child(tmpkey).update({ isActive: 1, userprio: new Date() });
                                                resolve(true);
                                            }
                                        }
                                    });
                                });
                            });
                        }
                        else {
                            resolve(true);
                        }
                    });
                });
            });
            return promise;
        }
    };
    ChatProvider.prototype.addnewmessagemultiple = function (msg, type, buddystatus) {
        var _this = this;
        var time = this.formatAMPM(new Date());
        var date = this.formatDate(new Date());
        if (this.buddy) {
            var promise = new Promise(function (resolve, reject) {
                _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).push({
                    sentby: _this.userId,
                    message: msg,
                    type: type,
                    timestamp: __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database.ServerValue.TIMESTAMP,
                    timeofmsg: time,
                    dateofmsg: date,
                    messageId: '',
                    isRead: true,
                    multiMessage: true,
                    isStarred: false,
                    userprio: ''
                })
                    .then(function (item) {
                    _this.getfirendlist(_this.userId).then(function (res) {
                        var friends = res;
                        for (var tmpkey in friends) {
                            if (friends[tmpkey].uid == _this.buddy.uid) {
                                _this.firefriends.child(_this.userId).child(tmpkey).update({ isActive: 1, userprio: new Date() });
                                resolve(true);
                            }
                        }
                    });
                    _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).child(item.key).update({ messageId: item.key, userprio: new Date() });
                    if (buddystatus == "online") {
                        _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).child(item.key).update({ isRead: true });
                    }
                    else {
                        _this.userservice.getstatusblock(_this.buddy).then(function (res) {
                            if (res == false) {
                                _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).child(item.key).update({ isRead: false });
                            }
                            else {
                                _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).child(item.key).update({ isRead: true });
                            }
                        });
                    }
                    _this.userservice.getstatusblock(_this.buddy).then(function (res) {
                        if (res == false) {
                            _this.firebuddychats.child(_this.buddy.uid).child(_this.userId).push({
                                sentby: _this.userId,
                                message: msg,
                                type: type,
                                timestamp: __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database.ServerValue.TIMESTAMP,
                                timeofmsg: time,
                                dateofmsg: date,
                                messageId: '',
                                multiMessage: true,
                                isStarred: false,
                                userprio: ''
                            }).then(function (items) {
                                _this.firebuddychats.child(_this.buddy.uid).child(_this.userId).child(items.key).update({ messageId: items.key, userprio: new Date() }).then(function () {
                                    if (buddystatus == "online") {
                                        _this.firebuddychats.child(_this.buddy.uid).child(_this.userId).child(items.key).update({ isRead: true });
                                    }
                                    else {
                                    }
                                    _this.getfirendlist(_this.buddy.uid).then(function (res) {
                                        var friends = res;
                                        for (var tmpkey in friends) {
                                            if (friends[tmpkey].uid == _this.userId) {
                                                _this.firefriends.child(_this.buddy.uid).child(tmpkey).update({ isActive: 1, userprio: new Date() });
                                                resolve(true);
                                            }
                                        }
                                    });
                                });
                            });
                        }
                        else {
                            resolve(true);
                        }
                    });
                });
            });
            return promise;
        }
    };
    ChatProvider.prototype.deleteMessages = function (items) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            for (var i = 0; i < items.length; i++) {
                _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).child(items[i].messageId).remove()
                    .then(function (res) {
                    _this.getfirendlist(_this.userId).then(function (res) {
                        var friends = res;
                        for (var tmpkey in friends) {
                            if (friends[tmpkey].uid == _this.buddy.uid) {
                                _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).once('value', function (snapshot) {
                                    var tempdata = _this.converanobj(snapshot.val());
                                    if (tempdata.length > 0) {
                                        var lastMsg = tempdata[tempdata.length - 1];
                                        _this.firefriends.child(_this.userId).child(tmpkey).update({ userprio: lastMsg.userprio });
                                        resolve(true);
                                    }
                                    else {
                                        _this.firefriends.child(_this.userId).child(tmpkey).update({ userprio: '' });
                                        resolve(true);
                                    }
                                });
                            }
                        }
                    });
                }).catch(function (err) {
                    reject(false);
                });
            }
        });
    };
    ChatProvider.prototype.deleteUserMessages = function (allbuddyuser) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (allbuddyuser.length > 0) {
                var _loop_1 = function (i) {
                    _this.firebuddychats.child(_this.userId).child(allbuddyuser[i].uid).remove()
                        .then(function (res) {
                        _this.getfirendlist(_this.userId).then(function (res) {
                            var friends = res;
                            for (var tmpkey in friends) {
                                if (friends[tmpkey].uid == allbuddyuser[i].uid) {
                                    _this.firefriends.child(_this.userId).child(tmpkey).update({ isActive: 0 }).then(function (res) {
                                        resolve(true);
                                        _this.events.publish('friends');
                                    });
                                }
                            }
                        });
                    });
                };
                for (var i = 0; i < allbuddyuser.length; i++) {
                    _loop_1(i);
                }
            }
        });
    };
    ChatProvider.prototype.getbuddymessages = function () {
        var _this = this;
        var temp;
        this.firebuddychats.child(this.userId).child(this.buddy.uid).on('value', function (snapshot) {
            _this.buddymessages = [];
            temp = snapshot.val();
            for (var tempkey in temp) {
                _this.buddymessages.push(temp[tempkey]);
            }
            _this.events.publish('newmessage');
        });
    };
    ChatProvider.prototype.getbuddyStatus = function () {
        var _this = this;
        var tmpStatus;
        this.fireuserStatus.child(this.buddy.uid).on('value', function (statuss) {
            tmpStatus = statuss.val();
            if (tmpStatus.status == 1) {
                _this.buddyStatus = tmpStatus.data;
            }
            else {
                var date = tmpStatus.timestamp;
                _this.buddyStatus = date;
            }
            _this.events.publish('onlieStatus');
        });
    };
    ChatProvider.prototype.setstatusUser = function () {
        var _this = this;
        var time = this.formatAMPM(new Date());
        var date = this.formatDate(new Date());
        var promise = new Promise(function (resolve, reject) {
            _this.fireuserStatus.child(_this.userId).set({
                status: 1,
                data: 'online',
                timestamp: date + ' at ' + time
            }).then(function () {
                resolve(true);
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    ChatProvider.prototype.setStatusOffline = function () {
        var _this = this;
        var time = this.formatAMPM(new Date());
        var date = this.formatDate(new Date());
        var promise = new Promise(function (resolve, reject) {
            _this.fireuserStatus.child(_this.userId).update({
                status: 0,
                data: 'offline',
                timestamp: date + ' at ' + time
            }).then(function () {
                resolve(true);
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    ChatProvider.prototype.getfirendlist = function (uid) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firefriends.child(uid).on('value', function (snapshot) {
                var friendsAll = snapshot.val();
                resolve(friendsAll);
            });
        });
    };
    ChatProvider.prototype.converanobj = function (obj) {
        var tmp = [];
        for (var key in obj) {
            tmp.push(obj[key]);
        }
        return tmp;
    };
    ChatProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_4__user_user__["a" /* UserProvider */]])
    ], ChatProvider);
    return ChatProvider;
}());

//# sourceMappingURL=chat.js.map

/***/ }),

/***/ 498:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_contacts__ = __webpack_require__(297);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ContactProvider = (function () {
    function ContactProvider(contacts) {
        this.contacts = contacts;
    }
    // Get All contact from the device
    ContactProvider.prototype.getSimJsonContacts = function () {
        var _this = this;
        var options = {
            filter: "",
            multiple: true,
            hasPhoneNumber: true
        };
        return new Promise(function (resolve, reject) {
            _this.contacts.find(["phoneNumbers", "displayName"], options).then(function (res) {
                if (res != null) {
                    resolve(_this.onSuccess(res));
                }
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    // Call onSuccess contact res
    ContactProvider.prototype.onSuccess = function (contacts) {
        var phoneContactsJson = [];
        for (var i = 0; i < contacts.length; i++) {
            var no = void 0;
            if (contacts[i].name != null) {
                no = contacts[i].name.formatted;
            }
            var phonenumber = contacts[i].phoneNumbers;
            if (phonenumber != null) {
                var type = phonenumber[0].type;
                if (type == 'mobile' || type == 'work' || type == 'home') {
                    var phone = phonenumber[0].value;
                    var tempMobile = void 0;
                    tempMobile = phone.replace(/[^0-9]/g, "");
                    var mobile = void 0;
                    if (tempMobile.length > 10) {
                        mobile = tempMobile.substr(tempMobile.length - 10);
                    }
                    else {
                        mobile = tempMobile;
                    }
                    var contactData = {
                        "_id": mobile,
                        "displayName": no,
                        "mobile": mobile,
                        "isUser": '0'
                    };
                    phoneContactsJson.push(contactData);
                }
            }
        }
        var collection = {
            contacts: phoneContactsJson
        };
        return collection;
    };
    ContactProvider.prototype.addContact = function (newContact) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var contact = _this.contacts.create();
            contact.displayName = newContact.displayName;
            var field = new __WEBPACK_IMPORTED_MODULE_1__ionic_native_contacts__["a" /* ContactField */]();
            field.type = 'mobile';
            field.value = newContact.phoneNumber;
            field.pref = true;
            var numberSection = [];
            numberSection.push(field);
            contact.phoneNumbers = numberSection;
            contact.save().then(function (value) {
                resolve(true);
            }, function (error) {
                reject(error);
            });
        });
    };
    ContactProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_contacts__["b" /* Contacts */]])
    ], ContactProvider);
    return ContactProvider;
}());

//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 499:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImagehandlerProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(359);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_loading_loading__ = __webpack_require__(240);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_image_picker__ = __webpack_require__(360);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_in_app_browser__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_media_capture__ = __webpack_require__(362);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_chooser__ = __webpack_require__(363);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var ImagehandlerProvider = (function () {
    function ImagehandlerProvider(loadingCtrl, loadingProvider, camera, actionSheetCtrl, toastCtrl, imagePicker, pf, iab, mediaCapture, chooser) {
        this.loadingCtrl = loadingCtrl;
        this.loadingProvider = loadingProvider;
        this.camera = camera;
        this.actionSheetCtrl = actionSheetCtrl;
        this.toastCtrl = toastCtrl;
        this.imagePicker = imagePicker;
        this.pf = pf;
        this.iab = iab;
        this.mediaCapture = mediaCapture;
        this.chooser = chooser;
        this.firestore = __WEBPACK_IMPORTED_MODULE_3_firebase___default.a.storage();
        var userID = localStorage.getItem('userUID');
        if (userID != undefined) {
            this.userId = userID;
        }
        else {
            this.userId = __WEBPACK_IMPORTED_MODULE_3_firebase___default.a.auth().currentUser.uid;
        }
    }
    ImagehandlerProvider.prototype.uploadimage = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var actionSheet = _this.actionSheetCtrl.create({
                title: 'Select Image From',
                buttons: [{
                        text: 'Load from Gallery',
                        handler: function () {
                            _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY, _this.camera.DestinationType.FILE_URI).then(function (data) {
                                resolve(data);
                            }).catch(function (err) {
                                reject(err);
                            });
                        }
                    }, {
                        text: 'Camera',
                        handler: function () {
                            _this.takePicture(_this.camera.PictureSourceType.CAMERA, _this.camera.DestinationType.FILE_URI).then(function (data) {
                                resolve(data);
                            }).catch(function (err) {
                                reject(err);
                            });
                        }
                    }, {
                        text: 'Cancel',
                        role: 'cancel'
                    }
                ]
            });
            actionSheet.present();
        });
    };
    ImagehandlerProvider.prototype.takePicture = function (sourceType, destinationType) {
        var _this = this;
        var options = {
            quality: 100,
            sourceType: sourceType,
            destinationType: destinationType,
            saveToPhotoAlbum: false,
            allowEdit: true,
            targetWidth: 500,
            targetHeight: 500,
            correctOrientation: true
        };
        return new Promise(function (resolve, reject) {
            _this.camera.getPicture(options).then(function (url) {
                _this.loadingProvider.presentLoading();
                window.resolveLocalFileSystemURL(url, function (res) {
                    res.file(function (resFile) {
                        var reader = new FileReader();
                        reader.readAsArrayBuffer(resFile);
                        reader.onloadend = function (evt) {
                            var imgBlob = new Blob([evt.target.result], { type: 'image/jpeg' });
                            var imageStore = _this.firestore.ref('/profileimages').child(_this.userId);
                            imageStore.put(imgBlob).then(function (res) {
                                _this.firestore.ref('/profileimages').child(_this.userId).getDownloadURL().then(function (url) {
                                    resolve(url);
                                }).catch(function (err) {
                                    reject(err);
                                });
                            }).catch(function (err) {
                                reject(err);
                            });
                        };
                    });
                });
            }, function (err) {
                _this.presentToast('Error while selecting image.');
                reject(err);
            });
        });
    };
    ImagehandlerProvider.prototype.getAllIomage = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firestore.ref('/picmsgs').child(_this.userId).getDownloadURL().then(function (url) {
            });
        });
    };
    ImagehandlerProvider.prototype.picmsgstore = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var options = {
                quality: 100,
                sourceType: _this.camera.PictureSourceType.PHOTOLIBRARY,
                destinationType: _this.camera.DestinationType.FILE_URI,
                saveToPhotoAlbum: false,
                allowEdit: true,
                targetWidth: 500,
                targetHeight: 500,
                correctOrientation: true
            };
            _this.imagePicker.getPictures(options).then(function (results) {
                var imageName = '';
                var imageCounter = 0;
                var responseArr = [];
                if (results.length > 0) {
                    _this.loadingProvider.presentLoading();
                    for (var i = 0; i < results.length; i++) {
                        if (_this.pf.is('ios')) {
                            imageName = 'file://' + results[i];
                        }
                        else {
                            imageName = results[i];
                        }
                        window.resolveLocalFileSystemURL(imageName, function (res) {
                            res.file(function (resFile) {
                                var reader = new FileReader();
                                reader.readAsArrayBuffer(resFile);
                                reader.onloadend = function (evt) {
                                    var imgBlob = new Blob([evt.target.result], { type: 'image/jpeg' });
                                    var uuid = _this.guid();
                                    var imageStore = _this.firestore.ref('/picmsgs').child(_this.userId).child('picmsg' + uuid);
                                    imageStore.put(imgBlob).then(function (res) {
                                        imageCounter++;
                                        responseArr.push(res.downloadURL);
                                        if (results.length == imageCounter) {
                                            _this.loadingProvider.dismissMyLoading();
                                            resolve(responseArr);
                                        }
                                    }).catch(function (err) {
                                        _this.loadingProvider.dismissMyLoading();
                                        reject(err);
                                    }).catch(function (err) {
                                        _this.loadingProvider.dismissMyLoading();
                                        reject(err);
                                    });
                                };
                            });
                        }, function (err) {
                            _this.loadingProvider.dismissMyLoading();
                        });
                    }
                }
                else {
                    resolve(true);
                }
            }).catch(function (err) {
                _this.loadingProvider.dismissMyLoading();
            });
        }).catch(function (err) {
        });
    };
    ImagehandlerProvider.prototype.cameraPicmsgStore = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            // Create options for the Camera Dialog
            var options = {
                quality: 100,
                sourceType: _this.camera.PictureSourceType.CAMERA,
                destinationType: _this.camera.DestinationType.FILE_URI,
                saveToPhotoAlbum: false,
                allowEdit: true,
                targetWidth: 500,
                targetHeight: 500,
                correctOrientation: true
            };
            _this.camera.getPicture(options).then(function (url) {
                _this.loadingProvider.presentLoading();
                window.resolveLocalFileSystemURL(url, function (res) {
                    res.file(function (resFile) {
                        var reader = new FileReader();
                        reader.readAsArrayBuffer(resFile);
                        reader.onloadend = function (evt) {
                            var imgBlob = new Blob([evt.target.result], { type: 'image/jpeg' });
                            var uuid = _this.guid();
                            var imageStore = _this.firestore.ref('/picmsgs').child(_this.userId).child('picmsg' + uuid);
                            imageStore.put(imgBlob).then(function (res) {
                                _this.loadingProvider.dismissMyLoading();
                                resolve(res.downloadURL);
                            }).catch(function (err) {
                                _this.loadingProvider.dismissMyLoading();
                                reject(err);
                            }).catch(function (err) {
                                _this.loadingProvider.dismissMyLoading();
                                reject(err);
                            });
                        };
                    });
                });
            });
            _this.loadingProvider.dismissMyLoading();
        });
    };
    ImagehandlerProvider.prototype.guid = function () {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    };
    ImagehandlerProvider.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    ImagehandlerProvider.prototype.gpuploadimage = function (groupname) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var actionSheet = _this.actionSheetCtrl.create({
                title: 'Select Image From',
                buttons: [{
                        text: 'Load from Gallery',
                        handler: function () {
                            _this.gptakePicture(_this.camera.PictureSourceType.PHOTOLIBRARY, _this.camera.DestinationType.FILE_URI, groupname).then(function (data) {
                                resolve(data);
                            }).catch(function (err) {
                                reject(err);
                            });
                        }
                    }, {
                        text: 'Camera',
                        handler: function () {
                            _this.gptakePicture(_this.camera.PictureSourceType.CAMERA, _this.camera.DestinationType.FILE_URI, groupname).then(function (data) {
                                resolve(data);
                            }).catch(function (err) {
                                reject(err);
                            });
                        }
                    }, {
                        text: 'Cancel',
                        role: 'cancel'
                    }
                ]
            });
            actionSheet.present();
        });
    };
    ImagehandlerProvider.prototype.gptakePicture = function (sourceType, destinationType, groupname) {
        var _this = this;
        // Create options for the Camera Dialog
        var options = {
            quality: 100,
            sourceType: sourceType,
            destinationType: destinationType,
            saveToPhotoAlbum: false,
            allowEdit: true,
            targetWidth: 500,
            targetHeight: 500,
            correctOrientation: true
        };
        return new Promise(function (resolve, reject) {
            _this.camera.getPicture(options).then(function (url) {
                _this.loadingProvider.presentLoading();
                window.resolveLocalFileSystemURL(url, function (res) {
                    res.file(function (resFile) {
                        var reader = new FileReader();
                        reader.readAsArrayBuffer(resFile);
                        reader.onloadend = function (evt) {
                            var imgBlob = new Blob([evt.target.result], { type: 'image/jpeg' });
                            var imageStore = _this.firestore.ref('/groupimages').child(_this.userId).child(groupname);
                            imageStore.put(imgBlob).then(function (res) {
                                _this.firestore.ref('/groupimages').child(_this.userId).child(groupname).getDownloadURL().then(function (url) {
                                    resolve(url);
                                }).catch(function (err) {
                                    _this.loadingProvider.dismissMyLoading();
                                    reject(err);
                                });
                            }).catch(function (err) {
                                _this.loadingProvider.dismissMyLoading();
                                reject(err);
                            });
                        };
                    });
                });
            }, function (err) {
                reject(err);
            });
        });
    };
    ImagehandlerProvider.prototype.get_url_extension = function (url) {
        return url.split(/\#|\?/)[0].split('.').pop().trim();
    };
    ImagehandlerProvider.prototype.selectDocument = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.chooser.getFile('application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet').then(function (url) {
                if ((url.mediaType == 'application/octet-stream' && _this.get_url_extension(url.uri) == 'txt') || (url.mediaType == 'application/octet-stream' && _this.get_url_extension(url.uri) == 'xls') || (url.mediaType == 'application/octet-stream' && _this.get_url_extension(url.uri) == 'xlsx') ||
                    (url.mediaType == 'application/octet-stream' && _this.get_url_extension(url.uri) == 'doc') ||
                    (url.mediaType == 'application/octet-stream' && _this.get_url_extension(url.uri) == 'docx') ||
                    (url.mediaType == 'application/octet-stream' && _this.get_url_extension(url.uri) == 'pdf') || url.mediaType == 'application/pdf' || url.mediaType == 'application/msword' || url.mediaType == 'application/vnd.ms-excel' || url.mediaType == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' || url.mediaType == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                    _this.loadingProvider.presentLoading();
                    window.resolveLocalFileSystemURL(url.uri, function (res) {
                        res.file(function (resFile) {
                            var reader = new FileReader();
                            reader.readAsArrayBuffer(resFile);
                            reader.onloadend = function (evt) {
                                var imgBlob = new Blob([evt.target.result], { type: 'document' });
                                var uuid = _this.guid();
                                var imageStore = _this.firestore.ref('/docs').child(_this.userId).child('doc' + uuid);
                                imageStore.put(imgBlob).then(function (res) {
                                    _this.loadingProvider.dismissMyLoading();
                                    resolve(res.downloadURL);
                                }).catch(function (err) {
                                    _this.loadingProvider.dismissMyLoading();
                                    reject(err);
                                }).catch(function (err) {
                                    _this.loadingProvider.dismissMyLoading();
                                    reject(err);
                                });
                            };
                        });
                    });
                }
                else {
                    _this.presentToast('Upload only document files.');
                }
            }).catch(function (err) {
                _this.loadingProvider.dismissMyLoading();
            });
        });
    };
    ImagehandlerProvider.prototype.openDocument = function (path) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var browser = _this.iab.create(path, "_system", { location: "yes" });
            browser.on('loadstop').subscribe(function (event) {
                resolve("File opened");
            });
        });
    };
    ImagehandlerProvider.prototype.recordAudio = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.mediaCapture.captureAudio().then(function (data) {
                _this.loadingProvider.presentLoading();
                window.resolveLocalFileSystemURL(data[0]["localURL"], function (res) {
                    res.file(function (resFile) {
                        var reader = new FileReader();
                        reader.readAsArrayBuffer(resFile);
                        reader.onloadend = function (evt) {
                            var imgBlob = new Blob([evt.target.result], { type: 'audio' });
                            var uuid = _this.guid();
                            var imageStore = _this.firestore.ref('/audios').child(_this.userId).child('audio' + uuid);
                            imageStore.put(imgBlob).then(function (res) {
                                _this.loadingProvider.dismissMyLoading();
                                resolve(res.downloadURL);
                            }).catch(function (err) {
                                _this.loadingProvider.dismissMyLoading();
                                reject(err);
                            }).catch(function (err) {
                                _this.loadingProvider.dismissMyLoading();
                                reject(err);
                            });
                        };
                    });
                });
            });
            _this.loadingProvider.dismissMyLoading();
        });
    };
    ImagehandlerProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["z" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_image_picker__["a" /* ImagePicker */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_in_app_browser__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_media_capture__["a" /* MediaCapture */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_chooser__["a" /* Chooser */]])
    ], ImagehandlerProvider);
    return ImagehandlerProvider;
}());

//# sourceMappingURL=imagehandler.js.map

/***/ }),

/***/ 500:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SmsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_sms__ = __webpack_require__(364);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SmsProvider = (function () {
    function SmsProvider(sms) {
        this.sms = sms;
        this.simulateSMS = false;
    }
    SmsProvider.prototype.sendSmsCustomMsg = function (mobile, msg) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (_this.simulateSMS) {
                //Simulate msg sent
                resolve(true);
            }
            else {
                //send message in actual
                console.log("Trying to send sms");
                try {
                    _this.sms.send(mobile, msg).then(function (res) {
                        resolve(true);
                    }).catch(function (err) {
                        reject(err);
                    });
                }
                catch (e) {
                }
            } //endif
        });
    }; //end function
    SmsProvider.prototype.sendSms = function (mobile) {
        var _this = this;
        var msg = "Install TapAlly App from https://googlestore.com";
        return new Promise(function (resolve, reject) {
            if (_this.simulateSMS) {
                //Simulate msg sent
                resolve(true);
            }
            else {
                //send message in actual
                try {
                    _this.sms.send(mobile, msg).then(function (res) {
                        resolve(true);
                    }).catch(function (err) {
                        reject(err);
                    });
                }
                catch (e) {
                }
            } //endif
        });
    }; //end function
    SmsProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_sms__["a" /* SMS */]])
    ], SmsProvider);
    return SmsProvider;
}());

/*
let contactsInvitedViaSMS = localStorage.getItem('contactsInvitedViaSMS');
if(!contactsInvitedViaSMS || contactsInvitedViaSMS== "0" || contactsInvitedViaSMS=0){
  contactsInvitedViaSMS = 1;
} else {
  contactsInvitedViaSMS = contactsInvitedViaSMS + 1;
}
this.storage.set('contactsInvitedViaSMS', contactsInvitedViaSMS);
*/
//# sourceMappingURL=sms.js.map

/***/ }),

/***/ 502:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CatProvider; });
var CatProvider = (function () {
    function CatProvider() {
    }
    CatProvider.prototype.getCats = function () {
        return [
            { "id": 1, "name": "Accountants", "sort": 1 },
            { "id": 2, "name": "Insurance Agents", "sort": 2 },
            { "id": 3, "name": "Real Estate Agents", "sort": 3 },
            { "id": 4, "name": "Mortgage Agents", "sort": 4 },
            { "id": 5, "name": "Travel (Air Ticket) Agents", "sort": 5 },
            { "id": 6, "name": "Lawyers", "sort": 6 },
            { "id": 7, "name": "Dentists", "sort": 7 },
            { "id": 8, "name": "Plumber", "sort": 8 },
            { "id": 9, "name": "Construction/Renovation", "sort": 9 },
            { "id": 10, "name": "Painter", "sort": 10 },
            { "id": 11, "name": "Electrician", "sort": 11 },
            { "id": 12, "name": "xxxxx", "sort": 12 },
            { "id": 13, "name": "Addiction Treatment Center", "sort": 13 },
            { "id": 14, "name": "Adoption Agency", "sort": 14 },
            { "id": 15, "name": "Adult Day Care Center", "sort": 15 },
            { "id": 16, "name": "Adult Education School", "sort": 16 },
            { "id": 17, "name": "Adult Entertainment Club", "sort": 17 },
            { "id": 18, "name": "Adult Entertainment Store", "sort": 18 },
            { "id": 19, "name": "Adult Foster Care Service", "sort": 19 },
            { "id": 20, "name": "Advertising Agency", "sort": 20 },
            { "id": 21, "name": "Aerobics Instructor", "sort": 21 },
            { "id": 22, "name": "Aerospace Company", "sort": 22 },
            { "id": 23, "name": "Afghani Restaurant", "sort": 23 },
            { "id": 24, "name": "African Goods Store", "sort": 24 },
            { "id": 25, "name": "African Restaurant", "sort": 25 },
            { "id": 26, "name": "After School Program", "sort": 26 },
            { "id": 27, "name": "Aged Care", "sort": 27 },
            { "id": 28, "name": "Agricultural Service", "sort": 28 },
            { "id": 29, "name": "Aikido School", "sort": 29 },
            { "id": 30, "name": "Air Compressor Repair Service", "sort": 30 },
            { "id": 31, "name": "Air Compressor Supplier", "sort": 31 },
            { "id": 32, "name": "Air Conditioning Contractor", "sort": 32 },
            { "id": 33, "name": "Air Conditioning Repair Service", "sort": 33 },
            { "id": 34, "name": "Air Duct Cleaning Service", "sort": 34 },
            { "id": 35, "name": "Air Filter Supplier", "sort": 35 },
            { "id": 36, "name": "Air Force Base", "sort": 36 },
            { "id": 37, "name": "Air Traffic Control Tower", "sort": 37 },
            { "id": 38, "name": "Airbrushing Service", "sort": 38 },
            { "id": 39, "name": "Airbrushing Supply Store", "sort": 39 },
            { "id": 40, "name": "Aircraft Rental Service", "sort": 40 },
            { "id": 41, "name": "Aircraft Supply Store", "sort": 41 },
            { "id": 42, "name": "Airline", "sort": 42 },
            { "id": 43, "name": "Airplane", "sort": 43 },
            { "id": 44, "name": "Airport", "sort": 44 },
            { "id": 45, "name": "Airport Shuttle Service", "sort": 45 },
            { "id": 46, "name": "Airport Terminal", "sort": 46 },
            { "id": 47, "name": "Airsoft Supply Store", "sort": 47 },
            { "id": 48, "name": "Airstrip", "sort": 48 },
            { "id": 49, "name": "Alcoholism Treatment Program", "sort": 49 },
            { "id": 50, "name": "Allergist", "sort": 50 }
        ];
    };
    return CatProvider;
}());

//# sourceMappingURL=cats.js.map

/***/ }),

/***/ 503:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_http__ = __webpack_require__(77);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthProvider = (function () {
    function AuthProvider(nativeHttp) {
        this.nativeHttp = nativeHttp;
        this.apiUrl = "https://api.authy.com/protected/json/phones/verification/start";
    }
    AuthProvider.prototype.sendVerificationCode = function (data, type) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.nativeHttp.post(_this.apiUrl, data, {})
                .then(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    AuthProvider.prototype.otpVerify = function (type) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.nativeHttp.get("https://api.authy.com/protected/json/" + type, {}, {})
                .then(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    AuthProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_http__["a" /* HTTP */]])
    ], AuthProvider);
    return AuthProvider;
}());

//# sourceMappingURL=auth.js.map

/***/ }),

/***/ 506:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(507);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(511);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 511:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_img_viewer__ = __webpack_require__(501);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common_http__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_component__ = __webpack_require__(934);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_angularfireconfig__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_angularfire2_auth__ = __webpack_require__(338);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_angularfire2__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_angularfire2_firestore__ = __webpack_require__(346);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_firebase__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_camera__ = __webpack_require__(359);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_contacts__ = __webpack_require__(297);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_firebase__ = __webpack_require__(345);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_sms__ = __webpack_require__(364);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__providers_groups_groups__ = __webpack_require__(496);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_native_image_picker__ = __webpack_require__(360);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_native_in_app_browser__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_media_capture__ = __webpack_require__(362);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_geolocation__ = __webpack_require__(505);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_splash_screen__ = __webpack_require__(493);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_status_bar__ = __webpack_require__(492);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_storage__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__providers_contact_contact__ = __webpack_require__(498);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__providers_loading_loading__ = __webpack_require__(240);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__providers_auth_auth__ = __webpack_require__(503);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__providers_fcm_fcm__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__providers_user_user__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__providers_imagehandler_imagehandler__ = __webpack_require__(499);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__providers_requests_requests__ = __webpack_require__(495);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__providers_chat_chat__ = __webpack_require__(497);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__providers_sms_sms__ = __webpack_require__(500);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__providers_cats_cats__ = __webpack_require__(502);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35_ionic_native_http_connection_backend__ = __webpack_require__(935);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__ionic_native_http__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__providers_gpmessage_gpmessage__ = __webpack_require__(940);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__ionic_native_chooser__ = __webpack_require__(363);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
// Ionic modules









// Entry component

// Firebase config

// Angular fireauth



// Firebase javascript

// Ionic native












// Custom providers














// Firebase config app initialize
__WEBPACK_IMPORTED_MODULE_12_firebase__["initializeApp"](__WEBPACK_IMPORTED_MODULE_8__app_angularfireconfig__["b" /* config */]);
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */]],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_10_angularfire2__["a" /* AngularFireModule */].initializeApp(__WEBPACK_IMPORTED_MODULE_8__app_angularfireconfig__["b" /* config */]),
                __WEBPACK_IMPORTED_MODULE_11_angularfire2_firestore__["b" /* AngularFirestoreModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["c" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_http__["d" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_forms__["b" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_35_ionic_native_http_connection_backend__["c" /* NativeHttpModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */], { tabsPlacement: 'top' }, {
                    links: [
                        { loadChildren: '../pages/add-contact/add-contact.module#AddContactPageModule', name: 'AddContactPage', segment: 'add-contact', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/allcontacts/allcontacts.module#AllcontactsPageModule', name: 'AllcontactsPage', segment: 'allcontacts', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/allimg/allimg.module#AllimgPageModule', name: 'AllimgPage', segment: 'allimg', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/allbuddy-contacts/allbuddy-contacts.module#AllbuddyContactsPageModule', name: 'AllbuddyContactsPage', segment: 'allbuddy-contacts', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/attachments/attachments.module#AttachmentsPageModule', name: 'AttachmentsPage', segment: 'attachments', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/broadcast/broadcast.module#BroadcastPageModule', name: 'BroadcastPage', segment: 'broadcast', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/block-user/block-user.module#BlockUserPageModule', name: 'BlockUserPage', segment: 'block-user', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/broadcast-submitted/broadcast-submitted.module#BroadcastSubmittedPageModule', name: 'BroadcastSubmittedPage', segment: 'broadcast-submitted', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/buddies/buddies.module#BuddiesPageModule', name: 'BuddiesPage', segment: 'buddies', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/chats/chats.module#ChatsPageModule', name: 'ChatsPage', segment: 'chats', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/contact/contact.module#ContactPageModule', name: 'ContactPage', segment: 'contact', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/country-list/country-list.module#CountryListPageModule', name: 'CountryListPage', segment: 'country-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/buddychat/buddychat.module#BuddychatPageModule', name: 'BuddychatPage', segment: 'buddychat', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/disc/disc.module#DiscPageModule', name: 'DiscPage', segment: 'disc', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/earnings/earnings.module#EarningsPageModule', name: 'EarningsPage', segment: 'earnings', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/displayname/displayname.module#DisplaynamePageModule', name: 'DisplaynamePage', segment: 'displayname', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/extra/extra.module#ExtraPageModule', name: 'ExtraPage', segment: 'extra', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/friend-asking-referral/friend-asking-referral.module#FriendAskingReferralPageModule', name: 'FriendAskingReferralPage', segment: 'friend-asking-referral', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/friend-list/friend-list.module#FriendListPageModule', name: 'FriendListPage', segment: 'friend-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/gpattachments/gpattachments.module#GpattachmentsPageModule', name: 'GpattachmentsPage', segment: 'gpattachments', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/group-chat-popover/group-chat-popover.module#GroupChatPopoverPageModule', name: 'GroupChatPopoverPage', segment: 'group-chat-popover', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/groupbuddies/groupbuddies.module#GroupbuddiesPageModule', name: 'GroupbuddiesPage', segment: 'groupbuddies', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/groupbuddiesadd/groupbuddiesadd.module#GroupbuddiesaddPageModule', name: 'GroupbuddiesaddPage', segment: 'groupbuddiesadd', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/groupchat/groupchat.module#GroupchatPageModule', name: 'GroupchatPage', segment: 'groupchat', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/groupinfo/groupinfo.module#GroupinfoPageModule', name: 'GroupinfoPage', segment: 'groupinfo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/groupmembers/groupmembers.module#GroupmembersPageModule', name: 'GroupmembersPage', segment: 'groupmembers', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/home-after-i-need-today/home-after-i-need-today.module#HomeAfterINeedTodayPageModule', name: 'HomeAfterINeedTodayPage', segment: 'home-after-i-need-today', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/groups/groups.module#GroupsPageModule', name: 'GroupsPage', segment: 'groups', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/home/home.module#HomePageModule', name: 'HomePage', segment: 'home', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/importp/importp.module#ImportpPageModule', name: 'ImportpPage', segment: 'importp', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/invite-friend/invite-friend.module#InviteFriendPageModule', name: 'InviteFriendPage', segment: 'invite-friend', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/invitemanually/invitemanually.module#InvitemanuallyPageModule', name: 'InvitemanuallyPage', segment: 'invitemanually', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/invitepeople/invitepeople.module#InvitepeoplePageModule', name: 'InvitepeoplePage', segment: 'invitepeople', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/map/map.module#MapPageModule', name: 'MapPage', segment: 'map', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/menu/menu.module#MenuPageModule', name: 'MenuPage', segment: 'menu', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/menu-block/menu-block.module#MenuBlockPageModule', name: 'MenuBlockPage', segment: 'menu-block', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/notification/notification.module#NotificationPageModule', name: 'NotificationPage', segment: 'notification', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/outreach-for-business/outreach-for-business.module#OutreachForBusinessPageModule', name: 'OutreachForBusinessPage', segment: 'outreach-for-business', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/phoneverfy/phoneverfy.module#PhoneverfyPageModule', name: 'PhoneverfyPage', segment: 'phoneverfy', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/phone/phone.module#PhonePageModule', name: 'PhonePage', segment: 'phone', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/pick-contact-execute/pick-contact-execute.module#DisplaynamePageModule', name: 'PickContactExecutePage', segment: 'pick-contact-execute', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/pick-contact-from/pick-contact-from.module#PickContactFromPageModule', name: 'PickContactFromPage', segment: 'pick-contact-from', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/pick-contact-to/pick-contact-to.module#PickContactToPageModule', name: 'PickContactToPage', segment: 'pick-contact-to', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/popover-chat/popover-chat.module#PopoverChatPageModule', name: 'PopoverChatPage', segment: 'popover-chat', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile-view/profile-view.module#ProfileViewPageModule', name: 'ProfileViewPage', segment: 'profile-view', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profilepic/profilepic.module#ProfilepicPageModule', name: 'ProfilepicPage', segment: 'profilepic', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/referral-infopage/referral-infopage.module#ReferralInfopagePageModule', name: 'ReferralInfopagePage', segment: 'referral-infopage', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/referral/referral.module#ReferralPageModule', name: 'ReferralPage', segment: 'referral', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/registerbusiness/registerbusiness.module#RegisterbusinessPageModule', name: 'RegisterbusinessPage', segment: 'registerbusiness', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/request-after-selected/request-after-selected.module#RequestAfterSelectedPageModule', name: 'RequestAfterSelectedPage', segment: 'request-after-selected', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/request-status/request-status.module#RequestStatusPageModule', name: 'RequestStatusPage', segment: 'request-status', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/request-submitted/request-submitted.module#RequestSubmittedPageModule', name: 'RequestSubmittedPage', segment: 'request-submitted', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/request/request.module#RequestPageModule', name: 'RequestPage', segment: 'request', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/search-chats/search-chats.module#SearchChatsPageModule', name: 'SearchChatsPage', segment: 'search-chats', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/search-group-chat/search-group-chat.module#SearchGroupChatPageModule', name: 'SearchGroupChatPage', segment: 'search-group-chat', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/search-next/search-next.module#SearchNextPageModule', name: 'SearchNextPage', segment: 'search-next', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/search-next-send/search-next-send.module#SearchNextSendPageModule', name: 'SearchNextSendPage', segment: 'search-next-send', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/send-referral-backward/send-referral-backward.module#SendReferralBackwardPageModule', name: 'SendReferralBackwardPage', segment: 'send-referral-backward', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/send-referral-forward/send-referral-forward.module#SendReferralForwardPageModule', name: 'SendReferralForwardPage', segment: 'send-referral-forward', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/send-referral-filter/send-referral-filter.module#SendReferralFilterPageModule', name: 'SendReferralFilterPage', segment: 'send-referral-filter', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/send-referral-pick/send-referral-pick.module#SendReferralPickPageModule', name: 'SendReferralPickPage', segment: 'send-referral-pick', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/setting/setting.module#SettingPageModule', name: 'SettingPage', segment: 'setting', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/stats/stats.module#StatsPageModule', name: 'StatsPage', segment: 'stats', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/show-contact/show-contact.module#ShowContactPageModule', name: 'ShowContactPage', segment: 'show-contact', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/status/status.module#StatusPageModule', name: 'StatusPage', segment: 'status', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tabs/tabs.module#TabsPageModule', name: 'TabsPage', segment: 'tabs', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/submit-receipt/submit-receipt.module#SubmitReceiptPageModule', name: 'SubmitReceiptPage', segment: 'submit-receipt', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tutorial/tutorial.module#TutorialPageModule', name: 'TutorialPage', segment: 'tutorial', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/view-buddy/view-buddy.module#ViewBuddyPageModule', name: 'ViewBuddyPage', segment: 'view-buddy', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/username/username.module#UsernamePageModule', name: 'UsernamePage', segment: 'username', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_24__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_3_ionic_img_viewer__["b" /* IonicImageViewerModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* IonicApp */]],
            entryComponents: [__WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */]],
            providers: [
                __WEBPACK_IMPORTED_MODULE_23__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_22__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_27__providers_auth_auth__["a" /* AuthProvider */],
                __WEBPACK_IMPORTED_MODULE_29__providers_user_user__["a" /* UserProvider */],
                __WEBPACK_IMPORTED_MODULE_26__providers_loading_loading__["a" /* LoadingProvider */],
                __WEBPACK_IMPORTED_MODULE_25__providers_contact_contact__["a" /* ContactProvider */],
                __WEBPACK_IMPORTED_MODULE_38__ionic_native_chooser__["a" /* Chooser */],
                __WEBPACK_IMPORTED_MODULE_9_angularfire2_auth__["a" /* AngularFireAuth */],
                __WEBPACK_IMPORTED_MODULE_30__providers_imagehandler_imagehandler__["a" /* ImagehandlerProvider */],
                __WEBPACK_IMPORTED_MODULE_31__providers_requests_requests__["a" /* RequestsProvider */],
                __WEBPACK_IMPORTED_MODULE_32__providers_chat_chat__["a" /* ChatProvider */],
                __WEBPACK_IMPORTED_MODULE_25__providers_contact_contact__["a" /* ContactProvider */],
                __WEBPACK_IMPORTED_MODULE_14__ionic_native_contacts__["b" /* Contacts */],
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_28__providers_fcm_fcm__["a" /* FcmProvider */],
                __WEBPACK_IMPORTED_MODULE_34__providers_cats_cats__["a" /* CatProvider */],
                __WEBPACK_IMPORTED_MODULE_15__ionic_native_firebase__["a" /* Firebase */],
                __WEBPACK_IMPORTED_MODULE_16__ionic_native_sms__["a" /* SMS */],
                __WEBPACK_IMPORTED_MODULE_33__providers_sms_sms__["a" /* SmsProvider */],
                __WEBPACK_IMPORTED_MODULE_17__providers_groups_groups__["a" /* GroupsProvider */],
                __WEBPACK_IMPORTED_MODULE_36__ionic_native_http__["a" /* HTTP */],
                __WEBPACK_IMPORTED_MODULE_18__ionic_native_image_picker__["a" /* ImagePicker */],
                { provide: __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["a" /* HttpBackend */], useClass: __WEBPACK_IMPORTED_MODULE_35_ionic_native_http_connection_backend__["b" /* NativeHttpFallback */], deps: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["x" /* Platform */], __WEBPACK_IMPORTED_MODULE_35_ionic_native_http_connection_backend__["a" /* NativeHttpBackend */], __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["g" /* HttpXhrBackend */]] },
                __WEBPACK_IMPORTED_MODULE_37__providers_gpmessage_gpmessage__["a" /* GpmessageProvider */],
                __WEBPACK_IMPORTED_MODULE_19__ionic_native_in_app_browser__["a" /* InAppBrowser */],
                __WEBPACK_IMPORTED_MODULE_20__ionic_native_media_capture__["a" /* MediaCapture */],
                __WEBPACK_IMPORTED_MODULE_21__ionic_native_geolocation__["a" /* Geolocation */],
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 70:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__ = __webpack_require__(338);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_app_angularfireconfig__ = __webpack_require__(142);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var UserProvider = (function () {
    function UserProvider(afireAuth, storage, events) {
        this.afireAuth = afireAuth;
        this.storage = storage;
        this.events = events;
        this.firedata = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/chatusers');
        this.firefriend = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/friends');
        this.firebuddychat = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/buddychats');
        this.users = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/users');
        this.userstatus = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/userstatus');
        this.firereq = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/requests');
        this.firenotify = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/notification');
        this.fireBusiness = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/business');
        this.fireInvitationSent = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/invitationsent');
        this.isUserExits = true;
        this.blockUsers = [];
        this.unblockUsers = [];
        this.isuserBlock = false;
        this.blockUsersCounter = 0;
    }
    UserProvider.prototype.initializeItem = function () {
        var _this = this;
        var userID = localStorage.getItem('userUID');
        if (userID != undefined) {
            this.userId = userID;
        }
        else {
            this.userId = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid;
        }
        return new Promise(function (resolve, reject) {
            _this.firenotify.child(_this.userId).child('isNotify').once('value', function (snapshot) {
                _this.isNotify = snapshot.val();
                resolve(_this.isNotify);
            });
        });
    };
    //mytodo : is this being used anywhere ?
    UserProvider.prototype.adduser = function (newuser) {
        var _this = this;
        var myuserPic = __WEBPACK_IMPORTED_MODULE_5__app_app_angularfireconfig__["e" /* userPicArr */][Math.floor(Math.random() * Object.keys(__WEBPACK_IMPORTED_MODULE_5__app_app_angularfireconfig__["e" /* userPicArr */]).length - 1)];
        var promise = new Promise(function (resolve, reject) {
            _this.afireAuth.auth.createUserWithEmailAndPassword(newuser.email, newuser.password).then(function () {
                _this.afireAuth.auth.currentUser.updateProfile({
                    displayName: newuser.username,
                    photoURL: myuserPic
                }).then(function () {
                    _this.firedata.child(_this.userId).set({
                        uid: _this.userId,
                        displayName: newuser.username,
                        photoURL: myuserPic,
                        invited_by: "none"
                    }).then(function () {
                        resolve(true);
                    }).catch(function (err) {
                        reject(err);
                    });
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    UserProvider.prototype.addmobileUser = function (verificationCredential, code, mobile) {
        var _this = this;
        var myuserPic = __WEBPACK_IMPORTED_MODULE_5__app_app_angularfireconfig__["e" /* userPicArr */][Math.floor(Math.random() * Object.keys(__WEBPACK_IMPORTED_MODULE_5__app_app_angularfireconfig__["e" /* userPicArr */]).length - 1)];
        return new Promise(function (resolve, reject) {
            localStorage.setItem('verificationCredential', verificationCredential);
            localStorage.setItem('code', code);
            var signInData = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth.PhoneAuthProvider.credential(verificationCredential, code);
            _this.afireAuth.auth.signInWithCredential(signInData).then(function (info) {
                var profileImage;
                if (_this.afireAuth.auth.currentUser.photoURL != null) {
                    profileImage = _this.afireAuth.auth.currentUser.photoURL;
                }
                else {
                    profileImage = myuserPic;
                }
                _this.afireAuth.auth.currentUser.updateProfile({ displayName: _this.afireAuth.auth.currentUser.displayName, photoURL: profileImage }).then(function () {
                    _this.firedata.child(_this.afireAuth.auth.currentUser.uid).set({
                        uid: _this.afireAuth.auth.currentUser.uid,
                        mobile: mobile,
                        displayName: _this.afireAuth.auth.currentUser.displayName,
                        disc: '',
                        photoURL: profileImage,
                        deviceToken: ''
                    }).then(function () {
                        resolve({ success: true });
                    }).catch(function (err) {
                        reject(err);
                    });
                }).catch(function (err) {
                });
            }).catch(function (err) {
                resolve({ success: false, msg: JSON.stringify(err.message) });
            });
        });
    };
    UserProvider.prototype.addmobileUserIOS = function (countryCode, mobile) {
        var _this = this;
        var myuserPic = __WEBPACK_IMPORTED_MODULE_5__app_app_angularfireconfig__["e" /* userPicArr */][Math.floor(Math.random() * Object.keys(__WEBPACK_IMPORTED_MODULE_5__app_app_angularfireconfig__["e" /* userPicArr */]).length - 1)];
        return new Promise(function (resolve, reject) {
            localStorage.setItem('verificationCredential', countryCode);
            localStorage.setItem('code', mobile);
            _this.firedata.once('value', function (snep) {
                var allData = snep.val();
                for (var tmpkey in allData) {
                    if (allData[tmpkey].mobile == mobile) {
                        _this.isUserExits = false;
                        localStorage.setItem('userUID', allData[tmpkey].uid);
                    }
                }
                if (_this.isUserExits) {
                    _this.afireAuth.auth.signInAnonymously().then(function (info) {
                        var profileImage;
                        if (_this.afireAuth.auth.currentUser.photoURL != null) {
                            profileImage = _this.afireAuth.auth.currentUser.photoURL;
                        }
                        else {
                            profileImage = myuserPic;
                        }
                        localStorage.setItem('userUID', _this.afireAuth.auth.currentUser.uid);
                        _this.afireAuth.auth.currentUser.updateProfile({ displayName: _this.afireAuth.auth.currentUser.displayName, photoURL: profileImage }).then(function () {
                            _this.firedata.child(_this.afireAuth.auth.currentUser.uid).set({
                                uid: _this.afireAuth.auth.currentUser.uid,
                                mobile: mobile,
                                displayName: _this.afireAuth.auth.currentUser.displayName,
                                disc: '',
                                photoURL: profileImage,
                                deviceToken: ''
                            }).then(function () {
                                resolve({ success: true });
                            }).catch(function (err) {
                                reject(err);
                            });
                        }).catch(function (err) {
                        });
                    }).catch(function (err) {
                        resolve({ success: false, msg: JSON.stringify(err.message) });
                    });
                }
                else {
                    resolve({ success: true });
                }
            });
        });
    };
    UserProvider.prototype.updateimage = function (imageurl) {
        var _this = this;
        var userID = localStorage.getItem('userUID');
        var userName = localStorage.getItem('userName');
        var userPic = localStorage.getItem('userPic');
        if (userID != undefined) {
            this.userId = userID;
            this.userName = userName;
            this.userPic = userPic;
        }
        else {
            this.userId = this.afireAuth.auth.currentUser.uid;
            this.userName = this.afireAuth.auth.currentUser.displayName;
            this.userPic = this.afireAuth.auth.currentUser.photoURL;
        }
        var promise = new Promise(function (resolve, reject) {
            _this.firedata.child(_this.userId).update({ photoURL: imageurl }).then(function () {
                __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/users/' + _this.userId).update({
                    photoURL: imageurl
                }).then(function () {
                    resolve({ success: true });
                }).catch(function (err) {
                    reject(err);
                });
            });
        });
        return promise;
    };
    UserProvider.prototype.getuserdetails = function () {
        var _this = this;
        var userID = localStorage.getItem('userUID');
        var userName = localStorage.getItem('userName');
        var userPic = localStorage.getItem('userPic');
        if (userID != undefined || userID != null) {
            this.userId = userID;
            this.userName = userName;
            this.userPic = userPic;
        }
        else {
            this.userId = this.afireAuth.auth.currentUser.uid;
            this.userName = this.afireAuth.auth.currentUser.displayName;
            this.userPic = this.afireAuth.auth.currentUser.photoURL;
        }
        var promise = new Promise(function (resolve, reject) {
            _this.firedata.child(_this.userId).once('value', function (snapshot) {
                resolve(snapshot.val());
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    UserProvider.prototype.getbuddydetails = function (buddyID) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.firedata.child(buddyID).once('value', function (snapshot) {
                resolve(snapshot.val());
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    UserProvider.prototype.updatedisplayname = function (newname) {
        var _this = this;
        var userID = localStorage.getItem('userUID');
        if (userID != undefined) {
            this.userId = userID;
        }
        else {
            this.userId = this.afireAuth.auth.currentUser.uid;
        }
        var promise = new Promise(function (resolve, reject) {
            _this.firedata.child(_this.userId).update({
                displayName: newname
            }).then(function () {
                localStorage.setItem('userName', newname);
                resolve({ success: true });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    UserProvider.prototype.updateDeviceToken = function (token, userID) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.firedata.child(userID).update({
                deviceToken: token
            }).then(function () {
                resolve({ success: true });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    UserProvider.prototype.updatedisc = function (disc) {
        var _this = this;
        var userID = localStorage.getItem('userUID');
        var userName = localStorage.getItem('userName');
        var userPic = localStorage.getItem('userPic');
        if (userID != undefined) {
            this.userId = userID;
            this.userName = userName;
            this.userPic = userPic;
        }
        else {
            this.userId = this.afireAuth.auth.currentUser.uid;
            this.userName = this.afireAuth.auth.currentUser.displayName;
            this.userPic = this.afireAuth.auth.currentUser.photoURL;
        }
        var promise = new Promise(function (resolve, reject) {
            _this.firedata.child(_this.userId).update({
                disc: disc
            }).then(function () {
                resolve({ success: true });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    UserProvider.prototype.getallusers = function () {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.firedata.orderByChild('uid').once('value', function (snapshot) {
                //mytodo immediate : Why its getting all users. it should only get me
                var userdata = snapshot.val();
                var temparr = [];
                for (var key in userdata) {
                    temparr.push(userdata[key]);
                }
                resolve(temparr);
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    UserProvider.prototype.directLogin = function () {
        return new Promise(function (resolve, reject) {
            resolve({ success: true });
        });
    };
    UserProvider.prototype.deleteUser = function () {
        var _this = this;
        var userID = localStorage.getItem('userUID');
        var userName = localStorage.getItem('userName');
        var userPic = localStorage.getItem('userPic');
        if (userID != undefined) {
            this.userId = userID;
            this.userName = userName;
            this.userPic = userPic;
        }
        else {
            this.userId = this.afireAuth.auth.currentUser.uid;
            this.userName = this.afireAuth.auth.currentUser.displayName;
            this.userPic = this.afireAuth.auth.currentUser.photoURL;
        }
        return new Promise(function (resolve, reject) {
            _this.firefriend;
            _this.firedata.child(_this.userId).remove().then(function () {
                _this.deletefriend(_this.userId);
                _this.deletefirebuddychat(_this.userId);
                _this.deleteusers(_this.userId);
                _this.deleteuserstatus(_this.userId);
                localStorage.clear();
                resolve({ success: true });
            }).catch(function (err) {
                reject({ success: true });
            });
        });
    };
    UserProvider.prototype.deletefriend = function (id) {
        this.firedata.child(id).remove().then(function (res) {
        });
    };
    UserProvider.prototype.deletefirebuddychat = function (id) {
        this.firebuddychat.child(id).remove().then(function (res) {
        });
    };
    UserProvider.prototype.deleteusers = function (id) {
        this.users.child(id).remove().then(function (res) {
        });
    };
    UserProvider.prototype.deleteuserstatus = function (id) {
        this.userstatus.child(id).remove().then(function (res) {
        });
    };
    UserProvider.prototype.blockUser = function (buddy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firefriend.child(_this.userId).orderByChild('uid').once('value', function (snapshot) {
                var allfriends = snapshot.val();
                for (var key in allfriends) {
                    if (allfriends[key].uid == buddy.uid) {
                        _this.firefriend.child(_this.userId).child(key).update({ isBlock: true });
                        resolve(true);
                    }
                }
            });
        });
    };
    UserProvider.prototype.unblockUser = function (buddy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firefriend.child(_this.userId).orderByChild('uid').once('value', function (snapshot) {
                var allfriends = snapshot.val();
                for (var key in allfriends) {
                    if (allfriends[key].uid == buddy.uid) {
                        _this.firefriend.child(_this.userId).child(key).update({ isBlock: false });
                        resolve(true);
                    }
                }
            });
        });
    };
    UserProvider.prototype.getstatus = function (buddy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firefriend.child(_this.userId).orderByChild('uid').once('value', function (snapshot) {
                var allfriends = snapshot.val();
                for (var key in allfriends) {
                    if (allfriends[key].uid == buddy.uid) {
                        resolve(allfriends[key].isBlock);
                    }
                }
            });
        });
    };
    UserProvider.prototype.getstatusblock = function (buddy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firefriend.child(buddy.uid).orderByChild('uid').once('value', function (snapshot) {
                var allfriends = snapshot.val();
                for (var key in allfriends) {
                    if (allfriends[key].uid == _this.userId) {
                        resolve(allfriends[key].isBlock);
                    }
                }
            });
        });
    };
    UserProvider.prototype.getuserblock = function (buddy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firefriend.child(buddy.uid).orderByChild('uid').once('value', function (snapshot) {
                var allfriends = snapshot.val();
                for (var key in allfriends) {
                    if (allfriends[key].uid == _this.userId) {
                        resolve(allfriends[key].isBlock);
                    }
                }
            });
        });
    };
    UserProvider.prototype.getmsgblock = function (buddy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firefriend.child(buddy).orderByChild('uid').once('value', function (snapshot) {
                var allfriends = snapshot.val();
                for (var key in allfriends) {
                    if (allfriends[key].uid == _this.userId) {
                        resolve(allfriends[key].isBlock);
                    }
                }
            });
        });
    };
    UserProvider.prototype.getstatusblockuser = function (buddy) {
        var _this = this;
        this.firefriend.child(buddy.uid).orderByChild('uid').on('value', function (snapshot) {
            var allfriends = snapshot.val();
            for (var key in allfriends) {
                if (allfriends[key].uid == _this.userId) {
                    _this.isuserBlock = allfriends[key].isBlock;
                }
            }
            _this.events.publish('isblock-user');
        });
    };
    UserProvider.prototype.getAllBlockUsers = function () {
        var _this = this;
        this.firefriend.child(this.userId).orderByChild('uid').once('value', function (snapshot) {
            var allfriends = snapshot.val();
            var blockuser = [];
            for (var key in allfriends) {
                if (allfriends[key].isBlock) {
                    _this.firedata.child(allfriends[key].uid).once('value', function (snapsho) {
                        blockuser.push(snapsho.val());
                    }).catch(function (err) {
                    });
                }
                _this.blockUsers = [];
                _this.blockUsers = blockuser;
            }
            _this.events.publish('block-users');
        });
    };
    UserProvider.prototype.getAllunBlockUsers = function () {
        var _this = this;
        this.firefriend.child(this.userId).orderByChild('uid').once('value', function (snapshot) {
            var allfriends = snapshot.val();
            var unblockuser = [];
            for (var key in allfriends) {
                if (allfriends[key].isBlock == false) {
                    _this.firedata.child(allfriends[key].uid).once('value', function (snapsho) {
                        unblockuser.push(snapsho.val());
                    }).catch(function (err) {
                    });
                }
                _this.unblockUsers = [];
                _this.unblockUsers = unblockuser;
            }
            _this.events.publish('unblock-users');
        });
    };
    UserProvider.prototype.getAllBlockUsersCounter = function () {
        var _this = this;
        // block-users-counter
        this.firefriend.child(this.userId).orderByChild('uid').once('value', function (snapshot) {
            var allfriends = snapshot.val();
            var blockalluser = [];
            for (var tmpkey in allfriends) {
                if (allfriends[tmpkey].isBlock) {
                    blockalluser.push(allfriends[tmpkey]);
                }
            }
            _this.firereq.child(_this.userId).orderByChild('uid').once('value', function (snapshot) {
                var allrequest = snapshot.val();
                for (var tmp in allrequest) {
                    if (allrequest[tmp].isBlock) {
                        blockalluser.push(allrequest[tmp]);
                    }
                }
                _this.blockUsersCounter = 0;
                _this.blockUsersCounter = blockalluser.length;
                _this.events.publish('block-users-counter');
            });
        });
    };
    UserProvider.prototype.notifyUser = function (isnotify) {
        this.firenotify.child(this.userId).set({
            isNotify: isnotify
        });
    };
    UserProvider.prototype.getNotifyStatus = function (buddy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firenotify.child(buddy.uid).child('isNotify').once('value', function (snapshot) {
                var isNotify = snapshot.val();
                if (isNotify == true)
                    resolve(true);
                else
                    resolve(false);
            });
        });
    };
    UserProvider.prototype.logInvitation = function (isInviteArray) {
        var _this = this;
        console.log("1");
        var userID = localStorage.getItem('userUID');
        if (userID != undefined) {
            this.userId = userID;
        }
        else {
            this.userId = this.afireAuth.auth.currentUser.uid;
        }
        console.log("2");
        //create   jaswinder
        return new Promise(function (resolve, reject) {
            _this.fireInvitationSent.child(_this.userId).set({
                phone: isInviteArray.phone
            }).then(function () {
                console.log("3");
                resolve(true);
            }).catch(function (err) {
                resolve(false);
            });
        });
    };
    UserProvider.prototype.registerBusiness = function (newname, useremail, bcats) {
        var _this = this;
        var userID = localStorage.getItem('userUID');
        if (userID != undefined) {
            this.userId = userID;
        }
        else {
            this.userId = this.afireAuth.auth.currentUser.uid;
        }
        //update email
        new Promise(function (resolve, reject) {
            _this.firedata.child(_this.userId).update({
                useremail: useremail
            }).then(function () {
                localStorage.setItem('useremail', useremail);
            }).catch(function (err) {
            });
        });
        var timeStamp = String(Date.now()); //
        //update/create business name
        var promise = new Promise(function (resolve, reject) {
            _this.fireBusiness.child(_this.userId).update({
                displayName: newname,
                timestamp_user_local_time_updated: timeStamp
            }).then(function () {
                if (localStorage.getItem('businessName') || localStorage.getItem('businessName') != newname) {
                    localStorage.setItem('businessName', "");
                }
                if (!localStorage.getItem('businessName')) {
                    //Business Just got registered
                    //var timestamp = firebase.database.ServerValue.TIMESTAMP;
                    new Promise(function (resolve, reject) {
                        _this.fireBusiness.child(_this.userId).update({
                            timestamp_user_local_time_created: timeStamp
                        }).then(function () {
                            resolve({ success: true });
                        }).catch(function (err) {
                        });
                    });
                    localStorage.setItem('business_created', timeStamp);
                } //endif
                localStorage.setItem('businessName', newname);
                resolve({ success: true });
            }).catch(function (err) {
                reject(err);
            });
        });
        //update cat
        new Promise(function (resolve, reject) {
            _this.fireBusiness.child(_this.userId).update({
                bcats: bcats
            }).then(function () {
                localStorage.setItem('bcats', bcats);
            }).catch(function (err) {
            });
        });
        return promise;
    }; //end function
    UserProvider.prototype.checkIfBusinessIsPaid = function (businessId) {
        var _this = this;
        var promise_cibp = new Promise(function (resolve, reject) {
            _this.fireBusiness.child(businessId).once('value', function (snapshot) {
                resolve(snapshot.val());
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise_cibp;
    };
    UserProvider.prototype.getBusinessDetails = function () {
        var _this = this;
        console.log("Calling Service getBusinessDetails()<-------------");
        var userID = localStorage.getItem('userUID');
        var userName = localStorage.getItem('userName');
        if (userID != undefined || userID != null) {
            this.userId = userID;
            this.userName = userName;
        }
        else {
            this.userId = this.afireAuth.auth.currentUser.uid;
            this.userName = this.afireAuth.auth.currentUser.displayName;
        }
        var promise = new Promise(function (resolve, reject) {
            _this.fireBusiness.child(_this.userId).once('value', function (snapshot) {
                var res = snapshot.val();
                console.log("res");
                console.log(res);
                if (res) {
                    console.log("GOOODA DAY");
                    if (Object.keys(res).length > 0) {
                        localStorage.setItem('business_created', res['timestamp_user_local_time_created']);
                        localStorage.setItem('businessName', res['displayName']);
                        localStorage.setItem('bcats', res['bcats']);
                        console.log("PAID FLAG FETCHED");
                        console.log(res['paid']);
                        if (res['paid']) {
                            localStorage.setItem('paid', "1");
                        }
                        else {
                            localStorage.setItem('paid', "-1");
                        }
                    }
                    else {
                        localStorage.setItem('paid', "-1");
                    }
                } //endif 
                resolve(res);
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    }; //end function
    UserProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__["a" /* AngularFireAuth */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["i" /* Events */]])
    ], UserProvider);
    return UserProvider;
}());

//# sourceMappingURL=user.js.map

/***/ }),

/***/ 934:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_status_bar__ = __webpack_require__(492);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_splash_screen__ = __webpack_require__(493);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_fcm_fcm__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_user_user__ = __webpack_require__(70);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// Ionic native


// Ionic-angular

// Providers


var MyApp = (function () {
    function MyApp(events, fcm, platform, statusBar, ionicApp, splashScreen, userService) {
        var _this = this;
        this.events = events;
        this.fcm = fcm;
        this.platform = platform;
        this.statusBar = statusBar;
        this.ionicApp = ionicApp;
        this.splashScreen = splashScreen;
        this.userService = userService;
        this.alertShown = false;
        // Okay, so the platform is ready and our plugins are available.
        // Here you can do any higher level native things you might need.
        this.platform.ready().then(function (readySource) {
            // used for an example of ngFor and navigation
            _this.pages = [
                { title: 'Home', component: "TabsPage" },
                { title: 'Settings', component: "SettingPage" },
                { title: 'Business Settings', component: "RegisterbusinessPage" },
                { title: 'Create new group', component: "GroupbuddiesPage" },
                { title: 'Your Earnings', component: "EarningsPage" },
            ];
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
            _this.statusBar.overlaysWebView(false);
            // Listen to incoming messages .
            //jaswinder disabled for now because of error
            /*
            this.fcm.listenToNotifications().subscribe(data => {
              if (data.wasTapped) {
                //Notification was received on device tray and tapped by the user.
                this.navCtrl.setRoot('ChatsPage');
              } else {
                //Notification was received in foreground. Maybe the user needs to be notified.
              }
            },(err) => {console.log("error in fcm")});
            */
            _this.events.subscribe('checkUserStatus', function () {
                _this.onResumeSubscription = platform.resume.subscribe(function () {
                    _this.fcm.setstatusUser().then(function (data) {
                    }).catch(function (error) {
                    });
                }, function (err) { console.log(err); });
                _this.onPauseSubscription = platform.pause.subscribe(function () {
                    _this.fcm.setStatusOffline().then(function (data) {
                    }).catch(function (error) {
                    });
                }, function (err) { console.log(err); });
            });
            var userID = localStorage.getItem('userUID');
            var username = localStorage.getItem("userName");
            if (userID != undefined) {
                if (username != undefined || username != null || username != " ") {
                    _this.userService.getbuddydetails(userID).then(function (res) {
                        if (res["displayName"]) {
                            _this.rootPage = "TabsPage"; //TabsPage RegisterbusinessPage
                        }
                        else {
                            _this.rootPage = "DisplaynamePage"; //DisplaynamePage
                        }
                    });
                }
            }
            else {
                _this.rootPage = 'TutorialPage'; //PhonePage
            }
        });
    }
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        if (page.component == "TabsPage") {
            this.nav.setRoot(page.component);
        }
        else {
            this.nav.push(page.component);
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('myNav'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["u" /* NavController */])
    ], MyApp.prototype, "navCtrl", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["t" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["t" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/app/app.html"*/'<ion-menu [content]="content">\n  <ion-header>\n    <ion-toolbar>\n      <ion-title>TapAlly</ion-title>\n    </ion-toolbar>\n  </ion-header>\n  <ion-content>\n    <ion-list>\n      <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n        {{p.title}}\n      </button>\n    </ion-list>\n  </ion-content> \n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_4__providers_fcm_fcm__["a" /* FcmProvider */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["x" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["m" /* IonicApp */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_5__providers_user_user__["a" /* UserProvider */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 940:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GpmessageProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var GpmessageProvider = (function () {
    function GpmessageProvider(http) {
        this.http = http;
    }
    GpmessageProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClient */]])
    ], GpmessageProvider);
    return GpmessageProvider;
}());

//# sourceMappingURL=gpmessage.js.map

/***/ })

},[506]);
//# sourceMappingURL=main.js.map