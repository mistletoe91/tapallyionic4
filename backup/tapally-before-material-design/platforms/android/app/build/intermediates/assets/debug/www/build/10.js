webpackJsonp([10],{

/***/ 1070:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SendReferralForwardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SendReferralForwardPage = (function () {
    function SendReferralForwardPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.buddy_displayName = navParams.get('buddy_displayName');
        this.buddy_uid = navParams.get('buddy_uid');
    }
    SendReferralForwardPage.prototype.forwardContact = function () {
        this.navCtrl.push("PickContactToPage", { friend_to_forward_uid: this.buddy_uid, friend_to_forward_displayName: this.buddy_displayName, friend_to_forward_catId: "", friend_to_forward_catName: "" });
    };
    SendReferralForwardPage.prototype.ionViewDidLoad = function () {
    };
    SendReferralForwardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-send-referral-forward',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/send-referral-forward/send-referral-forward.html"*/'<ion-header>\n  <ion-navbar color="primary" >\n    <ion-title>\n      Send Referral\n    </ion-title>\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name=\'menu\'></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n\n  <div class="pull-center givepadding">\n      <h3>Forward {{buddy_displayName}}\'s referral to someone you know</h3>\n      <p>This will help {{buddy_displayName}} connect with your known contact \n      </p>\n<br />\n        <button ion-button  (click)="forwardContact()" color="primary" block>Forward Contact</button>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/send-referral-forward/send-referral-forward.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */]])
    ], SendReferralForwardPage);
    return SendReferralForwardPage;
}());

//# sourceMappingURL=send-referral-forward.js.map

/***/ }),

/***/ 999:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SendReferralForwardPageModule", function() { return SendReferralForwardPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__send_referral_forward__ = __webpack_require__(1070);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SendReferralForwardPageModule = (function () {
    function SendReferralForwardPageModule() {
    }
    SendReferralForwardPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__send_referral_forward__["a" /* SendReferralForwardPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__send_referral_forward__["a" /* SendReferralForwardPage */]),
            ],
        })
    ], SendReferralForwardPageModule);
    return SendReferralForwardPageModule;
}());

//# sourceMappingURL=send-referral-forward.module.js.map

/***/ })

});
//# sourceMappingURL=10.js.map