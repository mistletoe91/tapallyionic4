webpackJsonp([60],{

/***/ 1024:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BuddychatPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_chat_chat__ = __webpack_require__(497);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_imagehandler_imagehandler__ = __webpack_require__(499);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_loading_loading__ = __webpack_require__(240);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_fcm_fcm__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_user_user__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_storage__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_requests_requests__ = __webpack_require__(495);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_contact_contact__ = __webpack_require__(498);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ionic_img_viewer__ = __webpack_require__(501);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var BuddychatPage = (function () {
    function BuddychatPage(navCtrl, navParams, storage, fcm, chatservice, events, zone, loadingCtrl, imgstore, fb, loading, userservice, actionSheetCtrl, eleRef, popoverCtrl, alertCtrl, requestservice, contactProvider, modalCtrl, platform, imageViewerCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.fcm = fcm;
        this.chatservice = chatservice;
        this.events = events;
        this.zone = zone;
        this.loadingCtrl = loadingCtrl;
        this.imgstore = imgstore;
        this.fb = fb;
        this.loading = loading;
        this.userservice = userservice;
        this.actionSheetCtrl = actionSheetCtrl;
        this.eleRef = eleRef;
        this.popoverCtrl = popoverCtrl;
        this.alertCtrl = alertCtrl;
        this.requestservice = requestservice;
        this.contactProvider = contactProvider;
        this.modalCtrl = modalCtrl;
        this.platform = platform;
        this.imageViewerCtrl = imageViewerCtrl;
        this.newmessage = '';
        this.allmessages = [];
        this.filterallmessages = [];
        this.tempFilterallmessages = [];
        this.isData = false;
        this.datacounter = 0;
        this.headercopyicon = true;
        this.selectAllMessage = [];
        this.selectCounter = 0;
        this.isuserBlock = false;
        this.msgstatus = false;
        this.backbuttonstatus = false;
        this.referralRequestPrefix = 'Referral Request';
        this.referralRequestPostfix = '';
        this._imageViewerCtrl = imageViewerCtrl;
        this.onResumeSubscription = platform.resume.subscribe(function () {
            _this.chatservice.buddymessageRead();
        });
        this.UnreadMSG = +localStorage.getItem('UnreadCount');
        this.authForm = this.fb.group({
            'message': [null, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].required])]
        });
        this.message = this.authForm.controls['message'];
        this.buddy = this.chatservice.buddy; // buddy is receipient of chat
        events.subscribe('isblock-user', function () {
            zone.run(function () {
                _this.isuserBlock = _this.userservice.isuserBlock;
            });
        });
        this.userservice.getstatusblockuser(this.buddy);
        this.userservice.getuserdetails().then(function (res) {
            _this.photoURL = res['photoURL'];
        });
        this.events.subscribe('newmessage', function () {
            _this.allmessages = [];
            _this.imgornot = [];
            _this.zone.run(function () {
                //this.allmessages = this.chatservice.buddymessages;
                var tempData = _this.chatservice.buddymessages;
                _this.todaydate = _this.formatDate(new Date());
                var allData = [{ date: _this.todaydate, messages: [] }];
                if (tempData) {
                    var _loop_1 = function (i) {
                        tempData[i].selection = false;
                        if (_this.todaydate == tempData[i].dateofmsg) {
                            allData[0].messages.push(tempData[i]);
                        }
                        else {
                            var singledata_1 = tempData[i].dateofmsg;
                            var validater = allData.filter(function (task) { return task.date == singledata_1; });
                            if (validater.length) {
                            }
                            else {
                                allData.push({ date: singledata_1, messages: [] });
                                for (var q = 0; q < tempData.length; q++) {
                                    if (singledata_1 == tempData[q].dateofmsg) {
                                        for (var j = 0; j < allData.length; j++) {
                                            if (allData[j].date == singledata_1) {
                                                allData[j].messages.push(tempData[q]);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    };
                    for (var i = 0; i < tempData.length; i++) {
                        _loop_1(i);
                    }
                    _this.datacounter = 10;
                    allData.sort(function (a, b) {
                        var nameA = a.date; // ignore upper and lowercase
                        var nameB = b.date; // ignore upper and lowercase
                        if (nameA < nameB) {
                            return -1;
                        }
                        if (nameA > nameB) {
                            return 1;
                        }
                        // names must be equal
                        return 0;
                    });
                    _this.allmessages = _this.sortdata(allData);
                    _this.storage.set('allBuddyChats', _this.allmessages);
                    if (_this.UnreadMSG == 0) {
                        _this.getAllMessage(_this.datacounter);
                    }
                    else {
                        if (_this.UnreadMSG != 10 && _this.UnreadMSG < 10) {
                            var x = 10 - _this.UnreadMSG;
                            var data = _this.UnreadMSG + x;
                            _this.getAllMessage(data);
                            if (_this.UnreadMSG < 5) {
                            }
                        }
                        else {
                            _this.getAllMessage(_this.UnreadMSG + 3);
                        }
                    }
                    if (_this.UnreadMSG == 0) {
                    }
                    else {
                    }
                }
                // for(let k =0; k <this.allmessages.length;k++){
                //   for (var key in this.allmessages[k].messages) {
                //     if (this.allmessages[k].messages[key].message.indexOf('https://firebasestorage.googleapis.com/') != '-1' ){
                //       this.imgornot.push(true);
                //     }else{
                //       this.imgornot.push(false);
                //     }
                //   }
                // }
            });
        });
        this.events.subscribe('onlieStatus', function () {
            _this.zone.run(function () {
                _this.buddyStatus = _this.chatservice.buddyStatus;
            });
        });
    }
    BuddychatPage.prototype.viewBuddy = function (name, mobile, disc, img, status) {
        this.navCtrl.push("ViewBuddyPage", { img: img, name: name, mobile: mobile, disc: disc, userstatus: status });
    };
    BuddychatPage.prototype.referralSendForward = function (myEvent) {
        // I want to send buddy's referral to someone else
        this.navCtrl.push("SendReferralForwardPage", { buddy_uid: this.buddy.uid, buddy_displayName: this.buddy.displayName });
    };
    BuddychatPage.prototype.referralSendBackward = function (myEvent) {
        this.navCtrl.push("SendReferralBackwardPage", { buddy_uid: this.buddy.uid, buddy_displayName: this.buddy.displayName });
    };
    BuddychatPage.prototype.sendReferral = function (friend_asking_referral, catid) {
        this.navCtrl.setRoot("FriendAskingReferralPage", { friend_asking_referral_uid: friend_asking_referral.uid, friend_asking_referral_displayName: friend_asking_referral.displayName, catid: catid });
    };
    BuddychatPage.prototype.formatDate = function (date) {
        var dd = date.getDate();
        var mm = date.getMonth() + 1;
        var yyyy = date.getFullYear();
        if (dd < 10)
            dd = '0' + dd;
        if (mm < 10)
            mm = '0' + mm;
        return dd + '/' + mm + '/' + yyyy;
    };
    BuddychatPage.prototype.ionViewDidEnter = function () {
        this.chatservice.getbuddymessages();
        this.chatservice.getbuddyStatus();
    };
    BuddychatPage.prototype.addmessage = function () {
        var _this = this;
        this.closeButtonClick();
        this.UnreadMSG = 0;
        this.selectAllMessage = [];
        this.selectCounter = 0;
        this.headercopyicon = true;
        for (var i = 0; i < this.filterallmessages.length; i++) {
            for (var j = 0; j < this.filterallmessages[i].messages.length; j++) {
                this.filterallmessages[i].messages[j].selection = false;
            }
        }
        /*
        console.log(this.buddy);
        OUTPUT>>>>>>
            deviceToken: ""
            disc: ""
            displayName: "Aman 416pages"
            isBlock: undefined
            isUser: "1"
            mobile: "4165775128"
            photoURL: "https://fireba4c2f"
            status: "friend"
            uid: "n1nery5tQJOraAsvdt1ZmPHzW1A3"
            useremail: "Aman@416pages.ca"
            __proto__: Object
        */
        this.userservice.getstatus(this.buddy).then(function (res) {
            if (!res) {
                _this.newmessage = _this.newmessage.trim();
                if (_this.newmessage != '') {
                    if (_this.buddyStatus != 'online') {
                        _this.userservice.getstatusblock(_this.buddy).then(function (res) {
                            if (res == false) {
                                var newMessage = 'You have received a message';
                                _this.fcm.sendNotification(_this.buddy, newMessage, 'chatpage');
                            }
                        });
                    }
                    _this.chatservice.addnewmessage(_this.newmessage, 'message', _this.buddyStatus).then(function () {
                        console.log("Lets add message");
                        _this.newmessage = '';
                        _this.scrollto();
                        _this.msgstatus = false;
                    });
                }
                else {
                    _this.loading.presentToast('Please enter valid message...');
                }
            }
            else {
                var alert_1 = _this.alertCtrl.create({
                    message: 'Unblock ' + _this.buddy.displayName + ' to send a message.',
                    buttons: [
                        {
                            text: 'CANCEL',
                            role: 'cancel',
                            handler: function () {
                            }
                        },
                        {
                            text: 'UNBLOCK',
                            handler: function () {
                                _this.userservice.unblockUser(_this.buddy).then(function (res) {
                                    if (res) {
                                        _this.addmessage();
                                    }
                                });
                            }
                        }
                    ]
                });
                alert_1.present();
            }
        });
    };
    BuddychatPage.prototype.scrollto = function () {
        var _this = this;
        setTimeout(function () {
            if (_this.content._scroll) {
                _this.content.scrollToBottom();
            }
        }, 1000);
    };
    BuddychatPage.prototype.sendPicMsg = function () {
        var _this = this;
        this.UnreadMSG = 0;
        this.userservice.getstatus(this.buddy).then(function (res) {
            if (!res) {
                _this.loading.presentLoading();
                _this.imgstore.picmsgstore().then(function (imgurl) {
                    _this.chatservice.addnewmessage(imgurl, 'image', _this.buddyStatus).then(function () {
                        _this.loading.dismissMyLoading();
                        if (_this.buddyStatus != 'online') {
                            var newMessage = 'You have received a message';
                            _this.fcm.sendNotification(_this.buddy, newMessage, 'chatpage');
                        }
                        _this.scrollto();
                        _this.newmessage = '';
                    });
                }).catch(function (err) {
                    _this.loading.dismissMyLoading();
                    alert(err);
                });
            }
            else {
                var alert_2 = _this.alertCtrl.create({
                    message: 'Unblock ' + _this.buddy.displayName + ' to send a message.',
                    buttons: [
                        {
                            text: 'CANCEL',
                            role: 'cancel',
                            handler: function () {
                            }
                        },
                        {
                            text: 'UNBLOCK',
                            handler: function () {
                                _this.userservice.unblockUser(_this.buddy).then(function (res) {
                                    if (res) {
                                        _this.sendPicMsg();
                                    }
                                });
                            }
                        }
                    ]
                });
                alert_2.present();
            }
        });
    };
    BuddychatPage.prototype.doRefresh = function (refresher) {
        this.UnreadMSG = 0;
        this.datacounter += 10;
        this.getAllMessage(this.datacounter);
        this.closeButtonClick();
        setTimeout(function () {
            refresher.complete();
        }, 500);
    };
    BuddychatPage.prototype.getAllMessage = function (key) {
        var messagesAll = [];
        var counter = 0;
        // for(let i = 0; i<key;i++){
        for (var j = this.allmessages.length; j > 0; j--) {
            if (counter < key) {
                var msg = [];
                for (var k = this.allmessages[j - 1].messages.length; k > 0; k--) {
                    if (counter < key) {
                        msg.push(this.allmessages[j - 1].messages[k - 1]);
                    }
                    else {
                        break;
                    }
                    counter++;
                }
                messagesAll.push({ date: this.allmessages[j - 1].date, messages: msg });
            }
            else {
                break;
            }
        }
        var filterallmessages = [];
        var sortDate = this.sortdata(messagesAll);
        var _loop_2 = function (i) {
            var tempmsg = [];
            for (var j = sortDate[i].messages.length; j > 0; j--) {
                tempmsg.push(sortDate[i].messages[j - 1]);
            }
            this_1.zone.run(function () {
                filterallmessages.push({ date: sortDate[i].date, messages: tempmsg });
            });
        };
        var this_1 = this;
        for (var i = 0; i < sortDate.length; i++) {
            _loop_2(i);
        }
        filterallmessages.sort(function (a, b) {
            var nameA = a.date; // ignore upper and lowercase
            var nameB = b.date; // ignore upper and lowercase
            if (nameA < nameB) {
                return -1;
            }
            if (nameA > nameB) {
                return 1;
            }
            // names must be equal
            return 0;
        });
        this.filterallmessages = filterallmessages;
        // this.filterallmessages = [];
        // for (let i = filterallmessages.length - 1; i >= 0; i--) {
        //   this.filterallmessages.push(filterallmessages[i]);
        // }
        // this.filterallmessages = this.sortdata(this.filterallmessages);
        this.scrollto();
        // this.filterallmessages = this.sortdata(messagesAll);
        //   let tmpmessage = [];
        //   this.filterallmessages.forEach(element => {
        //     if (element.messages.length > 3) {
        //       let imgcter=0
        //       let imgcontainer=[];
        //       element.messages.forEach(ele => {
        //         if(ele.type == 'image'){
        //           imgcter++;
        //           imgcontainer.push(ele)
        //             if(imgcter>4){
        //             }
        //         }else{
        //           tmpmessage.push(ele);
        //         }
        //       });
        //     }
        //     tmpmessage.push(element);
        //   });
    };
    BuddychatPage.prototype.sortdata = function (data) {
        return data.sort(function (a, b) {
            var keyA = new Date(a.date), keyB = new Date(b.date);
            // Compare the 2 dates
            if (keyA < keyB)
                return -1;
            if (keyA > keyB)
                return 1;
            return 0;
        });
    };
    BuddychatPage.prototype.backButtonClick = function () {
        this.navCtrl.pop();
    };
    BuddychatPage.prototype.closeButtonClick = function () {
        this.selectAllMessage = [];
        this.selectCounter = 0;
        this.headercopyicon = true;
        this.backbuttonstatus = false;
        for (var i = 0; i < this.filterallmessages.length; i++) {
            for (var j = 0; j < this.filterallmessages[i].messages.length; j++) {
                this.filterallmessages[i].messages[j].selection = false;
            }
        }
    };
    BuddychatPage.prototype.popoverdialog = function (item, event) {
        var flag = 0;
        if (this.selectAllMessage.length != 0) {
            for (var i = 0; i < this.selectAllMessage.length; i++) {
                if (item.messageId == this.selectAllMessage[i].messageId) {
                    flag = 1;
                    //event.target.classList.remove('select-item');
                    item.selection = false;
                    this.selectAllMessage.splice(i, 1);
                    break;
                }
                else {
                }
            }
            if (flag == 1) {
            }
            else {
                this.popoveranothermsg(item, event);
            }
        }
        else {
            item.selection = true;
            this.selectAllMessage.push(item);
            if (this.headercopyicon == true) {
                this.headercopyicon = false;
                this.backbuttonstatus = true;
            }
            else {
                this.headercopyicon = true;
                this.backbuttonstatus = false;
            }
        }
        this.selectCounter = this.selectAllMessage.length;
        if (this.selectAllMessage.length == 0) {
            this.headercopyicon = true;
            this.backbuttonstatus = false;
        }
    };
    BuddychatPage.prototype.popoveranothermsg = function (item, event) {
        var flag = 0;
        if (this.selectAllMessage.length >= 1) {
            for (var i = 0; i < this.selectAllMessage.length; i++) {
                if (item.messageId == this.selectAllMessage[i].messageId) {
                    item.selection = false;
                    flag = 1;
                    this.selectAllMessage.splice(i, 1);
                    break;
                }
                else {
                    flag = 0;
                }
            }
            if (flag == 1) {
            }
            else {
                item.selection = true;
                this.selectAllMessage.push(item);
            }
        }
        this.selectCounter = this.selectAllMessage.length;
        if (this.selectAllMessage.length == 0) {
            this.headercopyicon = true;
            this.backbuttonstatus = false;
        }
    };
    BuddychatPage.prototype.deleteMessage = function () {
        var _this = this;
        if (this.selectAllMessage.length > 1) {
            this.DeleteMsg = "Do you want to delete these messages?";
        }
        else {
            this.DeleteMsg = "Do you want to delete this message?";
        }
        this.loading.presentLoading();
        var alert = this.alertCtrl.create({
            title: 'Delete Message',
            message: this.DeleteMsg,
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: function () {
                        _this.closeButtonClick();
                        _this.loading.dismissMyLoading();
                    }
                },
                {
                    text: 'Yes',
                    handler: function () {
                        if (_this.selectAllMessage.length != 0) {
                            _this.chatservice.deleteMessages(_this.selectAllMessage).then(function (res) {
                                if (res) {
                                    _this.selectAllMessage = [];
                                    if (_this.selectAllMessage.length == 0) {
                                        _this.headercopyicon = true;
                                        _this.backbuttonstatus = false;
                                        _this.loading.dismissMyLoading();
                                    }
                                }
                            }).catch(function (err) {
                            });
                        }
                    }
                }
            ]
        });
        alert.present().then(function () {
        });
    };
    BuddychatPage.prototype.viewPopover = function (myEvent) {
        var _this = this;
        var popover = this.popoverCtrl.create('MenuBlockPage', { buddy: this.buddy });
        popover.present({ ev: myEvent });
        popover.onDidDismiss(function (data) {
            _this.userservice.getstatusblockuser(_this.buddy);
        });
    };
    BuddychatPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.requestservice.getmyrequests();
        this.requestservice.getmyfriends();
        this.myfriends = [];
        this.events.subscribe('friends', function () {
            _this.myfriends = [];
            _this.myfriends = _this.requestservice.myfriends;
            if (_this.myfriends.length > 0) {
                _this.loading.dismissMyLoading();
                _this.isData = false;
                _this.contactProvider.getSimJsonContacts().then(function (res) {
                    if (res['contacts'].length > 0) {
                        for (var i = 0; i < res['contacts'].length; ++i) {
                            for (var j = 0; j < _this.myfriends.length; ++j) {
                                if (res['contacts'][i].mobile == _this.myfriends[j].mobile) {
                                    _this.myfriends[j].displayName = res['contacts'][i].displayName;
                                }
                            }
                        }
                        _this.zone.run(function () {
                            _this.myfriendsList = _this.myfriends;
                        });
                        _this.tempmyfriendsList = _this.myfriendsList;
                    }
                    else {
                        _this.zone.run(function () {
                            _this.myfriendsList = _this.myfriends;
                        });
                        _this.tempmyfriendsList = _this.myfriendsList;
                    }
                }).catch(function (err) {
                    _this.loading.dismissMyLoading();
                });
            }
            else {
                _this.loading.dismissMyLoading();
                _this.isData = true;
            }
        });
    };
    BuddychatPage.prototype.openAttachment = function (myEvent) {
        var _this = this;
        var popover = this.popoverCtrl.create('AttachmentsPage', { buddy: this.buddy });
        popover.present({
            ev: myEvent
        });
        popover.onDidDismiss(function (pages) {
            if (pages != 0 || pages != undefined || pages != null) {
                _this.navCtrl.push(pages.page, { buddy: pages.buddy, flag: pages.flag });
            }
            else {
            }
        });
    };
    BuddychatPage.prototype.presentImage = function (myImage) {
        if (this.selectAllMessage.length == 0) {
            var imageViewer = this._imageViewerCtrl.create(myImage);
            imageViewer.present();
            //imageViewer.onDidDismiss(() => ));
        }
    };
    BuddychatPage.prototype.openAllImg = function (items, $event) {
        if (this.selectAllMessage.length == 0) {
            this.navCtrl.push("AllimgPage", { allimg: items });
        }
    };
    BuddychatPage.prototype.openDoc = function (path) {
        if (this.selectAllMessage.length == 0) {
            this.imgstore.openDocument(path).then(function (res) {
            }).catch(function (err) {
            });
        }
    };
    BuddychatPage.prototype.openContacts = function (contacts) {
        if (this.selectAllMessage.length == 0) {
            this.navCtrl.push("ShowContactPage", { contacts: contacts });
        }
    };
    BuddychatPage.prototype.showLocation = function (location) {
        if (this.selectAllMessage.length == 0) {
            this.navCtrl.push("MapPage", { location: location });
        }
    };
    BuddychatPage.prototype.onChange = function (e) {
        if (this.newmessage.trim()) {
            this.msgstatus = true;
        }
        else {
            this.msgstatus = false;
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('content'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Content */])
    ], BuddychatPage.prototype, "content", void 0);
    BuddychatPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-buddychat',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/buddychat/buddychat.html"*/'<ion-header>\n    <ion-navbar hideBackButton="{{backbuttonstatus}}" >\n        <div class="header-wrap" *ngIf="headercopyicon">\n            <div class="flex-start">\n                <!-- <ion-buttons class="left-arrow">\n                    <button ion-button icon-only (click)="backButtonClick()">\n                        <ion-icon ios="md-arrow-round-back" md="md-arrow-round-back"></ion-icon>\n                    </button>\n                </ion-buttons> -->\n                <div class="title-wrap" (click)="viewBuddy(buddy.displayName,buddy.mobile,buddy.disc,buddy.photoURL,isuserBlock)" text-left left>\n                   <ion-avatar class="user-img" left>\n                        <img src="{{buddy.photoURL}}" *ngIf="isuserBlock == false">\n                        <img src="assets/imgs/user.png" *ngIf="isuserBlock == true">\n                    </ion-avatar>\n\n                    <div class="title-uname">\n                        <h5>{{buddy.displayName}}</h5>\n                        <div class="font" *ngIf="isuserBlock == false">{{buddyStatus}}</div>\n                    </div>\n                </div>\n            </div>\n            <div class="flex-end">\n                <ion-buttons end>\n                    <button ion-button icon-only (click)="viewPopover($event)">\n                        <ion-icon ios="md-more" md="md-more"></ion-icon>\n                    </button>\n                </ion-buttons>\n            </div>\n        </div>\n        <div class="select-wrap" *ngIf="!headercopyicon">\n            <div class="flex-start">\n                <ion-buttons start class="left-arrow">\n                    <button start ion-button icon-only (click)="closeButtonClick()">\n                    <ion-icon ios="md-close" md="md-close"></ion-icon>\n                    </button>\n                </ion-buttons>\n                <div class="select-title" text-left left *ngIf="selectCounter > 0">\n                {{selectCounter}}\n                </div>\n            </div>\n            <div class="flex-end">\n                <ion-buttons end>\n                    <button ion-button icon-only (click)="deleteMessage()">\n                        <ion-icon ios="ios-trash" md="md-trash"></ion-icon>\n                    </button>\n                    <!-- <button ion-button icon-only (click)="starredMessage()">\n                        <ion-icon ios="ios-star" md="md-star"></ion-icon>\n                    </button> -->\n                </ion-buttons>\n            </div>\n        </div>\n    </ion-navbar>\n</ion-header>\n<ion-content #content>\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="Pull to refresh" refreshingSpinner="circles"\n            refreshingText="Refreshing...">\n        </ion-refresher-content>\n    </ion-refresher>\n    <div class="chatwindow">\n        <ion-list no-lines>\n            <ng-container *ngFor="let items of filterallmessages; let j = index">\n                <ion-list-header *ngIf="items.date == todaydate && items.messages.length > 0" align="center">Today</ion-list-header>\n                <ion-list-header *ngIf="items.date != todaydate" align="center">{{items.date}}</ion-list-header>\n                <div *ngFor="let item of items.messages ; let i = index; ">\n                    <div id="unreadID" style="text-align: center;" *ngIf="i == items.messages.length - UnreadMSG">\n                        <p>-------\n                            <ion-badge> Unread </ion-badge>\n                        -------</p>\n                    </div>\n                    <ion-item text-wrap>\n                        <div [ngClass]="{ \'select-item\': item.selection }" (press)="popoverdialog(item,$event)" (click)="popoveranothermsg(item,$event)">\n\n                            <div class="msg-wrap you" *ngIf="item.sentby === buddy.uid">\n\n                                <ion-avatar *ngIf="isuserBlock == false">\n                                    <img src="{{buddy.photoURL}}">\n                                </ion-avatar>\n                                <ion-avatar *ngIf="isuserBlock == true">\n                                    <img src="assets/imgs/user.png">\n                                </ion-avatar>\n\n                                <div [ngClass]="item.type == \'image\' || item.type == \'location\' || item.type == \'document\' ?\'bubble-img\':\'bubble\'">\n\n                                    <div class="msg" *ngIf="item.type == \'message\' && item.selectCatId && item.selectCatId>0"><p class="referraltext">{{referralRequestPrefix}}</p><ion-icon  class="referralicon"  name="arrow-dropright-circle"></ion-icon>\n                                    <div>{{item.message}}</div>\n                                    </div>\n\n                                    <div class="msg" *ngIf="item.type == \'message\' && (!item.selectCatId || item.selectCatId<=0)">{{item.message}}</div>\n\n                                    <img class="msg" *ngIf="item.type == \'document\'" src="assets/imgs/docs.png" (click)="openDoc(item.message)" />\n                                    <audio controls class="msg" *ngIf="item.type == \'audio\'">\n                                        <source src="{{item.message}}" type="audio/mpeg">\n                                    </audio>\n                                    <img class="msg" *ngIf="item.type == \'location\'" src="assets/imgs/location.png"\n                                        (click)="showLocation(item.message)" />\n                                    <!-- <div class="msg" *ngIf="item.type == \'audio\'" (click)="playAudio(item.message)">Audio</div> -->\n                                    <ng-container class="msg" *ngIf="item.multiMessage && item.type==\'image\'">\n                                        <div (click)="openAllImg(item.message,$event)" class="selection-data">\n                                            <img class="msg-partision" *ngFor="let pic of item.message | slice:0:4; let ii = index "\n                                                src="{{pic}}" />\n                                            <span class="img-number overlay">+ {{item.message.length - 4}}</span>\n                                        </div>\n                                    </ng-container>\n\n                                    <ng-container class="msg" *ngIf="!item.multiMessage && item.type == \'image\'">\n                                        <img class="msg" *ngIf="item.type == \'image\'" #mychatImage src="{{item.message}}"\n                                            (click)="presentImage(mychatImage)" />\n                                    </ng-container>\n\n                                    <ng-container class="msg" *ngIf="item.multiMessage && item.type == \'contact\'" >\n                                        <img class="msg contact" src="assets/imgs/user.png" />\n                                        <div *ngIf="item.message.length == 1"(click)="openContacts(item.message)" class="view-msg">View {{ item.message.length }} Contact</div>\n                                        <div *ngIf="item.message.length > 1" (click)="openContacts(item.message)" class="view-msg">View {{ item.message.length }} Contacts</div>\n                                    </ng-container>\n\n                                    <div class="read-recipients">\n                                            <ion-icon *ngIf="item.isStarred" name="star" class="icon"></ion-icon>\n                                            <p class="icon">{{item.timeofmsg}}</p>\n                                    </div>\n\n                                </div>\n                            </div>\n                            <div class="msg-wrap me" *ngIf="item.sentby != buddy.uid">\n                                <ion-avatar>\n                                    <img src="{{photoURL}}">\n                                </ion-avatar>\n                                <div [ngClass]="item.type == \'image\' || item.type == \'location\' || item.type == \'document\' ?\'bubble-img\':\'bubble\'">\n\n                                    <div class="msg" *ngIf="item.type == \'message\' && item.selectCatId && item.selectCatId>0"><p class="referraltext">{{referralRequestPrefix}}</p><ion-icon  class="referralicon" (click)="sendReferral (buddy, item.selectCatId)"  name="arrow-dropright-circle"></ion-icon>\n                                    <div>{{item.message}}</div>\n                                    </div>\n\n                                    <div class="msg" *ngIf="item.type == \'message\' && (!item.selectCatId || item.selectCatId<=0)">{{item.message}}</div>\n\n                                    <img class="msg" *ngIf="item.type == \'document\'" src="assets/imgs/docs.png" (click)="openDoc(item.message)" />\n                                    <img class="msg" *ngIf="item.type == \'location\'" src="assets/imgs/location.png"\n                                        (click)="showLocation(item.message)" />\n\n                                    <audio controls class="msg" *ngIf="item.type == \'audio\'">\n                                        <source src="{{item.message}}" type="audio/mpeg">\n                                    </audio>\n                                    <!-- <div class="msg" *ngIf="item.type == \'audio\'" (click)="playAudio(item.message)">Audio</div> -->\n                                    <ng-container class="msg" *ngIf="item.multiMessage && item.type==\'image\'" >\n                                        <div (click)="openAllImg(item.message)" class="selection-data">\n                                            <img class="msg-partision" *ngFor="let pic of item.message | slice:0:4; let ii = index "\n                                                src="{{pic}}" />\n                                            <span class="img-number overlay">+ {{item.message.length - 4}}</span>\n                                        </div>\n                                    </ng-container>\n                                    <ng-container class="msg" *ngIf="!item.multiMessage ">\n                                        <img class="msg" *ngIf="item.type == \'image\'" #mychatImage src="{{item.message}}"\n                                            (click)="presentImage(mychatImage)" />\n                                    </ng-container>\n                                    <ng-container class="msg" *ngIf="item.multiMessage && item.type == \'contact\'" >\n                                        <img class="msg contact" src="assets/imgs/user.png" />\n                                        <div *ngIf="item.message.length == 1"(click)="openContacts(item.message)" class="view-msg">View {{ item.message.length }} Contact</div>\n                                        <div *ngIf="item.message.length > 1" (click)="openContacts(item.message)" class="view-msg">View {{ item.message.length }} Contacts</div>\n                                    </ng-container>\n\n\n\n                                        <div class="read-recipients">\n                                            <ion-icon *ngIf="item.isStarred" name="star" class="icon"></ion-icon>\n                                            <p class="icon">{{item.timeofmsg}}</p>\n                                            <ion-icon *ngIf="!item.isRead" name="checkmark" class="icon"></ion-icon>\n                                            <!-- <ion-icon *ngIf="!item.isRead" name="checkmark"></ion-icon> -->\n                                            <ion-icon *ngIf="item.isRead" color="secondary" class="checkmark-icon" name="checkmark"></ion-icon>\n                                            <ion-icon *ngIf="item.isRead" color="secondary" class="checkmark-icon icon-align icon-align-ios" name="checkmark"></ion-icon>\n                                        </div>\n\n\n                                    </div>\n\n                            </div>\n                        </div>\n                    </ion-item>\n                </div>\n            </ng-container>\n        </ion-list>\n    </div>\n</ion-content>\n\n<ion-footer ion-fixed>\n  <ion-list style="display:none;">\n    <ion-item>\n      <button ion-button clear item-start> <ion-icon class="referralicon"  name="arrow-dropleft-circle"></ion-icon> </button>\n      <button  ion-button clear  item-end><ion-icon class="referralicon"  name="arrow-dropright-circle"></ion-icon></button>\n    </ion-item>\n  </ion-list>\n    <form [formGroup]="authForm">\n        <ion-toolbar class="no-border" color="white">\n            <ion-input [(ngModel)]="newmessage" [formControl]="message" (click)="scrollto()" (input)="onChange($event.keyCode)" name="message" class="newmsg" placeholder="Write your message ..."></ion-input>\n            <ion-buttons end *ngIf="selectAllMessage.length == 0">\n                <a ion-button (click)="openAttachment($event)">\n                    <ion-icon class="ion-image" ios="md-attach" md="md-attach"></ion-icon>\n                </a>\n                <button ion-button (click)="referralSendBackward($event)" clear item-start> <ion-icon class="referralicon"  name="arrow-dropleft-circle"></ion-icon> </button>\n                <button  ion-button (click)="referralSendForward($event)" clear  item-end><ion-icon class="referralicon"  name="arrow-dropright-circle"></ion-icon></button>\n            </ion-buttons>\n            <ion-buttons end>\n                <button ion-button round type="submit" class="sentbtn" [disabled]="!this.msgstatus || !selectAllMessage.length == 0"\n                    (click)="addmessage()">\n                    <ion-icon ios="md-send" md="md-send" color="wcolor" style="font-size: 2.2em;"></ion-icon>\n                </button>\n            </ion-buttons>\n        </ion-toolbar>\n    </form>\n</ion-footer>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/buddychat/buddychat.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_6__providers_fcm_fcm__["a" /* FcmProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers_chat_chat__["a" /* ChatProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_imagehandler_imagehandler__["a" /* ImagehandlerProvider */],
            __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_4__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["y" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_9__providers_requests_requests__["a" /* RequestsProvider */],
            __WEBPACK_IMPORTED_MODULE_10__providers_contact_contact__["a" /* ContactProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_11_ionic_img_viewer__["a" /* ImageViewerController */]])
    ], BuddychatPage);
    return BuddychatPage;
}());

//# sourceMappingURL=buddychat.js.map

/***/ }),

/***/ 953:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BuddychatPageModule", function() { return BuddychatPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__buddychat__ = __webpack_require__(1024);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var BuddychatPageModule = (function () {
    function BuddychatPageModule() {
    }
    BuddychatPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__buddychat__["a" /* BuddychatPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__buddychat__["a" /* BuddychatPage */]),
            ],
        })
    ], BuddychatPageModule);
    return BuddychatPageModule;
}());

//# sourceMappingURL=buddychat.module.js.map

/***/ })

});
//# sourceMappingURL=60.js.map