webpackJsonp([48],{

/***/ 1033:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupbuddiesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_requests_requests__ = __webpack_require__(495);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_groups_groups__ = __webpack_require__(496);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var GroupbuddiesPage = (function () {
    function GroupbuddiesPage(navCtrl, navParams, requestservice, events, groupservice, platform) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.requestservice = requestservice;
        this.events = events;
        this.groupservice = groupservice;
        this.platform = platform;
        this.myfriends = [];
        this.groupmembers = [];
        this.tempmyfriends = [];
        this.selectedMember = [];
    }
    GroupbuddiesPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        var myfriends = this.requestservice.myfriends;
        this.removeDuplicates(myfriends, 'mobile').then(function (response) {
            _this.myfriends = [];
            _this.myfriends = response;
        });
        this.tempmyfriends = this.requestservice.myfriends;
    };
    GroupbuddiesPage.prototype.removeDuplicates = function (originalArray, prop) {
        return new Promise(function (resolve) {
            var newArray = [];
            var lookupObject = {};
            for (var i in originalArray) {
                lookupObject[originalArray[i][prop]] = originalArray[i];
            }
            for (i in lookupObject) {
                newArray.push(lookupObject[i]);
            }
            resolve(newArray);
        });
    };
    GroupbuddiesPage.prototype.initializeItems = function () {
        this.myfriends = this.tempmyfriends;
    };
    GroupbuddiesPage.prototype.searchuser = function (ev) {
        this.initializeItems();
        var val = ev.target.value;
        if (val && val.trim() != '') {
            this.myfriends = this.myfriends.filter(function (item) {
                return (item.displayName.toLowerCase().indexOf(val.toLowerCase().trim()) > -1);
            });
        }
    };
    GroupbuddiesPage.prototype.addbuddy = function (buddy) {
        if (this.selectedMember.length == 0) {
            this.selectedMember.push(buddy);
        }
        else {
            var flagofbuddy = false;
            var posionofbuddy = 0;
            for (var i = 0; i < this.selectedMember.length; i++) {
                if (this.selectedMember[i].uid == buddy.uid) {
                    flagofbuddy = true;
                    posionofbuddy = i;
                    break;
                }
                else {
                    flagofbuddy = false;
                }
            }
            if (flagofbuddy == true) {
                this.selectedMember.splice(posionofbuddy, 1);
            }
            else {
                this.selectedMember.push(buddy);
            }
        }
    };
    GroupbuddiesPage.prototype.goBack = function () {
        this.navCtrl.setRoot("ChatsPage");
    };
    GroupbuddiesPage.prototype.goNext = function () {
        this.navCtrl.push("GroupsPage", { buddy: this.selectedMember });
    };
    GroupbuddiesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-groupbuddies',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/groupbuddies/groupbuddies.html"*/'<ion-header>\n  <ion-navbar>\n    <div class="header-wrap">\n      <!-- <ion-buttons class="left-arrow" left>\n        <button ion-button icon-only (click)="goBack()">\n          <ion-icon ios="md-arrow-round-back" md="md-arrow-round-back"></ion-icon>\n        </button>\n      </ion-buttons> -->\n      <div class="header-title">New group<p class="below-header">Add participants</p>\n      </div>\n    </div>\n  </ion-navbar>\n  <ion-searchbar placeholder="Search" class="searchbar" (ionInput)="searchuser($event)">\n  </ion-searchbar>\n  <ng-container *ngIf="selectedMember.length > 0">\n    <ion-list class="group-wrap">\n      <ion-item *ngFor="let item of selectedMember">\n          <ion-avatar class="buddy-pic" item-left>\n            <img *ngIf="item.isBlock == true" src="assets/imgs/user.png">\n            <img *ngIf="item.isBlock == false" src="{{item.photoURL}}">\n          </ion-avatar>\n        <p>{{item.displayName}}</p>\n      </ion-item>\n    </ion-list>\n  </ng-container>\n\n</ion-header>\n\n\n<ion-content padding [ngClass]="selectedMember.length != 0 ?\'group-wrap-active\':\'group-wrap-no-active\'">\n\n  <ion-list>\n    <ng-container *ngFor="let key of myfriends">\n      <ion-item (click)="addbuddy(key)">\n        <ion-avatar item-left>\n          <img *ngIf="key.isBlock == false" src="{{key.photoURL}}">\n          <img *ngIf="key.isBlock == true" src="{{\'assets/imgs/user.png\'}}">\n        </ion-avatar>\n        <h2>{{key.displayName}}</h2>\n      </ion-item>\n    </ng-container>\n  </ion-list>\n  <div *ngIf="myfriends?.length == 0" align="center">\n    No result found!\n  </div>\n\n  <ion-fab bottom right>\n    <button ion-fab [disabled]="selectedMember.length == 0" (click)="goNext()">\n      <ion-icon ios="ios-arrow-round-forward" md="md-arrow-round-forward"></ion-icon>\n    </button>\n  </ion-fab>\n\n</ion-content>'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/groupbuddies/groupbuddies.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_requests_requests__["a" /* RequestsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_3__providers_groups_groups__["a" /* GroupsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */]])
    ], GroupbuddiesPage);
    return GroupbuddiesPage;
}());

//# sourceMappingURL=groupbuddies.js.map

/***/ }),

/***/ 962:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupbuddiesPageModule", function() { return GroupbuddiesPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__groupbuddies__ = __webpack_require__(1033);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var GroupbuddiesPageModule = (function () {
    function GroupbuddiesPageModule() {
    }
    GroupbuddiesPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__groupbuddies__["a" /* GroupbuddiesPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__groupbuddies__["a" /* GroupbuddiesPage */]),
            ],
        })
    ], GroupbuddiesPageModule);
    return GroupbuddiesPageModule;
}());

//# sourceMappingURL=groupbuddies.module.js.map

/***/ })

});
//# sourceMappingURL=48.js.map