webpackJsonp([31],{

/***/ 1051:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PhonePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__ = __webpack_require__(503);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_loading_loading__ = __webpack_require__(240);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_app_angularfireconfig__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var PhonePage = (function () {
    function PhonePage(navCtrl, navParams, authService, loadingCtrl, fb, platform, alertCtrl, loading, http, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.authService = authService;
        this.loadingCtrl = loadingCtrl;
        this.fb = fb;
        this.platform = platform;
        this.alertCtrl = alertCtrl;
        this.loading = loading;
        this.http = http;
        this.modalCtrl = modalCtrl;
        this.phoneNumber = { number: '', type: '' };
        this.isCountrySelect = true;
        this.submitAttempt = false;
        this.countryName = "Select Country";
        this.countryTemp = false;
        this.countryCode = "";
        this.InvalidNumErr = '';
        this.authForm = this.fb.group({
            'number': [null, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].pattern('^[0-9]+$'), __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required])]
        });
        this.number = this.authForm.controls['number'];
        //Default Country :
        //mytodo : Right now its Canada
        this.countryName = 'Canada';
        this.countryCode = '1'; //test
        this.phoneNumber.type = this.countryCode;
        this.countryTemp = true;
    }
    PhonePage.prototype.ionViewDidLoad = function () {
        this.loading.dismissMyLoading();
        localStorage.removeItem("country");
        localStorage.removeItem("code");
    };
    PhonePage.prototype.listCountry = function () {
        var _this = this;
        var countryListModal = this.modalCtrl.create('CountryListPage', { type: this.phoneNumber.type });
        countryListModal.present();
        countryListModal.onDidDismiss(function (data) {
            if (data == undefined) {
                if (JSON.parse(localStorage.getItem("country")) == null) {
                    _this.countryName = "Select Country";
                    _this.countryCode = "";
                }
                else {
                    _this.countryName = JSON.parse(localStorage.getItem("country"));
                    _this.countryCode = JSON.parse(localStorage.getItem("code"));
                    _this.phoneNumber.type = _this.countryCode;
                    _this.countryTemp = true;
                }
            }
            else {
                _this.countryName = data.name;
                _this.phoneNumber.type = data.code;
                _this.countryCode = data.code;
                _this.countryTemp = false;
            }
        });
    };
    PhonePage.prototype.doLoginwithMobile = function (phoneNumber) {
        var _this = this;
        this.submitAttempt = true;
        if (this.phoneNumber.type == '') {
            this.countryTemp = true;
        }
        if (this.authForm.valid && this.phoneNumber.type != '') {
            this.loading.presentLoading();
            var data = {
                "via": "sms",
                "phone_number": phoneNumber.number,
                "country_code": phoneNumber.type,
                "api_key": __WEBPACK_IMPORTED_MODULE_6__app_app_angularfireconfig__["a" /* apiKey */]
            };
            this.authService.sendVerificationCode(data, 'phones/verification/start')
                .then(function (res) {
                _this.loading.dismissMyLoading();
                var data = JSON.parse(res['data']);
                if (res.status == 200) {
                    _this.loading.presentToast(data['message']);
                    _this.navCtrl.setRoot("PhoneverfyPage", { code: phoneNumber.type, number: phoneNumber.number });
                }
                else {
                    _this.loading.presentToast(data['message']);
                }
            }).catch(function (err) {
                _this.loading.dismissMyLoading();
                if (err.status == 400) {
                    _this.InvalidNumErr = "Invalid Mobile Number.";
                }
                else {
                    console.log(err);
                    _this.loading.presentToast('Something went wrong. Please try again.');
                }
            });
        }
    };
    PhonePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-phone',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/phone/phone.html"*/'<ion-content class="bg-img">\n  <div class="welcome-text" text-center> \n    <h1 class="text-white">Welcome</h1>\n    <p class="text-white">Enter your mobile number to get started !!</p>\n  </div>\n  <ion-card>\n    <ion-card-content>\n      <form  [formGroup]="authForm">\n        <ion-list>\n            <ion-item (click)="listCountry()" class="textarea-lbl" >\n              <div>\n                <p>{{countryName}}<span *ngIf="countryCode">{{\' (+\'+countryCode+\')\'}}</span></p>\n              </div>\n            </ion-item>\n            <div *ngIf="countryName == \'Select country\' && countryTemp" class="validation-msg">\n              <p>Please select country.</p>\n            </div>\n            <ion-item>\n                <ion-input type="number" [formControl]="number" id="number" name="number" [(ngModel)]="phoneNumber.number" placeholder="Enter your mobile number"></ion-input>\n            </ion-item>\n            <div *ngIf="number.errors && (number.touched || submitAttempt)" class="validation-msg">\n              <p> Please enter valid mobile number.</p>\n            </div>\n            <div *ngIf="InvalidNumErr" class="validation-msg">\n              <p> {{InvalidNumErr}}</p>\n            </div>\n            <ion-item>\n              <button ion-button class="button-secondary" type="submit" (click)="doLoginwithMobile(phoneNumber)">Submit</button>\n            </ion-item>\n            <ion-item>\n              <div id="re-container"></div>\n            </ion-item>\n        </ion-list>\n      </form>\n    </ion-card-content>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/phone/phone.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["q" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["x" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["b" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["s" /* ModalController */]])
    ], PhonePage);
    return PhonePage;
}());

//# sourceMappingURL=phone.js.map

/***/ }),

/***/ 980:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PhonePageModule", function() { return PhonePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__phone__ = __webpack_require__(1051);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PhonePageModule = (function () {
    function PhonePageModule() {
    }
    PhonePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__phone__["a" /* PhonePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__phone__["a" /* PhonePage */]),
            ],
        })
    ], PhonePageModule);
    return PhonePageModule;
}());

//# sourceMappingURL=phone.module.js.map

/***/ })

});
//# sourceMappingURL=31.js.map