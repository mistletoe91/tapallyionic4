webpackJsonp([33],{

/***/ 1048:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_requests_requests__ = __webpack_require__(495);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_contact_contact__ = __webpack_require__(498);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_loading_loading__ = __webpack_require__(240);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_fcm_fcm__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_user_user__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_groups_groups__ = __webpack_require__(496);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var NotificationPage = (function () {
    function NotificationPage(userservice, platform, fcm, loadingProvider, contactProvider, storage, events, navCtrl, navParams, requestservice, alertCtrl, groupservice) {
        this.userservice = userservice;
        this.platform = platform;
        this.fcm = fcm;
        this.loadingProvider = loadingProvider;
        this.contactProvider = contactProvider;
        this.storage = storage;
        this.events = events;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.requestservice = requestservice;
        this.alertCtrl = alertCtrl;
        this.groupservice = groupservice;
        this.isData = false;
        this.messageList = [];
        this.messages = 0;
        this.acceptStatus = false;
    }
    NotificationPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.loadingProvider.presentLoading();
        this.requestservice.getmyrequests();
        this.events.subscribe('gotrequests', function () {
            _this.myrequests = [];
            _this.myrequests = _this.requestservice.userdetails;
            if (_this.myrequests.length > 0) {
                _this.isData = false;
                _this.getallNotification(_this.myrequests);
            }
            else {
                _this.loadingProvider.dismissMyLoading();
                _this.notificationList = [];
                _this.isData = true;
            }
        });
    };
    NotificationPage.prototype.ionViewWillLeave = function () {
        this.requestservice.getmyfriends();
        this.groupservice.getmygroups();
        this.loadingProvider.dismissMyLoading();
    };
    NotificationPage.prototype.getallNotification = function (myrequests) {
        var _this = this;
        this.contactProvider.getSimJsonContacts().then(function (res) {
            if (res['contacts'].length > 0) {
                for (var j = 0; j < myrequests.length; j++) {
                    for (var i = 0; i < res['contacts'].length; i++) {
                        if (res['contacts'][i].mobile == myrequests[j].mobile) {
                            myrequests[j].displayName = res['contacts'][i].displayName;
                            break;
                        }
                        else {
                            myrequests[j].displayName = myrequests[j].mobile;
                        }
                    }
                }
                _this.notificationList = myrequests;
                _this.loadingProvider.dismissMyLoading();
            }
            else {
                for (var i = 0; i < myrequests.length; ++i) {
                    myrequests[i].displayName = myrequests[i].mobile;
                }
                _this.notificationList = myrequests;
            }
        }).catch(function (err) {
            _this.loadingProvider.dismissMyLoading();
            if (err == 20) {
                _this.platform.exitApp();
            }
        });
    };
    NotificationPage.prototype.accept = function (item, slidingItem) {
        var _this = this;
        var userName;
        this.userservice.getuserdetails().then(function (res) {
            userName = res.displayName;
            var newMessage = userName + " has accepted your friend request.";
            _this.fcm.sendNotification(item, newMessage, 'sendreq');
        });
        this.requestservice.acceptrequest(item).then(function () {
            var newalert = _this.alertCtrl.create({
                title: 'Friend added',
                subTitle: 'Tap on the friend to chat with him/her',
                buttons: ['Ok']
            });
            newalert.present();
        });
    };
    NotificationPage.prototype.ignore = function (item, slidingItem) {
        var _this = this;
        this.requestservice.deleterequest(item).then(function () {
            var newalert = _this.alertCtrl.create({
                title: 'Delete',
                subTitle: 'Request Successfully Deleted',
                buttons: ['Ok']
            });
            newalert.present();
        }).catch(function (err) {
            alert(err);
        });
    };
    NotificationPage.prototype.blockUser = function (item, slidingItem) {
        this.requestservice.blockuserprof(item).then(function (res) {
        });
    };
    NotificationPage.prototype.refreshPage = function (refresher) {
        this.ionViewDidLoad();
        setTimeout(function () {
            refresher.complete();
        }, 500);
    };
    NotificationPage.prototype.goBack = function () {
        this.navCtrl.setRoot('ChatsPage');
    };
    NotificationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-notification',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/notification/notification.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Notifications</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<!--\n<ion-header>\n  <ion-navbar color="darkblue" >\n    <div class="header-wrap">\n      <!-- <ion-buttons left class="left-arrow">\n        <button ion-button icon-only (click)="goBack()">\n          <ion-icon ios="md-arrow-round-back" md="md-arrow-round-back"></ion-icon>\n        </button>\n      </ion-buttons> -->\n      <!--\n      <div class="header-title">Notifications</div>\n    </div>\n  </ion-navbar>\n</ion-header>\n-->\n\n <ion-content padding>\n  <ion-refresher (ionRefresh)="refreshPage($event)">\n   <ion-refresher-content\n     pullingIcon="arrow-dropdown"\n     pullingText="Pull to refresh"\n     refreshingSpinner="circles"\n     refreshingText="Refreshing...">\n   </ion-refresher-content>\n </ion-refresher>\n\n     <ion-list *ngIf="notificationList">\n       <!-- <ion-item-sliding #slidingItem no-padding class="border-bottom" *ngFor="let item of notificationList"> -->\n         <ion-item *ngFor="let item of notificationList" no-lines padding-vertical class="bg-white">\n             <ion-avatar item-start>\n             <img src="{{item.photoURL}}">\n            </ion-avatar>\n               <div class="info-wrap">\n                 <h2 >{{item.displayName}}</h2>\n                 <p>{{item.disc}}</p>\n               </div>\n               <button ion-button (click)="accept(item,slidingItem)" [disabled]="acceptStatus"><ion-icon name="checkmark"></ion-icon></button>\n               <button ion-button (click)="ignore(item,slidingItem)" [disabled]="acceptStatus"><ion-icon name="trash"></ion-icon></button>\n               <button ion-button (click)="blockUser(item,slidingItem)"[disabled]="acceptStatus"><ion-icon name="contact"></ion-icon></button>\n         </ion-item>\n         <ng-container *ngFor="let key of messageList">\n           <ion-item *ngIf="key.msg"  >\n           <div class="align-notification">\n             <ion-avatar item-start>\n               <img src="{{key.senderPic}}">\n             </ion-avatar>\n             <div class="notifiy-wrap">\n                 <h6>{{key.msg}}</h6>\n                 <p>{{key.reqtime}}</p>\n             </div>\n\n           </div>\n           </ion-item>\n\n         </ng-container>\n         <!-- <ion-item-options side="left">\n           <button ion-button (click)="accept(item,slidingItem)" color="secondary"><ion-icon name="checkmark"></ion-icon></button>\n         </ion-item-options>\n         <ion-item-options side="right">\n           <button ion-button color="danger" (click)="ignore(item,slidingItem)"><ion-icon name="trash"></ion-icon></button>\n           <button class="btn-block-user" ion-button (click)="blockUser(item,slidingItem)"><ion-icon name="contact"></ion-icon>Block</button>\n         </ion-item-options>\n       </ion-item-sliding> -->\n     </ion-list>\n\n     <div *ngIf="isData" text-center>\n         No notification found!\n     </div>\n\n </ion-content>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/notification/notification.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_7__providers_user_user__["a" /* UserProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */], __WEBPACK_IMPORTED_MODULE_6__providers_fcm_fcm__["a" /* FcmProvider */], __WEBPACK_IMPORTED_MODULE_4__providers_loading_loading__["a" /* LoadingProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_contact_contact__["a" /* ContactProvider */], __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_requests_requests__["a" /* RequestsProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_8__providers_groups_groups__["a" /* GroupsProvider */]])
    ], NotificationPage);
    return NotificationPage;
}());

//# sourceMappingURL=notification.js.map

/***/ }),

/***/ 977:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationPageModule", function() { return NotificationPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__notification__ = __webpack_require__(1048);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var NotificationPageModule = (function () {
    function NotificationPageModule() {
    }
    NotificationPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__notification__["a" /* NotificationPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__notification__["a" /* NotificationPage */]),
            ],
        })
    ], NotificationPageModule);
    return NotificationPageModule;
}());

//# sourceMappingURL=notification.module.js.map

/***/ })

});
//# sourceMappingURL=33.js.map