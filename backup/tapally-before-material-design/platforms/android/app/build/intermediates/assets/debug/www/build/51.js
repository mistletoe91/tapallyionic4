webpackJsonp([51],{

/***/ 1030:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FriendListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_user_user__ = __webpack_require__(70);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FriendListPage = (function () {
    function FriendListPage(navCtrl, navParams, userservice, zone, events, platform) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.userservice = userservice;
        this.zone = zone;
        this.events = events;
        this.platform = platform;
        this.unblockUsers = [];
        this.haveData = false;
        this.totusers = 0;
        events.subscribe('unblock-users', function () {
            _this.unblockUsers = _this.userservice.unblockUsers;
            _this.haveData = true;
            _this.totusers = _this.unblockUsers.length;
        });
    }
    FriendListPage.prototype.ionViewDidLoad = function () {
        this.userservice.getAllunBlockUsers();
    };
    FriendListPage.prototype.addblockuser = function (item) {
        var _this = this;
        this.userservice.blockUser(item).then(function (res) {
            if (res) {
                _this.navCtrl.setRoot('BlockUserPage');
            }
        });
    };
    FriendListPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    FriendListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-friend-list',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/friend-list/friend-list.html"*/'<ion-header>\n\n  <ion-navbar >\n    <div class="header-wrap">\n        <!-- <ion-buttons class="left-arrow" left>\n            <button ion-button icon-only (click)="goBack()">\n              <ion-icon ios="md-arrow-round-back" md="md-arrow-round-back"></ion-icon>\n            </button>\n        </ion-buttons> -->\n        <div class="header-title">Select contact</div>\n    </div>\n     <!-- <div class="title-uname">\n       <small *ngIf="totusers > 0">{{totusers}} contacts</small>\n     </div> -->\n   <!-- <ion-buttons right><button ion-button icon-only ><ion-icon ios="md-search" md="md-search"></ion-icon></button></ion-buttons> -->\n  </ion-navbar>\n\n</ion-header>\n<ion-content class="bg-grey" padding>\n    <ion-list *ngIf="unblockUsers">\n        <ng-container *ngFor="let item of unblockUsers">\n          <ion-item (click)="addblockuser(item)">\n            <ion-avatar item-start class="avtar-class"><img src="{{item.photoURL || \'assets/imgs/user.png\'}}"></ion-avatar>\n            <h2>{{item.displayName}}</h2>\n          </ion-item>\n        </ng-container>\n    </ion-list>\n  <!-- <ion-list *ngIf="unblockUsers">\n    <ion-item-group>\n        <div *ngFor="let item of unblockUsers" >\n          <ion-item no-lines (click)="addblockuser(item)">\n              <ion-avatar item-start class="avtar-class">\n                <img src="{{item.photoURL || \'assets/imgs/user.png\'}}">\n              </ion-avatar>\n              <div class="border-bottom">\n                  <h2 class="name-class">{{item.displayName}}</h2>\n              </div>\n          </ion-item> \n        </div>\n    </ion-item-group>\n  </ion-list> -->\n\n  <span *ngIf="haveData">\n    <div padding text-center  *ngIf="unblockUsers.length == 0">\n    No record found!\n    </div>\n  </span>\n  \n</ion-content>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/friend-list/friend-list.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */]])
    ], FriendListPage);
    return FriendListPage;
}());

//# sourceMappingURL=friend-list.js.map

/***/ }),

/***/ 959:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FriendListPageModule", function() { return FriendListPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__friend_list__ = __webpack_require__(1030);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FriendListPageModule = (function () {
    function FriendListPageModule() {
    }
    FriendListPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__friend_list__["a" /* FriendListPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__friend_list__["a" /* FriendListPage */]),
            ],
        })
    ], FriendListPageModule);
    return FriendListPageModule;
}());

//# sourceMappingURL=friend-list.module.js.map

/***/ })

});
//# sourceMappingURL=51.js.map