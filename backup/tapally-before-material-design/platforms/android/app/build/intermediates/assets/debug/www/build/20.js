webpackJsonp([20],{

/***/ 1061:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RequestAfterSelectedPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the RequestAfterSelectedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RequestAfterSelectedPage = (function () {
    function RequestAfterSelectedPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    RequestAfterSelectedPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RequestAfterSelectedPage');
    };
    RequestAfterSelectedPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-request-after-selected',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/request-after-selected/request-after-selected.html"*/'<ion-header>\n  <ion-navbar color="primary" >\n    <ion-title>\n      View Referral\n    </ion-title>\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name=\'menu\'></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content >\n  <ion-item padding class="highcard profilecard pull-center" no-lines>\n       <h1> <br /><span class="text_color_primary strongwords">Garry Singh</span></h1>\n       <h3>ABC Hi-Solutions Ltd</h3>\n       <h3>(647) 123-3123</h3>\n       <h3>123 Octillo Blvd, Brampton, ON, Canada</h3>\n       <h3><p>Referred By Arando Qualiejo Djsuodis  <ion-icon color="strong" name="contact"></ion-icon>  <span class="hideme" style="color:#32db64"><ion-icon name="star-half"></ion-icon> 4.1 (24)</span></p></h3>\n\n   </ion-item>\n\n   <ion-list class="pull-center" no-lines>\n\n          <button ion-button color="primary"  > <ion-icon style="margin-right:5px;" item-start name="megaphone"></ion-icon> Request Service</button>\n            <br />\n          <button ion-button color="secondary"  >\n              <ion-icon style="margin-right:5px;" item-start name="logo-usd"></ion-icon>\n              Submit Receipt\n              </button>\n              <br /><small>Submit Receipt To Claim Points</small>\n\n          <br />\n\n   </ion-list>\n\n\n\n</ion-content>\n\n<ion-footer >\n  <ion-toolbar class="pull-center">\n    <button ion-button color="primary" clear >Go Back</button>\n\n  </ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/request-after-selected/request-after-selected.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */]])
    ], RequestAfterSelectedPage);
    return RequestAfterSelectedPage;
}());

//# sourceMappingURL=request-after-selected.js.map

/***/ }),

/***/ 990:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RequestAfterSelectedPageModule", function() { return RequestAfterSelectedPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__request_after_selected__ = __webpack_require__(1061);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RequestAfterSelectedPageModule = (function () {
    function RequestAfterSelectedPageModule() {
    }
    RequestAfterSelectedPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__request_after_selected__["a" /* RequestAfterSelectedPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__request_after_selected__["a" /* RequestAfterSelectedPage */]),
            ],
        })
    ], RequestAfterSelectedPageModule);
    return RequestAfterSelectedPageModule;
}());

//# sourceMappingURL=request-after-selected.module.js.map

/***/ })

});
//# sourceMappingURL=20.js.map