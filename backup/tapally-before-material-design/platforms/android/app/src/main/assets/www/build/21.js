webpackJsonp([21],{

/***/ 1060:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterbusinessPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_user_user__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_cats_cats__ = __webpack_require__(502);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_firebase__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_firebase__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var RegisterbusinessPage = (function () {
    function RegisterbusinessPage(navCtrl, navParams, fb, userservice, catservice) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.fb = fb;
        this.userservice = userservice;
        this.catservice = catservice;
        this.submitted = false;
        this.msgstatus = false;
        this.nextPage = "ImportpPage";
        this.cats = this.catservice.getCats();
        //this.assignCatName ();
        this.selectOptions = {
            title: 'Business Category',
            subTitle: 'Please select an appropriate category'
        };
        this.authForm = this.fb.group({
            businessname: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])],
            useremail: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])],
            bcats: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])]
        });
    }
    RegisterbusinessPage.prototype.assignCatName = function () {
        if (!localStorage.getItem('bcats')) {
            this.bcatName = "";
            return;
        }
        for (var x = 0; x <= this.cats.length; x++) {
            if (this.cats[x].id == localStorage.getItem('bcats')) {
                this.bcatName = this.cats[x].name;
            }
        } //end for
    };
    RegisterbusinessPage.prototype.backButtonClick = function () {
        //this.navCtrl.pop();
        this.navCtrl.setRoot("TabsPage");
    };
    RegisterbusinessPage.prototype.btnSkipStep = function () {
        //mytodo mytodonow : check if contacts are already imported then just go to tabspage
        this.navCtrl.setRoot(this.nextPage);
    };
    RegisterbusinessPage.prototype.ionViewDidLoad = function () {
        this.loadbusinessname();
    };
    RegisterbusinessPage.prototype.loadbusinessname = function () {
        var _this = this;
        var userId;
        if (__WEBPACK_IMPORTED_MODULE_5_firebase___default.a.auth().currentUser) {
            userId = __WEBPACK_IMPORTED_MODULE_5_firebase___default.a.auth().currentUser.uid;
        }
        else {
            userId = localStorage.getItem('userUID');
        }
        if (userId != undefined) {
            this.userservice.getBusinessDetails().then(function (res) {
                if (res) {
                    _this.msgstatus = true;
                    _this.businessname = res['displayName'];
                    _this.useremail = localStorage.getItem('useremail'); //res['useremail'];
                    _this.bcats = res['bcats'];
                    //this.assignCatName ();
                }
                else {
                    _this.businessname = '';
                    _this.useremail = '';
                    _this.bcats = 0;
                }
            }).catch(function (err) {
            });
        }
        else {
            this.businessname = '';
            this.useremail = '';
            this.bcats = 0;
        }
    };
    RegisterbusinessPage.prototype.onChange = function (e) {
        if (this.businessname.trim()) {
            this.msgstatus = true;
        }
        else {
            this.msgstatus = false;
        }
    };
    RegisterbusinessPage.prototype.save = function () {
        var _this = this;
        this.submitted = true;
        if (this.authForm.valid) {
            if (this.businessname.trim() != null || this.businessname.trim() != "") {
                this.businessname = this.businessname.trim();
                this.useremail = this.useremail.trim();
                this.userservice.registerBusiness(this.businessname, this.useremail, this.bcats).then(function (res) {
                    if (res.success) {
                        _this.navCtrl.setRoot(_this.nextPage);
                    }
                });
            }
            else {
            }
        }
    };
    RegisterbusinessPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-registerbusiness',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/registerbusiness/registerbusiness.html"*/'<ion-header>\n  <ion-navbar>\n    <div class="header-wrap">\n      <!--\n      <button ion-button icon-only (click)="backButtonClick()">\n          <ion-icon ios="md-arrow-round-back" md="md-arrow-round-back"></ion-icon>\n      </button>\n    -->\n      <div class="header-title">Register Business</div>\n    </div>\n  </ion-navbar>\n</ion-header>\n<ion-content padding>\n  <form [formGroup]="authForm" (submit)="save()">\n\n<ion-card>\n  <ion-card-content>\n    Give your business exposure it deserve\n  </ion-card-content>\n</ion-card>\n\n    <ion-label class="content-title"> Business Name</ion-label>\n    <ion-input type="text" (input)="onChange($event.keyCode)" formControlName="businessname" id="businessname" [value]="businessname" [(ngModel)]="businessname"\n    maxlength="30" placeholder="Business Name"></ion-input>\n    <ion-label *ngIf="(!authForm.controls.businessname.valid||!msgstatus)  && (authForm.controls.businessname.dirty || submitted)">\n        <p class="errormsg">Please enter a valid name.</p>\n    </ion-label>\n\n\n    <ion-label class="content-title"> Email</ion-label>\n    <ion-input type="text" (input)="onChange($event.keyCode)" formControlName="useremail" id="useremail" [value]="useremail" [(ngModel)]="useremail"\n    maxlength="30" placeholder="Email"></ion-input>\n    <ion-label *ngIf="(!authForm.controls.useremail.valid||!msgstatus)  && (authForm.controls.useremail.dirty || submitted)">\n        <p class="errormsg">Please enter a valid email.</p>\n    </ion-label>\n\n    <ion-label class="content-title"> Business Category</ion-label>\n\n    <ion-list>\n      <ion-item>\n        <ion-select placeholder="Please Select" [selectOptions]="selectOptions" [(ngModel)]="bcats" formControlName="bcats" id="bcats" >\n             <ion-option *ngFor="let item of cats" value="{{item.id}}">{{item.name}}</ion-option>\n        </ion-select>\n      </ion-item>\n    </ion-list>\n\n\n\n\n    <button ion-button class="button-secondary" type="submit" [disabled]="!authForm.valid || !this.msgstatus" >Submit</button>\n    <button ion-button (click)="btnSkipStep()" clear >SKIP STEP</button>\n  </form>\n</ion-content>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/registerbusiness/registerbusiness.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_3__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_cats_cats__["a" /* CatProvider */]])
    ], RegisterbusinessPage);
    return RegisterbusinessPage;
}());

//# sourceMappingURL=registerbusiness.js.map

/***/ }),

/***/ 989:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterbusinessPageModule", function() { return RegisterbusinessPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__registerbusiness__ = __webpack_require__(1060);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RegisterbusinessPageModule = (function () {
    function RegisterbusinessPageModule() {
    }
    RegisterbusinessPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__registerbusiness__["a" /* RegisterbusinessPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__registerbusiness__["a" /* RegisterbusinessPage */]),
            ],
        })
    ], RegisterbusinessPageModule);
    return RegisterbusinessPageModule;
}());

//# sourceMappingURL=registerbusiness.module.js.map

/***/ })

});
//# sourceMappingURL=21.js.map