webpackJsonp([5],{

/***/ 1007:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubmitReceiptPageModule", function() { return SubmitReceiptPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__submit_receipt__ = __webpack_require__(1078);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SubmitReceiptPageModule = (function () {
    function SubmitReceiptPageModule() {
    }
    SubmitReceiptPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__submit_receipt__["a" /* SubmitReceiptPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__submit_receipt__["a" /* SubmitReceiptPage */]),
            ],
        })
    ], SubmitReceiptPageModule);
    return SubmitReceiptPageModule;
}());

//# sourceMappingURL=submit-receipt.module.js.map

/***/ }),

/***/ 1078:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubmitReceiptPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the SubmitReceiptPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SubmitReceiptPage = (function () {
    function SubmitReceiptPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    SubmitReceiptPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SubmitReceiptPage');
    };
    SubmitReceiptPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-submit-receipt',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/submit-receipt/submit-receipt.html"*/'<ion-header>\n  <ion-navbar color="primary" >\n    <ion-title>\n      Submit Receipt\n    </ion-title>\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name=\'menu\'></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content >\n  <ion-item padding class="highcard profilecard pull-center "  no-lines>\n      <h2  >Service Provider </h2>\n       <h1> <span class="text_color_primary strongwords">Garry Singh</span></h1>\n       <h3>ABC Hi-Solutions Ltd</h3>\n       <h3>(647) 123-3123</h3>\n       <h3>123 Octillo Blvd, Brampton, ON, Canada</h3>\n       <h3><p>Referred By Arando Qualiejo Djsuodis  <ion-icon color="strong" name="contact"></ion-icon>  <span class="hideme" style="color:#32db64"><ion-icon name="star-half"></ion-icon> 4.1 (24)</span></p></h3>\n\n   </ion-item>\n\n   <ion-list class="pull-center" no-lines>\n\n      <p></p>\n       <ion-list  >\n         <ion-item style="border-color:#ccc;text-align: center;border-width:0px 0px 0px 0px;border-style:solid;">\n          <ion-label color="secondary"  >Points <strong>150</strong></ion-label>\n        </ion-item>\n         <ion-item style="border:1px solid #ccc;">\n          <ion-label color="primary" style="text-align:left"  >Total Amount You Paid (including Taxes)</ion-label> \n        </ion-item>\n        <ion-item style="border-color:#ccc;text-align: center;padding:50px;border-width:0px 1px 1px 1px;border-style:solid;">\n\n            <div   class="card-title text_color_primary">  <ion-icon name="camera"></ion-icon> </div>\n            <div  class="card-subtitle text_color_primary">Tap To Take Picture Of Receipt</div>\n\n       </ion-item>\n       </ion-list>\n\n          <button ion-button color="secondary"  >\n              <ion-icon style="margin-right:5px;" item-start name="logo-usd"></ion-icon>\n              Submit Receipt\n              </button>\n              <br /><small>You will get points right away Gary Singh approves </small>\n\n          <br />\n\n   </ion-list>\n\n\n\n</ion-content>\n\n<ion-footer >\n  <ion-toolbar class="pull-center">\n    <button ion-button color="primary" clear >Go Back</button>\n\n  </ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/submit-receipt/submit-receipt.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */]])
    ], SubmitReceiptPage);
    return SubmitReceiptPage;
}());

//# sourceMappingURL=submit-receipt.js.map

/***/ })

});
//# sourceMappingURL=5.js.map