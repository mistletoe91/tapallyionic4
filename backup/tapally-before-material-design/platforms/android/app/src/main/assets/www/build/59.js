webpackJsonp([59],{

/***/ 1021:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_requests_requests__ = __webpack_require__(495);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_chat_chat__ = __webpack_require__(497);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_user_user__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_contact_contact__ = __webpack_require__(498);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_loading_loading__ = __webpack_require__(240);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_fcm_fcm__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_groups_groups__ = __webpack_require__(496);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_imagehandler_imagehandler__ = __webpack_require__(499);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_firebase__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_firebase__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var ChatsPage = (function () {
    function ChatsPage(fcm, platform, loadingProvider, contactProvider, navCtrl, navParams, requestservice, events, alertCtrl, chatservice, zone, popoverCtrl, imghandler, userservice, loadingCtrl, eleRef, modalCtrl, groupservice) {
        this.fcm = fcm;
        this.platform = platform;
        this.loadingProvider = loadingProvider;
        this.contactProvider = contactProvider;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.requestservice = requestservice;
        this.events = events;
        this.alertCtrl = alertCtrl;
        this.chatservice = chatservice;
        this.zone = zone;
        this.popoverCtrl = popoverCtrl;
        this.imghandler = imghandler;
        this.userservice = userservice;
        this.loadingCtrl = loadingCtrl;
        this.eleRef = eleRef;
        this.modalCtrl = modalCtrl;
        this.groupservice = groupservice;
        this.myrequests = [];
        this.myfriendsList = [];
        this.tempmyfriendsList = [];
        this.debugMe = true;
        this.showPaidSign = false;
        this.isData = false;
        this.isNoRecord = false;
        this.requestcounter = null;
        this.username = "";
        this.selectContact = false;
        this.selectedContact = [];
        this.selectCounter = 0;
        this.allmygroups = [];
        this.tempallmygroups = [];
        this.allChatListing = [];
        this.archiveChats = [];
        this.counter = 0;
        this.messages = [];
        this.whitelabelObj = {};
    }
    ChatsPage.prototype.ionViewDidLoad = function () {
        console.log("ionViewDidLoad");
        this.today = new Date();
    };
    ChatsPage.prototype.sendto_invite = function () {
        if (localStorage.getItem('businessName')) {
            this.navCtrl.push("OutreachForBusinessPage");
        }
        else {
            this.navCtrl.push("RegisterbusinessPage");
        }
    };
    ChatsPage.prototype.referralSendForward = function (buddy) {
        // I want to send buddy's referral to someone else
        this.navCtrl.push("SendReferralForwardPage", { buddy_uid: buddy.uid, buddy_displayName: buddy.displayName });
    };
    ChatsPage.prototype.referralSendBackward = function (buddy) {
        this.navCtrl.push("SendReferralBackwardPage", { buddy_uid: buddy.uid, buddy_displayName: buddy.displayName });
    };
    ChatsPage.prototype.ionViewWillLeave = function () {
        this.loadingProvider.dismissMyLoading(); //
    };
    ChatsPage.prototype.sendToBroadcastPage = function () {
        this.navCtrl.push("BroadcastPage");
    };
    //KEep it same as chats.ts
    ChatsPage.prototype.paidOrNot = function () {
        var _this = this;
        this.showPaidSign = false;
        if (this.debugMe) {
            console.log("paidornot checkiing");
            console.log(localStorage.getItem('business_created'));
        }
        if (localStorage.getItem('business_created')) {
            var AcceptableDelayInMicroseconds = 1000; //Before we show warning to business that they have not paid yet
            var timeStamp = Date.now();
            var business_created = parseInt(localStorage.getItem('business_created'));
            if (this.debugMe) {
                console.log("business_created checkiing");
            }
            //Do not check for first few minutes/hours/days ( as specified in AcceptableDelayInMicroseconds )
            //But check every single time afterwards
            if ((business_created + AcceptableDelayInMicroseconds) < timeStamp) {
                var paid_1 = parseInt(localStorage.getItem('paid'));
                if (this.debugMe) {
                    console.log("paid checkiing");
                    console.log(paid_1);
                }
                //check API everytime
                if (true) {
                    if (this.debugMe) {
                        console.log("calling getBusinessDetails");
                    }
                    this.userservice.getBusinessDetails().then(function (res) {
                        //localStorage.setItem('businessName', res['displayName']);
                        //localStorage.setItem('bcats', res['bcats']);
                        if (!paid_1 || paid_1 <= 0) {
                            console.log("Indeed Business has not paid yet");
                            _this.showPaidSign = true;
                        }
                    }).catch(function (err) {
                    });
                } //endif
            }
            else {
            } //endif
        } //endif
    }; //end function
    ChatsPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        console.log("ionViewWillEnter");
        //console.log (Date.now());
        this.whitelabelObj = {};
        if (localStorage.getItem('invited_by') && localStorage.getItem('invited_by') != "none") {
            this.whitelabelObj.buddy_uid = localStorage.getItem('invited_by');
        }
        else {
            this.whitelabelObj.buddy_uid = 0;
        }
        if (localStorage.getItem('sponseredbusines_name')) {
            this.whitelabelObj.buddy_displayName = localStorage.getItem('sponseredbusines_name');
        }
        else {
            this.whitelabelObj.buddy_displayName = "";
        }
        if (localStorage.getItem('sponseredbusines_businessName')) {
            this.whitelabelObj.sponseredbusines_businessName = localStorage.getItem('sponseredbusines_businessName');
        }
        else {
            this.whitelabelObj.sponseredbusines_businessName = "";
        }
        console.log(this.whitelabelObj);
        this.loadingProvider.presentLoading();
        this.paidOrNot();
        this.setStatus();
        this.fcm.getToken();
        this.events.publish('checkUserStatus');
        this.requestservice.getmyrequests();
        this.requestservice.getmyfriends();
        this.loaduserdetails();
        this.myfriends = [];
        this.events.subscribe('gotrequests', function () {
            _this.events.unsubscribe('gotrequests');
            console.log("gotrequests()");
            _this.myrequests = [];
            _this.myrequests = _this.requestservice.userdetails;
            if (_this.myrequests) {
                _this.requestcounter = _this.myrequests.length;
            }
        });
        this.events.subscribe('friends', function () {
            _this.events.unsubscribe('friends');
            console.log("Resetting allChatListing to []");
            //this.allChatListing = [];
            _this.tempallmygroups = [];
            _this.allmygroups = [];
            _this.myfriends = [];
            _this.myfriendsList = [];
            _this.myfriends = _this.sortdatalist(_this.requestservice.myfriends);
            if (_this.myfriends.length > 0) {
                _this.isData = false;
                _this.convertWithContacts(_this.myfriends).then(function (res) {
                    if (res.length > 0) {
                        _this.removeDuplicates(res, 'mobile').then(function (responce) {
                            _this.myfriendsList = [];
                            _this.myfriendsList = responce;
                            _this.doRefresh();
                            _this.events.subscribe('allmygroups', function () {
                                _this.events.unsubscribe('allmygroups');
                                _this.allmygroups = [];
                                _this.allmygroups = _this.groupservice.mygroups;
                                _this.doRefresh();
                            });
                            _this.groupservice.getmygroups();
                        });
                    }
                });
            }
            else {
                _this.isData = true;
                _this.loadingProvider.dismissMyLoading();
            }
        });
        //this.setForWhiteLabeling ();
    };
    ChatsPage.prototype.removeDuplicates = function (originalArray, prop) {
        return new Promise(function (resolve) {
            var newArray = [];
            var lookupObject = {};
            for (var i in originalArray) {
                lookupObject[originalArray[i][prop]] = originalArray[i];
            }
            for (i in lookupObject) {
                newArray.push(lookupObject[i]);
            }
            resolve(newArray);
        });
    };
    //Function might not be used : depriciated
    /*
    setForWhiteLabeling (){
      if(localStorage.getItem('invited_by')){
          if(localStorage.getItem('invited_by') == "none"){
              //When you show the user , highlight it as sponsered

          }
      } else {
         //check once and it not then set
         //--if none then set the localStorage with none
         this.loaduserdetails ();
      }
    }
    */
    ChatsPage.prototype.doRefresh = function () {
        var _this = this;
        console.log("Do Refersh()");
        var tmpalllist = [];
        if (this.allmygroups.length > 0) {
            for (var i_1 = 0; i_1 < this.allmygroups.length; i_1++) {
                this.allmygroups[i_1].gpflag = 1;
                tmpalllist.push(this.allmygroups[i_1]);
            }
        }
        if (this.myfriendsList.length > 0) {
            for (var i_2 = 0; i_2 < this.myfriendsList.length; i_2++) {
                this.myfriendsList[i_2].gpflag = 0;
                this.myfriendsList[i_2].whitelabelcss = '';
                //check for whitelabeling
                //if(localStorage.setItem('business_created') && localStorage.setItem('paid')){
                //I am business owner myself so I should get whitelabeling benfits
                //} else {
                if (localStorage.getItem('invited_by')) {
                    //console.log ( this.myfriendsList[i].uid +":"+ localStorage.getItem('invited_by'));
                    if (this.myfriendsList[i_2].uid == localStorage.getItem('invited_by')) {
                        //this.myfriendsList[i].whitelabelcss = 'whitelabelcss';
                        localStorage.setItem('sponseredbusines_name', this.myfriendsList[i_2].displayName);
                        //localStorage.setItem('sponseredbusines_businessName');
                    }
                }
                else {
                    //For some reason the local storage is empty. Let's fill it : Disabled for now
                    //this.loaduserdetails ();
                }
                //}
                tmpalllist.push(this.myfriendsList[i_2]);
            }
        }
        if (this.selectedContact.length > 0) {
            this.selectedContact = [];
            this.selectCounter = 0;
            this.selectContact = false;
            for (var i = 0; i < this.allChatListing.length; i++) {
                this.allChatListing[i].selection = false;
            }
        }
        setTimeout(function () {
            console.log("Set Timeout inside doRefersh ()");
            if (_this.myfriendsList.length > 0 || _this.allmygroups.length > 0) {
                _this.zone.run(function () {
                    _this.archiveChats = [];
                    _this.allChatListing = [];
                    _this.tempallmygroups = [];
                    _this.counter = [];
                    _this.allChatListing = tmpalllist.sort(function (a, b) { return new Date(b.userprio).getTime() - new Date(a.userprio).getTime(); });
                    _this.tempallmygroups = _this.allChatListing;
                    //console.log ("Done Setting allChatListing after SORTING");
                    _this.loadingProvider.dismissMyLoading();
                });
            }
        }, 1000);
    };
    ChatsPage.prototype.closeButtonClick = function () {
        console.log("CloseButton Click");
        if (this.selectedContact.length > 0) {
            this.selectedContact = [];
            this.selectCounter = 0;
            this.selectContact = false;
            for (var i = 0; i < this.allChatListing.length; i++) {
                this.allChatListing[i].selection = false;
            }
        }
    };
    ChatsPage.prototype.CheckLastMSG = function (lastmsg, msg) {
        if (!(msg instanceof Array) && msg.match("^(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|www\\.){1}([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?")) {
            return false;
        }
        else if (!(msg instanceof Array) && lastmsg) {
            return true;
        }
        else {
            return false;
        }
    };
    ChatsPage.prototype.CheckLastMsgUrl = function (url) {
        if (!(url instanceof Array) && url.match("^(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|www\\.){1}([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?")) {
            return true;
        }
        else if ((url instanceof Array) && url.length > 0) {
            return true;
        }
    };
    ChatsPage.prototype.sortdatalist = function (data) {
        return data.sort(function (a, b) {
            var result = null;
            if (a.userprio != null) {
                var dateA = new Date(a.userprio).getTime();
                var dateB = new Date(b.userprio).getTime();
            }
            result = dateA < dateB ? 1 : -1;
            return result;
        });
    };
    ChatsPage.prototype.refreshPage = function (refresher) {
        this.ionViewWillEnter();
        setTimeout(function () {
            refresher.complete();
        }, 500);
    };
    ChatsPage.prototype.SortByDate = function (array) {
        array.sort(function (a, b) {
            if (a === void 0) { a = []; }
            if (b === void 0) { b = []; }
            if (a.lastMessage.timestamp > b.lastMessage.timestamp) {
                return -1;
            }
            else if (a.lastMessage.timestamp < b.lastMessage.timestamp) {
                return 1;
            }
            else {
                return 0;
            }
        });
        return array;
    };
    ChatsPage.prototype.viewPofile = function (url, name) {
        if (this.selectedContact.length == 0) {
            var modal = this.modalCtrl.create("ProfileViewPage", { img: url, name: name });
            modal.present();
        }
    };
    ChatsPage.prototype.initializeContacts = function () {
        console.log("initializeContacts ()");
        this.allChatListing = this.tempallmygroups;
    };
    ChatsPage.prototype.searchuser = function (ev) {
        this.initializeContacts();
        this.isNoRecord = false;
        var val = ev.target.value;
        if (val && val.trim() != '') {
            this.allChatListing = this.allChatListing.filter(function (item) {
                if (item.displayName) {
                    return (item.displayName.toLowerCase().indexOf(val.toLowerCase().trim()) > -1);
                }
                else if (item.groupName) {
                    return (item.groupName.toLowerCase().indexOf(val.toLowerCase().trim()) > -1);
                }
            });
            if (this.allChatListing.length == 0) {
                this.isNoRecord = true;
            }
        }
    };
    ChatsPage.prototype.setStatus = function () {
        this.chatservice.setstatusUser().then(function (res) {
            if (res) {
            }
        }).catch(function (err) {
        });
    };
    ChatsPage.prototype.presentPopover = function (myEvent) {
        this.navCtrl.push("NotificationPage");
        this.requestcounter = this.myrequests.length;
    };
    ChatsPage.prototype.viewPopover = function (myEvent) {
        var _this = this;
        var popover = this.popoverCtrl.create('PopoverChatPage', { page: '0' });
        popover.present({
            ev: myEvent
        });
        popover.onDidDismiss(function (pages) {
            if (pages != 0 || pages != undefined || pages != null) {
                _this.navCtrl.push(pages.page);
            }
        });
    };
    ChatsPage.prototype.addbuddy = function () {
        this.navCtrl.push('ContactPage');
    };
    //profile data
    ChatsPage.prototype.loaduserdetails = function () {
        var _this = this;
        this.userservice.getuserdetails().then(function (res) {
            _this.username = res.displayName;
            localStorage.setItem('invited_by', res.invited_by);
            /*
            Check if the whitelabeled user is paid user : But only check like once a day . Not more than that
            */
            if (res.invited_by) {
                var AcceptableDelayInMicroseconds = 1000; //1 day : Before we check if another business is paid member or not
                var invited_by_business_paid_flag_checked_date = parseInt(localStorage.getItem('invited_by_business_paid_flag_checked_date'));
                var timeStamp = Date.now();
                if (!invited_by_business_paid_flag_checked_date) {
                    invited_by_business_paid_flag_checked_date = 0;
                }
                if ((invited_by_business_paid_flag_checked_date + AcceptableDelayInMicroseconds) < timeStamp) {
                    console.log("Attempting to check Cloud for checkIfBusinessIsPaid");
                    if (!res.invited_by || res.invited_by != "none") {
                        _this.userservice.checkIfBusinessIsPaid(res.invited_by).then(function (res_invitedby) {
                            localStorage.setItem('sponseredbusines_businessName', res_invitedby.displayName);
                            localStorage.setItem('invited_by_business_paid_flag_checked_date', String(Date.now())); //jaswinder
                            console.log("----> CHECK");
                            if (res_invitedby.paid && res_invitedby.paid == "1") {
                                localStorage.setItem('invited_by', res.invited_by);
                                console.log("--> Business Paid Flag Checked and set invited_by");
                            }
                            else {
                                //member is not paid so lets not do whitelabeling
                                localStorage.setItem('invited_by', "none");
                                console.log("--> Business Paid Flag Checked and REMOVED whitelabeling because he/she did not paid ");
                                console.log("RESET the invited by " + res.invited_by);
                                localStorage.setItem('invited_by', "");
                                localStorage.setItem('sponseredbusines_businessName', "");
                                localStorage.setItem('sponseredbusines_name', "");
                            }
                            console.log("ENDIF DONE");
                        });
                    }
                    else {
                        console.log("RESET the invited by " + res.invited_by);
                        localStorage.setItem('invited_by', "none");
                        localStorage.setItem('sponseredbusines_businessName', "");
                        localStorage.setItem('sponseredbusines_name', "");
                    }
                } //endif
            }
            _this.zone.run(function () {
                _this.avatar = res.photoURL;
            });
        });
    };
    ChatsPage.prototype.logout = function () {
        var _this = this;
        this.chatservice.setStatusOffline().then(function (res) {
            if (res) {
                __WEBPACK_IMPORTED_MODULE_10_firebase___default.a.auth().signOut().then(function () {
                    _this.navCtrl.setRoot('LoginPage');
                });
            }
        });
    };
    ChatsPage.prototype.editname = function () {
        var _this = this;
        var statusalert = this.alertCtrl.create({
            buttons: ['okay']
        });
        var alert = this.alertCtrl.create({
            title: 'Edit Nickname',
            inputs: [{
                    name: 'nickname',
                    placeholder: 'Nickname'
                }],
            buttons: [{
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function (data) {
                    }
                },
                {
                    text: 'Edit',
                    handler: function (data) {
                        if (data.nickname) {
                            _this.userservice.updatedisplayname(data.nickname).then(function (res) {
                                if (res.success) {
                                    statusalert.setTitle('Updated');
                                    statusalert.setSubTitle('Your username has been changed successfully!!');
                                    statusalert.present();
                                    _this.zone.run(function () {
                                        _this.username = data.nickname;
                                    });
                                }
                                else {
                                    statusalert.setTitle('Failed');
                                    statusalert.setSubTitle('Your username was not changed');
                                    statusalert.present();
                                }
                            });
                        }
                    }
                }]
        });
        alert.present();
    };
    ChatsPage.prototype.editimage = function () {
        var _this = this;
        var statusalert = this.alertCtrl.create({
            buttons: ['okay']
        });
        this.imghandler.uploadimage().then(function (url) {
            _this.userservice.updateimage(url).then(function (res) {
                if (res.success) {
                    statusalert.setTitle('Updated');
                    statusalert.setSubTitle('Your profile pic has been changed successfully!!');
                    statusalert.present();
                    _this.zone.run(function () {
                        _this.avatar = url;
                    });
                }
            }).catch(function (err) {
                statusalert.setTitle('Failed');
                statusalert.setSubTitle('Your profile pic was not changed');
                statusalert.present();
            });
        });
    };
    ChatsPage.prototype.deleteAllMessage = function () {
        var _this = this;
        var title;
        var message;
        if (this.selectedContact[0].uid) {
            title = 'Clear Chat';
            message = 'Do you want to clear this chat ?';
        }
        else {
            title = 'Delete Group';
            message = 'Do you want to Delete this group ?';
        }
        var alert = this.alertCtrl.create({
            title: title,
            message: message,
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                    }
                },
                {
                    text: 'Ok',
                    handler: function () {
                        if (_this.selectedContact.length > 0) {
                            for (var j = 0; j < _this.selectedContact.length; j++) {
                                if (_this.selectedContact[j].uid) {
                                    for (var i = 0; i < _this.selectedContact.length; i++) {
                                        _this.chatservice.deleteUserMessages(_this.selectedContact).then(function (res) {
                                            if (res) {
                                                _this.selectedContact = [];
                                                if (_this.selectedContact.length == 0) {
                                                    _this.selectContact = false;
                                                }
                                                for (var k = 0; k < _this.allChatListing.length; k++) {
                                                    _this.allChatListing[k].selection = false;
                                                }
                                            }
                                        });
                                    }
                                }
                                else {
                                    var _loop_1 = function (i) {
                                        var groupname = _this.selectedContact[i].groupName;
                                        _this.groupservice.getgroupInfo(groupname).then(function (ress) {
                                            var owners = _this.groupservice.converanobj(ress.owner);
                                            if (owners.length == 1) {
                                                _this.groupservice.getownership(groupname).then(function (res) {
                                                    if (res) {
                                                        _this.groupservice.deletegroups(groupname).then(function (res) {
                                                            if (res) {
                                                                _this.selectedContact = [];
                                                                if (_this.selectedContact.length == 0) {
                                                                    _this.selectContact = false;
                                                                }
                                                                for (var k = 0; k < _this.allChatListing.length; k++) {
                                                                    _this.allChatListing[k].selection = false;
                                                                }
                                                            }
                                                        });
                                                    }
                                                    else {
                                                        _this.groupservice.leavegroups(groupname).then(function (res) {
                                                            if (res) {
                                                                _this.selectedContact = [];
                                                                if (_this.selectedContact.length == 0) {
                                                                    _this.selectContact = false;
                                                                }
                                                                for (var m = 0; m < _this.allChatListing.length; m++) {
                                                                    _this.allChatListing[m].selection = false;
                                                                }
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                            else {
                                                _this.groupservice.getownership(groupname).then(function (res) {
                                                    if (res) {
                                                        _this.groupservice.leavegroupsowner(groupname).then(function (res) {
                                                            if (res) {
                                                                _this.selectedContact = [];
                                                                if (_this.selectedContact.length == 0) {
                                                                    _this.selectContact = false;
                                                                }
                                                                for (var p = 0; p < _this.allChatListing.length; p++) {
                                                                    _this.allChatListing[p].selection = false;
                                                                }
                                                            }
                                                        });
                                                    }
                                                    else {
                                                        _this.groupservice.leavegroups(groupname).then(function (res) {
                                                            if (res) {
                                                                _this.selectedContact = [];
                                                                if (_this.selectedContact.length == 0) {
                                                                    _this.selectContact = false;
                                                                }
                                                                for (var o = 0; o < _this.allChatListing.length; o++) {
                                                                    _this.allChatListing[o].selection = false;
                                                                }
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    };
                                    for (var i = 0; i < _this.selectedContact.length; i++) {
                                        _loop_1(i);
                                    }
                                }
                            }
                        }
                    }
                }
            ]
        });
        alert.present();
    };
    ChatsPage.prototype.popoveroncontact = function (item, event) {
        if (this.selectedContact.length == 0) {
            item.selection = true;
            this.selectedContact.push(item);
            if (this.selectContact == true) {
                this.selectContact = false;
            }
            else {
                this.selectContact = true;
            }
        }
        this.selectCounter = this.selectedContact.length;
        if (this.selectedContact.length == 0) {
            this.selectContact = false;
            item.selection = false;
        }
    };
    ChatsPage.prototype.buddychat = function (item, event, UnreadCount) {
        console.log("Buddy chat");
        if (event.target.classList.contains('avtar-class') || event.target.classList.contains('avtar-img-list')) {
        }
        else {
            if (this.selectedContact.length == 0) {
                localStorage.setItem('UnreadCount', UnreadCount);
                this.chatservice.initializebuddy(item);
                this.navCtrl.push('BuddychatPage');
            }
            else if (this.selectedContact.length == 1) {
                if (this.selectedContact[0].uid == item.uid) {
                    item.selection = false;
                    this.selectedContact = [];
                    this.selectCounter = 0;
                    this.selectContact = false;
                }
                else {
                    this.selectedContact = [];
                    this.selectCounter = 0;
                    this.selectContact = false;
                    for (var i = 0; i < this.allChatListing.length; i++) {
                        this.allChatListing[i].selection = false;
                    }
                    item.selection = true;
                    this.selectedContact.push(item);
                    this.selectCounter = this.selectedContact.length;
                    this.selectContact = true;
                }
            }
            else {
                this.popoveroncontact(item, event);
            }
        }
    };
    ChatsPage.prototype.openchat = function (item, event) {
        console.log("Open Chat");
        if (this.selectedContact.length == 0) {
            this.groupservice.getintogroup(item.groupName);
            this.navCtrl.push('GroupchatPage', { groupName: item.groupName, groupImage: item.groupimage });
        }
        else if (this.selectedContact.length == 1) {
            if (this.selectedContact[0].groupName == item.groupName) {
                item.selection = false;
                this.selectedContact = [];
                this.selectCounter = 0;
                this.selectContact = false;
            }
            else {
                this.selectedContact = [];
                this.selectCounter = 0;
                this.selectContact = false;
                for (var i = 0; i < this.allChatListing.length; i++) {
                    this.allChatListing[i].selection = false;
                }
                item.selection = true;
                this.selectedContact.push(item);
                this.selectCounter = this.selectedContact.length;
                this.selectContact = true;
            }
        }
        else {
            this.popoveroncontact(item, event);
        }
    };
    ChatsPage.prototype.convertWithContacts = function (data) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.contactProvider.getSimJsonContacts().then(function (res) {
                if (res['contacts'].length > 0) {
                    for (var i = 0; i < res['contacts'].length; ++i) {
                        for (var j = 0; j < data.length; ++j) {
                            if (res['contacts'][i].mobile == data[j].mobile) {
                                data[j].displayName = res['contacts'][i].displayName;
                            }
                        }
                    }
                    var islastMessage = [];
                    var islastMessageNot = [];
                    for (var i = 0; i < data.length; ++i) {
                        if (data[i].lastMessage) {
                            islastMessage.push(data[i]);
                        }
                        else {
                            islastMessageNot.push(data[i]);
                        }
                    }
                    data = _this.SortByDate(islastMessage);
                    data = data.concat(islastMessageNot);
                    resolve(data);
                }
                else {
                    resolve(data);
                }
            }).catch(function (err) {
                if (err == 20) {
                    _this.platform.exitApp();
                }
                resolve(data);
            });
        });
    };
    ChatsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-chats',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/chats/chats.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      TapAlly\n    </ion-title>\n    <div class="header-wrap" *ngIf="selectContact != true">\n\n      <ion-buttons left>\n        <button ion-button icon-only menuToggle>\n          <ion-icon name=\'menu\'></ion-icon>\n        </button>\n      </ion-buttons>\n      <div class="flex-end">\n        <ion-buttons end>\n          <button ion-button icon-only (click)="sendToBroadcastPage()">\n            <ion-icon name="megaphone"   class="megaphone"></ion-icon>\n          </button>\n\n          <button ion-button icon-only (click)="presentPopover($event)">\n            <ion-icon name="notifications" class="notification"></ion-icon>\n            <ion-badge class="notification" color="danger" *ngIf="requestcounter != null">{{requestcounter}}</ion-badge>\n          </button>\n          <button class="popover-btn hide" ion-button icon-only (click)="viewPopover($event)">\n            <ion-icon ios="md-more" md="md-more"></ion-icon>\n          </button>\n        </ion-buttons>\n      </div>\n    </div>\n    <div class="select-wrap" *ngIf="selectContact == true">\n      <div class="flex-start">\n        <ion-buttons start class="left-arrow">\n          <button start ion-button icon-only (click)="closeButtonClick()">\n            <ion-icon ios="md-close" md="md-close"></ion-icon>\n          </button>\n        </ion-buttons>\n        <div class="select-title" text-left left *ngIf="selectCounter > 0">\n          {{selectCounter}} Selected\n        </div>\n      </div>\n      <div class="flex-end">\n        <button ion-button icon-only (click)="deleteAllMessage()">\n          <ion-icon ios="md-trash" md="md-trash"></ion-icon>\n        </button>\n        <!-- <button ion-button icon-only (click)="archiveChat()">\n            <ion-icon ios="md-archive" md="md-archive"></ion-icon>\n          </button> -->\n      </div>\n    </div>\n  </ion-navbar>\n  <!--\n  <ion-searchbar class="border-bottom" placeholder="Search" (ionInput)="searchuser($event)"></ion-searchbar>\n-->\n</ion-header>\n\n<ion-content padding>\n\n  <ion-card *ngIf="false && showPaidSign">\n    <ion-card-content>\n       You have not paid yet for business. Please pay\n    </ion-card-content>\n  </ion-card>\n\n\n  <ion-refresher (ionRefresh)="refreshPage($event)">\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="Pull to refresh" refreshingSpinner="circles"\n      refreshingText="Refreshing...">\n    </ion-refresher-content>\n  </ion-refresher>\n\n\n  <div text-center padding *ngIf="isNoRecord">No record found!</div>\n  <ion-list  >\n\n    <ng-container *ngIf="allChatListing.length > 0">\n      <div *ngFor="let item of allChatListing" (press)="popoveroncontact(item,$event)" class="list {{item.whitelabelcss}}">\n        <ng-container *ngIf="item.gpflag == 0" class="list ">\n          <ion-item  *ngIf="false && item.whitelabelcss" class="sponseredbusines">\n                 Sponsered Business\n          </ion-item>\n          <ion-item *ngIf="item.isActive == 1 " class="list-item"\n            [ngClass]="{ \'select-contact\': item.selection}">\n            <ion-avatar item-start class="avtar-class">\n              <img *ngIf="item.isBlock == false" class="avtar-img-list" (click)="viewPofile(item.photoURL || \'assets/imgs/user.png\',item.displayName )"\n                src="{{item.photoURL || \'assets/imgs/user.png\'}}">\n              <img *ngIf="item.isBlock == true" class="avtar-img-list" (click)="viewPofile(\'assets/imgs/user.png\',item.displayName )"\n                src="{{\'assets/imgs/user.png\'}}">\n            </ion-avatar>\n\n            <ion-label class="list " (click)="buddychat(item,$event,item.unreadmessage)">\n              <b class="list">{{item.displayName}}</b>\n              <br>\n              <ng-container *ngIf="item.lastMessage" class="list">\n                <span *ngIf="item.lastMessage.type == \'message\'" class="lastMSG">{{item.lastMessage.message}}</span>\n                <span *ngIf="item.lastMessage.type == \'image\'" class="lastMSG">\n                  <ion-icon name="camera" class="list"></ion-icon> Photo\n                </span>\n                <span *ngIf="item.lastMessage.type == \'contact\'" class="lastMSG">\n                  <ion-icon name="contact" class="list"></ion-icon> Contact\n                </span>\n                <span *ngIf="item.lastMessage.type == \'audio\'" class="lastMSG">\n                  <ion-icon name="musical-note" class="list"></ion-icon> Audio\n                </span>\n                <span *ngIf="item.lastMessage.type == \'document\'" class="lastMSG">\n                  <ion-icon name="document" class="list"></ion-icon> document\n                </span>\n                <span *ngIf="item.lastMessage.type == \'location\'" class="lastMSG">\n                  <ion-icon name="locate" class="list"></ion-icon> location\n                </span>\n              </ng-container>\n            </ion-label>\n\n            <div class="endItem btnsonthelast " item-end outline>\n                <button ion-button (click)="referralSendBackward(item)" clear item-start> <ion-icon class="referralicon"  name="arrow-dropleft-circle"></ion-icon> </button>\n                <button  ion-button (click)="referralSendForward(item)" clear  item-end><ion-icon class="referralicon"  name="arrow-dropright-circle"></ion-icon></button>\n            </div>\n\n            <div class="endItem hide" item-end outline>\n              <span *ngIf="item.lastMessage" class="list">\n                <p *ngIf="(today |  date:\'dd/MM/yyyy\') != item.lastMessage.dateofmsg" class="list">\n                  {{item.lastMessage.dateofmsg}}</p>\n                <p *ngIf="(today |  date:\'dd/MM/yyyy\') == item.lastMessage.dateofmsg" class="list">\n                  {{item.lastMessage.timeofmsg}}</p>\n              </span>\n              <button class="list">\n                <ion-badge item-end *ngIf="item.unreadmessage " class="list">{{item.unreadmessage}}</ion-badge>\n              </button>\n            </div>\n\n\n          </ion-item>\n        </ng-container>\n        <ion-item *ngIf="item.gpflag == 1" class="list" [ngClass]="{ \'select-contact\': item.selection}">\n          <ion-avatar item-start class="avtar-class">\n            <img class="list" src="{{item.groupimage || \'assets/imgs/user.png\'}}">\n          </ion-avatar>\n          <ion-label class="list" (click)="openchat(item, $event)" >\n            <b class="list">{{item.groupName}} </b>\n            <br>\n\n            <ng-container *ngIf="item.lastmsg" class="list">\n\n              <span *ngIf="item.lastmsg !== \'image\' && item.lastmsg !== \'contact\' && item.lastmsg !== \'audio\' && item.lastmsg !== \'document\' && item.lastmsg !== \'location\' "\n                class="lastMSG">{{item.lastmsg}}</span>\n              <span *ngIf="item.lastmsg == \'image\'" class="lastMSG">\n                <ion-icon name="camera" class="list"></ion-icon> Photo\n              </span>\n              <span *ngIf="item.lastmsg == \'contact\'" class="lastMSG">\n                <ion-icon name="contact" class="list"></ion-icon> Contact\n              </span>\n              <span *ngIf="item.lastmsg == \'audio\'" class="lastMSG">\n                <ion-icon name="musical-note" class="list"></ion-icon> Audio\n              </span>\n              <span *ngIf="item.lastmsg == \'document\'" class="lastMSG">\n                <ion-icon name="document" class="list"></ion-icon> document\n              </span>\n              <span *ngIf="item.lastmsg == \'location\'" class="lastMSG">\n                <ion-icon name="locate" class="list"></ion-icon> location\n              </span>\n            </ng-container>\n\n          </ion-label>\n            <div class="endItem btnsonthelast " item-end outline>\n                <button ion-button (click)="referralSendBackward(item)" clear item-start> <ion-icon class="referralicon"  name="arrow-dropleft-circle"></ion-icon> </button>\n                <button  ion-button (click)="referralSendForward(item)" clear  item-end><ion-icon class="referralicon"  name="arrow-dropright-circle"></ion-icon></button>\n            </div>\n          <div class="endItem hide" item-end outline>\n            <span *ngIf="item.lastmsg" class="list">\n              <p class="list" *ngIf="(today |  date:\'dd/MM/yyyy\') != item.dateofmsg"> {{item.dateofmsg}}</p>\n              <p class="list" *ngIf="(today |  date:\'dd/MM/yyyy\') == item.dateofmsg"> {{item.timeofmsg}}</p>\n            </span>\n          </div>\n        </ion-item>\n\n      </div>\n\n    </ng-container>\n    <div *ngIf="isData" padding text-center>\n      No chat found!\n    </div>\n  </ion-list>\n\n\n  <ion-fab bottom right>\n    <button ion-fab (click)="addbuddy()">\n      <ion-icon ios="md-person-add" md="md-person-add"></ion-icon>\n    </button>\n  </ion-fab>\n\n</ion-content>\n\n\n<ion-footer *ngIf="whitelabelObj.buddy_uid && this.whitelabelObj.buddy_uid!=\'undefined\'">\n  <div style="border:0px solid #ddd;">\n    <ion-item style="height:20px;padding:0px 10px;background-color:#dedede;color:#999;">\n      Your Sponsered Business\n    </ion-item>\n    <ion-item   class="sponseredbusines_bottom" >\n      <h2><span class="text_color_primary strongwords">{{whitelabelObj.buddy_displayName}}</span></h2>\n      <p>{{whitelabelObj.sponseredbusines_businessName}}</p>\n      <div class="endItem btnsonthelast " item-end outline>\n          <button ion-button (click)="referralSendBackward(whitelabelObj)" clear item-start> <ion-icon class="referralicon"  name="arrow-dropleft-circle"></ion-icon> </button>\n          <button  ion-button (click)="referralSendForward(whitelabelObj)" clear  item-end><ion-icon class="referralicon"  name="arrow-dropright-circle"></ion-icon></button>\n      </div>\n    </ion-item>\n  </div>\n</ion-footer>\n<ion-footer *ngIf="false && !whitelabelObj.buddy_uid && this.whitelabelObj.buddy_uid!=\'undefined\'">\n  <!-- DISABLED FOR NOW :  Will be shown to those who have invited_by set but their invetee (person who invited them) did not paid -->\n  <div style="border:0px solid #ddd;">\n    <ion-item  (click)="sendto_invite()" style="padding-left:10px;background-color:#dedede;color:#999;">\n      Whitelabel and Outreach your clients\n    </ion-item>\n  </div>\n</ion-footer>\n<!-- This does not work\n<ion-footer *ngIf="!whitelabelObj.buddy_uid && !iAmBusinessOwner">\n  <div style="border:0px solid #ddd;">\n    <ion-item (click)="sendto_registerbusiness()" style="background-color:#dedede;color:#999;">\n      Register Your Business\n    </ion-item>\n  </div>\n</ion-footer>\n-->\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/chats/chats.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_7__providers_fcm_fcm__["a" /* FcmProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_6__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_contact_contact__["a" /* ContactProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_requests_requests__["a" /* RequestsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_chat_chat__["a" /* ChatProvider */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["y" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_9__providers_imagehandler_imagehandler__["a" /* ImagehandlerProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_8__providers_groups_groups__["a" /* GroupsProvider */]])
    ], ChatsPage);
    return ChatsPage;
}());

//# sourceMappingURL=chats.js.map

/***/ }),

/***/ 950:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatsPageModule", function() { return ChatsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__chats__ = __webpack_require__(1021);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ChatsPageModule = (function () {
    function ChatsPageModule() {
    }
    ChatsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__chats__["a" /* ChatsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__chats__["a" /* ChatsPage */]),
            ],
        })
    ], ChatsPageModule);
    return ChatsPageModule;
}());

//# sourceMappingURL=chats.module.js.map

/***/ })

});
//# sourceMappingURL=59.js.map