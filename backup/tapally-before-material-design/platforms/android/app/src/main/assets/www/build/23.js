webpackJsonp([23],{

/***/ 1058:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReferralInfopagePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ReferralInfopagePage = (function () {
    function ReferralInfopagePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.iamABusinessOwner = false;
    }
    ReferralInfopagePage.prototype.ionViewWillEnter = function () {
        if (localStorage.getItem('business_created')) {
            this.iamABusinessOwner = true;
        }
        else {
            this.iamABusinessOwner = false;
        }
    };
    ReferralInfopagePage.prototype.sendreferraldirectly = function () {
        this.navCtrl.push("ReferralPage");
    };
    ReferralInfopagePage.prototype.sendToRegisterBuesiness = function () {
        this.navCtrl.push("RegisterbusinessPage");
    };
    ReferralInfopagePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ReferralInfopagePage');
    };
    ReferralInfopagePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-referral-infopage',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/referral-infopage/referral-infopage.html"*/'<ion-header>\n  <ion-navbar >\n    <ion-title>\n      Exchange Referral\n    </ion-title>\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name=\'menu\'></ion-icon>\n      </button>\n    </ion-buttons>\n    <div class="flex-end-notif">\n      <ion-buttons end>\n          <button ion-button icon-only (click)="presentPopover($event)">\n            <ion-icon name="notifications" class="notification"></ion-icon>\n            <ion-badge class="notification" color="danger" *ngIf="requestcounter != null">{{requestcounter}}</ion-badge>\n          </button>\n      </ion-buttons>\n    </div>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n\n<div *ngIf="!ever_referral_sent">\n        <div class="pull-center" style="margin:20px 0px;">\n          <h4 class="" >Exchange Referrals with your friends and earn loyality</h4>\n          <button (click)="sendreferraldirectly()" ion-button color="primary" block>Get Started Now</button>\n          <button *ngIf="iamABusinessOwner" clear (click)="sendToRegisterBuesiness()" ion-button color="primary" block>Business Owner? Automate your referrals</button>\n        </div>\n</div>\n\n</ion-content>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/referral-infopage/referral-infopage.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */]])
    ], ReferralInfopagePage);
    return ReferralInfopagePage;
}());

//# sourceMappingURL=referral-infopage.js.map

/***/ }),

/***/ 987:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReferralInfopagePageModule", function() { return ReferralInfopagePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__referral_infopage__ = __webpack_require__(1058);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ReferralInfopagePageModule = (function () {
    function ReferralInfopagePageModule() {
    }
    ReferralInfopagePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__referral_infopage__["a" /* ReferralInfopagePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__referral_infopage__["a" /* ReferralInfopagePage */]),
            ],
        })
    ], ReferralInfopagePageModule);
    return ReferralInfopagePageModule;
}());

//# sourceMappingURL=referral-infopage.module.js.map

/***/ })

});
//# sourceMappingURL=23.js.map