webpackJsonp([35],{

/***/ 1047:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuBlockPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_user_user__ = __webpack_require__(70);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MenuBlockPage = (function () {
    function MenuBlockPage(modalCtrl, navCtrl, navParams, userservice, viewCtrl) {
        this.modalCtrl = modalCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.userservice = userservice;
        this.viewCtrl = viewCtrl;
        this.buddyStatus = false;
        this.buddy = this.navParams.get('buddy');
        if (this.buddy) {
            this.getbuddysatus(this.buddy);
        }
    }
    MenuBlockPage.prototype.userProfileBlock = function () {
        var _this = this;
        this.userservice.blockUser(this.buddy).then(function (res) {
            if (res) {
                _this.viewCtrl.dismiss();
            }
        });
    };
    MenuBlockPage.prototype.userProfileunBlock = function () {
        var _this = this;
        this.userservice.unblockUser(this.buddy).then(function (res) {
            if (res) {
                _this.viewCtrl.dismiss();
            }
        });
    };
    MenuBlockPage.prototype.getbuddysatus = function (buddy) {
        var _this = this;
        this.userservice.getstatus(buddy).then(function (res) {
            _this.buddyStatus = res;
        });
    };
    MenuBlockPage.prototype.searchMessage = function (myEvent) {
        var _this = this;
        var searchChatModal = this.modalCtrl.create('SearchChatsPage');
        searchChatModal.present({
            ev: myEvent
        });
        searchChatModal.onDidDismiss(function () {
            _this.viewCtrl.dismiss();
        });
    };
    MenuBlockPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-menu-block',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/menu-block/menu-block.html"*/'<ion-item (click)="userProfileBlock()" *ngIf="!buddyStatus" no-line>\n	<ion-label no-line >Block</ion-label>\n</ion-item>\n<ion-item (click)="userProfileunBlock()" *ngIf="buddyStatus" no-line>\n	<ion-label no-line >Unblock</ion-label>\n</ion-item>\n<ion-item (click)="searchMessage($event)" no-line>\n	<ion-label no-line >Search</ion-label>\n</ion-item>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/menu-block/menu-block.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["B" /* ViewController */]])
    ], MenuBlockPage);
    return MenuBlockPage;
}());

//# sourceMappingURL=menu-block.js.map

/***/ }),

/***/ 976:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuBlockPageModule", function() { return MenuBlockPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__menu_block__ = __webpack_require__(1047);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MenuBlockPageModule = (function () {
    function MenuBlockPageModule() {
    }
    MenuBlockPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__menu_block__["a" /* MenuBlockPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__menu_block__["a" /* MenuBlockPage */]),
            ],
        })
    ], MenuBlockPageModule);
    return MenuBlockPageModule;
}());

//# sourceMappingURL=menu-block.module.js.map

/***/ })

});
//# sourceMappingURL=35.js.map