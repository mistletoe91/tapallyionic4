webpackJsonp([27],{

/***/ 1054:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PickContactToPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_user_user__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_contact_contact__ = __webpack_require__(498);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_requests_requests__ = __webpack_require__(495);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_loading_loading__ = __webpack_require__(240);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_chat_chat__ = __webpack_require__(497);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_sms_sms__ = __webpack_require__(500);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_fcm_fcm__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_storage__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_groups_groups__ = __webpack_require__(496);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__app_app_angularfireconfig__ = __webpack_require__(142);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var PickContactToPage = (function () {
    function PickContactToPage(events, platform, storage, loadingProvider, contactProvider, userservice, navCtrl, navParams, alertCtrl, requestservice, chatservice, zone, smsservice, fcm, modalCtrl, toastCtrl, groupservice) {
        this.events = events;
        this.platform = platform;
        this.storage = storage;
        this.loadingProvider = loadingProvider;
        this.contactProvider = contactProvider;
        this.userservice = userservice;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.requestservice = requestservice;
        this.chatservice = chatservice;
        this.zone = zone;
        this.smsservice = smsservice;
        this.fcm = fcm;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.groupservice = groupservice;
        this.contactList = [];
        this.allregisteUserData = [];
        this.metchContact = [];
        this.mainArray = [];
        this.tempArray = [];
        this.userpic = __WEBPACK_IMPORTED_MODULE_11__app_app_angularfireconfig__["c" /* env */].userPic;
        this.isData = false;
        this.reqStatus = false;
        this.isInviteArray = [];
        this.newrequest = {};
        this.temparr = [];
        this.filteredusers = [];
        this.userId = localStorage.getItem('userUID');
        this.friend_to_forward_uid = navParams.get('friend_to_forward_uid');
        this.friend_to_forward_displayName = navParams.get('friend_to_forward_displayName');
        //this.friend_to_forward_catId = navParams.get('friend_to_forward_catId');
        //this.friend_to_forward_catName = navParams.get('friend_to_forward_catName');
        this.cntCounter = 0;
    }
    PickContactToPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.requestservice.getmyfriends();
        this.myfriends = [];
        this.events.subscribe('friends', function () {
            _this.myfriends = [];
            _this.myfriends = _this.requestservice.myfriends;
        });
    };
    PickContactToPage.prototype.pickContactExecute = function (buddy, isTapAllyMember) {
        //Tapally Member so send push notification
        /*
                          buddy : Person to whome we need to send this.friend_to_forward_uid's contact
        */
        for (var key = 0; key < this.myfriends.length; key++) {
            //console.log (this.myfriends[key].displayName)
            //Find out who is receipient
            if (this.friend_to_forward_uid == this.myfriends[key].uid) {
                //send to the one who asked for referral
                this.newmessage = 'Hello I am sending referral to you : ' + this.myfriends[key].displayName + " (" + this.myfriends[key].mobile + ")";
                this.addmessage(buddy, this.myfriends[key], this.myfriends[key]);
                //also tell the one who got referred
                if (isTapAllyMember) {
                    //referral is member of Tapally
                    this.newmessage = 'Hello I send your referral to ' + buddy.displayName + " (" + buddy.mobile + ")";
                    this.addmessage(this.myfriends[key], buddy, this.myfriends[key]);
                }
                else {
                    //referral is not member of Tapally
                    //This should not be possible
                    //mytodo : incorporate this feature
                } //endif
                break;
            }
        } //end for
    };
    PickContactToPage.prototype.sendToNextPage = function (originalChat) {
        this.cntCounter++;
        if (this.cntCounter >= 2) {
            this.cntCounter = 0;
            this.chatservice.initializebuddy(originalChat);
            this.navCtrl.push('BuddychatPage');
        }
    };
    //here buddy is receipient and referral is contact being sent
    PickContactToPage.prototype.addmessage = function (buddy, referral, originalChat) {
        var _this = this;
        console.log("aaaa");
        //Send Push notification : mytodo check if the buddy is online or not. if online then no need to send push notification
        var newMessage = 'You have received a message';
        this.fcm.sendNotification(buddy, newMessage, 'chatpage');
        //Send chat message
        this.chatservice.addnewmessageBroadcast(this.newmessage, 'message', buddy, 0).then(function () {
            _this.newmessage = '';
            _this.sendToNextPage(originalChat);
        });
    };
    PickContactToPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.loadingProvider.presentLoading();
        this.storage.get('isInvitedContact').then(function (res) {
            if (res != null) {
                _this.isInviteArray = res;
            }
        });
        this.userservice.getallusers().then(function (res) {
            _this.allregisteUserData = res;
            var userID = localStorage.getItem('userUID');
            var _loop_1 = function (i) {
                _this.requestservice.pendingetmyrequests(res[i].uid).then(function (data) {
                    for (var q = 0; q < data.length; q++) {
                        if (data[q].uid == userID) {
                            _this.allregisteUserData[i].disc = _this.allregisteUserData[i].disc.replace(new RegExp('<br />', 'g'), '');
                            _this.allregisteUserData[i].status = "pending";
                        }
                        else {
                            _this.allregisteUserData[i].status = null;
                        }
                    }
                });
            };
            for (var i = 0; i < res.length; i++) {
                _loop_1(i);
            }
            _this.requestservice.getmyfriends();
            _this.myfriends = [];
            _this.events.subscribe('friends', function () {
                _this.myfriends = [];
                _this.myfriends = _this.requestservice.myfriends;
                _this.getAllDeviceContacts(_this.myfriends);
            });
        });
    };
    // Get All contact from the device
    PickContactToPage.prototype.getAllDeviceContacts = function (myfriends) {
        var _this = this;
        this.contactProvider.getSimJsonContacts()
            .then(function (res) {
            _this.loadingProvider.dismissMyLoading();
            var ref = localStorage.getItem("updated");
            if (ref == "show") {
                localStorage.removeItem("updated");
                var toast = _this.toastCtrl.create({
                    message: 'Contacts updated successfully.',
                    duration: 2500,
                    position: 'bottom'
                });
                toast.present();
            }
            if (res['contacts'].length > 0) {
                var newConatctList = [];
                newConatctList = res['contacts'];
                _this.contactList = _this.removeDuplicates(newConatctList, 'mobile');
                for (var i = 0; i < _this.contactList.length; i++) {
                    _this.contactList[i].isBlock = false;
                    if (_this.contactList[i].displayName != undefined) {
                        _this.contactList[i].displayName = _this.contactList[i].displayName.trim();
                    }
                }
                _this.metchContact = _this.allregisteUserData.filter(function (item) {
                    return _this.contactList.some(function (data) { return item.mobile == data.mobile; });
                });
                for (var i = 0; i < _this.contactList.length; i++) {
                    for (var j = 0; j < _this.metchContact.length; j++) {
                        if (_this.contactList[i] != undefined && _this.metchContact[j] != undefined) {
                            if (_this.contactList[i].mobile == _this.metchContact[j].mobile) {
                                _this.metchContact[j].isUser = "1";
                                _this.metchContact[j].isBlock = _this.contactList[i].isBlock;
                                _this.metchContact[j].displayName = _this.contactList[i].displayName;
                            }
                        }
                    }
                }
                for (var i = 0; i < _this.metchContact.length; i++) {
                    for (var j = 0; j < myfriends.length; j++) {
                        if (_this.metchContact[i] != undefined && myfriends[j] != undefined) {
                            if (_this.metchContact[i].mobile == myfriends[j].mobile) {
                                _this.metchContact[i].status = 'friend';
                                _this.metchContact[i].isBlock = myfriends[j].isBlock;
                            }
                        }
                    }
                }
                _this.contactList = _this.contactList.filter(function (o1) {
                    return !_this.metchContact.some(function (o2) {
                        return o1.mobile === o2.mobile; // assumes unique id
                    });
                });
                _this.metchContact = _this.metchContact.sort(function (a, b) {
                    if (a.displayName != undefined) {
                        var nameA = a.displayName.toUpperCase(); // ignore upper and lowercase
                    }
                    if (b.displayName != undefined) {
                        var nameB = b.displayName.toUpperCase(); // ignore upper and lowercase
                    }
                    if (nameA < nameB) {
                        return -1;
                    }
                    if (nameA > nameB) {
                        return 1;
                    }
                    // names must be equal
                    return 0;
                });
                // sort by name
                _this.contactList = _this.contactList.sort(function (a, b) {
                    if (a.displayName != undefined) {
                        var nameA = a.displayName.toUpperCase(); // ignore upper and lowercase
                    }
                    if (b.displayName != undefined) {
                        var nameB = b.displayName.toUpperCase(); // ignore upper and lowercase
                    }
                    if (nameA < nameB) {
                        return -1;
                    }
                    if (nameA > nameB) {
                        return 1;
                    }
                    // names must be equal
                    return 0;
                });
                var myArray_1 = _this.metchContact.concat(_this.contactList);
                if (_this.isInviteArray.length > 0) {
                    for (var i = 0; i < myArray_1.length; i++) {
                        for (var j = 0; j < _this.isInviteArray.length; j++) {
                            if (myArray_1[i].mobile == _this.isInviteArray[j].mobile) {
                                myArray_1[i].isdisable = true;
                            }
                            else {
                                myArray_1[i].isdisable = false;
                            }
                        }
                    }
                }
                _this.zone.run(function () {
                    _this.mainArray = myArray_1;
                });
                _this.loadingProvider.dismissMyLoading();
                _this.tempArray = _this.mainArray;
            }
            else {
                _this.isData = true;
            }
        }).catch(function (err) {
            _this.loadingProvider.dismissMyLoading();
            if (err == 20) {
                _this.platform.exitApp();
            }
        });
    };
    PickContactToPage.prototype.removeDuplicates = function (originalArray, prop) {
        var newArray = [];
        var lookupObject = {};
        originalArray.forEach(function (item, index) {
            lookupObject[originalArray[index][prop]] = originalArray[index];
        });
        Object.keys(lookupObject).forEach(function (element) {
            newArray.push(lookupObject[element]);
        });
        return newArray;
    };
    PickContactToPage.prototype.ionViewWillLeave = function () {
        this.groupservice.getmygroups();
        this.requestservice.getmyfriends();
        this.loadingProvider.dismissMyLoading();
    };
    PickContactToPage.prototype.initializeContacts = function () {
        this.mainArray = this.tempArray;
    };
    PickContactToPage.prototype.refreshPage = function () {
        this.ionViewDidLoad();
        localStorage.setItem('updated', 'show');
    };
    PickContactToPage.prototype.searchuser = function (ev) {
        this.initializeContacts();
        var val = ev.target.value;
        if (val && val.trim() != '') {
            this.mainArray = this.mainArray.filter(function (item) {
                if (item.displayName != undefined) {
                    return (item.displayName.toLowerCase().indexOf(val.toLowerCase()) > -1);
                }
            });
        }
    };
    PickContactToPage.prototype.inviteReq = function (recipient) {
        var alert = this.alertCtrl.create({
            title: 'Invitation',
            subTitle: 'Invitation already sent to ' + recipient.displayName + '.',
            buttons: ['Ok']
        });
        alert.present();
    };
    PickContactToPage.prototype.sendreq = function (recipient) {
        var _this = this;
        this.reqStatus = true;
        this.newrequest.sender = localStorage.getItem('userUID');
        this.newrequest.recipient = recipient.uid;
        if (this.newrequest.sender === this.newrequest.recipient)
            alert('You are your friend always');
        else {
            var successalert_1 = this.alertCtrl.create({
                title: 'Request sent',
                subTitle: 'Your request has been sent to ' + recipient.displayName + '.',
                buttons: ['ok']
            });
            var userName_1;
            this.userservice.getuserdetails().then(function (res) {
                userName_1 = res.displayName;
                var newMessage = userName_1 + " has sent you friend request.";
                _this.fcm.sendNotification(recipient, newMessage, 'sendreq');
            });
            this.requestservice.sendrequest(this.newrequest).then(function (res) {
                if (res.success) {
                    _this.reqStatus = false;
                    successalert_1.present();
                    var sentuser = _this.mainArray.indexOf(recipient);
                    _this.mainArray[sentuser].status = "pending";
                }
            }).catch(function (err) {
            });
        }
    };
    PickContactToPage.prototype.buddychat = function (buddy) {
        if (buddy.status == 'friend') {
            this.chatservice.initializebuddy(buddy);
            this.navCtrl.push('BuddychatPage');
        }
    };
    PickContactToPage.prototype.sendSMSCustomMsg = function (item, msg) {
        var _this = this;
        this.loadingProvider.presentLoading();
        this.smsservice.sendSmsCustomMsg(item.mobile, msg).then(function (res) {
            _this.loadingProvider.dismissMyLoading();
        }).catch(function (err) {
            _this.loadingProvider.dismissMyLoading();
            var alert = _this.alertCtrl.create({
                title: 'Message Send',
                subTitle: 'Error for sending message to ' + item.displayName + '.',
                buttons: ['Ok']
            });
            alert.present();
        });
    }; //end function
    PickContactToPage.prototype.sendSMS = function (item) {
        var _this = this;
        this.loadingProvider.presentLoading();
        this.smsservice.sendSms(item.mobile).then(function (res) {
            _this.loadingProvider.dismissMyLoading();
            if (res) {
                for (var i = 0; i < _this.mainArray.length; ++i) {
                    if (_this.mainArray[i].mobile == item.mobile) {
                        _this.mainArray[i].isdisable = true;
                    }
                }
                var alert_1 = _this.alertCtrl.create({
                    title: 'Message Send',
                    subTitle: 'Your message has been sent to ' + item.displayName + '.',
                    buttons: ['Ok']
                });
                alert_1.present();
                _this.isInviteArray.push(item);
                _this.storage.set('isInvitedContact', _this.isInviteArray);
            }
            else {
                var alert_2 = _this.alertCtrl.create({
                    title: 'Message Send',
                    subTitle: 'Error for sending message to ' + item.displayName + '.',
                    buttons: ['Ok']
                });
                alert_2.present();
            }
        }).catch(function (err) {
            _this.loadingProvider.dismissMyLoading();
            var alert = _this.alertCtrl.create({
                title: 'Message Send',
                subTitle: 'Error for sending message to ' + item.displayName + '.',
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    PickContactToPage.prototype.addnewContact = function (myEvent) {
        var profileModal = this.modalCtrl.create("AddContactPage");
        profileModal.present({
            ev: myEvent
        });
        profileModal.onDidDismiss(function (pages) {
        });
    };
    PickContactToPage.prototype.goBack = function () {
        this.navCtrl.setRoot('ChatsPage');
    };
    PickContactToPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-pick-contact-to',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/pick-contact-to/pick-contact-to.html"*/'<ion-header>\n  <ion-navbar color="darkblue" >\n    <div class="header-wrap">\n        <div class="flex-start">\n          <!-- <ion-buttons class="left-arrow" left>\n              <button ion-button icon-only (click)="goBack()">\n                <ion-icon ios="md-arrow-round-back" md="md-arrow-round-back"></ion-icon>\n              </button>\n          </ion-buttons> -->\n          <div class="header-title">Forward Referral</div>\n        </div>\n        <div class="flex-end">\n          <ion-buttons end>\n              <button ion-button icon-only (click)="refreshPage()">\n                <ion-icon ios="md-refresh" md="md-refresh"></ion-icon>\n              </button>\n              <button ion-button icon-only (click)="addnewContact($event)">\n                <ion-icon ios="md-person-add" md="md-person-add"></ion-icon>\n              </button>\n          </ion-buttons>\n        </div>\n    </div>\n  </ion-navbar>\n  <ion-searchbar placeholder="Search" (ionInput)="searchuser($event)"></ion-searchbar>\n  <p style="color:#000;">Forward {{friend_to_forward_displayName}}\'s referral to your contact\'</p>\n</ion-header>\n<ion-content>\n <ion-list>\n   <ion-item-group>\n     <ion-item-divider></ion-item-divider>\n        <div *ngFor="let item of mainArray"  >\n          <div  class="border-bottom" *ngIf="userId != item.uid">\n            <ion-item no-lines class="contact-list">\n                <div (click)="buddychat(item)">\n                  <ion-avatar item-start>\n                  <!-- <ion-icon name="person"></ion-icon> -->\n                    <img *ngIf="item.isBlock == false" src="{{item.photoURL || userpic}}">\n                    <img *ngIf="item.isBlock == true" src="{{\'assets/imgs/user.png\' || userpic }}">\n                  </ion-avatar>\n                  <div class="item-info">\n                    <h2 *ngIf="item.displayName">{{item.displayName}}</h2>\n                    <p *ngIf="item.disc"  [innerHTML]="item.disc" ></p>\n                    <!-- <ng-container *ngIf="item.isUser == \'1\'"> -->\n                  </div>\n\n                </div>\n\n                <button item-end outline class="btn-plain" (click)="pickContactExecute(item,true)"\n                *ngIf="item.status != null && item.status != \'pending\' && item.isUser == \'1\' && item.isUser != \'0\' && !item.isdisable"\n                >\n                   <ion-icon end item-end name=\'arrow-dropright-circle\'> </ion-icon>\n                </button>\n\n                <button item-end outline class="btn-plain" [disabled]="reqStatus" (click)="pickContactExecute(item,true)" *ngIf="item.status == null && item.isUser == \'1\'">\n                  <ion-icon name="person-add"></ion-icon>Add\n                </button>\n                <button item-end outline class="btn-plain" (click)="pickContactExecute(item,true)" *ngIf="item.status == \'pending\' && item.isUser == \'1\'">\n                  <ion-icon name="time"></ion-icon>Pending\n                </button>\n                <!-- <button item-end outline class="btn-plain" (click)="buddychat(item)" *ngIf="(item.status != null && item.status != \'pending\' && item.isUser == \'1\')">\n                  Go to Chat\n                </button> -->\n                <!-- <button item-end outline class="btn-plain" *ngIf="item.status == \'friend\' && item.isUser == \'1\'">\n                  <ion-icon name="time"></ion-icon>Chat\n                </button> -->\n              <!-- </ng-container> -->\n\n\n\n                <button item-end outline class="btn-plain" (click)="pickContactExecute(item,false)" *ngIf="item.isUser == \'0\' && !item.isdisable">\n                  <ion-icon name="add"></ion-icon>Invite\n                </button>\n\n                <button item-end outline class="btn-plain" (click)="pickContactExecute(item,false)" *ngIf="item.isdisable">\n                  <ion-icon name="add"></ion-icon>Invited\n                </button>\n\n            </ion-item>\n        </div>\n        </div>\n   </ion-item-group>\n </ion-list>\n <div *ngIf="isData" padding-horizontal>\n        No contacts found!\n </div>\n</ion-content>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/pick-contact-to/pick-contact-to.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_5__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_contact_contact__["a" /* ContactProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_requests_requests__["a" /* RequestsProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_chat_chat__["a" /* ChatProvider */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */],
            __WEBPACK_IMPORTED_MODULE_7__providers_sms_sms__["a" /* SmsProvider */],
            __WEBPACK_IMPORTED_MODULE_8__providers_fcm_fcm__["a" /* FcmProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["z" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_10__providers_groups_groups__["a" /* GroupsProvider */]])
    ], PickContactToPage);
    return PickContactToPage;
}());

//# sourceMappingURL=pick-contact-to.js.map

/***/ }),

/***/ 983:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PickContactToPageModule", function() { return PickContactToPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pick_contact_to__ = __webpack_require__(1054);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PickContactToPageModule = (function () {
    function PickContactToPageModule() {
    }
    PickContactToPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__pick_contact_to__["a" /* PickContactToPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__pick_contact_to__["a" /* PickContactToPage */]),
            ],
        })
    ], PickContactToPageModule);
    return PickContactToPageModule;
}());

//# sourceMappingURL=pick-contact-to.module.js.map

/***/ })

});
//# sourceMappingURL=27.js.map