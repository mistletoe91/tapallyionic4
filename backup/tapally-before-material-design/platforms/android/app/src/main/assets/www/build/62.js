webpackJsonp([62],{

/***/ 1017:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BroadcastPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_chat_chat__ = __webpack_require__(497);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_user_user__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_fcm_fcm__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_requests_requests__ = __webpack_require__(495);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_loading_loading__ = __webpack_require__(240);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_forms__ = __webpack_require__(40);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var BroadcastPage = (function () {
    function BroadcastPage(fcm, loading, requestservice, fb, events, chatservice, userservice, navCtrl, toastCtrl, navParams) {
        this.fcm = fcm;
        this.loading = loading;
        this.requestservice = requestservice;
        this.fb = fb;
        this.events = events;
        this.chatservice = chatservice;
        this.userservice = userservice;
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.navParams = navParams;
        this.myrequests = [];
        this.requestcounter = null;
        this.newmessage = '';
        this.debugMe = true;
        this.showPaidSign = false;
        this.doIhavebusinessAccount = false;
        this.isData = false;
        this.authForm = this.fb.group({
            'message': [null, __WEBPACK_IMPORTED_MODULE_7__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_7__angular_forms__["f" /* Validators */].required])]
        });
        this.message = this.authForm.controls['message'];
    }
    BroadcastPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        console.log("TESTING LOCAL STORAGE -->");
        this.paidOrNot();
        if (this.debugMe) {
            console.log("businessName checkiing");
            console.log(localStorage.getItem('businessName'));
        }
        if (localStorage.getItem('businessName')) {
            this.doIhavebusinessAccount = true;
        }
        else {
            this.doIhavebusinessAccount = false;
            /*
              this.userservice.getBusinessDetails().then((res)=>{
                if(res){
                  localStorage.setItem('businessName', res['displayName']);
                  localStorage.setItem('bcats', res['bcats']);
                  this.doIhavebusinessAccount = true;
                }
              }).catch(err=>{
              });
              */
        }
        this.requestservice.getmyfriends();
        this.myfriends = [];
        this.events.subscribe('friends', function () {
            _this.myfriends = [];
            _this.myfriends = _this.requestservice.myfriends;
        });
        this.requestservice.getmyrequests();
        this.events.subscribe('gotrequests', function () {
            _this.myrequests = [];
            _this.myrequests = _this.requestservice.userdetails;
            if (_this.myrequests) {
                _this.requestcounter = _this.myrequests.length;
            }
        });
    };
    //KEep it same as chats.ts
    BroadcastPage.prototype.paidOrNot = function () {
        var _this = this;
        this.showPaidSign = false;
        if (this.debugMe) {
            console.log("paidornot checkiing");
            console.log(localStorage.getItem('business_created'));
        }
        if (localStorage.getItem('business_created')) {
            var AcceptableDelayInMicroseconds = 1000; //Before we show warning to business that they have not paid yet
            var timeStamp = Date.now();
            var business_created = parseInt(localStorage.getItem('business_created'));
            if (this.debugMe) {
                console.log("business_created checkiing");
            }
            //Do not check for first few minutes/hours/days ( as specified in AcceptableDelayInMicroseconds )
            //But check every single time afterwards
            if ((business_created + AcceptableDelayInMicroseconds) < timeStamp) {
                var paid_1 = parseInt(localStorage.getItem('paid'));
                if (this.debugMe) {
                    console.log("paid checkiing");
                    console.log(paid_1);
                }
                //check API everytime
                if (true) {
                    if (this.debugMe) {
                        console.log("calling getBusinessDetails");
                    }
                    this.userservice.getBusinessDetails().then(function (res) {
                        //localStorage.setItem('businessName', res['displayName']);
                        //localStorage.setItem('bcats', res['bcats']); 
                        if (!paid_1 || paid_1 <= 0) {
                            console.log("Indeed Business has not paid yet");
                            _this.showPaidSign = true;
                        }
                    }).catch(function (err) {
                    });
                } //endif
            }
            else {
            } //endif
        } //endif
    }; //end function
    BroadcastPage.prototype.broadcast = function () {
        this.newmessage = this.newmessage.trim();
        if (this.newmessage != '') {
        }
        else {
            this.loading.presentToast('Please enter valid message...');
            return;
        }
        this.cntCounter = 0;
        for (var key = 0; key < this.myfriends.length; key++) {
            this.addmessage(this.myfriends[key]);
        } //end for
        //mytodo : remove this from here and just call asyn a web service API to send messages.
        //Now the mobile is doing heavylifting. But ideally the server should do and send to nosql database
        /*
        this.cntCounter = 0;
        this.events.subscribe('friends', () => {
          if (this.requestservice.myfriends.length > 0) {
              console.log ("-->" + this.requestservice.myfriends.length);
              for (var key=0;key<this.requestservice.myfriends.length;key++) {
                 this.buddy = this.requestservice.myfriends[key];
                 console.log (key);
                 //console.log(this.buddy);
                 this.addmessage();
              }//end for
          }
        });
        */
    };
    BroadcastPage.prototype.presentPopover = function (myEvent) {
        this.navCtrl.push("NotificationPage");
        this.requestcounter = this.myrequests.length;
    };
    BroadcastPage.prototype.sendToNextPage = function () {
        this.cntCounter++;
        if (this.cntCounter >= this.myfriends.length) {
            this.cntCounter = 0;
            this.navCtrl.setRoot("BroadcastSubmittedPage");
        }
    };
    BroadcastPage.prototype.sendToBusinessReg = function () {
        this.navCtrl.setRoot("RegisterbusinessPage");
    };
    BroadcastPage.prototype.addmessage = function (buddy) {
        var _this = this;
        this.newmessage = this.newmessage.trim();
        //Send Push notification : mytodo check if the buddy is online or not. if online then no need to send push notification
        var newMessage = 'You have received a message';
        this.fcm.sendNotification(buddy, newMessage, 'chatpage');
        //Send chat message
        this.chatservice.addnewmessageBroadcast(this.newmessage, 'message', buddy, 0).then(function () {
            _this.newmessage = '';
            _this.sendToNextPage();
        });
    };
    BroadcastPage.prototype.ionViewDidLoad = function () {
    };
    BroadcastPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-broadcast',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/broadcast/broadcast.html"*/'<ion-header>\n  <ion-navbar >\n    <ion-title>\n      Broadcast\n    </ion-title>\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name=\'menu\'></ion-icon>\n      </button>\n    </ion-buttons>\n    <div class="flex-end-notif">\n      <ion-buttons end>\n          <button ion-button icon-only (click)="presentPopover($event)">\n            <ion-icon name="notifications" class="notification"></ion-icon>\n            <ion-badge class="notification" color="danger" *ngIf="requestcounter != null">{{requestcounter}}</ion-badge>\n          </button>\n      </ion-buttons>\n    </div>\n  </ion-navbar>\n</ion-header>\n\n\n\n<ion-content padding>\n\n\n    <div class="pull-center givepadding">\n        <h3>Broadcast to your clients</h3>\n        <p>Send branded push notifications with high level of delivery & better engagement than emails, text </p>\n\n\n    </div>\n\n<div *ngIf="!doIhavebusinessAccount">\n  <h4>This feature works best with your business account. Let\'s get started </h4>\n  <button ion-button (click)="sendToBusinessReg()" color="primary" block>Register Your Business</button>\n</div>\n\n<ion-card *ngIf="false && doIhavebusinessAccount && showPaidSign">\n  <ion-card-content>\n     You have not paid yet for business. Please pay before using this feature\n  </ion-card-content>\n</ion-card>\n\n<div *ngIf="doIhavebusinessAccount && !showPaidSign">\n  <h5>Text To Broadcast (e.g. Promo, Ad)</h5>\n  <ion-list inset>\n    <ion-item>\n     <ion-label color="primary" floating></ion-label>\n     <ion-textarea  [(ngModel)]="newmessage" [formControl]="message"  type="text"  rows="7" name="message" class="newmsg" placeholder="Write your message ..." ></ion-textarea>\n   </ion-item>\n   <ion-item inset>\n   </ion-item>\n  </ion-list>\n  <div padding>\n    <button ion-button (click)="broadcast()" color="primary" block>Broadcast</button>\n  </div>\n</div>\n\n\n</ion-content>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/broadcast/broadcast.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__providers_fcm_fcm__["a" /* FcmProvider */], __WEBPACK_IMPORTED_MODULE_6__providers_loading_loading__["a" /* LoadingProvider */], __WEBPACK_IMPORTED_MODULE_5__providers_requests_requests__["a" /* RequestsProvider */], __WEBPACK_IMPORTED_MODULE_7__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */], __WEBPACK_IMPORTED_MODULE_2__providers_chat_chat__["a" /* ChatProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_user_user__["a" /* UserProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["z" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */]])
    ], BroadcastPage);
    return BroadcastPage;
}());

//# sourceMappingURL=broadcast.js.map

/***/ }),

/***/ 946:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BroadcastPageModule", function() { return BroadcastPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__broadcast__ = __webpack_require__(1017);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var BroadcastPageModule = (function () {
    function BroadcastPageModule() {
    }
    BroadcastPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__broadcast__["a" /* BroadcastPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__broadcast__["a" /* BroadcastPage */]),
            ],
        })
    ], BroadcastPageModule);
    return BroadcastPageModule;
}());

//# sourceMappingURL=broadcast.module.js.map

/***/ })

});
//# sourceMappingURL=62.js.map