webpackJsonp([55],{

/***/ 1027:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DisplaynamePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_user_user__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_firebase__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var DisplaynamePage = (function () {
    function DisplaynamePage(navCtrl, navParams, fb, userservice) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.fb = fb;
        this.userservice = userservice;
        this.submitted = false;
        this.msgstatus = false;
        this.authForm = this.fb.group({
            username: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])]
        });
    }
    DisplaynamePage.prototype.ionViewDidLoad = function () {
        this.loadusername();
    };
    DisplaynamePage.prototype.loadusername = function () {
        var _this = this;
        var userId;
        if (__WEBPACK_IMPORTED_MODULE_4_firebase___default.a.auth().currentUser) {
            userId = __WEBPACK_IMPORTED_MODULE_4_firebase___default.a.auth().currentUser.uid;
        }
        else {
            userId = localStorage.getItem('userUID');
        }
        if (userId != undefined) {
            this.userservice.getuserdetails().then(function (res) {
                if (res) {
                    _this.msgstatus = true;
                    _this.username = res['displayName'];
                }
                else {
                    _this.username = '';
                }
            }).catch(function (err) {
            });
        }
        else {
            this.username = '';
        }
    };
    DisplaynamePage.prototype.onChange = function (e) {
        if (this.username.trim()) {
            this.msgstatus = true;
        }
        else {
            this.msgstatus = false;
        }
    };
    DisplaynamePage.prototype.save = function () {
        var _this = this;
        this.submitted = true;
        if (this.authForm.valid) {
            if (this.username.trim() != null || this.username.trim() != "") {
                this.username = this.username.trim();
                this.userservice.updatedisplayname(this.username).then(function (res) {
                    if (res.success) {
                        _this.navCtrl.setRoot("RegisterbusinessPage"); //ChatsPage
                    }
                });
            }
            else {
            }
        }
    };
    DisplaynamePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-displayname',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/displayname/displayname.html"*/'<ion-header>\n  <ion-navbar>\n    <div class="header-wrap">\n      <div class="header-title">Your Name</div>\n    </div>\n  </ion-navbar>\n</ion-header>\n<ion-content padding>\n  <form [formGroup]="authForm" (submit)="save()">\n \n\n    <ion-label class="content-title"> Let\'s give you a name</ion-label>\n    <ion-input type="text" (input)="onChange($event.keyCode)"formControlName="username" id="username" [value]="username" [(ngModel)]="username" maxlength="30" placeholder="Your Name"></ion-input>\n    <ion-label *ngIf="(!authForm.controls.username.valid||!msgstatus)  && (authForm.controls.username.dirty || submitted)">\n        <p class="errormsg">Please enter a valid Name.</p>\n    </ion-label>\n    <button ion-button class="button-secondary" type="submit" [disabled]="!authForm.valid || !this.msgstatus" >Submit</button>\n  </form>\n</ion-content>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/displayname/displayname.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_3__providers_user_user__["a" /* UserProvider */]])
    ], DisplaynamePage);
    return DisplaynamePage;
}());

//# sourceMappingURL=displayname.js.map

/***/ }),

/***/ 956:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DisplaynamePageModule", function() { return DisplaynamePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__displayname__ = __webpack_require__(1027);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DisplaynamePageModule = (function () {
    function DisplaynamePageModule() {
    }
    DisplaynamePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__displayname__["a" /* DisplaynamePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__displayname__["a" /* DisplaynamePage */]),
            ],
        })
    ], DisplaynamePageModule);
    return DisplaynamePageModule;
}());

//# sourceMappingURL=displayname.module.js.map

/***/ })

});
//# sourceMappingURL=55.js.map