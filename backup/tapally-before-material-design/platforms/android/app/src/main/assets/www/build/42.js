webpackJsonp([42],{

/***/ 1038:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeAfterINeedTodayPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_chat_chat__ = __webpack_require__(497);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_user_user__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_fcm_fcm__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_requests_requests__ = __webpack_require__(495);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_loading_loading__ = __webpack_require__(240);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_forms__ = __webpack_require__(40);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var HomeAfterINeedTodayPage = (function () {
    function HomeAfterINeedTodayPage(fcm, loading, requestservice, fb, events, chatservice, userservice, navCtrl, toastCtrl, navParams) {
        this.fcm = fcm;
        this.loading = loading;
        this.requestservice = requestservice;
        this.fb = fb;
        this.events = events;
        this.chatservice = chatservice;
        this.userservice = userservice;
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.navParams = navParams;
        this.newmessage = '';
        this.isData = false;
        this.selectCatId = navParams.get('cat_id');
        this.authForm = this.fb.group({
            'message': [null, __WEBPACK_IMPORTED_MODULE_7__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_7__angular_forms__["f" /* Validators */].required])]
        });
        this.message = this.authForm.controls['message'];
    }
    HomeAfterINeedTodayPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.requestservice.getmyfriends();
        this.myfriends = [];
        this.events.subscribe('friends', function () {
            _this.myfriends = [];
            _this.myfriends = _this.requestservice.myfriends;
        });
    };
    HomeAfterINeedTodayPage.prototype.broadcast = function () {
        this.cntCounter = 0;
        for (var key = 0; key < this.myfriends.length; key++) {
            this.addmessage(this.myfriends[key]);
        } //end for
        //mytodo : remove this from here and just call asyn a web service API to send messages.
        //Now the mobile is doing heavylifting. But ideally the server should do and send to nosql database
        /*
        this.cntCounter = 0;
        this.events.subscribe('friends', () => {
          if (this.requestservice.myfriends.length > 0) {
              console.log ("-->" + this.requestservice.myfriends.length);
              for (var key=0;key<this.requestservice.myfriends.length;key++) {
                 this.buddy = this.requestservice.myfriends[key];
                 console.log (key);
                 //console.log(this.buddy);
                 this.addmessage();
              }//end for
          }
        });
        */
    };
    HomeAfterINeedTodayPage.prototype.sendToNextPage = function () {
        this.cntCounter++;
        if (this.cntCounter >= this.myfriends.length) {
            this.cntCounter = 0;
            this.navCtrl.setRoot("RequestSubmittedPage");
        }
    };
    HomeAfterINeedTodayPage.prototype.addmessage = function (buddy) {
        var _this = this;
        this.newmessage = this.newmessage.trim();
        //Send Push notification : mytodo check if the buddy is online or not. if online then no need to send push notification
        var newMessage = 'You have received a message';
        this.fcm.sendNotification(buddy, newMessage, 'chatpage');
        //Send chat message
        this.chatservice.addnewmessageBroadcast(this.newmessage, 'message', buddy, this.selectCatId).then(function () {
            _this.newmessage = '';
            localStorage.setItem('ever_referral_sent', "1");
            _this.sendToNextPage();
        });
    };
    HomeAfterINeedTodayPage.prototype.ionViewDidLoad = function () {
    };
    HomeAfterINeedTodayPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home-after-i-need-today',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/home-after-i-need-today/home-after-i-need-today.html"*/'<ion-header>\n  <ion-navbar color="primary" >\n    <ion-title>\n      Submit Request\n    </ion-title>\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name=\'menu\'></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n \n<ion-content padding>\n    <div class="pull-center givepadding">\n        <h3>Get Trusted Referrals</h3>\n        <p>Instead of calling or texting to each of your friend, ask to whole network with ease</p>\n    </div>\n  <ion-list inset>\n    <ion-item>\n     <ion-label color="primary" floating>Request Details (Optional)</ion-label>\n     <ion-textarea  [(ngModel)]="newmessage" [formControl]="message"  type="text"  rows="7" name="message" class="newmsg"  ></ion-textarea>\n   </ion-item>\n   <ion-item inset>\n   </ion-item>\n  </ion-list>\n\n  <div padding>\n    <button ion-button (click)="broadcast()" color="primary" block>Post Request</button>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/home-after-i-need-today/home-after-i-need-today.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__providers_fcm_fcm__["a" /* FcmProvider */], __WEBPACK_IMPORTED_MODULE_6__providers_loading_loading__["a" /* LoadingProvider */], __WEBPACK_IMPORTED_MODULE_5__providers_requests_requests__["a" /* RequestsProvider */], __WEBPACK_IMPORTED_MODULE_7__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */], __WEBPACK_IMPORTED_MODULE_2__providers_chat_chat__["a" /* ChatProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_user_user__["a" /* UserProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["z" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */]])
    ], HomeAfterINeedTodayPage);
    return HomeAfterINeedTodayPage;
}());

//# sourceMappingURL=home-after-i-need-today.js.map

/***/ }),

/***/ 967:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeAfterINeedTodayPageModule", function() { return HomeAfterINeedTodayPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_after_i_need_today__ = __webpack_require__(1038);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var HomeAfterINeedTodayPageModule = (function () {
    function HomeAfterINeedTodayPageModule() {
    }
    HomeAfterINeedTodayPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__home_after_i_need_today__["a" /* HomeAfterINeedTodayPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__home_after_i_need_today__["a" /* HomeAfterINeedTodayPage */]),
            ],
        })
    ], HomeAfterINeedTodayPageModule);
    return HomeAfterINeedTodayPageModule;
}());

//# sourceMappingURL=home-after-i-need-today.module.js.map

/***/ })

});
//# sourceMappingURL=42.js.map