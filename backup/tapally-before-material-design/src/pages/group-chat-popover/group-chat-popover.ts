import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';
import { GroupsProvider } from '../../providers/groups/groups';
@IonicPage()
@Component({
  selector: 'page-group-chat-popover',
  templateUrl: 'group-chat-popover.html',
})
export class GroupChatPopoverPage {
  groupName: any;
  groupMembers = [];
  CurrentUser;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private groupService: GroupsProvider,
    private modelCtrl: ModalController,
    public viewCtrl: ViewController
  ) {
    if (this.groupService.currentgroupname != undefined) {
      this.groupName = this.groupService.currentgroupname;
    } else {
      this.groupName = this.navParams.get('groupName');
      this.groupService.currentgroupname = this.groupName;
    }
    this.CurrentUser = localStorage.getItem('userUID');
  }

  searchMessage(myEvent) {
    let searchChatModal = this.modelCtrl.create('SearchGroupChatPage');
    searchChatModal.present({
      ev: myEvent
    });
    searchChatModal.onDidDismiss(() => {
      this.viewCtrl.dismiss();
    })
  }
}