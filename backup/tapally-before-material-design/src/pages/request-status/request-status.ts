import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
  
@IonicPage()
@Component({
  selector: 'page-request-status',
  templateUrl: 'request-status.html',
})
export class RequestStatusPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  gotoPage() {
    //testing
      this.navCtrl.setRoot("ChooseClubPage");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RequestStatusPage');
  }

}
