import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchGroupChatPage } from './search-group-chat';

@NgModule({
  declarations: [
    SearchGroupChatPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchGroupChatPage),
  ],
})
export class SearchGroupChatPageModule {}
