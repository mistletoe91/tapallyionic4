import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Events, ModalController, ToastController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { ContactProvider } from '../../providers/contact/contact';
import { RequestsProvider } from '../../providers/requests/requests';
import { connreq } from '../../models/interfaces/request';
import { LoadingProvider } from '../../providers/loading/loading';
import { ChatProvider } from '../../providers/chat/chat';
import { SmsProvider } from '../../providers/sms/sms';
import { FcmProvider } from '../../providers/fcm/fcm';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
	selector: 'page-invite-friend',
	templateUrl: 'invite-friend.html',
})
export class InviteFriendPage {
	contactList: any = [];
	allregisteUserData: any = [];
	metchContact: any = [];
	mainArray: any = [];
	tempArray: any = [];
	myfriends;
	isData: boolean = false;

	isInviteArray: any = [];

	newrequest = {} as connreq;
	temparr = [];
	filteredusers = [];
	userId = localStorage.getItem('userUID');
	constructor(
		public events: Events,
		public storage: Storage,
		public loadingProvider: LoadingProvider,
		public contactProvider: ContactProvider,
		public userservice: UserProvider,
		public navCtrl: NavController,
		public navParams: NavParams,
		public alertCtrl: AlertController,
		public requestservice: RequestsProvider,
		public chatservice: ChatProvider,
		public zone: NgZone,
		public smsservice: SmsProvider,
		public fcm: FcmProvider,
		public modalCtrl: ModalController,
		private toastCtrl: ToastController
	) {
	}

	ionViewDidLoad() {
		this.loadingProvider.presentLoading();
		this.storage.get('isInvitedContact').then(res => {
			if (res != null) {
				this.isInviteArray = res;
			}
		});
		this.requestservice.getmyfriends();

		this.myfriends = [];

		this.events.subscribe('friends', () => {
			this.myfriends = [];
			this.myfriends = this.requestservice.myfriends;
			this.getAllDeviceContacts(this.myfriends);
		});
	}
	getAllDeviceContacts(myfriends) {

		this.contactProvider.getSimJsonContacts()
			.then(res => {
				this.loadingProvider.dismissMyLoading();
				var ref = localStorage.getItem("updated");
				if (ref == "show") {
					localStorage.removeItem("updated");
					let toast = this.toastCtrl.create({
						message: 'Contacts updated successfully',
						duration: 2500,
						position: 'bottom'
					});
					toast.present();
				}

				if (res['contacts'].length > 0) {

					let newConatctList = [];

					newConatctList = res['contacts'];

					this.contactList = this.removeDuplicates(newConatctList, 'mobile');
					for (let i = 0; i < this.contactList.length; i++) {
						if (this.contactList[i].displayName != undefined) {
							this.contactList[i].displayName = this.contactList[i].displayName.trim();
						}
					}
					this.metchContact = this.allregisteUserData.filter((item) =>
						this.contactList.some(data => item.mobile == data.mobile));

					for (let i = 0; i < this.contactList.length; i++) {
						for (let j = 0; j < this.metchContact.length; j++) {
							if (this.contactList[i] != undefined && this.metchContact[j] != undefined) {
								if (this.contactList[i].mobile == this.metchContact[j].mobile) {
									this.metchContact[j].isUser = "1";
									this.metchContact[j].displayName = this.contactList[i].displayName;
								}
							}

						}
					}
					this.contactList = this.contactList.filter(o1 => {
						return !this.metchContact.some(o2 => {
							return o1.mobile === o2.mobile;          // assumes unique id
						});
					});
					this.metchContact = this.metchContact.sort(function (a, b) {
						if (a.displayName != undefined) {
							var nameA = a.displayName.toUpperCase(); // ignore upper and lowercase
						}
						if (b.displayName != undefined) {
							var nameB = b.displayName.toUpperCase(); // ignore upper and lowercase
						}
						if (nameA < nameB) {
							return -1;
						}
						if (nameA > nameB) {
							return 1;
						}
						return 0;
					});

					// sort by name
					this.contactList = this.contactList.sort(function (a, b) {
						if (a.displayName != undefined) {
							var nameA = a.displayName.toUpperCase(); // ignore upper and lowercase
						}
						if (b.displayName != undefined) {
							var nameB = b.displayName.toUpperCase(); // ignore upper and lowercase
						}
						if (nameA < nameB) {
							return -1;
						}
						if (nameA > nameB) {
							return 1;
						}
						// names must be equal
						return 0;
					});
					let myArray = this.metchContact.concat(this.contactList);
					if (this.isInviteArray.length > 0) {
						for (let i = 0; i < myArray.length; i++) {
							for (let j = 0; j < this.isInviteArray.length; j++) {
								if (myArray[i].mobile == this.isInviteArray[j].mobile) {
									myArray[i].isdisable = true;
								} else {
									myArray[i].isdisable = false;
								}
							}
						}
					}
					this.zone.run(() => {
						this.mainArray = myArray;
					})
					this.loadingProvider.dismissMyLoading();
					this.tempArray = this.mainArray;

				} else {
					this.isData = true;
				}
			}).catch(err => {
				this.loadingProvider.dismissMyLoading();
			});
	}

	removeDuplicates(originalArray: any[], prop) {
		let newArray = [];
		let lookupObject = {};

		originalArray.forEach((item, index) => {
			lookupObject[originalArray[index][prop]] = originalArray[index];
		});

		Object.keys(lookupObject).forEach(element => {
			newArray.push(lookupObject[element]);
		});
		return newArray;
	}

	goBack() {
		this.navCtrl.setRoot('ChatsPage');
	}
	ionViewWillLeave() {
		this.loadingProvider.dismissMyLoading();
	}
	initializeContacts() {
		this.mainArray = this.tempArray;
	}
	refreshPage() {
		this.ionViewDidLoad();
		localStorage.setItem('updated', 'show');
	}
	searchuser(ev: any) {
		this.initializeContacts();
		let val = ev.target.value;
		if (val && val.trim() != '') {
			this.mainArray = this.mainArray.filter((item) => {
				if (item.displayName != undefined) {
					return (item.displayName.toLowerCase().indexOf(val.toLowerCase()) > -1);
				}
			});
		}
	}

	inviteReq(recipient) {
		let alert = this.alertCtrl.create({
			title: 'Invitation',
			subTitle: 'Invitation already sent to ' + recipient.displayName,
			buttons: ['Ok']
		});
		alert.present();
	}

	sendreq(recipient) {
		this.newrequest.sender = localStorage.getItem('userUID');
		this.newrequest.recipient = recipient.uid;
		if (this.newrequest.sender === this.newrequest.recipient)
			alert('You are your friend always');
		else {
			let successalert = this.alertCtrl.create({
				title: 'Request sent',
				subTitle: 'Your request has been sent to ' + recipient.displayName,
				buttons: ['ok']
			});


			let userName: any;

			this.userservice.getuserdetails().then((res: any) => {
				userName = res.displayName;

				let newMessage = userName + " has sent you friend request.";

				this.fcm.sendNotification(recipient, newMessage, 'sendreq');
			});

			this.requestservice.sendrequest(this.newrequest).then((res: any) => {
				if (res.success) {
					successalert.present();
					let sentuser = this.mainArray.indexOf(recipient);
					this.mainArray[sentuser].status = "pending";
				}
			}).catch((err) => {
			})
		}
	}

	sendSMS(item) {

		this.loadingProvider.presentLoading();

		this.smsservice.sendSms(item.mobile).then((res) => {
			this.loadingProvider.dismissMyLoading();
			if (res == 'OK') {

				for (var i = 0; i < this.mainArray.length; ++i) {
					if (this.mainArray[i].mobile == item.mobile) {
						this.mainArray[i].isdisable = true
					}
				}

				let alert = this.alertCtrl.create({
					title: 'Message Send',
					subTitle: 'Your message has been sent to ' + item.displayName,
					buttons: ['Ok']
				});
				alert.present();

				this.isInviteArray.push(item);

				this.storage.set('isInvitedContact', this.isInviteArray);
			} else {
				let alert = this.alertCtrl.create({
					title: 'Message Send',
					subTitle: 'Error for sending message to ' + item.displayName,
					buttons: ['Ok']
				});
				alert.present();
			}
		}).catch(err => {
			this.loadingProvider.dismissMyLoading();
			let alert = this.alertCtrl.create({
				title: 'Message Send',
				subTitle: 'Error for sending message to ' + item.displayName,
				buttons: ['Ok']
			});
			alert.present();
		});
	}
}
