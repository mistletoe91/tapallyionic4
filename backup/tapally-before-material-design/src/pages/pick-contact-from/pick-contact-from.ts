import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, Platform, NavParams, AlertController, Events, ModalController, ToastController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { ContactProvider } from '../../providers/contact/contact';
import { RequestsProvider } from '../../providers/requests/requests';
import { connreq } from '../../models/interfaces/request';
import { LoadingProvider } from '../../providers/loading/loading';
import { ChatProvider } from '../../providers/chat/chat';
import { SmsProvider } from '../../providers/sms/sms';
import { FcmProvider } from '../../providers/fcm/fcm';
import { Storage } from '@ionic/storage';
import { GroupsProvider } from '../../providers/groups/groups';
import { env } from '../../app/app.angularfireconfig';

@IonicPage()
@Component({
	selector: 'page-pick-contact-from',
	templateUrl: 'pick-contact-from.html',
})
export class PickContactFromPage {
  friend_asking_referral_uid;
  friend_asking_referral_displayName;
  friend_asking_referral_catId;
  friend_asking_referral_catName;
  cntCounter;
	contactList: any = [];
	allregisteUserData: any = [];
	metchContact: any = [];
	mainArray: any = [];
	tempArray: any = [];
	myfriends;
	userpic = env.userPic;
	isData: boolean = false;
	reqStatus = false;
	isInviteArray: any = [];
  newmessage;

	newrequest = {} as connreq;
	temparr = [];
	filteredusers = [];
	userId = localStorage.getItem('userUID');
	constructor(
		public events: Events,
		private platform: Platform,
		public storage: Storage,
		public loadingProvider: LoadingProvider,
		public contactProvider: ContactProvider,
		public userservice: UserProvider,
		public navCtrl: NavController,
		public navParams: NavParams,
		public alertCtrl: AlertController,
		public requestservice: RequestsProvider,
		public chatservice: ChatProvider,
		public zone: NgZone,
		public smsservice: SmsProvider,
		public fcm: FcmProvider,
		public modalCtrl: ModalController,
		private toastCtrl: ToastController,
		public groupservice: GroupsProvider
	) {
    this.friend_asking_referral_uid = navParams.get('friend_asking_referral_uid');
    this.friend_asking_referral_displayName = navParams.get('friend_asking_referral_displayName');
    this.friend_asking_referral_catId = navParams.get('friend_asking_referral_catId');
    this.friend_asking_referral_catName = navParams.get('friend_asking_referral_catName');
    this.cntCounter = 0;
	}

  ionViewWillEnter() {
      this.requestservice.getmyfriends();
      this.myfriends = [];
      this.events.subscribe('friends', () => {
        this.myfriends = [];
        this.myfriends = this.requestservice.myfriends;
      });
  }

  pickContactExecute (buddy, isTapAllyMember){
          //Tapally Member so send push notification
          /*
             - First Get all buddies
             - Then select send to. It should be this.friend_asking_referral_uid
             - Then select message with buddy information
             - Then send push notification
             - Also send chat messages
             - Redirect back to buddypage
          */
          for (var key=0;key<this.myfriends.length;key++) {
             //console.log (this.myfriends[key].displayName)
             //Find out who is receipient
             if(this.friend_asking_referral_uid == this.myfriends[key].uid){
                  //send to the one who asked for referral

									let catStr = '';
									if(this.friend_asking_referral_catName){
										catStr = '('+this.friend_asking_referral_catName+')';
									}

                  this.newmessage = 'Hello I am sending '+catStr+' referral to you : '+buddy.displayName + " ("+buddy.mobile+")";
                  this.addmessage(this.myfriends[key], buddy, this.myfriends[key]);

                  //also tell the one who got referred
                  if(isTapAllyMember){
                      //referral is member of Tapally
                      this.newmessage = 'Hello I send your referral to '+this.myfriends[key].displayName + " ("+this.myfriends[key].mobile+")";
											if(catStr){
												this.newmessage += '. '+this.myfriends[key].displayName+' is asking a referral for '+catStr;
											}//endif

                      this.addmessage(buddy, this.myfriends[key], this.myfriends[key]);
                  } else {
                      //referral is not member of Tapally
                      console.log ( buddy.displayName + ":" + isTapAllyMember )
                      let msg = 'Hello I send your referral to '+this.myfriends[key].displayName + " ("+this.myfriends[key].mobile+"). Learn more at https://tapally.com " ;
                      this.sendSMSCustomMsg(buddy ,msg);
                  }//endif
                  break;
             }
          }//end for
  }

  sendToNextPage (originalChat){
      this.cntCounter++;
      if(this.cntCounter>=2){
        this.cntCounter = 0;
        this.chatservice.initializebuddy(originalChat);
  			this.navCtrl.push('BuddychatPage');
      }
  }

  //here buddy is receipient and referral is contact being sent
  addmessage(buddy, referral, originalChat) {

    //Send Push notification : mytodo check if the buddy is online or not. if online then no need to send push notification
    let newMessage = 'You have received a message';
    this.fcm.sendNotification(buddy, newMessage, 'chatpage');

    //Send chat message
    this.chatservice.addnewmessageBroadcast(this.newmessage, 'message', buddy , 0).then(() => {
        this.newmessage = '';
        this.sendToNextPage (originalChat);
    });
  }

	ionViewDidLoad() {
		this.loadingProvider.presentLoading();

		this.storage.get('isInvitedContact').then(res => {
			if (res != null) {
				this.isInviteArray = res;
			}
		});
		this.userservice.getallusers().then((res: any) => {
			this.allregisteUserData = res;
			let userID = localStorage.getItem('userUID');
			for (let i = 0; i < res.length; i++) {
				this.requestservice.pendingetmyrequests(res[i].uid).then((data: any) => {
					for (let q = 0; q < data.length; q++) {
						if (data[q].uid == userID) {
							this.allregisteUserData[i].disc = this.allregisteUserData[i].disc.replace(new RegExp('<br />', 'g'), '');
							this.allregisteUserData[i].status = "pending";
						} else {
							this.allregisteUserData[i].status = null;
						}
					}
				})
			}
			this.requestservice.getmyfriends();
			this.myfriends = [];
			this.events.subscribe('friends', () => {
				this.myfriends = [];
				this.myfriends = this.requestservice.myfriends;
				this.getAllDeviceContacts(this.myfriends);

			});
		});
	}
	// Get All contact from the device
	getAllDeviceContacts(myfriends) {
		this.contactProvider.getSimJsonContacts()
			.then(res => {
				this.loadingProvider.dismissMyLoading();
				var ref = localStorage.getItem("updated");
				if (ref == "show") {
					localStorage.removeItem("updated");
					let toast = this.toastCtrl.create({
						message: 'Contacts updated successfully.',
						duration: 2500,
						position: 'bottom'
					});
					toast.present();
				}
				if (res['contacts'].length > 0) {
					let newConatctList = [];
					newConatctList = res['contacts'];
					this.contactList = this.removeDuplicates(newConatctList, 'mobile');
					for (let i = 0; i < this.contactList.length; i++) {
						this.contactList[i].isBlock = false;
						if (this.contactList[i].displayName != undefined) {
							this.contactList[i].displayName = this.contactList[i].displayName.trim();
						}
					}
					this.metchContact = this.allregisteUserData.filter((item) =>
						this.contactList.some(data => item.mobile == data.mobile));

					for (let i = 0; i < this.contactList.length; i++) {
						for (let j = 0; j < this.metchContact.length; j++) {
							if (this.contactList[i] != undefined && this.metchContact[j] != undefined) {
								if (this.contactList[i].mobile == this.metchContact[j].mobile) {
									this.metchContact[j].isUser = "1";
									this.metchContact[j].isBlock = this.contactList[i].isBlock;
									this.metchContact[j].displayName = this.contactList[i].displayName;
								}
							}

						}
					}
					for (let i = 0; i < this.metchContact.length; i++) {
						for (let j = 0; j < myfriends.length; j++) {
							if (this.metchContact[i] != undefined && myfriends[j] != undefined) {
								if (this.metchContact[i].mobile == myfriends[j].mobile) {
									this.metchContact[i].status = 'friend';
									this.metchContact[i].isBlock = myfriends[j].isBlock;
								}
							}

						}
					}
					this.contactList = this.contactList.filter(o1 => {
						return !this.metchContact.some(o2 => {
							return o1.mobile === o2.mobile;          // assumes unique id
						});
					});
					this.metchContact = this.metchContact.sort(function (a, b) {
						if (a.displayName != undefined) {
							var nameA = a.displayName.toUpperCase(); // ignore upper and lowercase
						}
						if (b.displayName != undefined) {
							var nameB = b.displayName.toUpperCase(); // ignore upper and lowercase
						}
						if (nameA < nameB) {
							return -1;
						}
						if (nameA > nameB) {
							return 1;
						}

						// names must be equal
						return 0;
					});
					// sort by name
					this.contactList = this.contactList.sort(function (a, b) {
						if (a.displayName != undefined) {
							var nameA = a.displayName.toUpperCase(); // ignore upper and lowercase
						}
						if (b.displayName != undefined) {
							var nameB = b.displayName.toUpperCase(); // ignore upper and lowercase
						}
						if (nameA < nameB) {
							return -1;
						}
						if (nameA > nameB) {
							return 1;
						}
						// names must be equal
						return 0;
					});
					let myArray = this.metchContact.concat(this.contactList);
					if (this.isInviteArray.length > 0) {
						for (let i = 0; i < myArray.length; i++) {
							for (let j = 0; j < this.isInviteArray.length; j++) {
								if (myArray[i].mobile == this.isInviteArray[j].mobile) {
									myArray[i].isdisable = true;
								} else {
									myArray[i].isdisable = false;
								}
							}
						}
					}
					this.zone.run(() => {
						this.mainArray = myArray;
					})
					this.loadingProvider.dismissMyLoading();
					this.tempArray = this.mainArray;
				} else {
					this.isData = true;
				}
			}).catch(err => {
				this.loadingProvider.dismissMyLoading();
				if (err == 20) {
					this.platform.exitApp();
				}
			});
	}
	removeDuplicates(originalArray: any[], prop) {
		let newArray = [];
		let lookupObject = {};

		originalArray.forEach((item, index) => {
			lookupObject[originalArray[index][prop]] = originalArray[index];
		});

		Object.keys(lookupObject).forEach(element => {
			newArray.push(lookupObject[element]);
		});
		return newArray;
	}

	ionViewWillLeave() {
		this.groupservice.getmygroups();
		this.requestservice.getmyfriends();
		this.loadingProvider.dismissMyLoading();
	}
	initializeContacts() {
		this.mainArray = this.tempArray;
	}
	refreshPage() {
		this.ionViewDidLoad();
		localStorage.setItem('updated', 'show');
	}

	searchuser(ev: any) {
		this.initializeContacts();
		let val = ev.target.value;
		if (val && val.trim() != '') {
			this.mainArray = this.mainArray.filter((item) => {
				if (item.displayName != undefined) {
					return (item.displayName.toLowerCase().indexOf(val.toLowerCase()) > -1);
				}
			});
		}
	}

	inviteReq(recipient) {
		let alert = this.alertCtrl.create({
			title: 'Invitation',
			subTitle: 'Invitation already sent to ' + recipient.displayName + '.',
			buttons: ['Ok']
		});
		alert.present();
	}

	sendreq(recipient) {
		this.reqStatus = true;
		this.newrequest.sender = localStorage.getItem('userUID');
		this.newrequest.recipient = recipient.uid;
		if (this.newrequest.sender === this.newrequest.recipient)
			alert('You are your friend always');
		else {
			let successalert = this.alertCtrl.create({
				title: 'Request sent',
				subTitle: 'Your request has been sent to ' + recipient.displayName + '.',
				buttons: ['ok']
			});

			let userName: any;

			this.userservice.getuserdetails().then((res: any) => {
				userName = res.displayName;

				let newMessage = userName + " has sent you friend request.";

				this.fcm.sendNotification(recipient, newMessage, 'sendreq');
			});

			this.requestservice.sendrequest(this.newrequest).then((res: any) => {
				if (res.success) {
					this.reqStatus = false;
					successalert.present();
					let sentuser = this.mainArray.indexOf(recipient);
					this.mainArray[sentuser].status = "pending";
				}
			}).catch((err) => {
			})
		}
	}

	buddychat(buddy) {
		if (buddy.status == 'friend') {
			this.chatservice.initializebuddy(buddy);
			this.navCtrl.push('BuddychatPage');
		}
	}

  sendSMSCustomMsg(item ,msg) {
		this.loadingProvider.presentLoading();
		this.smsservice.sendSmsCustomMsg(item.mobile,msg).then((res) => {
			this.loadingProvider.dismissMyLoading();
		}).catch(err => {
			this.loadingProvider.dismissMyLoading();
			let alert = this.alertCtrl.create({
				title: 'Message Send',
				subTitle: 'Error for sending message to ' + item.displayName + '.',
				buttons: ['Ok']
			});
			alert.present();
		});
	}//end function

	sendSMS(item) {

		this.loadingProvider.presentLoading();

		this.smsservice.sendSms(item.mobile).then((res) => {
			this.loadingProvider.dismissMyLoading();
			if (res) {

				for (var i = 0; i < this.mainArray.length; ++i) {
					if (this.mainArray[i].mobile == item.mobile) {
						this.mainArray[i].isdisable = true
					}
				}

				let alert = this.alertCtrl.create({
					title: 'Message Send',
					subTitle: 'Your message has been sent to ' + item.displayName + '.',
					buttons: ['Ok']
				});
				alert.present();

				this.isInviteArray.push(item);

				this.storage.set('isInvitedContact', this.isInviteArray);
			} else {
				let alert = this.alertCtrl.create({
					title: 'Message Send',
					subTitle: 'Error for sending message to ' + item.displayName + '.',
					buttons: ['Ok']
				});
				alert.present();
			}
		}).catch(err => {
			this.loadingProvider.dismissMyLoading();
			let alert = this.alertCtrl.create({
				title: 'Message Send',
				subTitle: 'Error for sending message to ' + item.displayName + '.',
				buttons: ['Ok']
			});
			alert.present();
		});
	}
	addnewContact(myEvent) {
		let profileModal = this.modalCtrl.create("AddContactPage");
		profileModal.present({
			ev: myEvent
		});
		profileModal.onDidDismiss((pages) => {
		})
	}
	goBack() {
		this.navCtrl.setRoot('ChatsPage');
	}
}
