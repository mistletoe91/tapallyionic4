import { Component ,NgZone} from '@angular/core';
import { IonicPage, NavController, NavParams ,Events ,AlertController,Platform} from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { RequestsProvider } from '../../providers/requests/requests';

@IonicPage()
@Component({
  selector: 'page-block-user',
  templateUrl: 'block-user.html',
})
export class BlockUserPage {

  blockUsers:any = [];
  blockRequest:any=[];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public userservice : UserProvider,
    public zone : NgZone,
    public events : Events,
    public alertCtrl: AlertController,
    public requestservice: RequestsProvider,
    public platform:Platform
  ){
    events.subscribe('block-users', () => {
      this.blockUsers = this.userservice.blockUsers;
    })
    events.subscribe('block-request',()=>{
        this.blockRequest = this.requestservice.blockRequest;
    })
  }
  ionViewDidLoad() {
    this.userservice.getAllBlockUsers();
    this.requestservice.getAllBlockRequest();
  }
  openFriend(){
    this.navCtrl.push('FriendListPage');
  }
  goBack(){
    this.navCtrl.setRoot("SettingPage")
  }
  ubblockuser(item){
    let alert = this.alertCtrl.create({
      message: 'Unblock '+item.displayName+'.',
      buttons: [
        {
          text: 'CANCEL',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'UNBLOCK',
          handler: () => {
            this.userservice.unblockUser(item).then((res)=>{
              if(res){
                this.userservice.getAllBlockUsers();
              }
            })
          }
        }
      ]
    });
    alert.present();
  }
  ubblockrequest(item){
    let alert = this.alertCtrl.create({
      message: 'Unblock '+item.displayName+'.',
      buttons: [
        {
          text: 'CANCEL',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'UNBLOCK',
          handler: () => {
            this.requestservice.unblockRequest(item).then((res)=>{
              if(res){
                this.requestservice.getAllBlockRequest();
              }
            })
          }
        }
      ]
    });
    alert.present();
  }
}
