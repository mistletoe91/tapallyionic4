import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import firebase from 'firebase';
import { Storage } from '@ionic/storage';
import { UserProvider } from '../../providers/user/user';
import { RequestsProvider } from '../../providers/requests/requests';
import { env } from '../../app/app.angularfireconfig';
import { ChatProvider } from '../../providers/chat/chat';

@IonicPage()
@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class SettingPage {
  imgurl = env.userPic;
  isDisabled: boolean = false;
  isNotify: boolean = true;
  profileData: any = {
    username: '',
    disc: '',
    imgurl: env.userPic
  }
  buddy: any;
  blockcounter: any = 0;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public zone: NgZone,
    public events: Events,
    public storage: Storage,
    public userservice: UserProvider,
    public requestservice: RequestsProvider,
    public chatservice: ChatProvider,
  ) {
    this.loaduserdetails();
    events.subscribe('block-users-counter', () => {
      this.blockcounter = this.userservice.blockUsersCounter;
    })

    this.userservice.initializeItem().then((res) => {
      if (res == true) {
        this.isNotify = true;
      } else {
        this.isNotify = false;
      }
    });


  }
  ionViewDidLoad() {
    this.userservice.getAllBlockUsersCounter();
  }
  goBack() {
    this.navCtrl.setRoot('TabsPage');
  }
  loaduserdetails() {
    let userId;
    if (firebase.auth().currentUser) {
      userId = firebase.auth().currentUser.uid;
    } else {
      userId = localStorage.getItem('userUID');
    }
    if (userId != undefined) {
      this.isDisabled = true;
      this.userservice.getuserdetails().then((res: any) => {
        if (res) {
          let desc = res['disc'].replace(new RegExp('\n', 'g'), "<br />");
            if (res['displayName'] != undefined) {
              this.profileData.username = res['displayName'];
            }
            if (res['photoURL'] != undefined) {
              this.profileData.imgurl = res['photoURL']
            }
            if (desc != undefined) {
              this.profileData.disc = desc;
            }
        }
      }).catch(err => {
      });
    } else {
      this.profileData = {
        username: '',
        disc: '',
        imgurl: env.userPic
      }
    }
  }
  goBlockuser() {
    this.navCtrl.push('BlockUserPage');
  }
  openProfilePage() {
    this.navCtrl.push('ProfilepicPage')
  }
  inviteFriends() {
    this.navCtrl.push('InviteFriendPage');
  }
  notify() {
    this.isNotify != this.isNotify;
    this.userservice.notifyUser(this.isNotify);
  }
}
