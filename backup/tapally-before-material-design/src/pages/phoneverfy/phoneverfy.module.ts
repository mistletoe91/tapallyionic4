import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PhoneverfyPage } from './phoneverfy';

@NgModule({
  declarations: [
    PhoneverfyPage,
  ],
  imports: [
    IonicPageModule.forChild(PhoneverfyPage),
  ],
})
export class PhoneverfyPageModule {}
