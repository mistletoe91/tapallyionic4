import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { ImageViewerController } from 'ionic-img-viewer';

@IonicPage()
@Component({
  selector: 'page-profile-view',
  templateUrl: 'profile-view.html',
})
export class ProfileViewPage {
	name;
	img;
	_imageViewerCtrl: ImageViewerController;
  constructor(public navCtrl: NavController, public navParams: NavParams, public imageViewerCtrl: ImageViewerController, public viewCtrl: ViewController) {

  	 this.name=navParams.get('name');
  	 this.img=navParams.get('img');
  	 this._imageViewerCtrl = imageViewerCtrl;

  }

  presentImage(myImage) {
    const imageViewer = this._imageViewerCtrl.create(myImage);
    imageViewer.present();
  }

  closePofile(){
  	this.viewCtrl.dismiss();
  }
  ionViewDidLoad() {
  }

}
