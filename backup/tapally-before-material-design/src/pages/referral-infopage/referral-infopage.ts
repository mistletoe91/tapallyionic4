import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-referral-infopage',
  templateUrl: 'referral-infopage.html',
})
export class ReferralInfopagePage {
  iamABusinessOwner:boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }


  ionViewWillEnter() {
     if(localStorage.getItem('business_created')){
       this.iamABusinessOwner = true;
     } else {
       this.iamABusinessOwner = false;  
     }
  }

  sendreferraldirectly (){
    this.navCtrl.push("ReferralPage");
  }

  sendToRegisterBuesiness (){
    this.navCtrl.push("RegisterbusinessPage");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReferralInfopagePage');
  }

}
