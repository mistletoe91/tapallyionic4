import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Events,Platform } from 'ionic-angular';
import { GroupsProvider } from '../../providers/groups/groups';
import { ImagehandlerProvider } from '../../providers/imagehandler/imagehandler';
import { LoadingProvider } from '../../providers/loading/loading';
import {env} from '../../app/app.angularfireconfig';

@IonicPage()
@Component({
  selector: 'page-groups',
  templateUrl: 'groups.html',
})
export class GroupsPage {
  newgroup = {
    groupName: 'GroupName',
      groupPic: env.userPic
  }
  selectedBuddy: any = [];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public groupservice: GroupsProvider,
    public loadingCtrl: LoadingController,
    public imghandler: ImagehandlerProvider,
    public events: Events,
    public loadingProvider: LoadingProvider,
    public platform:Platform
  ) {
    this.selectedBuddy = navParams.get('buddy');
  }
  ionViewDidLoad() {
  }

  chooseimage() {
    this.imghandler.gpuploadimage(this.newgroup.groupName).then((res: any) => {
      this.loadingProvider.dismissMyLoading();
      if (res) {
        this.newgroup.groupPic = res;
      } else {
        this.loadingProvider.dismissMyLoading();
      }
    }).catch((err) => {
      this.loadingProvider.dismissMyLoading();
    })
  }
  creategroup() {
    if (this.newgroup.groupName == "" || this.newgroup.groupName == "GroupName") {
      let namealert = this.alertCtrl.create({
        buttons: ['okay'],
        message: 'Please enter the Group name.'
      });
      namealert.present();
    } else {
      if (this.newgroup.groupName.length < 26) {
        if(this.newgroup.groupName.trim()){
          this.groupservice.addgroup(this.newgroup, this.selectedBuddy).then((res) => {
            this.navCtrl.setRoot('ChatsPage');
          }).catch((err) => {
            alert(JSON.stringify(err));
          })
        }else{
          let namealert = this.alertCtrl.create({
            buttons: ['okay'],
            message: 'Please Enter Valid Groupname!'
          });
          namealert.present();
          this.newgroup.groupName = 'GroupName';
        }
      } else {
        let namealert = this.alertCtrl.create({
          buttons: ['okay'],
          message: 'Group name maximum length is 25 characters'
        });
        namealert.present();
      }

    }
  }
  editgroupname() {
    let alert = this.alertCtrl.create({
      title: 'Group Name',
      inputs: [{
        name: 'groupname',
        placeholder: 'Give a new groupname',
      }],
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        handler: data => {

        }
      }, {
        text: 'Set',
        handler: data => {
          if (data.groupname) {
            this.newgroup.groupName = data.groupname
          }
          else {
            this.newgroup.groupName = 'GroupName';
          }
        }
      }
      ]
    });
    alert.present();
  }

  goBack(){
    this.navCtrl.pop();
  }

}
