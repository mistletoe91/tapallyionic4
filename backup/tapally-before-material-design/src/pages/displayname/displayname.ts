import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserProvider } from '../../providers/user/user';
import firebase from 'firebase';
@IonicPage()
@Component({
  selector: 'page-displayname',
  templateUrl: 'displayname.html',
})
export class DisplaynamePage {
  authForm : FormGroup;
  username;
  submitted:boolean=false;
  msgstatus: boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public fb: FormBuilder,
    public userservice: UserProvider,) {
      
    this.authForm=this.fb.group({
      username:['',Validators.compose([Validators.required])]
    })
  }

  ionViewDidLoad() {
    this.loadusername();
  }
  loadusername(){
    let userId;
    if (firebase.auth().currentUser) {
      userId = firebase.auth().currentUser.uid;
  } else {
      userId = localStorage.getItem('userUID');
  }
  if (userId != undefined) {
    this.userservice.getuserdetails().then((res)=>{
      if(res){
        this.msgstatus=true;
        this.username=res['displayName'];
      }else{
        this.username='';
      }
    }).catch(err=>{
    });
  }else{
    this.username='';
  }

  }
  onChange(e) {
    if (this.username.trim()) {
        this.msgstatus = true;
    }
    else {
        this.msgstatus = false;
    }
  }
  save(){
    this.submitted=true;
    if(this.authForm.valid){
      if(this.username.trim()!=null || this.username.trim()!= ""){
     this.username=this.username.trim();
        this.userservice.updatedisplayname(this.username).then((res: any) => {
          if (res.success) {
            this.navCtrl.setRoot("RegisterbusinessPage");        //ChatsPage
          }
        })
        } else {
          
        }
    }
    
    }
  
}
