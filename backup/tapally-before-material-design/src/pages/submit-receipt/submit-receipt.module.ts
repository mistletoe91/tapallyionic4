import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubmitReceiptPage } from './submit-receipt';

@NgModule({
  declarations: [
    SubmitReceiptPage,
  ],
  imports: [
    IonicPageModule.forChild(SubmitReceiptPage),
  ],
})
export class SubmitReceiptPageModule {}
