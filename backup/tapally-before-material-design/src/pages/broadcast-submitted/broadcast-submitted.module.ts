import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BroadcastSubmittedPage } from './broadcast-submitted';

@NgModule({
  declarations: [
    BroadcastSubmittedPage,
  ],
  imports: [
    IonicPageModule.forChild(BroadcastSubmittedPage),
  ],
})
export class BroadcastSubmittedPageModule {}
