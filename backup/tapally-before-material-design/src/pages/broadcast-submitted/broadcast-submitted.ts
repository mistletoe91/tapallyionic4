import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-broadcast-submitted',
  templateUrl: 'broadcast-submitted.html',
})
export class BroadcastSubmittedPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  gotBackToBroadcastPage() {
      this.navCtrl.setRoot("BroadcastPage");
  }

  ionViewDidLoad() { 
  }

}
