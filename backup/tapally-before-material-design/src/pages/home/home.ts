import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Events } from 'ionic-angular';
import { RequestsProvider } from '../../providers/requests/requests';
@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  total_connections;
  total_referral_send;
  total_referral_recieved;
  total_earned;
  myrequests = [];
  requestcounter = null;
  constructor(public navCtrl: NavController, public events: Events,public navParams: NavParams,public requestservice: RequestsProvider) {
      this.total_connections = 0;
      this.total_referral_send = 0;
      this.total_referral_recieved = 0;
      this.total_earned = 0;
  }
  presentPopover(myEvent) {
      this.navCtrl.push("NotificationPage");
      this.requestcounter = this.myrequests.length;
  }
  ionViewDidLoad() {
  }
  ionViewWillEnter() {
      this.requestservice.getmyrequests();
      this.events.subscribe('gotrequests', () => {
          this.myrequests = [];
          this.myrequests = this.requestservice.userdetails;
          if (this.myrequests) {
              this.requestcounter = this.myrequests.length;
          }
      })
  }
  fnGoToSearchNextPage (){
    this.navCtrl.setRoot('ContactPage');
  }

  addnewContact() {
      this.navCtrl.setRoot('ContactPage');
  }
}
