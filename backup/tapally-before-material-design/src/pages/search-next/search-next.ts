import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ToastController } from 'ionic-angular'; 

@IonicPage()
@Component({
  selector: 'page-search-next',
  templateUrl: 'search-next.html',
})
export class SearchNextPage {
  items;
  myParam = 0;
  constructor(public navCtrl: NavController, public toastCtrl: ToastController, public navParams: NavParams) {
      this.myParam = navParams.get('src');
      this.initializeItems();
  }

  fnRefer (){
      if(this.myParam == 2 ){
        this.navCtrl.setRoot('SearchPage',  {}, {animate: true} );
        this.presentToast2();
      } else{
        if(this.myParam == 3 ){
          this.navCtrl.setRoot('SettingsPage',  {}, {animate: true} );
          this.presentToast2();
        } else{
          this.navCtrl.setRoot('ChooseClubPage',  {}, {animate: true} );
          this.presentToast1();
        }
      }

  }


  presentToast1() {
    const toast = this.toastCtrl.create({
      position : "top",
      message: 'Your successfully referred Gary to your contact' ,
      duration: 3000
    });
    toast.present();
  }

  presentToast2() {
    const toast = this.toastCtrl.create({
      position : "top",
      message: 'Your send referral successfully',
      duration: 3000
    });
    toast.present();
  }
  send1 (){
    this.navCtrl.setRoot('SearchNextSendPage',  {}, {animate: true} );
  }

  initializeItems() {
    this.items = [
      'Kelly	Roberson' ,
      'Colleen	Bryant',
      'Buenos Aires',
      'Eric	Pearson',
      'Christina	Bowen',
      'Pat	Moore',
      'Myra	Parks',
      'Dianna	Mack',
      'Clayton	Barrett',
      'Cynthia	Ross',
      'Noah	Bryan',
      'Pat	Moore',
      'Myra	Parks',
      'Dianna	Mack',
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchNextPage');
  }

}
