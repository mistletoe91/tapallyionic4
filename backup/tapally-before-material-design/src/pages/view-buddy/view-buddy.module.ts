import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewBuddyPage } from './view-buddy';

@NgModule({
  declarations: [
    ViewBuddyPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewBuddyPage),
  ],
})
export class ViewBuddyPageModule {}
