import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CatProvider } from '../../providers/cats/cats';
@IonicPage()
@Component({
  selector: 'page-friend-asking-referral',
  templateUrl: 'friend-asking-referral.html',
})
export class FriendAskingReferralPage {

  friend_asking_referral_uid;
  friend_asking_referral_displayName;
  friend_asking_referral_catName;
  friend_asking_referral_catId;
  cats;
  constructor(public navCtrl: NavController, public navParams: NavParams,public catservice: CatProvider) {
    this.cats = this.catservice.getCats();
    this.friend_asking_referral_uid = navParams.get('friend_asking_referral_uid');
    this.friend_asking_referral_displayName = navParams.get('friend_asking_referral_displayName');


    for (var i = 0; i < this.cats.length; i++) {
        if(this.cats[i].id == navParams.get('catid')){
          this.friend_asking_referral_catId = this.cats[i].id;
          this.friend_asking_referral_catName =  this.cats[i].name;
          break;
        }
    }
  }

  sendPickContactTo (){
    this.navCtrl.setRoot("PickContactFromPage", {friend_asking_referral_uid : this.friend_asking_referral_uid,friend_asking_referral_displayName : this.friend_asking_referral_displayName, friend_asking_referral_catId:this.friend_asking_referral_catId, friend_asking_referral_catName : this.friend_asking_referral_catName});
  }

  ionViewDidLoad() {
  }

}
