import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { LoadingProvider } from '../../providers/loading/loading';
import 'rxjs/add/operator/map';

@IonicPage()
@Component({
  selector: 'page-country-list',
  templateUrl: 'country-list.html',
})
export class CountryListPage {
	countryList:any;
	tempcountryList:any;
	CountryValue:any;
	selection:any;

  constructor(public loading :LoadingProvider,
              public navCtrl: NavController,
              public navParams: NavParams,
              public http : HttpClient,
              public viewCtrl :ViewController) { }

  ionViewDidLoad() {
    this.loading.presentLoading();
    this.http.get('assets/data/countryList.json')
    .subscribe(data => {
       var arr = Object.keys(data).map(function(k) { return data[k] });
        this.loading.dismissMyLoading();
        this.countryList = arr;
        this.tempcountryList = arr;
    });

    let code = this.navParams.get('type');


    if(!code){
      this.selection  = '';
    }else{
      this.selection = code;
    }

  }


  SelectedCountry(name){
  	localStorage.setItem("country",JSON.stringify(name));
  	localStorage.setItem("code", JSON.stringify(this.selection.toString()));
  	let data = { 'name' : name,'code' : this.selection};
  	this.viewCtrl.dismiss(data);
  }

  initializeItems(){
  	this.countryList = this.tempcountryList;
  }

  closePopover(){
    this.viewCtrl.dismiss();
  }

  searchItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.countryList = this.countryList.filter((item) => {
        let plus = "+"+item.code;
        return (item.name.toLowerCase().indexOf(val.toLowerCase().trim()) > -1) || (item.code.toLowerCase().indexOf(val.toLowerCase().trim()) > -1) || (plus.toLowerCase().indexOf(val.toLowerCase().trim()) > -1);
      });
    }
  }

}
