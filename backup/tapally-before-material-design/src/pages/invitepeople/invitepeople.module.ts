import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InvitepeoplePage } from './invitepeople';

@NgModule({
  declarations: [
    InvitepeoplePage,
  ],
  imports: [
    IonicPageModule.forChild(InvitepeoplePage),
  ],
})
export class InvitepeoplePageModule {}
