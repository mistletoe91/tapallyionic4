import { Component } from '@angular/core';
import { IonicPage, MenuController, NavController, Platform } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html'
})
export class TutorialPage {
  slides;

  constructor(public navCtrl: NavController, public menu: MenuController, public platform: Platform) {
    this.slides = [
      {
        title: "Grow Your Business Revenue!",
        description: "Lorem Ipsum isjoda siodj aoisjd ioajs dioaosi d",
        image: "assets/img/ica-slidebox-img-1.png",
      },
      {
        title: "What is not Tapally?",
        description: "Lorem Ipsum isjoda siodj aoisjd ioajs dioaosi d",
        image: "assets/img/ica-slidebox-img-2.png",
      },
      {
        title: "What is Tapally Cloud?",
        description: "Lorem Ipsum isjoda siodj aoisjd ioajs dioaosi d",
        image: "assets/img/ica-slidebox-img-3.png",
      }
    ];
  }

  gotoStartPage (){
      this.navCtrl.setRoot('PhonePage');
  }

  onSlideChangeStart(slider) {
    //this.showSkip = !slider.isEnd();
  }

  ionViewDidEnter() {
    // the root left menu should be disabled on the tutorial page
    this.menu.enable(false);
  }

  ionViewWillLeave() {
    // enable the root left menu when leaving the tutorial page
    this.menu.enable(true);
  }

}
