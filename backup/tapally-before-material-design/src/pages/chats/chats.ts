import { Component, NgZone, ElementRef } from '@angular/core';

import { IonicPage, NavController, Platform, NavParams, Events, AlertController, PopoverController, LoadingController, ModalController } from 'ionic-angular';

import { RequestsProvider } from '../../providers/requests/requests';
import { ChatProvider } from '../../providers/chat/chat';
import { UserProvider } from '../../providers/user/user';
import { ContactProvider } from '../../providers/contact/contact';
import { LoadingProvider } from '../../providers/loading/loading';
import { FcmProvider } from '../../providers/fcm/fcm';
import { GroupsProvider } from '../../providers/groups/groups';
import { ImagehandlerProvider } from '../../providers/imagehandler/imagehandler';

import firebase from 'firebase';

@IonicPage()
@Component({
    selector: 'page-chats',
    templateUrl: 'chats.html',
})
export class ChatsPage {

    myrequests = [];
    myfriends;
    myfriendsList = [];
    tempmyfriendsList = [];
    debugMe:boolean = true;
    showPaidSign:boolean = false;
    today;
    isData: boolean = false;
    isNoRecord: boolean = false;
    requestcounter = null;
    username: string = "";
    avatar: string;
    selectContact = false;
    selectedContact = [];
    selectCounter = 0
    allmygroups = [];
    tempallmygroups = [];
    allChatListing = [];
    archiveChats = [];
    counter: any = 0;
    messages = [];
    whitelabelObj;
    businessName;
    iAmBusinessOwner: string;
    constructor(public fcm: FcmProvider,
        public platform: Platform,
        public loadingProvider: LoadingProvider,
        public contactProvider: ContactProvider,
        public navCtrl: NavController,
        public navParams: NavParams,
        public requestservice: RequestsProvider,
        public events: Events,
        public alertCtrl: AlertController,
        public chatservice: ChatProvider,
        public zone: NgZone,
        public popoverCtrl: PopoverController,
        public imghandler: ImagehandlerProvider,
        public userservice: UserProvider,
        public loadingCtrl: LoadingController,
        public eleRef: ElementRef,
        public modalCtrl: ModalController,
        public groupservice: GroupsProvider,
    ) {
      this.whitelabelObj = {};
    }
    ionViewDidLoad() {
        console.log ("ionViewDidLoad");
        this.today = new Date();

    }

    sendto_invite (){
      if(localStorage.getItem('businessName')){
        this.navCtrl.push("OutreachForBusinessPage");
      } else {
        this.navCtrl.push("RegisterbusinessPage");
      }
    }

    referralSendForward (buddy){
        // I want to send buddy's referral to someone else
        this.navCtrl.push("SendReferralForwardPage", {buddy_uid : buddy.uid,buddy_displayName :buddy.displayName});

    }
    referralSendBackward (buddy){
      this.navCtrl.push("SendReferralBackwardPage", {buddy_uid : buddy.uid,buddy_displayName : buddy.displayName});
    }


    ionViewWillLeave() {
        this.loadingProvider.dismissMyLoading();//
    }

    sendToBroadcastPage (){
      this.navCtrl.push("BroadcastPage");
    }



    //KEep it same as chats.ts
    paidOrNot (){
        this.showPaidSign = false;
        if(this.debugMe){console.log ("paidornot checkiing");console.log (localStorage.getItem('business_created'));}
        if(localStorage.getItem('business_created')){
            let AcceptableDelayInMicroseconds:number = 1000; //Before we show warning to business that they have not paid yet
            let timeStamp:number = Date.now();
            let business_created:number = parseInt(localStorage.getItem('business_created'));
            if(this.debugMe){console.log ("business_created checkiing");}
            //Do not check for first few minutes/hours/days ( as specified in AcceptableDelayInMicroseconds )
            //But check every single time afterwards
            if((business_created+AcceptableDelayInMicroseconds)<timeStamp){
                  let paid:number = parseInt(localStorage.getItem('paid'));
                  if(this.debugMe){console.log ("paid checkiing");console.log(paid);}
                  //check API everytime
                  if(true){
                          if(this.debugMe){console.log ("calling getBusinessDetails");}
                          this.userservice.getBusinessDetails().then((res)=>{
                                  //localStorage.setItem('businessName', res['displayName']);
                                  //localStorage.setItem('bcats', res['bcats']);
                                  if(!paid || paid<=0){
                                      console.log ("Indeed Business has not paid yet");
                                      this.showPaidSign = true;
                                  }
                          }).catch(err=>{
                          });
                  }//endif
            } else {

            }//endif
        }//endif
    }//end function


    ionViewWillEnter() {
        console.log ("ionViewWillEnter");
        //console.log (Date.now());

        this.whitelabelObj = {};
        if(localStorage.getItem('invited_by') && localStorage.getItem('invited_by')!= "none"){
          this.whitelabelObj.buddy_uid = localStorage.getItem('invited_by');
        } else {
          this.whitelabelObj.buddy_uid = 0;
        }
        if(localStorage.getItem('sponseredbusines_name')){
          this.whitelabelObj.buddy_displayName = localStorage.getItem('sponseredbusines_name');
        }else {
          this.whitelabelObj.buddy_displayName = "";
        }
        if(localStorage.getItem('sponseredbusines_businessName')){
          this.whitelabelObj.sponseredbusines_businessName = localStorage.getItem('sponseredbusines_businessName');
        } else {
          this.whitelabelObj.sponseredbusines_businessName = "";
        }

        console.log (this.whitelabelObj);

        this.loadingProvider.presentLoading();
        this.paidOrNot ();
        this.setStatus();
        this.fcm.getToken();
        this.events.publish('checkUserStatus');
        this.requestservice.getmyrequests();
        this.requestservice.getmyfriends();
        this.loaduserdetails();
        this.myfriends = [];
        this.events.subscribe('gotrequests', () => {
            this.events.unsubscribe('gotrequests');
            console.log ("gotrequests()");
            this.myrequests = [];
            this.myrequests = this.requestservice.userdetails;
            if (this.myrequests) {
                this.requestcounter = this.myrequests.length;
            }
        })
        this.events.subscribe('friends', () => {
            this.events.unsubscribe('friends');
            console.log ("Resetting allChatListing to []");
            //this.allChatListing = [];
            this.tempallmygroups = [];
            this.allmygroups = [];
            this.myfriends = [];
            this.myfriendsList = [];
            this.myfriends = this.sortdatalist(this.requestservice.myfriends);
            if (this.myfriends.length > 0) {
                this.isData = false;
                this.convertWithContacts(this.myfriends).then((res: any) => {
                    if (res.length > 0) {
                        this.removeDuplicates(res, 'mobile').then((responce: any) => {
                            this.myfriendsList = [];
                            this.myfriendsList = responce;
                            this.doRefresh();
                            this.events.subscribe('allmygroups', () => {
                                this.events.unsubscribe('allmygroups');
                                this.allmygroups = [];
                                this.allmygroups = this.groupservice.mygroups;
                                this.doRefresh();
                            });
                            this.groupservice.getmygroups();

                        })
                    }
                })
            } else {
                this.isData = true;
                this.loadingProvider.dismissMyLoading();
            }
        });

        //this.setForWhiteLabeling ();

    }
    removeDuplicates(originalArray, prop) {
        return new Promise((resolve) => {
            var newArray = [];
            var lookupObject = {};
            for (var i in originalArray) {
                lookupObject[originalArray[i][prop]] = originalArray[i];
            }
            for (i in lookupObject) {
                newArray.push(lookupObject[i]);
            }
            resolve(newArray);
        })
    }


    //Function might not be used : depriciated
    /*
    setForWhiteLabeling (){
      if(localStorage.getItem('invited_by')){
          if(localStorage.getItem('invited_by') == "none"){
              //When you show the user , highlight it as sponsered

          }
      } else {
         //check once and it not then set
         //--if none then set the localStorage with none
         this.loaduserdetails ();
      }
    }
    */

    doRefresh() {
        console.log ("Do Refersh()");
        let tmpalllist: any = [];

        if (this.allmygroups.length > 0) {
            for (let i = 0; i < this.allmygroups.length; i++) {
                this.allmygroups[i].gpflag = 1;
                tmpalllist.push(this.allmygroups[i]);
            }
        }
        if (this.myfriendsList.length > 0) {
            for (let i = 0; i < this.myfriendsList.length; i++) {
                this.myfriendsList[i].gpflag = 0;
                this.myfriendsList[i].whitelabelcss = '';

                //check for whitelabeling
                //if(localStorage.setItem('business_created') && localStorage.setItem('paid')){
                  //I am business owner myself so I should get whitelabeling benfits
                //} else {
                  if(localStorage.getItem('invited_by')){
                    //console.log ( this.myfriendsList[i].uid +":"+ localStorage.getItem('invited_by'));
                    if( this.myfriendsList[i].uid == localStorage.getItem('invited_by')){
                        //this.myfriendsList[i].whitelabelcss = 'whitelabelcss';
                        localStorage.setItem('sponseredbusines_name',this.myfriendsList[i].displayName);

                        //localStorage.setItem('sponseredbusines_businessName');
                    }
                  } else {
                    //For some reason the local storage is empty. Let's fill it : Disabled for now
                    //this.loaduserdetails ();
                  }
                //}



                tmpalllist.push(this.myfriendsList[i]);
            }
        }
        if (this.selectedContact.length > 0) {
            this.selectedContact = [];
            this.selectCounter = 0;
            this.selectContact = false;
            for (var i = 0; i < this.allChatListing.length; i++) {
                this.allChatListing[i].selection = false;
            }
        }

        setTimeout(() => {
            console.log ("Set Timeout inside doRefersh ()");
            if (this.myfriendsList.length > 0 || this.allmygroups.length > 0) {
                this.zone.run(() => {
                    this.archiveChats = [];
                    this.allChatListing = [];
                    this.tempallmygroups = [];
                    this.counter = [];
                    this.allChatListing = tmpalllist.sort((a, b) => new Date(b.userprio).getTime() - new Date(a.userprio).getTime());
                    this.tempallmygroups = this.allChatListing;
                    //console.log ("Done Setting allChatListing after SORTING");



                    this.loadingProvider.dismissMyLoading();
                });
            }
        }, 1000);
    }
    closeButtonClick() {
        console.log ("CloseButton Click");
        if (this.selectedContact.length > 0) {
            this.selectedContact = [];
            this.selectCounter = 0;
            this.selectContact = false;
            for (var i = 0; i < this.allChatListing.length; i++) {
                this.allChatListing[i].selection = false;
            }
        }
    }
    CheckLastMSG(lastmsg, msg) {
        if (!(msg instanceof Array) && msg.match("^(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|www\\.){1}([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?")) {
            return false
        } else if (!(msg instanceof Array) && lastmsg) {
            return true
        } else {
            return false
        }
    }
    CheckLastMsgUrl(url) {

        if (!(url instanceof Array) && url.match("^(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|www\\.){1}([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?")) {

            return true;
        } else if ((url instanceof Array) && url.length > 0) {

            return true;
        }
    }
    sortdatalist(data) {
        return data.sort((a, b) => {
            let result = null;
            if (a.userprio != null) {
                var dateA = new Date(a.userprio).getTime();
                var dateB = new Date(b.userprio).getTime();
            }
            result = dateA < dateB ? 1 : -1;
            return result;
        });
    }
    refreshPage(refresher) {
        this.ionViewWillEnter();
        setTimeout(() => {
            refresher.complete();
        }, 500);
    }
    SortByDate(array) {

        array.sort((a: any = [], b: any = []) => {
            if (a.lastMessage.timestamp > b.lastMessage.timestamp) {
                return -1;
            } else if (a.lastMessage.timestamp < b.lastMessage.timestamp) {
                return 1;
            } else {
                return 0;
            }

        });
        return array;
    }
    viewPofile(url, name) {
        if (this.selectedContact.length == 0) {
            let modal = this.modalCtrl.create("ProfileViewPage", { img: url, name: name });
            modal.present();
        }
    }
    initializeContacts() {
        console.log ("initializeContacts ()");
        this.allChatListing = this.tempallmygroups;
    }
    searchuser(ev: any) {
        this.initializeContacts();
        this.isNoRecord = false;
        let val = ev.target.value;
        if (val && val.trim() != '') {
            this.allChatListing = this.allChatListing.filter((item) => {
                if (item.displayName) {
                    return (item.displayName.toLowerCase().indexOf(val.toLowerCase().trim()) > -1);
                }
                else if (item.groupName) {
                    return (item.groupName.toLowerCase().indexOf(val.toLowerCase().trim()) > -1);
                }
            });
            if (this.allChatListing.length == 0) {
                this.isNoRecord = true;
            }
        }
    }
    setStatus() {
        this.chatservice.setstatusUser().then((res) => {
            if (res) {
            }
        }).catch((err) => {
        })
    }
    presentPopover(myEvent) {

        this.navCtrl.push("NotificationPage");
        this.requestcounter = this.myrequests.length;
    }
    viewPopover(myEvent) {
        let popover = this.popoverCtrl.create('PopoverChatPage', { page: '0' });
        popover.present({
            ev: myEvent
        });
        popover.onDidDismiss((pages) => {
            if (pages != 0 || pages != undefined || pages != null) {
                this.navCtrl.push(pages.page);

            }
        });
    }

    addbuddy() {
        this.navCtrl.push('ContactPage');
    }
    //profile data
    loaduserdetails() {
        this.userservice.getuserdetails().then((res: any) => {
            this.username = res.displayName;
            localStorage.setItem('invited_by', res.invited_by);

            /*
            Check if the whitelabeled user is paid user : But only check like once a day . Not more than that
            */
            if(res.invited_by){
                  let AcceptableDelayInMicroseconds:number = 1000; //1 day : Before we check if another business is paid member or not
                  let invited_by_business_paid_flag_checked_date:number = parseInt(localStorage.getItem('invited_by_business_paid_flag_checked_date'));
                  let timeStamp:number = Date.now();
                  if(!invited_by_business_paid_flag_checked_date) {
                    invited_by_business_paid_flag_checked_date = 0;
                  }

                  if((invited_by_business_paid_flag_checked_date+AcceptableDelayInMicroseconds)<timeStamp){
                      console.log ("Attempting to check Cloud for checkIfBusinessIsPaid");
                      if(!res.invited_by || res.invited_by != "none"){
                          this.userservice.checkIfBusinessIsPaid(res.invited_by).then((res_invitedby: any) => {

                                  localStorage.setItem('sponseredbusines_businessName',res_invitedby.displayName);
                                  localStorage.setItem('invited_by_business_paid_flag_checked_date', String(Date.now()));//jaswinder
                                  console.log ("----> CHECK");
                                  if(res_invitedby.paid && res_invitedby.paid == "1" ){
                                      localStorage.setItem('invited_by', res.invited_by);
                                      console.log ("--> Business Paid Flag Checked and set invited_by");
                                  } else {
                                      //member is not paid so lets not do whitelabeling
                                      localStorage.setItem('invited_by', "none");
                                      console.log ("--> Business Paid Flag Checked and REMOVED whitelabeling because he/she did not paid ");

                                      console.log ("RESET the invited by " + res.invited_by);
                                      localStorage.setItem('invited_by', "");
                                      localStorage.setItem('sponseredbusines_businessName', "");
                                      localStorage.setItem('sponseredbusines_name', "");

                                  }
                                  console.log ("ENDIF DONE");
                          });
                      } else {
                              console.log ("RESET the invited by " + res.invited_by);
                              localStorage.setItem('invited_by', "none");
                              localStorage.setItem('sponseredbusines_businessName', "");
                              localStorage.setItem('sponseredbusines_name', "");
                      }
                  }//endif
            }

            this.zone.run(() => {
                this.avatar = res.photoURL;
            })
        })
    }
    logout() {
        this.chatservice.setStatusOffline().then((res) => {
            if (res) {
                firebase.auth().signOut().then(() => {
                    this.navCtrl.setRoot('LoginPage');
                })
            }
        })
    }
    editname() {
        let statusalert = this.alertCtrl.create({
            buttons: ['okay']
        });
        let alert = this.alertCtrl.create({
            title: 'Edit Nickname',
            inputs: [{
                name: 'nickname',
                placeholder: 'Nickname'
            }],
            buttons: [{
                text: 'Cancel',
                role: 'cancel',
                handler: data => {

                }
            },
            {
                text: 'Edit',
                handler: data => {
                    if (data.nickname) {
                        this.userservice.updatedisplayname(data.nickname).then((res: any) => {
                            if (res.success) {
                                statusalert.setTitle('Updated');
                                statusalert.setSubTitle('Your username has been changed successfully!!');
                                statusalert.present();
                                this.zone.run(() => {
                                    this.username = data.nickname;
                                })
                            }
                            else {
                                statusalert.setTitle('Failed');
                                statusalert.setSubTitle('Your username was not changed');
                                statusalert.present();
                            }

                        })
                    }
                }

            }]
        });
        alert.present();
    }
    editimage() {
        let statusalert = this.alertCtrl.create({
            buttons: ['okay']
        });
        this.imghandler.uploadimage().then((url: any) => {
            this.userservice.updateimage(url).then((res: any) => {
                if (res.success) {
                    statusalert.setTitle('Updated');
                    statusalert.setSubTitle('Your profile pic has been changed successfully!!');
                    statusalert.present();
                    this.zone.run(() => {
                        this.avatar = url;
                    })
                }
            }).catch((err) => {
                statusalert.setTitle('Failed');
                statusalert.setSubTitle('Your profile pic was not changed');
                statusalert.present();
            })
        })
    }
    deleteAllMessage() {
        let title;
        let message;

        if (this.selectedContact[0].uid) {
            title = 'Clear Chat';
            message = 'Do you want to clear this chat ?';
        } else {
            title = 'Delete Group';
            message = 'Do you want to Delete this group ?';
        }
        let alert = this.alertCtrl.create({
            title: title,
            message: message,
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                    }
                },
                {
                    text: 'Ok',
                    handler: () => {
                        if (this.selectedContact.length > 0) {
                            for (let j = 0; j < this.selectedContact.length; j++) {
                                if (this.selectedContact[j].uid) {
                                    for (let i = 0; i < this.selectedContact.length; i++) {
                                        this.chatservice.deleteUserMessages(this.selectedContact).then((res) => {
                                                if (res) {
                                                    this.selectedContact = []
                                                    if (this.selectedContact.length == 0) {
                                                        this.selectContact = false;
                                                    }
                                                    for (let k = 0; k < this.allChatListing.length; k++) {
                                                        this.allChatListing[k].selection = false;
                                                    }
                                                }
                                        })
                                    }
                                } else {
                                    for (let i = 0; i < this.selectedContact.length; i++) {
                                        let groupname = this.selectedContact[i].groupName;

                                        this.groupservice.getgroupInfo(groupname).then((ress: any) => {

                                            let owners = this.groupservice.converanobj(ress.owner)
                                            if (owners.length == 1) {
                                                this.groupservice.getownership(groupname).then((res) => {
                                                    if (res) {

                                                        this.groupservice.deletegroups(groupname).then((res) => {
                                                            if (res) {
                                                                this.selectedContact = []
                                                                if (this.selectedContact.length == 0) {
                                                                    this.selectContact = false;
                                                                }
                                                                for (let k = 0; k < this.allChatListing.length; k++) {
                                                                    this.allChatListing[k].selection = false;
                                                                }
                                                            }
                                                        });
                                                    } else {
                                                        this.groupservice.leavegroups(groupname).then((res) => {
                                                            if (res) {
                                                                this.selectedContact = []
                                                                if (this.selectedContact.length == 0) {
                                                                    this.selectContact = false;
                                                                }
                                                                for (var m = 0; m < this.allChatListing.length; m++) {
                                                                    this.allChatListing[m].selection = false;
                                                                }
                                                            }

                                                        });
                                                    }
                                                });
                                            } else {
                                                this.groupservice.getownership(groupname).then((res) => {
                                                    if (res) {
                                                        this.groupservice.leavegroupsowner(groupname).then((res) => {
                                                            if (res) {
                                                                this.selectedContact = []
                                                                if (this.selectedContact.length == 0) {
                                                                    this.selectContact = false;
                                                                }
                                                                for (var p = 0; p < this.allChatListing.length; p++) {
                                                                    this.allChatListing[p].selection = false;
                                                                }
                                                            }

                                                        });
                                                    } else {
                                                        this.groupservice.leavegroups(groupname).then((res) => {
                                                            if (res) {
                                                                this.selectedContact = []
                                                                if (this.selectedContact.length == 0) {
                                                                    this.selectContact = false;
                                                                }
                                                                for (var o = 0; o < this.allChatListing.length; o++) {
                                                                    this.allChatListing[o].selection = false;
                                                                }
                                                            }

                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                }
                            }
                        }
                    }
                }
            ]
        });
        alert.present();
    }
    popoveroncontact(item, event) {
        if (this.selectedContact.length == 0) {
            item.selection = true;
            this.selectedContact.push(item)
            if (this.selectContact == true) {
                this.selectContact = false;
            } else {
                this.selectContact = true;
            }
        }
        this.selectCounter = this.selectedContact.length;
        if (this.selectedContact.length == 0) {
            this.selectContact = false;
            item.selection = false;
        }
    }
    buddychat(item, event, UnreadCount) {
      console.log ("Buddy chat");
        if (event.target.classList.contains('avtar-class') || event.target.classList.contains('avtar-img-list')) {
        } else {
            if (this.selectedContact.length == 0) {
                localStorage.setItem('UnreadCount', UnreadCount);
                this.chatservice.initializebuddy(item);
                this.navCtrl.push('BuddychatPage');
            }
            else if (this.selectedContact.length == 1) {
                if (this.selectedContact[0].uid == item.uid) {
                    item.selection = false;
                    this.selectedContact = [];
                    this.selectCounter = 0;
                    this.selectContact = false;
                } else {
                    this.selectedContact = [];
                    this.selectCounter = 0;
                    this.selectContact = false;
                    for (var i = 0; i < this.allChatListing.length; i++) {
                        this.allChatListing[i].selection = false;
                    }
                    item.selection = true;
                    this.selectedContact.push(item);
                    this.selectCounter = this.selectedContact.length;
                    this.selectContact = true;
                }
            } else {
                this.popoveroncontact(item, event);
            }

        }
    }
    openchat(item, event) {
        console.log ("Open Chat");

        if (this.selectedContact.length == 0) {
            this.groupservice.getintogroup(item.groupName);
            this.navCtrl.push('GroupchatPage', { groupName: item.groupName, groupImage: item.groupimage });
        } else if (this.selectedContact.length == 1) {
            if (this.selectedContact[0].groupName == item.groupName) {
                item.selection = false;
                this.selectedContact = [];
                this.selectCounter = 0;
                this.selectContact = false;
            } else {
                this.selectedContact = [];
                this.selectCounter = 0;
                this.selectContact = false;
                for (var i = 0; i < this.allChatListing.length; i++) {
                    this.allChatListing[i].selection = false;
                }
                item.selection = true;
                this.selectedContact.push(item);
                this.selectCounter = this.selectedContact.length;
                this.selectContact = true;
            }
        } else {
            this.popoveroncontact(item, event);
        }
    }
    convertWithContacts(data) {
        return new Promise((resolve) => {
            this.contactProvider.getSimJsonContacts().then(res => {
                if (res['contacts'].length > 0) {
                    for (let i = 0; i < res['contacts'].length; ++i) {
                        for (let j = 0; j < data.length; ++j) {
                            if (res['contacts'][i].mobile == data[j].mobile) {
                                data[j].displayName = res['contacts'][i].displayName;
                            }
                        }
                    }
                    let islastMessage: any = [];
                    let islastMessageNot: any = [];
                    for (let i = 0; i < data.length; ++i) {
                        if (data[i].lastMessage) {
                            islastMessage.push(data[i]);
                        } else {
                            islastMessageNot.push(data[i]);
                        }
                    }
                    data = this.SortByDate(islastMessage);
                    data = data.concat(islastMessageNot);
                    resolve(data);
                } else {
                    resolve(data);
                }
            }).catch(err => {
                if (err == 20) {
                    this.platform.exitApp();
                }
                resolve(data);
            });
        })

    }
}
