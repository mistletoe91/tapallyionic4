import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {

  //tab1Root: string = 'HomePage';
  tab2Root: string = 'ChatsPage';
  tab3Root: string = 'ReferralPage';
  //tab4Root: string = 'BroadcastPage';

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewWillEnter() {
    if(localStorage.getItem('ever_referral_sent')){
      this.tab3Root = "ReferralPage";
    } else {
      this.tab3Root = "ReferralInfopagePage";
    }
  }

  ionViewDidLoad() {
  }

}
