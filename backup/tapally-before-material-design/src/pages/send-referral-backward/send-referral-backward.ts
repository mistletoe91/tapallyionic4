import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-send-referral-backward',
  templateUrl: 'send-referral-backward.html',
})
export class SendReferralBackwardPage {

    buddy_displayName;
    buddy_uid;
    constructor(public navCtrl: NavController, public navParams: NavParams) {
      this.buddy_displayName = navParams.get('buddy_displayName');
      this.buddy_uid = navParams.get('buddy_uid');
    }

    pickContact (){
      this.navCtrl.push("PickContactFromPage", {friend_asking_referral_uid : this.buddy_uid,friend_asking_referral_displayName : this.buddy_displayName, friend_asking_referral_catId:0, friend_asking_referral_catName : 0});
    }

    ionViewDidLoad() {
    }
}
