import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SendReferralPickPage } from './send-referral-pick';

@NgModule({
  declarations: [
    SendReferralPickPage,
  ],
  imports: [
    IonicPageModule.forChild(SendReferralPickPage),
  ],
})
export class SendReferralPickPageModule {}
