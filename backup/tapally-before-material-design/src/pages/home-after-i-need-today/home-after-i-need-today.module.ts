import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomeAfterINeedTodayPage } from './home-after-i-need-today';

@NgModule({
  declarations: [
    HomeAfterINeedTodayPage,
  ],
  imports: [
    IonicPageModule.forChild(HomeAfterINeedTodayPage),
  ],
})
export class HomeAfterINeedTodayPageModule {}
