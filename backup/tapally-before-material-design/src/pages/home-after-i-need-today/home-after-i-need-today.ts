import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Events } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { ChatProvider } from '../../providers/chat/chat';
import { UserProvider } from '../../providers/user/user';
import { FcmProvider } from '../../providers/fcm/fcm';
import { RequestsProvider } from '../../providers/requests/requests';
import { LoadingProvider } from '../../providers/loading/loading';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-home-after-i-need-today',
  templateUrl: 'home-after-i-need-today.html',
})
export class HomeAfterINeedTodayPage {
  buddy: any;
  newmessage = '';
  cntCounter;
  myfriends;
  myfriendsList;
  tempmyfriendsList;
  isData: boolean = false;
  authForm: FormGroup;
  message: AbstractControl;
  selectCatId;
  constructor(public fcm: FcmProvider,public loading: LoadingProvider,public requestservice: RequestsProvider,public fb: FormBuilder,public events: Events,public chatservice: ChatProvider,public userservice: UserProvider,public navCtrl: NavController, public toastCtrl: ToastController, public navParams: NavParams) {
    this.selectCatId = navParams.get('cat_id');
    this.authForm = this.fb.group({
        'message': [null, Validators.compose([Validators.required])]
    });
    this.message = this.authForm.controls['message'];
  }

  ionViewWillEnter() {
      this.requestservice.getmyfriends();
      this.myfriends = [];
      this.events.subscribe('friends', () => {
        this.myfriends = [];
        this.myfriends = this.requestservice.myfriends;
      });
  }

  broadcast() {
      this.cntCounter = 0;
      for (var key=0;key<this.myfriends.length;key++) {
        this.addmessage(this.myfriends[key]);
      }//end for


      //mytodo : remove this from here and just call asyn a web service API to send messages.
      //Now the mobile is doing heavylifting. But ideally the server should do and send to nosql database
      /*
      this.cntCounter = 0;
      this.events.subscribe('friends', () => {
        if (this.requestservice.myfriends.length > 0) {
            console.log ("-->" + this.requestservice.myfriends.length);
            for (var key=0;key<this.requestservice.myfriends.length;key++) {
               this.buddy = this.requestservice.myfriends[key];
               console.log (key);
               //console.log(this.buddy);
               this.addmessage();
            }//end for
        }
      });
      */

  }

  sendToNextPage (){
      this.cntCounter++;
      if(this.cntCounter>=this.myfriends.length){
        this.cntCounter = 0;
        this.navCtrl.setRoot("RequestSubmittedPage");
      }
  }

  addmessage(buddy) {
    this.newmessage = this.newmessage.trim();

    //Send Push notification : mytodo check if the buddy is online or not. if online then no need to send push notification
    let newMessage = 'You have received a message';
    this.fcm.sendNotification(buddy, newMessage, 'chatpage');

    //Send chat message
    this.chatservice.addnewmessageBroadcast(this.newmessage, 'message', buddy, this.selectCatId).then(() => {
        this.newmessage = '';
        localStorage.setItem('ever_referral_sent', "1");
        this.sendToNextPage ();
    });
  }

  ionViewDidLoad() {
  }

}
