import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-send-referral-forward',
  templateUrl: 'send-referral-forward.html',
})
export class SendReferralForwardPage {

  buddy_displayName;
  buddy_uid;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.buddy_displayName = navParams.get('buddy_displayName');
    this.buddy_uid = navParams.get('buddy_uid');
  }

  forwardContact (){
    this.navCtrl.push("PickContactToPage", {friend_to_forward_uid : this.buddy_uid,friend_to_forward_displayName : this.buddy_displayName, friend_to_forward_catId:"", friend_to_forward_catName : ""});
  }

  ionViewDidLoad() {
  }

}
