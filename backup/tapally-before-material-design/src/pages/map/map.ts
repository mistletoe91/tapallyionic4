import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, IonicPage, ViewController, NavParams, AlertController, Platform } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { GroupsProvider } from '../../providers/groups/groups';
import { UserProvider } from '../../providers/user/user';
import { ChatProvider } from '../../providers/chat/chat';
import { FcmProvider } from '../../providers/fcm/fcm';
import { LoadingProvider } from '../../providers/loading/loading';

declare var google;

@IonicPage()
@Component({
    selector: 'page-map',
    templateUrl: 'map.html',
})
export class MapPage {
    @ViewChild('map') mapElement: ElementRef;
    map: any;
    loginuserInfo: any = [];
    location: any = { lat: "", long: "" };
    latLng: any;
    flag: boolean;
    newmessage: any;
    buddy: any;
    buddyStatus: any;
    UnreadMSG: any;
    gpname: any;
    constructor(public navCtrl: NavController,
        public geolocation: Geolocation,
        public groupservice: GroupsProvider,
        public userservice: UserProvider,
        private viewCtrl: ViewController,
        public navParams: NavParams,
        public chatservice: ChatProvider,
        private alertCtrl: AlertController,
        public fcm: FcmProvider,
        public loading: LoadingProvider,
        public platform: Platform
    ) {
        this.buddy = this.navParams.get('buddy')
    }

    ionViewDidLoad() {
        this.loading.presentLoading();

        this.gpname = this.navParams.get('gpname');

        this.buddyStatus = this.chatservice.buddyStatus;
        this.userservice.getuserdetails().then((res: any) => {
            this.loginuserInfo = res;
            this.loading.dismissMyLoading();
        }).catch(err => {
        });
        if (this.navParams.get('location')) {
            this.flag = false;
            let latlong = this.navParams.get('location');
            this.latLng = new google.maps.LatLng(latlong.lat, latlong.long);
            this.location.lat = latlong.lat;
            this.location.long = latlong.long;
            setTimeout(() => {
                this.loadMap();
            }, 300);

        } else {
            this.flag = true;
            this.geolocation.getCurrentPosition().then((position) => {
                this.location.lat = position.coords.latitude;
                this.location.long = position.coords.longitude;
                this.latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                setTimeout(() => {
                    this.loadMap();
                }, 300)
            }, (err) => {
                if (err.code == 1) {
                    this.navCtrl.pop();
                }

            });
        }


    }
    loadMap() {
        let mapOptions = {
            center: this.latLng,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
        let marker: any = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: {
                lat: this.location.lat,
                lng: this.location.long
            }
        });
        console.log(marker);
        this.loading.dismissMyLoading();
    }
    goBack() {
        this.loading.dismissMyLoading();
        this.navCtrl.pop();
    }
    share() {
        let temp = this.navParams.get('flag');
        if (temp == true) {
            this.loading.presentLoading();
            let flag = 0;
            this.groupservice.addgroupmsgmultiple(this.location, this.loginuserInfo, 'location', this.gpname).then((res) => {
                this.loading.dismissMyLoading();
                if (res) {
                    flag++;
                }
                this.navCtrl.pop();

            }).catch((err) => {
                this.loading.dismissMyLoading();
            })
        } else if (temp == false) {
            this.loading.presentLoading();
            this.UnreadMSG = 0;
            this.userservice.getstatus(this.buddy).then((res) => {
                if (!res) {

                    this.chatservice.addnewmessagemultiple(this.location, 'location', this.buddyStatus).then((res) => {
                        this.viewCtrl.dismiss();
                        if (this.buddyStatus != 'online') {
                            let newMessage = 'You have received a message';
                            this.fcm.sendNotification(this.buddy, newMessage, 'chatpage');
                        }
                        if (res) {
                        }
                        this.loading.dismissMyLoading();
                        this.navCtrl.pop();
                    }).catch((err) => {
                        this.loading.dismissMyLoading();
                        alert(err);
                    })
                } else {
                    let alert = this.alertCtrl.create({
                        message: 'Unblock ' + this.buddy.displayName + ' to send a message.',
                        buttons: [
                            {
                                text: 'CANCEL',
                                role: 'cancel',
                                handler: () => {
                                }
                            },
                            {
                                text: 'UNBLOCK',
                                handler: () => {
                                    this.userservice.unblockUser(this.buddy).then((res) => {

                                    })
                                }
                            }
                        ]
                    });
                    alert.present();
                }
                this.viewCtrl.dismiss()
            });


        }

    }

}
