import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import { FormBuilder } from '@angular/forms';
@IonicPage()
@Component({
  selector: 'page-username',
  templateUrl: 'username.html',
})
export class UsernamePage {
  username;
  msgstatus: boolean = true;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public fb: FormBuilder,) {
    this.username = this.navParams.get('userName');
  }
  ionViewDidLoad() {
  }
  save() {
      let data = { username:this.username };
      this.viewCtrl.dismiss(data);
  }
 onChange(e) {
  if (this.username.trim()) {
      this.msgstatus = true;
  }
  else {
      this.msgstatus = false;
  }
}
 dismiss(){
   this.viewCtrl.dismiss();
 }
}
