import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-outreach-for-business',
  templateUrl: 'outreach-for-business.html',
})
export class OutreachForBusinessPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  senToInvite (){
    this.navCtrl.push("ContactPage");
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad OutreachForBusinessPage');
  }

}
