import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,Events,Platform} from 'ionic-angular';
import { RequestsProvider } from '../../providers/requests/requests';
import { GroupsProvider } from '../../providers/groups/groups';

@IonicPage()
@Component({
  selector: 'page-groupbuddies',
  templateUrl: 'groupbuddies.html',
})
export class GroupbuddiesPage {

	myfriends:any = [];
	groupmembers = [];
	tempmyfriends = [];
  selectedMember:any=[];

  constructor(
  	public navCtrl: NavController,
  	public navParams: NavParams,
  	public requestservice: RequestsProvider,
    public events: Events,
    public groupservice: GroupsProvider,
    public platform:Platform) {
     
    }

  ionViewWillEnter() {
   let myfriends=this.requestservice.myfriends;
      this.removeDuplicates(myfriends,'mobile').then((response: any) => {
        this.myfriends =[];
        this.myfriends = response; 
      });
    this.tempmyfriends = this.requestservice.myfriends;
  }
  removeDuplicates(originalArray, prop) {
    return new Promise((resolve)=>{
       var newArray = [];
       var lookupObject  = {};
       for(var i in originalArray) {
          lookupObject[originalArray[i][prop]] = originalArray[i];
       }
       for(i in lookupObject) {
           newArray.push(lookupObject[i]);
       }
        resolve(newArray);
  })
}
  initializeItems(){
    this.myfriends = this.tempmyfriends;
  }

  searchuser(ev: any){
    this.initializeItems();

    const val = ev.target.value;

    if (val && val.trim() != '') {
      this.myfriends = this.myfriends.filter((item) => {
        return (item.displayName.toLowerCase().indexOf(val.toLowerCase().trim()) > -1);  
      });
    }
  }
  
   addbuddy(buddy) {
    if(this.selectedMember.length == 0){
      this.selectedMember.push(buddy);
    }else{
      let flagofbuddy = false;
      let posionofbuddy = 0;
      for(let i=0;i<this.selectedMember.length;i++){
        if(this.selectedMember[i].uid == buddy.uid){
          flagofbuddy = true;
          posionofbuddy = i;
          break;
        }else{
          flagofbuddy = false;
        }
      }
      if(flagofbuddy == true){
          this.selectedMember.splice(posionofbuddy,1);
      }else{
          this.selectedMember.push(buddy);
      }
    }
  }

  goBack(){
    this.navCtrl.setRoot("ChatsPage")
  }

  goNext(){
    this.navCtrl.push("GroupsPage",{buddy:this.selectedMember})
  }

}
