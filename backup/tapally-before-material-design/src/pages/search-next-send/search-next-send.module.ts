import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchNextSendPage } from './search-next-send';

@NgModule({
  declarations: [
    SearchNextSendPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchNextSendPage),
  ],
})
export class SearchNextSendPageModule {}
