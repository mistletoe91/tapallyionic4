import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuBlockPage } from './menu-block';

@NgModule({
  declarations: [
    MenuBlockPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuBlockPage),
  ],
})
export class MenuBlockPageModule {}
