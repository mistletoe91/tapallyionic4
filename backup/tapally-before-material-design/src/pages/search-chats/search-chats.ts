import { Component, NgZone } from '@angular/core';
import { IonicPage, ViewController,Platform ,NavController} from 'ionic-angular';
import { ChatProvider } from '../../providers/chat/chat';
import { Storage } from '@ionic/storage';
import { UserProvider } from '../../providers/user/user';
import { App } from 'ionic-angular';
@IonicPage()
@Component({
	selector: 'page-search-chats',
	templateUrl: 'search-chats.html',
})
export class SearchChatsPage {

	allChats: any;
	tempallChats: any;
	buddy: any;
	photoURL: any;
	todaydate: any;
	isNoRecord: boolean = false;

	constructor(public zone: NgZone,public app: App,public navCtrl:NavController, public chatservice: ChatProvider, public viewCtrl: ViewController, public storage: Storage, public userservice: UserProvider,platform:Platform) {
	
	}

	ionViewDidLoad() {
	}
	
	ionViewWillEnter() {
		this.storage.get('allBuddyChats').then(res => {
			this.buddy = this.chatservice.buddy;


			this.userservice.getuserdetails().then((res: any) => {
				this.photoURL = res['photoURL'];
			});


			this.todaydate = this.formatDate(new Date());
			this.allChats = res;
			this.tempallChats = this.allChats;
		});
	}

	dismiss() {
		this.viewCtrl.dismiss();
	}

	initializeContacts() {
		this.zone.run(() => {
			this.allChats = this.tempallChats;
		});
	}

	searchuser(ev: any) {
		this.initializeContacts();
		let myArray = [];
		let isNoRecord: boolean = false;
		this.isNoRecord = false;
		let val = ev.target.value;
		if (val && val.trim() != '') {
			for (var i = 0; i < this.allChats.length; ++i) {
				myArray.push({ date: this.allChats[i].date, messages: [] })
				for (var j = 0; j < this.allChats[i].messages.length; ++j) {
					if(this.allChats[i].messages[j].multiMessage  ){
					}else if( this.allChats[i].messages[j].type == 'image'){
						
					}else if( this.allChats[i].messages[j].type == 'document'){
						
					}
					else{
						if (this.allChats[i].messages[j].message.toLowerCase().indexOf(val.toLowerCase()) > -1) {
							isNoRecord = true;
							myArray[i].messages.push(this.allChats[i].messages[j]);
						}
					}
				}
			}

			if (isNoRecord != true) {
				this.isNoRecord = true;
			}
			this.allChats = myArray;
		}
	}

	formatDate(date) {
		var dd = date.getDate();
		var mm = date.getMonth() + 1;
		var yyyy = date.getFullYear();
		if (dd < 10)
			dd = '0' + dd;
		if (mm < 10)
			mm = '0' + mm;
		return dd + '/' + mm + '/' + yyyy;
	}

}
