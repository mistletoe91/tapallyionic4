import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Platform } from 'ionic-angular';

import { ContactProvider } from '../../providers/contact/contact';
import { GroupsProvider } from '../../providers/groups/groups';
import { ImagehandlerProvider } from '../../providers/imagehandler/imagehandler';
import { UserProvider } from '../../providers/user/user';
import { LoadingProvider } from '../../providers/loading/loading';
@IonicPage()
@Component({
    selector: 'page-allcontacts',
    templateUrl: 'allcontacts.html',
})
export class AllcontactsPage {
    selectedMember: any = [];
    myFriends: any = [];
    loginuserInfo: any = [];
    msgstatus: boolean = false;
    gpname:any;
    tmpmyfriends=[];
    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public contact: ContactProvider,
        public imgstore: ImagehandlerProvider,
        public groupservice: GroupsProvider,
        public userservice: UserProvider,
        public loading: LoadingProvider,
        public platform:Platform
    ) {

    }

    ionViewDidLoad() {
        this.gpname = this.navParams.get('gpname');
        this.userservice.getuserdetails().then((res: any) => {
            this.loginuserInfo = res;
        }).catch(err => {
        });
        this.contact.getSimJsonContacts().then((res: any) => {
            this.myFriends = res["contacts"]; let myFriends = res["contacts"];
            this.tmpmyfriends = myFriends.sort(function (a, b) {
                if (a.displayName != undefined) {
                    var nameA = a.displayName.toUpperCase(); // ignore upper and lowercase
                }
                if (b.displayName != undefined) {
                    var nameB = b.displayName.toUpperCase(); // ignore upper and lowercase
                }
                if (nameA < nameB) {
                    return -1;
                }
                if (nameA > nameB) {
                    return 1;
                }
                return 0;
            });
            this.myFriends = this.tmpmyfriends;
        });
    }
    initializeItems(){
        this.myFriends = this.tmpmyfriends;
    }
    addContact(contact) {
         if (this.selectedMember.length == 0) {
            this.selectedMember.push(contact);
        }
       else {
            let flagofbuddy = false;
            let posionofbuddy = 0;
            for (let i = 0; i < this.selectedMember.length; i++) {
                if (this.selectedMember[i]._id == contact._id) {
                    flagofbuddy = true;
                    posionofbuddy = i;

                    break;
                } else {
                    flagofbuddy = false;
                }
            }
            if (flagofbuddy == true) {
                this.selectedMember.splice(posionofbuddy, 1);
            }else if(this.selectedMember.length == 6){
                this.loading.presentToast("Maximum six contacts allowed");

              }
             else {
                this.selectedMember.push(contact);
            }
        }

    }
    searchuser(ev: any){
        this.initializeItems();

        const val = ev.target.value;

        if (val && val.trim() != '') {
        this.myFriends = this.tmpmyfriends;
          this.myFriends = this.myFriends.filter((item) => {
            return (item.displayName.toLowerCase().indexOf(val.toLowerCase().trim()) > -1);
          });
        }
    }
    goBack() {
        this.navCtrl.pop();
    }
    goNext() {
        this.loading.presentLoading();
        this.groupservice.addgroupmsgmultiple(this.selectedMember, this.loginuserInfo, 'contact',this.gpname).then(() => {
            this.selectedMember = [];
            this.msgstatus = false;
            this.loading.dismissMyLoading();
            this.navCtrl.pop();
        })
    }
}
