import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InvitemanuallyPage } from './invitemanually';

@NgModule({
  declarations: [
    InvitemanuallyPage,
  ],
  imports: [
    IonicPageModule.forChild(InvitemanuallyPage),
  ],
})
export class InvitemanuallyPageModule {}
