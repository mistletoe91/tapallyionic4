import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController,Platform} from 'ionic-angular';

import {ContactProvider} from '../../providers/contact/contact';
import { GroupsProvider} from '../../providers/groups/groups';

import { UserProvider } from '../../providers/user/user';
import { LoadingProvider } from '../../providers/loading/loading';


import { ChatProvider } from '../../providers/chat/chat';
import { FcmProvider } from '../../providers/fcm/fcm';


@IonicPage()
@Component({
  selector: 'page-allbuddy-contacts',
  templateUrl: 'allbuddy-contacts.html',
})
export class AllbuddyContactsPage {
   selectedMember:any=[];
   myFriends:any=[];
   loginuserInfo:any=[];
   msgstatus: boolean = false;

  UnreadMSG: any;
  buddy: any;
  buddyStatus: any;
  newmessage: any;
  tmpmyfriends=[];
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public contact : ContactProvider, 
    public chatservice: ChatProvider,
    public fcm: FcmProvider,
    public groupservice :GroupsProvider,
    public userservice: UserProvider,
   
    public loading: LoadingProvider,
    private alertCtrl: AlertController,
    public platform:Platform
  ) {
  }

  ionViewDidLoad() {
   this.buddyStatus= this.chatservice.buddyStatus;
    this.buddy = this.navParams.get('buddy')
    this.contact.getSimJsonContacts().then((res:any)=>{
      this.tmpmyfriends=res["contacts"];
       this.tmpmyfriends= this.tmpmyfriends.sort(function (a, b) {
        if (a.displayName != undefined) {
          var nameA = a.displayName.toUpperCase(); // ignore upper and lowercase
        }
        if (b.displayName != undefined) {
          var nameB = b.displayName.toUpperCase(); // ignore upper and lowercase
        }
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }

        // names must be equal
        return 0;
      });
      this.myFriends=this.tmpmyfriends;
    }); 
  }
  initializeItems(){
    this.myFriends = this.tmpmyfriends;
  }
  addContact(contact){
    if(this.selectedMember.length == 0){
      this.selectedMember.push(contact);

    }
     
    else{
      let flagofbuddy = false;
      let posionofbuddy = 0;
      for(let i=0;i< this.selectedMember.length;i++){
        if(this.selectedMember[i]._id == contact._id){
          flagofbuddy = true;
          posionofbuddy = i;

          break;
        }else{
          flagofbuddy = false;
        }
      }
      if(flagofbuddy == true){
          this.selectedMember.splice(posionofbuddy,1);
      }else if(this.selectedMember.length == 6){
        this.loading.presentToast("Maximum six contacts allowed");
  
      }
      else{
          this.selectedMember.push(contact);
      }
    }
  }

  searchuser(ev: any){
    this.initializeItems();

    const val = ev.target.value;

    if (val && val.trim() != '') {
    this.myFriends = this.tmpmyfriends;
      this.myFriends = this.myFriends.filter((item) => {
        return (item.displayName.toLowerCase().indexOf(val.toLowerCase().trim()) > -1);  
      });
    }
  }
  goBack(){
    this.navCtrl.pop();
  }

  goNext(){
    
    this.UnreadMSG = 0;
    this.userservice.getstatus(this.buddy).then((res) => {
      if (!res) {
          
          this.chatservice.addnewmessagemultiple(this.selectedMember, 'contact', this.buddyStatus).then(() => {
            if (this.buddyStatus != 'online') {
              let newMessage = 'You have received a message';
              this.fcm.sendNotification(this.buddy, newMessage, 'chatpage');
          }
            this.newmessage = '';
          })
       
      } else {
        let alert = this.alertCtrl.create({
          message: 'Unblock ' + this.buddy.displayName + ' to send a message.',
          buttons: [
            {
              text: 'CANCEL',
              role: 'cancel',
              handler: () => {
              }
            },
            {
              text: 'UNBLOCK',
              handler: () => {
                this.userservice.unblockUser(this.buddy).then((res) => {
                  if (res) {
                  
                  }
                })
              }
            }
          ]
        });
        alert.present();
      }

    });

   

   this.navCtrl.pop();
  }
}
