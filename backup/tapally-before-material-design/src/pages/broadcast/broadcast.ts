import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Events } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { ChatProvider } from '../../providers/chat/chat';
import { UserProvider } from '../../providers/user/user';
import { FcmProvider } from '../../providers/fcm/fcm';
import { RequestsProvider } from '../../providers/requests/requests';
import { LoadingProvider } from '../../providers/loading/loading';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
@IonicPage()
@Component({
  selector: 'page-broadcast',
  templateUrl: 'broadcast.html',
})
export class BroadcastPage {
  myrequests = [];
  requestcounter = null;
  buddy: any;
  newmessage = '';
  debugMe:boolean = true;
  showPaidSign:boolean = false;
  doIhavebusinessAccount = false;
  cntCounter;
  myfriends;
  myfriendsList;
  tempmyfriendsList;
  isData: boolean = false;
  authForm: FormGroup;
  message: AbstractControl;
  constructor(public fcm: FcmProvider,public loading: LoadingProvider,public requestservice: RequestsProvider,public fb: FormBuilder,public events: Events,public chatservice: ChatProvider,public userservice: UserProvider,public navCtrl: NavController, public toastCtrl: ToastController, public navParams: NavParams) {

    this.authForm = this.fb.group({
        'message': [null, Validators.compose([Validators.required])]
    });
    this.message = this.authForm.controls['message'];
  }

  ionViewWillEnter() {

      console.log ("TESTING LOCAL STORAGE -->");
      this.paidOrNot ();
      if(this.debugMe){console.log ("businessName checkiing");console.log (localStorage.getItem('businessName'));}
      if(localStorage.getItem('businessName')){
          this.doIhavebusinessAccount = true;
      } else {
          this.doIhavebusinessAccount = false;
        /*
          this.userservice.getBusinessDetails().then((res)=>{
            if(res){
              localStorage.setItem('businessName', res['displayName']);
              localStorage.setItem('bcats', res['bcats']);
              this.doIhavebusinessAccount = true;
            }
          }).catch(err=>{
          });
          */
      }

      this.requestservice.getmyfriends();
      this.myfriends = [];
      this.events.subscribe('friends', () => {
        this.myfriends = [];
        this.myfriends = this.requestservice.myfriends;
      });

      this.requestservice.getmyrequests();
      this.events.subscribe('gotrequests', () => {
          this.myrequests = [];
          this.myrequests = this.requestservice.userdetails;
          if (this.myrequests) {
              this.requestcounter = this.myrequests.length;
          }
      })
  }

  //KEep it same as chats.ts
  paidOrNot (){
      this.showPaidSign = false;
      if(this.debugMe){console.log ("paidornot checkiing");console.log (localStorage.getItem('business_created'));}
      if(localStorage.getItem('business_created')){
          let AcceptableDelayInMicroseconds:number = 1000; //Before we show warning to business that they have not paid yet
          let timeStamp:number = Date.now();
          let business_created:number = parseInt(localStorage.getItem('business_created'));
          if(this.debugMe){console.log ("business_created checkiing");}
          //Do not check for first few minutes/hours/days ( as specified in AcceptableDelayInMicroseconds )
          //But check every single time afterwards
          if((business_created+AcceptableDelayInMicroseconds)<timeStamp){
                let paid:number = parseInt(localStorage.getItem('paid'));
                if(this.debugMe){console.log ("paid checkiing");console.log(paid);}
                //check API everytime
                if(true){
                        if(this.debugMe){console.log ("calling getBusinessDetails");}
                        this.userservice.getBusinessDetails().then((res)=>{
                                //localStorage.setItem('businessName', res['displayName']);
                                //localStorage.setItem('bcats', res['bcats']); 
                                if(!paid || paid<=0){
                                    console.log ("Indeed Business has not paid yet");
                                    this.showPaidSign = true;
                                }
                        }).catch(err=>{
                        });
                }//endif
          } else {

          }//endif
      }//endif
  }//end function

  broadcast() {
      this.newmessage = this.newmessage.trim();
      if (this.newmessage != '') {
      } else {
        this.loading.presentToast('Please enter valid message...');
        return;
      }

      this.cntCounter = 0;
      for (var key=0;key<this.myfriends.length;key++) {
        this.addmessage(this.myfriends[key]);
      }//end for


      //mytodo : remove this from here and just call asyn a web service API to send messages.
      //Now the mobile is doing heavylifting. But ideally the server should do and send to nosql database
      /*
      this.cntCounter = 0;
      this.events.subscribe('friends', () => {
        if (this.requestservice.myfriends.length > 0) {
            console.log ("-->" + this.requestservice.myfriends.length);
            for (var key=0;key<this.requestservice.myfriends.length;key++) {
               this.buddy = this.requestservice.myfriends[key];
               console.log (key);
               //console.log(this.buddy);
               this.addmessage();
            }//end for
        }
      });
      */

  }
  presentPopover(myEvent) {
      this.navCtrl.push("NotificationPage");
      this.requestcounter = this.myrequests.length;
  }
  sendToNextPage (){
      this.cntCounter++;
      if(this.cntCounter>=this.myfriends.length){
        this.cntCounter = 0;
        this.navCtrl.setRoot("BroadcastSubmittedPage");
      }
  }

  sendToBusinessReg (){
    this.navCtrl.setRoot("RegisterbusinessPage");
  }

  addmessage(buddy) {
    this.newmessage = this.newmessage.trim();

    //Send Push notification : mytodo check if the buddy is online or not. if online then no need to send push notification
    let newMessage = 'You have received a message';
    this.fcm.sendNotification(buddy, newMessage, 'chatpage');

    //Send chat message
    this.chatservice.addnewmessageBroadcast(this.newmessage, 'message', buddy , 0).then(() => {
        this.newmessage = '';
        this.sendToNextPage ();
    });
  }

  ionViewDidLoad() {
  }

}
