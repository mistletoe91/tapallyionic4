import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { IonicPage, NavController, NavParams, LoadingController, AlertController, ModalController, Platform} from 'ionic-angular';

import { AuthProvider }from  '../../providers/auth/auth';
import { LoadingProvider } from '../../providers/loading/loading';

import { apiKey } from '../../app/app.angularfireconfig';
import 'rxjs/add/operator/map';

@IonicPage()
@Component({
  selector: 'page-phone',
  templateUrl: 'phone.html',
})
export class PhonePage {

  phoneNumber:any={number:'',type:''};
  isCountrySelect:boolean=true;
  submitAttempt:boolean= false;
  number: AbstractControl;
  authForm : FormGroup;
  countryName = "Select Country";
  countryTemp:boolean = false;
  countryCode = "";
  InvalidNumErr='';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public authService: AuthProvider,
    public loadingCtrl: LoadingController,
    public fb: FormBuilder,
    public platform: Platform,
    public alertCtrl: AlertController,
    public loading :LoadingProvider,
    public http : HttpClient,
    public modalCtrl : ModalController
  ) {
    this.authForm = this.fb.group({
      'number' : [null, Validators.compose([Validators.pattern('^[0-9]+$'),Validators.required])]
    });
    this.number = this.authForm.controls['number'];
    
    //Default Country :
    //mytodo : Right now its Canada
    this.countryName = 'Canada';
    this.countryCode = '1';//test
    this.phoneNumber.type = this.countryCode;
    this.countryTemp = true
  }

  ionViewDidLoad() {
    this.loading.dismissMyLoading();
    localStorage.removeItem("country");
    localStorage.removeItem("code");
  }

  listCountry(){
      let countryListModal = this.modalCtrl.create('CountryListPage', { type: this.phoneNumber.type });
      countryListModal.present();
      countryListModal.onDidDismiss(data => {
        if(data == undefined){
          if(JSON.parse(localStorage.getItem("country")) == null){
            this.countryName = "Select Country";
            this.countryCode = "";
          }else{
            this.countryName = JSON.parse(localStorage.getItem("country"));
            this.countryCode = JSON.parse(localStorage.getItem("code"));
            this.phoneNumber.type = this.countryCode;
            this.countryTemp = true
          }
        }else{
          this.countryName = data.name;
          this.phoneNumber.type = data.code;
          this.countryCode = data.code;
          this.countryTemp = false;
        }
      });
  }

  doLoginwithMobile(phoneNumber){
    this.submitAttempt = true;

    if(this.phoneNumber.type == ''){
      this.countryTemp = true;
    }

    if (this.authForm.valid && this.phoneNumber.type != '') {
      this.loading.presentLoading();

      let data = {
        "via":"sms",
        "phone_number":phoneNumber.number,
        "country_code":phoneNumber.type,
        "api_key":apiKey
      }


      this.authService.sendVerificationCode(data,'phones/verification/start')
      .then(res => {
        this.loading.dismissMyLoading();
        let data = JSON.parse(res['data']);
        if (res.status == 200) {
          this.loading.presentToast(data['message']);
          this.navCtrl.setRoot("PhoneverfyPage", {code:phoneNumber.type,number:phoneNumber.number});
        }else{
          this.loading.presentToast(data['message']);
        }
      }).catch(err => {

        this.loading.dismissMyLoading();

        if (err.status == 400) {
           this.InvalidNumErr = "Invalid Mobile Number.";
        }
        else{
          console.log (err);
          this.loading.presentToast('Something went wrong. Please try again.');
        }
      });
    }
  }

}
