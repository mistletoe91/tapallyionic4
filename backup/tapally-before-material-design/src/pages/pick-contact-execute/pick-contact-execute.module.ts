import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PickContactExecutePage } from './pick-contact-execute';

@NgModule({
  declarations: [
    PickContactExecutePage,
  ],
  imports: [
    IonicPageModule.forChild(PickContactExecutePage),
  ],
})
export class DisplaynamePageModule {}
