import { Component, ViewChild, NgZone, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, Content, LoadingController, ActionSheetController, PopoverController, AlertController, ModalController, Platform } from 'ionic-angular';
import { ChatProvider } from '../../providers/chat/chat';
import { ImagehandlerProvider } from '../../providers/imagehandler/imagehandler';
import { LoadingProvider } from '../../providers/loading/loading';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { FcmProvider } from '../../providers/fcm/fcm';
import { UserProvider } from '../../providers/user/user';
import { Storage } from '@ionic/storage';
import { RequestsProvider } from '../../providers/requests/requests';
import { ContactProvider } from '../../providers/contact/contact';
import { Subscription } from 'rxjs';
import { ImageViewerController } from 'ionic-img-viewer';

@IonicPage()
@Component({
    selector: 'page-pick-contact-execute',
    templateUrl: 'pick-contact-execute.html',
})
export class PickContactExecutePage {
    @ViewChild('content') content: Content;
    authForm: FormGroup;
    message: AbstractControl;
    buddy: any;
    newmessage = '';
    referralRequestPrefix;
    referralRequestPostfix;
    allmessages = [];
    filterallmessages = [];
    tempFilterallmessages = [];
    photoURL;
    imgornot;
    todaydate;
    myfriends;
    myfriendsList;
    tempmyfriendsList;
    isData: boolean = false;
    buddyStatus: any;
    datacounter = 0;
    headercopyicon = true;
    selectAllMessage = [];
    selectCounter = 0;
    UnreadMSG: number;
    isuserBlock: boolean = false;
    msgstatus: boolean = false;
    backbuttonstatus: boolean = false;
    DeleteMsg;
    _imageViewerCtrl: ImageViewerController;
    private onResumeSubscription: Subscription;
    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public storage: Storage,
        public fcm: FcmProvider,
        public chatservice: ChatProvider,
        public events: Events,
        public zone: NgZone,
        public loadingCtrl: LoadingController,
        public imgstore: ImagehandlerProvider,
        public fb: FormBuilder,
        public loading: LoadingProvider,
        public userservice: UserProvider,
        public actionSheetCtrl: ActionSheetController,
        public eleRef: ElementRef,
        public popoverCtrl: PopoverController,
        private alertCtrl: AlertController,
        public requestservice: RequestsProvider,
        public contactProvider: ContactProvider,
        public modalCtrl: ModalController,
        public platform: Platform,
        public imageViewerCtrl: ImageViewerController
    ) {

        this.referralRequestPrefix = 'Referral Request';
        this.referralRequestPostfix = '';

        this._imageViewerCtrl = imageViewerCtrl;
        this.onResumeSubscription = platform.resume.subscribe(() => {
            this.chatservice.buddymessageRead();
        });

        this.UnreadMSG = +localStorage.getItem('UnreadCount');

        this.authForm = this.fb.group({
            'message': [null, Validators.compose([Validators.required])]
        });
        this.message = this.authForm.controls['message'];

        this.buddy = this.chatservice.buddy;// buddy is receipient of chat


        events.subscribe('isblock-user', () => {
            zone.run(() => {
                this.isuserBlock = this.userservice.isuserBlock;
            })
        })
        this.userservice.getstatusblockuser(this.buddy);

        this.userservice.getuserdetails().then((res: any) => {
            this.photoURL = res['photoURL'];
        });

        this.events.subscribe('newmessage', () => {
            this.allmessages = [];
            this.imgornot = [];
            this.zone.run(() => {
                //this.allmessages = this.chatservice.buddymessages;
                let tempData = this.chatservice.buddymessages;
                this.todaydate = this.formatDate(new Date());
                let allData = [{ date: this.todaydate, messages: [] }];
                if (tempData) {
                    for (let i = 0; i < tempData.length; i++) {
                        tempData[i].selection = false;
                        if (this.todaydate == tempData[i].dateofmsg) {
                            allData[0].messages.push(tempData[i]);
                        } else {
                            let singledata = tempData[i].dateofmsg;
                            let validater = allData.filter((task) => task.date == singledata);
                            if (validater.length) {
                            } else {
                                allData.push({ date: singledata, messages: [] })
                                for (let q = 0; q < tempData.length; q++) {
                                    if (singledata == tempData[q].dateofmsg) {
                                        for (let j = 0; j < allData.length; j++) {
                                            if (allData[j].date == singledata) {
                                                allData[j].messages.push(tempData[q]);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    this.datacounter = 10;
                    allData.sort(function (a, b) {
                        var nameA = a.date; // ignore upper and lowercase
                        var nameB = b.date; // ignore upper and lowercase
                        if (nameA < nameB) {
                            return -1;
                        }
                        if (nameA > nameB) {
                            return 1;
                        }

                        // names must be equal
                        return 0;
                    });


                    this.allmessages = this.sortdata(allData);
                    this.storage.set('allBuddyChats', this.allmessages);

                    if (this.UnreadMSG == 0) {
                        this.getAllMessage(this.datacounter);
                    } else {

                        if (this.UnreadMSG != 10 && this.UnreadMSG < 10) {
                            let x = 10 - this.UnreadMSG;
                            let data = this.UnreadMSG + x;
                            this.getAllMessage(data);

                            if (this.UnreadMSG < 5) {
                            }

                        } else {
                            this.getAllMessage(this.UnreadMSG + 3);
                        }
                    }
                    if (this.UnreadMSG == 0) {
                    } else {
                    }

                }
                // for(let k =0; k <this.allmessages.length;k++){
                //   for (var key in this.allmessages[k].messages) {
                //     if (this.allmessages[k].messages[key].message.indexOf('https://firebasestorage.googleapis.com/') != '-1' ){
                //       this.imgornot.push(true);
                //     }else{
                //       this.imgornot.push(false);
                //     }
                //   }
                // }
            })
        })
        this.events.subscribe('onlieStatus', () => {
            this.zone.run(() => {
                this.buddyStatus = this.chatservice.buddyStatus;
            })
        })

        platform.ready().then(() => {
          //this.message = "Referral Send to you";
          //this.addmessage ();
        });

    }

    viewBuddy(name, mobile, disc, img, status) {
        this.navCtrl.push("ViewBuddyPage", { img: img, name: name, mobile: mobile, disc: disc, userstatus: status });


    }


    formatDate(date) {
        var dd = date.getDate();
        var mm = date.getMonth() + 1;
        var yyyy = date.getFullYear();
        if (dd < 10)
            dd = '0' + dd;
        if (mm < 10)
            mm = '0' + mm;
        return dd + '/' + mm + '/' + yyyy;
    }
    ionViewDidEnter() {
        this.chatservice.getbuddymessages();
        this.chatservice.getbuddyStatus();

    }
    sendReferral (friend_asking_referral, catid){
        this.navCtrl.setRoot("FriendAskingReferralPage", {friend_asking_referral_uid : friend_asking_referral.uid,friend_asking_referral_displayName : friend_asking_referral.displayName, catid:catid});
    }
    addmessage() {
        this.closeButtonClick()
        this.UnreadMSG = 0;
        this.selectAllMessage = [];
        this.selectCounter = 0;
        this.headercopyicon = true;

        for (var i = 0; i < this.filterallmessages.length; i++) {
            for (var j = 0; j < this.filterallmessages[i].messages.length; j++) {
                this.filterallmessages[i].messages[j].selection = false;
            }
        }
        /*
        console.log(this.buddy);
        OUTPUT>>>>>>
            deviceToken: ""
            disc: ""
            displayName: "Aman 416pages"
            isBlock: undefined
            isUser: "1"
            mobile: "4165775128"
            photoURL: "https://fireba4c2f"
            status: "friend"
            uid: "n1nery5tQJOraAsvdt1ZmPHzW1A3"
            useremail: "Aman@416pages.ca"
            __proto__: Object
        */
        this.userservice.getstatus(this.buddy).then((res) => {
            if (!res) {
                if (this.newmessage != '') {
                    if (this.buddyStatus != 'online') {
                        this.userservice.getstatusblock(this.buddy).then((res) => {

                            if (res == false) {
                                let newMessage = 'You have received a message';
                                this.fcm.sendNotification(this.buddy, newMessage, 'chatpage');
                            }
                        });
                    }
                    this.chatservice.addnewmessage(this.newmessage, 'message', this.buddyStatus).then(() => {
                        this.newmessage = '';
                        this.scrollto();
                        this.msgstatus = false;
                    })
                } else {
                    this.loading.presentToast('Please enter valid message...');
                }
            } else {
                let alert = this.alertCtrl.create({
                    message: 'Unblock ' + this.buddy.displayName + ' to send a message.',
                    buttons: [
                        {
                            text: 'CANCEL',
                            role: 'cancel',
                            handler: () => {
                            }
                        },
                        {
                            text: 'UNBLOCK',
                            handler: () => {
                                this.userservice.unblockUser(this.buddy).then((res) => {
                                    if (res) {
                                        this.addmessage();
                                    }
                                })
                            }
                        }
                    ]
                });
                alert.present();
            }
        })
    }
    scrollto() {
        setTimeout(() => {
          if(this.content._scroll)  {
            this.content.scrollToBottom();
          }
        }, 1000);
    }
    sendPicMsg() {
        this.UnreadMSG = 0;
        this.userservice.getstatus(this.buddy).then((res) => {
            if (!res) {
                this.loading.presentLoading();
                this.imgstore.picmsgstore().then((imgurl) => {
                    this.chatservice.addnewmessage(imgurl, 'image', this.buddyStatus).then(() => {
                        this.loading.dismissMyLoading();
                        if (this.buddyStatus != 'online') {
                            let newMessage = 'You have received a message';
                            this.fcm.sendNotification(this.buddy, newMessage, 'chatpage');
                        }
                        this.scrollto();
                        this.newmessage = '';
                    })
                }).catch((err) => {
                    this.loading.dismissMyLoading();
                    alert(err);
                })
            } else {
                let alert = this.alertCtrl.create({
                    message: 'Unblock ' + this.buddy.displayName + ' to send a message.',
                    buttons: [
                        {
                            text: 'CANCEL',
                            role: 'cancel',
                            handler: () => {
                            }
                        },
                        {
                            text: 'UNBLOCK',
                            handler: () => {
                                this.userservice.unblockUser(this.buddy).then((res) => {
                                    if (res) {
                                        this.sendPicMsg();
                                    }
                                })
                            }
                        }
                    ]
                });
                alert.present();
            }
        })
    }
    doRefresh(refresher) {
        this.UnreadMSG = 0;
        this.datacounter += 10;
        this.getAllMessage(this.datacounter);
        this.closeButtonClick()
        setTimeout(() => {
            refresher.complete();
        }, 500);
    }
    getAllMessage(key) {
        let messagesAll = [];
        let counter = 0;
        // for(let i = 0; i<key;i++){
        for (let j = this.allmessages.length; j > 0; j--) {
            if (counter < key) {
                let msg = [];
                for (let k = this.allmessages[j - 1].messages.length; k > 0; k--) {
                    if (counter < key) {
                        msg.push(this.allmessages[j - 1].messages[k - 1])
                    } else {
                        break
                    }
                    counter++;
                }
                messagesAll.push({ date: this.allmessages[j - 1].date, messages: msg });
            } else {
                break;
            }
        }
        let filterallmessages = [];
        let sortDate = this.sortdata(messagesAll);
        for (let i = 0; i < sortDate.length; i++) {
            let tempmsg = [];
            for (let j = sortDate[i].messages.length; j > 0; j--) {
                tempmsg.push(sortDate[i].messages[j - 1])
            }
            this.zone.run(() => {
                filterallmessages.push({ date: sortDate[i].date, messages: tempmsg })
            })
        }
        filterallmessages.sort(function (a, b) {
            var nameA = a.date; // ignore upper and lowercase
            var nameB = b.date; // ignore upper and lowercase
            if (nameA < nameB) {
                return -1;
            }
            if (nameA > nameB) {
                return 1;
            }
            // names must be equal
            return 0;
        });
        this.filterallmessages = filterallmessages;
        // this.filterallmessages = [];
        // for (let i = filterallmessages.length - 1; i >= 0; i--) {
        //   this.filterallmessages.push(filterallmessages[i]);
        // }
        // this.filterallmessages = this.sortdata(this.filterallmessages);
        this.scrollto();
        // this.filterallmessages = this.sortdata(messagesAll);
        //   let tmpmessage = [];
        //   this.filterallmessages.forEach(element => {
        //     if (element.messages.length > 3) {
        //       let imgcter=0
        //       let imgcontainer=[];
        //       element.messages.forEach(ele => {

        //         if(ele.type == 'image'){
        //           imgcter++;
        //           imgcontainer.push(ele)
        //             if(imgcter>4){

        //             }
        //         }else{
        //           tmpmessage.push(ele);
        //         }
        //       });
        //     }
        //     tmpmessage.push(element);
        //   });
    }
    sortdata(data) {
        return data.sort(function (a, b) {
            var keyA = new Date(a.date),
                keyB = new Date(b.date);
            // Compare the 2 dates
            if (keyA < keyB) return -1;
            if (keyA > keyB) return 1;
            return 0;
        });
    }

    backButtonClick() {
        this.navCtrl.pop();
    }

    closeButtonClick() {
        this.selectAllMessage = [];
        this.selectCounter = 0;
        this.headercopyicon = true;
        this.backbuttonstatus = false;
        for (var i = 0; i < this.filterallmessages.length; i++) {
            for (var j = 0; j < this.filterallmessages[i].messages.length; j++) {
                this.filterallmessages[i].messages[j].selection = false;
            }
        }
    }

    popoverdialog(item, event) {
        let flag = 0;
        if (this.selectAllMessage.length != 0) {
            for (let i = 0; i < this.selectAllMessage.length; i++) {
                if (item.messageId == this.selectAllMessage[i].messageId) {
                    flag = 1;
                    //event.target.classList.remove('select-item');
                    item.selection = false;
                    this.selectAllMessage.splice(i, 1);

                    break;
                } else {
                }
            }
            if (flag == 1) {
            } else {
                this.popoveranothermsg(item, event);
            }
        } else {
            item.selection = true
            this.selectAllMessage.push(item);

            if (this.headercopyicon == true) {
                this.headercopyicon = false;
                this.backbuttonstatus = true;

            } else {
                this.headercopyicon = true;
                this.backbuttonstatus = false;
            }
        }
        this.selectCounter = this.selectAllMessage.length;
        if (this.selectAllMessage.length == 0) {
            this.headercopyicon = true;
            this.backbuttonstatus = false;
        }

    }
    popoveranothermsg(item, event) {
        let flag = 0;
        if (this.selectAllMessage.length >= 1) {

            for (let i = 0; i < this.selectAllMessage.length; i++) {
                if (item.messageId == this.selectAllMessage[i].messageId) {
                    item.selection = false;
                    flag = 1;
                    this.selectAllMessage.splice(i, 1);
                    break;
                } else {
                    flag = 0;
                }
            }
            if (flag == 1) {
            } else {
                item.selection = true
                this.selectAllMessage.push(item);
            }
        }
        this.selectCounter = this.selectAllMessage.length;
        if (this.selectAllMessage.length == 0) {
            this.headercopyicon = true;
            this.backbuttonstatus = false;
        }
    }

    deleteMessage() {
        if(this.selectAllMessage.length > 1){
            this.DeleteMsg="Do you want to delete these messages?"
        }else{
            this.DeleteMsg="Do you want to delete this message?"
        }
        this.loading.presentLoading();
        let alert = this.alertCtrl.create({
            title: 'Delete Message',
            message: this.DeleteMsg,
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: () => {
                        this.closeButtonClick();
                        this.loading.dismissMyLoading();
                    }
                },
                {
                    text: 'Yes',
                    handler: () => {
                        if (this.selectAllMessage.length != 0) {
                            this.chatservice.deleteMessages(this.selectAllMessage).then((res) => {
                                if (res) {
                                    this.selectAllMessage = [];
                                    if (this.selectAllMessage.length == 0) {
                                        this.headercopyicon = true;
                                        this.backbuttonstatus = false;
                                        this.loading.dismissMyLoading();
                                    }
                                }
                            }).catch((err) => {
                            })
                        }
                    }
                }
            ]
        });
        alert.present().then(() => {
        });
    }
    viewPopover(myEvent) {
        let popover = this.popoverCtrl.create('MenuBlockPage', { buddy: this.buddy });
        popover.present({ ev: myEvent });
        popover.onDidDismiss(data => {
            this.userservice.getstatusblockuser(this.buddy);
        });
    }
    ionViewWillEnter() {
        this.requestservice.getmyrequests();
        this.requestservice.getmyfriends();
        this.myfriends = [];
        this.events.subscribe('friends', () => {
            this.myfriends = [];
            this.myfriends = this.requestservice.myfriends;
            if (this.myfriends.length > 0) {
                this.loading.dismissMyLoading();
                this.isData = false;
                this.contactProvider.getSimJsonContacts().then(res => {
                    if (res['contacts'].length > 0) {
                        for (var i = 0; i < res['contacts'].length; ++i) {
                            for (var j = 0; j < this.myfriends.length; ++j) {
                                if (res['contacts'][i].mobile == this.myfriends[j].mobile) {
                                    this.myfriends[j].displayName = res['contacts'][i].displayName;
                                }
                            }
                        }
                        this.zone.run(() => {
                            this.myfriendsList = this.myfriends;
                        })
                        this.tempmyfriendsList = this.myfriendsList;
                    } else {
                        this.zone.run(() => {
                            this.myfriendsList = this.myfriends;
                        })
                        this.tempmyfriendsList = this.myfriendsList;
                    }
                }).catch(err => {
                    this.loading.dismissMyLoading();
                });
            } else {
                this.loading.dismissMyLoading();
                this.isData = true
            }
        });
    }

    openAttachment(myEvent) {
        let popover = this.popoverCtrl.create('AttachmentsPage', { buddy: this.buddy });
        popover.present({
            ev: myEvent
        });
        popover.onDidDismiss((pages) => {
            if (pages != 0 || pages != undefined  || pages != null ) {
                this.navCtrl.push(pages.page, { buddy: pages.buddy, flag: pages.flag });
            }else{
            }
        });
    }
    presentImage(myImage) {
        if (this.selectAllMessage.length == 0) {
            const imageViewer = this._imageViewerCtrl.create(myImage);
            imageViewer.present();
            //imageViewer.onDidDismiss(() => ));
        }
    }

    openAllImg(items, $event) {
        if (this.selectAllMessage.length == 0) {
            this.navCtrl.push("AllimgPage", { allimg: items });
        }
    }

    openDoc(path) {
        if (this.selectAllMessage.length == 0) {
            this.imgstore.openDocument(path).then(res => {
            }).catch(err => {
            })
        }
    }

    openContacts(contacts) {
        if (this.selectAllMessage.length == 0) {
            this.navCtrl.push("ShowContactPage", { contacts: contacts });
        }
    }
    showLocation(location) {
        if (this.selectAllMessage.length == 0) {
            this.navCtrl.push("MapPage", { location: location })
        }
    }
    onChange(e) {
        if (this.newmessage.trim()) {
            this.msgstatus = true;
        }
        else {
            this.msgstatus = false;
        }
    }
}
