import { Component ,NgZone} from '@angular/core';
import { IonicPage, NavController, NavParams ,Events,Platform } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';


@IonicPage()
@Component({
  selector: 'page-friend-list',
  templateUrl: 'friend-list.html',
})
export class FriendListPage {

  unblockUsers:any = [];
  haveData :boolean = false;
  totusers:any=0;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public userservice : UserProvider,
    public zone : NgZone,
    public events : Events,
    public platform:Platform
  ){
    
    events.subscribe('unblock-users', () => {
      this.unblockUsers = this.userservice.unblockUsers;
      this.haveData = true;
      this.totusers = this.unblockUsers.length;
    })
  }

  ionViewDidLoad() {
      this.userservice.getAllunBlockUsers();
  }
  addblockuser(item){
    this.userservice.blockUser(item).then((res)=>{
      if(res){
        this.navCtrl.setRoot('BlockUserPage')
      }
    })
  }
  goBack(){
    this.navCtrl.pop();
  }
}
