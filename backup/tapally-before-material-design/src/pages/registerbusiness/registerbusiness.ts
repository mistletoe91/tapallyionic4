import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserProvider } from '../../providers/user/user';
import { CatProvider } from '../../providers/cats/cats';
import firebase from 'firebase';
@IonicPage()
@Component({
  selector: 'page-registerbusiness',
  templateUrl: 'registerbusiness.html',
})
export class RegisterbusinessPage {
  authForm : FormGroup;
  businessname;
  useremail;
  cats;
  bcats;
  bcatName;
  nextPage;
  submitted:boolean=false;
  msgstatus: boolean = false;
  selectOptions;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public fb: FormBuilder,
    public userservice: UserProvider,
    public catservice: CatProvider) {
      this.nextPage = "ImportpPage";
      this.cats = this.catservice.getCats();
      //this.assignCatName ();

      this.selectOptions = {
       title: 'Business Category',
       subTitle: 'Please select an appropriate category'
     };

    this.authForm=this.fb.group({
      businessname:['',Validators.compose([Validators.required])],
      useremail:['',Validators.compose([Validators.required])],
      bcats:['',Validators.compose([Validators.required])]
    })
  }


  assignCatName (){
    if(!localStorage.getItem('bcats')){
      this.bcatName = "";
      return;
    }
    for(var x=0;x<=this.cats.length;x++){
       if(this.cats[x].id == localStorage.getItem('bcats')){
          this.bcatName = this.cats[x].name;
       }
    }//end for
  }

  backButtonClick (){
    //this.navCtrl.pop();
    this.navCtrl.setRoot("TabsPage");
  }

  btnSkipStep (){
    //mytodo mytodonow : check if contacts are already imported then just go to tabspage
    this.navCtrl.setRoot(this.nextPage);
  }

  ionViewDidLoad() {
    this.loadbusinessname();
  }
  loadbusinessname(){
    let userId;
    if (firebase.auth().currentUser) {
      userId = firebase.auth().currentUser.uid;
  } else {
      userId = localStorage.getItem('userUID');
  }
  if (userId != undefined) {
    this.userservice.getBusinessDetails().then((res)=>{ 
      if(res){
        this.msgstatus=true;
        this.businessname=res['displayName'];
        this.useremail=localStorage.getItem('useremail'); //res['useremail'];
        this.bcats=res['bcats'];
        //this.assignCatName ();
      }else{
        this.businessname='';
        this.useremail='';
        this.bcats = 0;
      }
    }).catch(err=>{
    });
  }else{
    this.businessname='';
    this.useremail='';
    this.bcats = 0;
  }

  }
  onChange(e) {
    if (this.businessname.trim()) {
        this.msgstatus = true;
    }
    else {
        this.msgstatus = false;
    }
  }
  save(){
    this.submitted=true;
    if(this.authForm.valid){
      if(this.businessname.trim()!=null || this.businessname.trim()!= ""){
        this.businessname=this.businessname.trim();
        this.useremail=this.useremail.trim();
        this.userservice.registerBusiness(this.businessname,this.useremail,this.bcats).then((res: any) => {
          if (res.success) {
            this.navCtrl.setRoot(this.nextPage);
          }
        })
        } else {

        }
    }

    }

}
