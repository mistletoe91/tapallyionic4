import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterbusinessPage } from './registerbusiness';

@NgModule({
  declarations: [
    RegisterbusinessPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterbusinessPage),
  ],
})
export class RegisterbusinessPageModule {}
