import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-allimg',
  templateUrl: 'allimg.html',
})
export class AllimgPage {
  allImg:any=[];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
	this.allImg =this.navParams.get('allimg');
  }

}
