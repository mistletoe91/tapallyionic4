import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GroupbuddiesaddPage } from './groupbuddiesadd';

@NgModule({
  declarations: [
    GroupbuddiesaddPage,
  ],
  imports: [
    IonicPageModule.forChild(GroupbuddiesaddPage),
  ],
})
export class GroupbuddiesaddPageModule {}
