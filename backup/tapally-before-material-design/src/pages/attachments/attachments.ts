import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content, AlertController, ViewController, Platform } from 'ionic-angular';

import { UserProvider } from '../../providers/user/user';
import { LoadingProvider } from '../../providers/loading/loading';
import { ImagehandlerProvider } from '../../providers/imagehandler/imagehandler';
import { ChatProvider } from '../../providers/chat/chat';
import { FcmProvider } from '../../providers/fcm/fcm';
import { App } from 'ionic-angular';

@IonicPage()
@Component({
    selector: 'page-attachments',
    templateUrl: 'attachments.html',
})
export class AttachmentsPage {
    @ViewChild('content') content: Content;

    UnreadMSG: any;
    buddy: any;
    buddyStatus: any;
    newmessage: any;

    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public userservice: UserProvider,
        public loading: LoadingProvider,
        public imgstore: ImagehandlerProvider,
        public chatservice: ChatProvider,
        public fcm: FcmProvider,
        private alertCtrl: AlertController,
        private viewCtrl: ViewController,
        public app:App,
        public platform: Platform
    ) {
        this.buddy = this.navParams.get('buddy');
        this.buddyStatus= this.chatservice.buddyStatus;
    }
    sendPicMsg() {
        this.viewCtrl.dismiss();
        this.UnreadMSG = 0;
        this.userservice.getstatus(this.buddy).then((res) => {
            if (!res) {
                this.imgstore.cameraPicmsgStore().then((imgurl) => {

                    this.chatservice.addnewmessage(imgurl, 'image', this.buddyStatus).then(() => {
                        if (this.buddyStatus != 'online') {
                            let newMessage = 'You have received a message';
                            this.fcm.sendNotification(this.buddy, newMessage, 'chatpage');
                        }
                        this.newmessage = '';
                    })
                }).catch((err) => {
                    if(err == 20){
                        this.navCtrl.pop();
                    }
                    alert(err);
                })
            } else {
                let alert = this.alertCtrl.create({
                    message: 'Unblock ' + this.buddy.displayName + ' to send a message.',
                    buttons: [
                        {
                            text: 'CANCEL',
                            role: 'cancel',
                            handler: () => {
                            }
                        },
                        {
                            text: 'UNBLOCK',
                            handler: () => {
                                this.userservice.unblockUser(this.buddy).then((res) => {
                                    if (res) {
                                        this.sendPicMsg();
                                    }
                                })
                            }
                        }
                    ]
                });
                alert.present();
            }
        })
    }
    sendGalleryPicMsg() {
        this.viewCtrl.dismiss();
        this.UnreadMSG = 0;
        this.userservice.getstatus(this.buddy).then((res) => {
            if (!res) {
                this.imgstore.picmsgstore().then((imgurl: any) => {
                    if (imgurl.length > 4) {
                        this.chatservice.addnewmessagemultiple(imgurl, 'image', this.buddyStatus).then(() => {
                            if (this.buddyStatus != 'online') {
                                let newMessage = 'You have received a message';
                                this.fcm.sendNotification(this.buddy, newMessage, 'chatpage');
                            }
                            this.newmessage = '';
                        }).catch((err)=>{
                        })
                    } else {
                        for (let i = 0; i < imgurl.length; i++) {
                            this.chatservice.addnewmessage(imgurl[i], 'image', this.buddyStatus).then(() => {
                                if (this.buddyStatus != 'online') {
                                    let newMessage = 'You have received a message';
                                    this.fcm.sendNotification(this.buddy, newMessage, 'chatpage');
                                }
                                this.newmessage = '';
                            }).catch((err)=>{
                            })
                        }
                    }
                }).catch((err) => {
                })
            } else {
                let alert = this.alertCtrl.create({
                    message: 'Unblock ' + this.buddy.displayName + ' to send a message.',
                    buttons: [
                        {
                            text: 'CANCEL',
                            role: 'cancel',
                            handler: () => {
                            }
                        }, {
                            text: 'UNBLOCK',
                            handler: () => {
                                this.userservice.unblockUser(this.buddy).then((res) => {
                                    if (res) {
                                        this.sendPicMsg();
                                    }
                                })
                            }
                        }
                    ]
                });
                alert.present();
            }
        }).catch((err)=>{
        })

    }

    sendDocument() {
        this.viewCtrl.dismiss();
        this.UnreadMSG = 0;
        this.userservice.getstatus(this.buddy).then((res) => {
            if (!res) {
                this.imgstore.selectDocument().then((imgurl) => {
                    this.chatservice.addnewmessage(imgurl, 'document', this.buddyStatus).then(() => {
                        this.loading.dismissMyLoading();
                        if (this.buddyStatus != 'online') {
                            let newMessage = 'You have received a message';
                            this.fcm.sendNotification(this.buddy, newMessage, 'chatpage');
                        }
                        this.newmessage = '';
                    })
                }).catch((err) => {
                    this.loading.dismissMyLoading();
                    alert(err);
                })
            } else {
                let alert = this.alertCtrl.create({
                    message: 'Unblock ' + this.buddy.displayName + ' to send a message.',
                    buttons: [
                        {
                            text: 'CANCEL',
                            role: 'cancel',
                            handler: () => {
                            }
                        },
                        {
                            text: 'UNBLOCK',
                            handler: () => {
                                this.userservice.unblockUser(this.buddy).then((res) => {
                                    if (res) {
                                        this.sendPicMsg();
                                    }
                                })
                            }
                        }
                    ]
                });
                alert.present();
            }

        });

    }

    sendAudioMsg() {
        this.viewCtrl.dismiss();
        this.userservice.getstatus(this.buddy).then((res) => {
            if (!res) {
                this.imgstore.recordAudio().then((imgurl) => {
                    this.chatservice.addnewmessage(imgurl, 'audio', this.buddyStatus).then(() => {
                        if (this.buddyStatus != 'online') {
                            let newMessage = 'You have received a message';
                            this.fcm.sendNotification(this.buddy, newMessage, 'chatpage');
                        }
                        this.newmessage = '';
                    })
                }).catch((err) => {
                    alert(err);
                })
            } else {
                let alert = this.alertCtrl.create({
                    message: 'Unblock ' + this.buddy.displayName + ' to send a message.',
                    buttons: [
                        {
                            text: 'CANCEL',
                            role: 'cancel',
                            handler: () => {
                            }
                        },
                        {
                            text: 'UNBLOCK',
                            handler: () => {
                                this.userservice.unblockUser(this.buddy).then((res) => {
                                    if (res) {
                                        this.sendPicMsg();
                                    }
                                })
                            }
                        }
                    ]
                });
                alert.present();
            }
        });
    }
    openContacts() {
            this.viewCtrl.dismiss({page:"AllbuddyContactsPage",buddy: this.buddy});


    }
    openMap() {
        this.viewCtrl.dismiss({page:"MapPage",flag: false, buddy: this.buddy});
    }
}
