// Ionic modules
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule} from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { IonicImageViewerModule } from 'ionic-img-viewer';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { HttpBackend, HttpXhrBackend } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { Platform } from 'ionic-angular';

// Entry component
import { MyApp } from './app.component';

// Firebase config
import { config } from './app.angularfireconfig';

// Angular fireauth
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';

// Firebase javascript
import * as firebase from 'firebase';

// Ionic native
import { Camera} from '@ionic-native/camera';
import { Contacts } from '@ionic-native/contacts';
import { Firebase } from '@ionic-native/firebase';
import { SMS } from '@ionic-native/sms';
import { GroupsProvider } from '../providers/groups/groups';
import { ImagePicker } from '@ionic-native/image-picker';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { MediaCapture} from '@ionic-native/media-capture';
import { Geolocation } from '@ionic-native/geolocation';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';

// Custom providers
import { ContactProvider } from '../providers/contact/contact';
import { LoadingProvider } from '../providers/loading/loading';
import { AuthProvider } from '../providers/auth/auth';
import { FcmProvider } from '../providers/fcm/fcm';
import { UserProvider } from '../providers/user/user';
import { ImagehandlerProvider } from '../providers/imagehandler/imagehandler';
import { RequestsProvider } from '../providers/requests/requests';
import { ChatProvider } from '../providers/chat/chat';
import { SmsProvider } from '../providers/sms/sms';
import { CatProvider } from '../providers/cats/cats';
import { NativeHttpModule, NativeHttpBackend, NativeHttpFallback } from 'ionic-native-http-connection-backend';
import { HTTP } from '@ionic-native/http';
import { GpmessageProvider } from '../providers/gpmessage/gpmessage';
import { Chooser } from '@ionic-native/chooser';


// Firebase config app initialize
firebase.initializeApp(config);

@NgModule({
  declarations: [MyApp],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(config),
    AngularFirestoreModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    NativeHttpModule,
    IonicModule.forRoot(MyApp,{tabsPlacement : 'top'}),
    IonicStorageModule.forRoot(),
    IonicImageViewerModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [MyApp],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    UserProvider,
    LoadingProvider,
    ContactProvider,
    Chooser,
    AngularFireAuth,
    ImagehandlerProvider,
    RequestsProvider,
    ChatProvider,
    ContactProvider,
    Contacts,
    Camera,
    FcmProvider,
    CatProvider,
    Firebase,
    SMS,
    SmsProvider,
    GroupsProvider,
    HTTP,
    ImagePicker,
    {provide: HttpBackend, useClass: NativeHttpFallback, deps: [Platform, NativeHttpBackend, HttpXhrBackend]},
    GpmessageProvider ,
    InAppBrowser,
    MediaCapture,
     Geolocation,
  ]
})

export class AppModule {}
