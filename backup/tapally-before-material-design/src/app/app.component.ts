import { Component, ViewChild } from '@angular/core';

// Ionic native
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// Ionic-angular
import { Nav,Platform, NavController, IonicApp, Events } from 'ionic-angular';
import { Subscription } from 'rxjs';

// Providers
import { FcmProvider } from '../providers/fcm/fcm';
import { UserProvider } from '../providers/user/user';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  rootPage: any;
  @ViewChild('myNav') navCtrl: NavController;
  @ViewChild(Nav) nav: Nav;
  pages: Array<{title: string, component: any}>;
  public onResumeSubscription: Subscription;
  public onPauseSubscription: Subscription;
  public alertShown: boolean = false;

  constructor(public events: Events,
    public fcm: FcmProvider,
    public platform: Platform,
    public statusBar: StatusBar,
    public ionicApp: IonicApp,
    public splashScreen: SplashScreen,
    public userService: UserProvider) {



    // Okay, so the platform is ready and our plugins are available.
    // Here you can do any higher level native things you might need.
    this.platform.ready().then((readySource) => {

    // used for an example of ngFor and navigation
    this.pages = [
        { title: 'Home', component: "TabsPage" },
        { title: 'Settings', component: "SettingPage" },
        { title: 'Business Settings', component: "RegisterbusinessPage" },
        { title: 'Create new group', component: "GroupbuddiesPage" },
        { title: 'Your Earnings', component: "EarningsPage" },
    ];

      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.statusBar.overlaysWebView(false);

      // Listen to incoming messages .

      //jaswinder disabled for now because of error
      /*
      this.fcm.listenToNotifications().subscribe(data => {
        if (data.wasTapped) {
          //Notification was received on device tray and tapped by the user.
          this.navCtrl.setRoot('ChatsPage');
        } else {
          //Notification was received in foreground. Maybe the user needs to be notified.
        }
      },(err) => {console.log("error in fcm")});
      */

      this.events.subscribe('checkUserStatus', () => {
        this.onResumeSubscription = platform.resume.subscribe(() => {
          this.fcm.setstatusUser().then((data) => {
          }).catch((error) => {
          });
        },(err) => {console.log(err)});
        this.onPauseSubscription = platform.pause.subscribe(() => {
          this.fcm.setStatusOffline().then((data) => {
          }).catch((error) => {
          });
        },(err) => {console.log(err)});
      });

      let userID = localStorage.getItem('userUID');
      let username = localStorage.getItem("userName");
      if (userID != undefined) {
        if (username != undefined || username != null || username != " ") {
          this.userService.getbuddydetails(userID).then((res) => {
            if (res["displayName"]) {
                this.rootPage = "TabsPage";//TabsPage RegisterbusinessPage
            } else {
              this.rootPage = "DisplaynamePage";//DisplaynamePage
            }
          })
        }
      } else {
        this.rootPage = 'TutorialPage';//PhonePage
      }
    })
  }


    openPage(page) {
      // Reset the content nav to have just this page
      // we wouldn't want the back button to show in this scenario
      if(page.component == "TabsPage"){
        this.nav.setRoot(page.component);
      } else {
        this.nav.push(page.component);
      }
    }
}
