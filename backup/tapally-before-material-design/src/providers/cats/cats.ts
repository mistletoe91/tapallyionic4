export class CatProvider {
  constructor() {
  }
 

  getCats (){
  return [
      {"id":1,"name":"Accountants","sort":1},
      {"id":2,"name":"Insurance Agents","sort":2},
      {"id":3,"name":"Real Estate Agents","sort":3},
      {"id":4,"name":"Mortgage Agents","sort":4},
      {"id":5,"name":"Travel (Air Ticket) Agents","sort":5},
      {"id":6,"name":"Lawyers","sort":6},
      {"id":7,"name":"Dentists","sort":7},
      {"id":8,"name":"Plumber","sort":8},
      {"id":9,"name":"Construction/Renovation","sort":9},
      {"id":10,"name":"Painter","sort":10},
      {"id":11,"name":"Electrician","sort":11},
      {"id":12,"name":"xxxxx","sort":12},
      {"id":13,"name":"Addiction Treatment Center","sort":13},
      {"id":14,"name":"Adoption Agency","sort":14},
      {"id":15,"name":"Adult Day Care Center","sort":15},
      {"id":16,"name":"Adult Education School","sort":16},
      {"id":17,"name":"Adult Entertainment Club","sort":17},
      {"id":18,"name":"Adult Entertainment Store","sort":18},
      {"id":19,"name":"Adult Foster Care Service","sort":19},
      {"id":20,"name":"Advertising Agency","sort":20},
      {"id":21,"name":"Aerobics Instructor","sort":21},
      {"id":22,"name":"Aerospace Company","sort":22},
      {"id":23,"name":"Afghani Restaurant","sort":23},
      {"id":24,"name":"African Goods Store","sort":24},
      {"id":25,"name":"African Restaurant","sort":25},
      {"id":26,"name":"After School Program","sort":26},
      {"id":27,"name":"Aged Care","sort":27},
      {"id":28,"name":"Agricultural Service","sort":28},
      {"id":29,"name":"Aikido School","sort":29},
      {"id":30,"name":"Air Compressor Repair Service","sort":30},
      {"id":31,"name":"Air Compressor Supplier","sort":31},
      {"id":32,"name":"Air Conditioning Contractor","sort":32},
      {"id":33,"name":"Air Conditioning Repair Service","sort":33},
      {"id":34,"name":"Air Duct Cleaning Service","sort":34},
      {"id":35,"name":"Air Filter Supplier","sort":35},
      {"id":36,"name":"Air Force Base","sort":36},
      {"id":37,"name":"Air Traffic Control Tower","sort":37},
      {"id":38,"name":"Airbrushing Service","sort":38},
      {"id":39,"name":"Airbrushing Supply Store","sort":39},
      {"id":40,"name":"Aircraft Rental Service","sort":40},
      {"id":41,"name":"Aircraft Supply Store","sort":41},
      {"id":42,"name":"Airline","sort":42},
      {"id":43,"name":"Airplane","sort":43},
      {"id":44,"name":"Airport","sort":44},
      {"id":45,"name":"Airport Shuttle Service","sort":45},
      {"id":46,"name":"Airport Terminal","sort":46},
      {"id":47,"name":"Airsoft Supply Store","sort":47},
      {"id":48,"name":"Airstrip","sort":48},
      {"id":49,"name":"Alcoholism Treatment Program","sort":49},
      {"id":50,"name":"Allergist","sort":50}
      ];
  }

}
