import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import firebase from 'firebase';
import { Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { userPicArr } from '../../app/app.angularfireconfig';
@Injectable()
export class UserProvider {
  userId;
  userName;
  userPic;
  firedata = firebase.database().ref('/chatusers');
  firefriend = firebase.database().ref('/friends');
  firebuddychat = firebase.database().ref('/buddychats');
  users = firebase.database().ref('/users');
  userstatus = firebase.database().ref('/userstatus');
  firereq = firebase.database().ref('/requests');
  firenotify = firebase.database().ref('/notification');
  fireBusiness = firebase.database().ref('/business');
  fireInvitationSent = firebase.database().ref('/invitationsent');

  isUserExits: boolean = true;
  blockUsers: any = [];
  unblockUsers: any = [];
  isuserBlock: boolean = false;
  blockUsersCounter = 0;
  isNotify: boolean;
  constructor(

    private afireAuth: AngularFireAuth,
    public storage: Storage,
    public events: Events
  ) {

  }
  initializeItem() {
    let userID = localStorage.getItem('userUID');

    if (userID != undefined) {
      this.userId = userID;
    } else {
      this.userId = firebase.auth().currentUser.uid;
    }
    return new Promise((resolve, reject) => {
      this.firenotify.child(this.userId).child('isNotify').once('value', (snapshot) => {
        this.isNotify = snapshot.val();
        resolve(this.isNotify);
      })
    })

  }


  //mytodo : is this being used anywhere ?
  adduser(newuser) {
    let myuserPic = userPicArr [Math.floor(Math.random() * Object.keys(userPicArr).length-1)];
    var promise = new Promise((resolve, reject) => {
      this.afireAuth.auth.createUserWithEmailAndPassword(newuser.email, newuser.password).then(() => {
        this.afireAuth.auth.currentUser.updateProfile({
          displayName: newuser.username,
          photoURL: myuserPic
        }).then(() => {
          this.firedata.child(this.userId).set({
            uid: this.userId,
            displayName: newuser.username,
            photoURL: myuserPic,
            invited_by:"none"
          }).then(() => {
            resolve(true);
          }).catch((err) => {
            reject(err);
          })
        }).catch((err) => {
          reject(err);
        })
      }).catch((err) => {
        reject(err);
      })
    })
    return promise
  }
  addmobileUser(verificationCredential, code, mobile) {
    let myuserPic = userPicArr [Math.floor(Math.random() * Object.keys(userPicArr).length-1)];
    return new Promise((resolve, reject) => {
      localStorage.setItem('verificationCredential', verificationCredential);
      localStorage.setItem('code', code);

      let signInData = firebase.auth.PhoneAuthProvider.credential(verificationCredential, code);
      this.afireAuth.auth.signInWithCredential(signInData).then((info) => {
        let profileImage;
        if (this.afireAuth.auth.currentUser.photoURL != null) {
          profileImage = this.afireAuth.auth.currentUser.photoURL;
        } else {
          profileImage = myuserPic
        }

        this.afireAuth.auth.currentUser.updateProfile({ displayName: this.afireAuth.auth.currentUser.displayName, photoURL: profileImage }).then(() => {
          this.firedata.child(this.afireAuth.auth.currentUser.uid).set({
            uid: this.afireAuth.auth.currentUser.uid,
            mobile: mobile,
            displayName: this.afireAuth.auth.currentUser.displayName,
            disc: '',
            photoURL: profileImage,
            deviceToken: ''
          }).then(() => {
            resolve({ success: true });
          }).catch((err) => {
            reject(err);
          })
        }).catch((err) => {
        })
      }).catch((err) => {
        resolve({ success: false, msg: JSON.stringify(err.message) });
      })
    })
  }

  addmobileUserIOS(countryCode, mobile) {
    let myuserPic = userPicArr [Math.floor(Math.random() * Object.keys(userPicArr).length-1)];
    return new Promise((resolve, reject) => {
      localStorage.setItem('verificationCredential', countryCode);
      localStorage.setItem('code', mobile);


      this.firedata.once('value', (snep) => {
        let allData = snep.val();

        for (let tmpkey in allData) {
          if (allData[tmpkey].mobile == mobile) {
            this.isUserExits = false;
            localStorage.setItem('userUID', allData[tmpkey].uid);
          }
        }

        if (this.isUserExits) {
          this.afireAuth.auth.signInAnonymously().then((info) => {


            let profileImage;
            if (this.afireAuth.auth.currentUser.photoURL != null) {
              profileImage = this.afireAuth.auth.currentUser.photoURL;
            } else {
              profileImage = myuserPic
            }

            localStorage.setItem('userUID', this.afireAuth.auth.currentUser.uid);

            this.afireAuth.auth.currentUser.updateProfile({ displayName: this.afireAuth.auth.currentUser.displayName, photoURL: profileImage }).then(() => {
              this.firedata.child(this.afireAuth.auth.currentUser.uid).set({
                uid: this.afireAuth.auth.currentUser.uid,
                mobile: mobile,
                displayName: this.afireAuth.auth.currentUser.displayName,
                disc: '',
                photoURL: profileImage,
                deviceToken: ''
              }).then(() => {
                resolve({ success: true });
              }).catch((err) => {
                reject(err);
              })
            }).catch((err) => {
            })
          }).catch((err) => {
            resolve({ success: false, msg: JSON.stringify(err.message) });
          });
        } else {
          resolve({ success: true });
        }
      });
    });
  }

  updateimage(imageurl) {


    let userID = localStorage.getItem('userUID');
    let userName = localStorage.getItem('userName');
    let userPic = localStorage.getItem('userPic');


    if (userID != undefined) {
      this.userId = userID;
      this.userName = userName;
      this.userPic = userPic;
    } else {
      this.userId = this.afireAuth.auth.currentUser.uid;
      this.userName = this.afireAuth.auth.currentUser.displayName;
      this.userPic = this.afireAuth.auth.currentUser.photoURL;
    }
    var promise = new Promise((resolve, reject) => {
      this.firedata.child(this.userId).update({ photoURL: imageurl }).then(() => {
        firebase.database().ref('/users/' + this.userId).update({
          photoURL: imageurl
        }).then(() => {
          resolve({ success: true });
        }).catch((err) => {
          reject(err);
        })
      })
    })
    return promise;
  }
  getuserdetails() {

    let userID = localStorage.getItem('userUID');
    let userName = localStorage.getItem('userName');
    let userPic = localStorage.getItem('userPic');


    if (userID != undefined || userID != null) {
      this.userId = userID;
      this.userName = userName;
      this.userPic = userPic;
    } else {
      this.userId = this.afireAuth.auth.currentUser.uid;
      this.userName = this.afireAuth.auth.currentUser.displayName;
      this.userPic = this.afireAuth.auth.currentUser.photoURL;
    }

    var promise = new Promise((resolve, reject) => {
      this.firedata.child(this.userId).once('value', (snapshot) => {
        resolve(snapshot.val());
      }).catch((err) => {
        reject(err);
      })
    })
    return promise;
  }
  getbuddydetails(buddyID) {
    var promise = new Promise((resolve, reject) => {
      this.firedata.child(buddyID).once('value', (snapshot) => {
        resolve(snapshot.val());
      }).catch((err) => {
        reject(err);
      })
    })
    return promise;
  }
  updatedisplayname(newname) {
    let userID = localStorage.getItem('userUID');



    if (userID != undefined) {
      this.userId = userID;

    } else {
      this.userId = this.afireAuth.auth.currentUser.uid;

    }

    var promise = new Promise((resolve, reject) => {

      this.firedata.child(this.userId).update({
        displayName: newname

      }).then(() => {
        localStorage.setItem('userName', newname);

        resolve({ success: true });
      }).catch((err) => {
        reject(err);
      })
    })
    return promise;
  }
  updateDeviceToken(token, userID) {

    var promise = new Promise((resolve, reject) => {
      this.firedata.child(userID).update({
        deviceToken: token
      }).then(() => {
        resolve({ success: true });
      }).catch((err) => {
        reject(err);
      })
    })
    return promise;
  }
  updatedisc(disc) {
    let userID = localStorage.getItem('userUID');
    let userName = localStorage.getItem('userName');
    let userPic = localStorage.getItem('userPic');


    if (userID != undefined) {
      this.userId = userID;
      this.userName = userName;
      this.userPic = userPic;
    } else {
      this.userId = this.afireAuth.auth.currentUser.uid;
      this.userName = this.afireAuth.auth.currentUser.displayName;
      this.userPic = this.afireAuth.auth.currentUser.photoURL;
    }
    var promise = new Promise((resolve, reject) => {
      this.firedata.child(this.userId).update({
        disc: disc
      }).then(() => {
        resolve({ success: true });
      }).catch((err) => {
        reject(err);
      })
    })
    return promise;
  }
  getallusers() {
    var promise = new Promise((resolve, reject) => {
      this.firedata.orderByChild('uid').once('value', (snapshot) => {
        //mytodo immediate : Why its getting all users. it should only get me
        let userdata = snapshot.val();
        let temparr = [];
        for (var key in userdata) {
          temparr.push(userdata[key]);
        }
        resolve(temparr);
      }).catch((err) => {
        reject(err);
      })
    })
    return promise;
  }
  directLogin() {
    return new Promise((resolve, reject) => {
      resolve({ success: true })
    })
  }
  deleteUser() {
    let userID = localStorage.getItem('userUID');
    let userName = localStorage.getItem('userName');
    let userPic = localStorage.getItem('userPic');


    if (userID != undefined) {
      this.userId = userID;
      this.userName = userName;
      this.userPic = userPic;
    } else {
      this.userId = this.afireAuth.auth.currentUser.uid;
      this.userName = this.afireAuth.auth.currentUser.displayName;
      this.userPic = this.afireAuth.auth.currentUser.photoURL;
    }
    return new Promise((resolve, reject) => {
      this.firefriend
      this.firedata.child(this.userId).remove().then(() => {
        this.deletefriend(this.userId);
        this.deletefirebuddychat(this.userId);
        this.deleteusers(this.userId);
        this.deleteuserstatus(this.userId);
        localStorage.clear();
        resolve({ success: true })
      }).catch((err) => {
        reject({ success: true })
      })
    });
  }
  deletefriend(id) {
    this.firedata.child(id).remove().then((res) => {
    })
  }
  deletefirebuddychat(id) {
    this.firebuddychat.child(id).remove().then((res) => {
    })
  }
  deleteusers(id) {
    this.users.child(id).remove().then((res) => {
    })
  }
  deleteuserstatus(id) {
    this.userstatus.child(id).remove().then((res) => {
    })
  }
  blockUser(buddy) {
    return new Promise((resolve, reject) => {
      this.firefriend.child(this.userId).orderByChild('uid').once('value', (snapshot) => {
        let allfriends = snapshot.val();
        for (var key in allfriends) {
          if (allfriends[key].uid == buddy.uid) {
            this.firefriend.child(this.userId).child(key).update({ isBlock: true });
            resolve(true);
          }
        }
      })
    })
  }
  unblockUser(buddy) {
    return new Promise((resolve, reject) => {
      this.firefriend.child(this.userId).orderByChild('uid').once('value', (snapshot) => {
        let allfriends = snapshot.val();
        for (var key in allfriends) {
          if (allfriends[key].uid == buddy.uid) {
            this.firefriend.child(this.userId).child(key).update({ isBlock: false });
            resolve(true);
          }
        }
      })
    })
  }

  getstatus(buddy) {
    return new Promise((resolve, reject) => {
      this.firefriend.child(this.userId).orderByChild('uid').once('value', (snapshot) => {
        let allfriends = snapshot.val();
        for (var key in allfriends) {
          if (allfriends[key].uid == buddy.uid) {
            resolve(allfriends[key].isBlock)
          }
        }
      })
    })
  }

  getstatusblock(buddy) {
    return new Promise((resolve, reject) => {
      this.firefriend.child(buddy.uid).orderByChild('uid').once('value', (snapshot) => {
        let allfriends = snapshot.val();
        for (var key in allfriends) {
          if (allfriends[key].uid == this.userId) {
            resolve(allfriends[key].isBlock)
          }
        }
      })
    })
  }

  getuserblock(buddy) {
    return new Promise((resolve, reject) => {
      this.firefriend.child(buddy.uid).orderByChild('uid').once('value', (snapshot) => {
        let allfriends = snapshot.val();
        for (var key in allfriends) {
          if (allfriends[key].uid == this.userId) {
            resolve(allfriends[key].isBlock);
          }
        }
      })
    })
  }

  getmsgblock(buddy) {
    return new Promise((resolve, reject) => {
      this.firefriend.child(buddy).orderByChild('uid').once('value', (snapshot) => {
        let allfriends = snapshot.val();
        for (var key in allfriends) {
          if (allfriends[key].uid == this.userId) {
            resolve(allfriends[key].isBlock);
          }
        }
      })
    })
  }

  getstatusblockuser(buddy) {
    this.firefriend.child(buddy.uid).orderByChild('uid').on('value', (snapshot) => {
      let allfriends = snapshot.val();
      for (var key in allfriends) {
        if (allfriends[key].uid == this.userId) {
          this.isuserBlock = allfriends[key].isBlock;
        }
      }
      this.events.publish('isblock-user');
    })
  }
  getAllBlockUsers() {
    this.firefriend.child(this.userId).orderByChild('uid').once('value', (snapshot) => {
      let allfriends = snapshot.val();
      let blockuser = []
      for (var key in allfriends) {
        if (allfriends[key].isBlock) {
          this.firedata.child(allfriends[key].uid).once('value', (snapsho) => {
            blockuser.push(snapsho.val());
          }).catch((err) => {
          })
        }
        this.blockUsers = [];
        this.blockUsers = blockuser;
      }
      this.events.publish('block-users');
    })
  }
  getAllunBlockUsers() {
    this.firefriend.child(this.userId).orderByChild('uid').once('value', (snapshot) => {
      let allfriends = snapshot.val();
      let unblockuser = [];
      for (var key in allfriends) {
        if (allfriends[key].isBlock == false) {
          this.firedata.child(allfriends[key].uid).once('value', (snapsho) => {
            unblockuser.push(snapsho.val());
          }).catch((err) => {
          })
        }
        this.unblockUsers = [];
        this.unblockUsers = unblockuser;
      }
      this.events.publish('unblock-users');
    })
  }
  getAllBlockUsersCounter() {
    // block-users-counter
    this.firefriend.child(this.userId).orderByChild('uid').once('value', (snapshot) => {
      let allfriends = snapshot.val();
      let blockalluser = [];
      for (let tmpkey in allfriends) {
        if (allfriends[tmpkey].isBlock) {
          blockalluser.push(allfriends[tmpkey]);
        }
      }
      this.firereq.child(this.userId).orderByChild('uid').once('value', (snapshot) => {
        let allrequest = snapshot.val();
        for (let tmp in allrequest) {
          if (allrequest[tmp].isBlock) {
            blockalluser.push(allrequest[tmp]);
          }
        }
        this.blockUsersCounter = 0;
        this.blockUsersCounter = blockalluser.length;
        this.events.publish('block-users-counter');
      })
    })
  }
  notifyUser(isnotify) {
    this.firenotify.child(this.userId).set({
      isNotify: isnotify
    })
  }
  getNotifyStatus(buddy) {
    return new Promise((resolve, reject) => {
      this.firenotify.child(buddy.uid).child('isNotify').once('value', (snapshot) => {
        let isNotify = snapshot.val();
        if (isNotify == true)
          resolve(true);
        else
          resolve(false);
      })
    })
  }


  logInvitation(isInviteArray) {
    console.log ("1");
    let userID = localStorage.getItem('userUID');
    if (userID != undefined) {
      this.userId = userID;
    } else {
      this.userId = this.afireAuth.auth.currentUser.uid;
    }

    console.log ("2");
    //create   jaswinder
    return new Promise((resolve, reject) => {
      this.fireInvitationSent.child(this.userId).set({
        phone: isInviteArray.phone
      }).then(() => {
        console.log ("3");
        resolve(true);
      }).catch((err) => {
        resolve(false);
      })
    })

   }


  registerBusiness(newname,useremail,bcats) {
    let userID = localStorage.getItem('userUID');
    if (userID != undefined) {
      this.userId = userID;
    } else {
      this.userId = this.afireAuth.auth.currentUser.uid;
    }

    //update email
    new Promise((resolve, reject) => {
      this.firedata.child(this.userId).update({
        useremail: useremail
      }).then(() => {
        localStorage.setItem('useremail', useremail);
      }).catch((err) => {
      })
    })

    let timeStamp:string = String(Date.now());//

    //update/create business name
    var promise = new Promise((resolve, reject) => {
      this.fireBusiness.child(this.userId).update({
        displayName: newname,
        timestamp_user_local_time_updated : timeStamp
      }).then(() => {

        if(localStorage.getItem('businessName') || localStorage.getItem('businessName')!= newname){
           localStorage.setItem('businessName', "");
        }

        if(!localStorage.getItem('businessName')){
            //Business Just got registered
            //var timestamp = firebase.database.ServerValue.TIMESTAMP;
            new Promise((resolve, reject) => {
              this.fireBusiness.child(this.userId).update({
                timestamp_user_local_time_created: timeStamp
              }).then(() => {
                 resolve({ success: true });
              }).catch((err) => {
              })
            });
            localStorage.setItem('business_created', timeStamp);
        }//endif

        localStorage.setItem('businessName', newname);
        resolve({ success: true });
      }).catch((err) => {
        reject(err);
      })
    })


        //update cat
        new Promise((resolve, reject) => {
          this.fireBusiness.child(this.userId).update({
            bcats: bcats
          }).then(() => {
            localStorage.setItem('bcats', bcats);
          }).catch((err) => {
          })
        })

    return promise;
  }//end function

  checkIfBusinessIsPaid (businessId){
    var promise_cibp = new Promise((resolve, reject) => {
      this.fireBusiness.child(businessId).once('value', (snapshot) => {
         resolve(snapshot.val());
      }).catch((err) => {
        reject(err);
      })
    })
    return promise_cibp;
  }


  getBusinessDetails() {
    console.log ("Calling Service getBusinessDetails()<-------------");
    let userID = localStorage.getItem('userUID');
    let userName = localStorage.getItem('userName');

    if (userID != undefined || userID != null) {
      this.userId = userID;
      this.userName = userName;
    } else {
      this.userId = this.afireAuth.auth.currentUser.uid;
      this.userName = this.afireAuth.auth.currentUser.displayName;
    }

    var promise = new Promise((resolve, reject) => {
      this.fireBusiness.child(this.userId).once('value', (snapshot) => {
         let res = snapshot.val();
         console.log ("res");
         console.log (res);
         if(res){
           console.log ("GOOODA DAY");

               if(Object.keys(res).length>0){
                 localStorage.setItem('business_created', res['timestamp_user_local_time_created']);
                 localStorage.setItem('businessName', res['displayName']);
                 localStorage.setItem('bcats', res['bcats']);
                 console.log ("PAID FLAG FETCHED");
                 console.log (res['paid']);
                 if(res['paid']){
                    localStorage.setItem('paid', "1");
                 } else {
                    localStorage.setItem('paid', "-1");
                 }
               } else {
                    localStorage.setItem('paid', "-1");
               }

         }//endif 



         resolve(res);
      }).catch((err) => {
        reject(err);
      })
    })
    return promise;
  }//end function


}
