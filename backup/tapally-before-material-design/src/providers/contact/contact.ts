import { Injectable } from '@angular/core';
import { Contacts, Contact, ContactField } from '@ionic-native/contacts';

@Injectable()
export class ContactProvider {

    constructor(public contacts: Contacts) {
    }

    // Get All contact from the device
    getSimJsonContacts() {

        var options = {
            filter: "",
            multiple: true,
            hasPhoneNumber: true
        };
        return new Promise((resolve, reject) => {
            this.contacts.find(["phoneNumbers", "displayName"], options).then((res) => {
                if (res != null) {
                    resolve(this.onSuccess(res));
                }
            }).catch((err) => {
                reject(err);
            });
        });
    }

    // Call onSuccess contact res
    onSuccess(contacts) {
        var phoneContactsJson = [];
        for (var i = 0; i < contacts.length; i++) {
            let no;
            if (contacts[i].name != null) {
                no = contacts[i].name.formatted;

            }
            var phonenumber = contacts[i].phoneNumbers;
            if (phonenumber != null) {

                var type = phonenumber[0].type;
                if (type == 'mobile' || type == 'work' || type == 'home') {

                    let phone = phonenumber[0].value;
                    let tempMobile;
                    tempMobile = phone.replace(/[^0-9]/g, "");

                    let mobile;
                    if (tempMobile.length > 10) {
                        mobile = tempMobile.substr(tempMobile.length - 10);
                    } else {
                        mobile = tempMobile;
                    }
                    var contactData = {
                        "_id": mobile,
                        "displayName": no,
                        "mobile": mobile,
                        "isUser": '0'
                    }
                    phoneContactsJson.push(contactData);
                }
            }
        }

        var collection = {
            contacts: phoneContactsJson
        };

        return collection;
    }
    addContact(newContact: any) {
        return new Promise((resolve, reject) => {
            let contact: Contact = this.contacts.create();
            contact.displayName = newContact.displayName;
            let field = new ContactField();
            field.type = 'mobile';
            field.value = newContact.phoneNumber;
            field.pref = true;
            var numberSection = [];
            numberSection.push(field);
            contact.phoneNumbers = numberSection;
            contact.save().then((value) => {
                resolve(true);
            }, (error) => {
                reject(error);
            })
        })
    }
}
