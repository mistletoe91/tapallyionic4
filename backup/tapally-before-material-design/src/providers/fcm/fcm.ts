import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { RequestOptions, Headers, Http } from "@angular/http";
import { Firebase } from '@ionic-native/firebase';
import { AngularFirestore } from 'angularfire2/firestore';
import { UserProvider } from '../../providers/user/user';
import firebase from 'firebase';

import { server_key } from '../../app/app.angularfireconfig';
@Injectable()
export class FcmProvider {
  userId;
  fireuserStatus = firebase.database().ref('/userstatus');

  constructor(
    public userservice: UserProvider,
    private http: Http,
    public firebaseNative: Firebase,
    public afs: AngularFirestore,
    public platform: Platform
  ) {
  }

  // Get permission from the user
  async getToken() { 

    let userID = localStorage.getItem('userUID');
    if (userID != undefined) {
      this.userId = userID;
    } else {
      this.userId = firebase.auth().currentUser.uid;
    }
    let token;
    if (this.platform.is('android')) {
      token = await this.firebaseNative.getToken();

      this.firebaseNative.onTokenRefresh()
        .subscribe((token: string) => {
          this.userservice.updateDeviceToken(token, this.userId);
        });
    }
    if (this.platform.is('ios')) {
      token = await this.firebaseNative.getToken();
      await this.firebaseNative.grantPermission();
      await this.firebaseNative.onTokenRefresh()
        .subscribe((token: string) => {
          this.userservice.updateDeviceToken(token, this.userId);
        });
    }

  }
  // Listen to incoming FCM messages
  listenToNotifications() {
    this.firebaseNative.grantPermission();
    return this.firebaseNative.onNotificationOpen();
  }

  setstatusUser() {
    let time = this.formatAMPM(new Date());
    let date = this.formatDate(new Date());
    var promise = new Promise((resolve, reject) => {
      this.fireuserStatus.child(this.userId).set({
        status: 1,
        data: 'online',
        timestamp: date + ' at ' + time
      }).then(() => {
        resolve(true);
      }).catch((err) => {
        reject(err);
      })
    })
    return promise;
  }
  setStatusOffline() {
    let time = this.formatAMPM(new Date());
    let date = this.formatDate(new Date());

    var promise = new Promise((resolve, reject) => {
      this.fireuserStatus.child(this.userId).update({
        status: 0,
        data: 'offline',
        timestamp: date + ' at ' + time
      }).then(() => {
        resolve(true);
      }).catch((err) => {
        reject(err);
      })

    })
    return promise;
  }

  formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
  }
  formatDate(date) {
    var dd = date.getDate();
    var mm = date.getMonth() + 1;
    var yyyy = date.getFullYear();
    if (dd < 10)
      dd = '0' + dd;
    if (mm < 10)
      mm = '0' + mm;
    return dd + '/' + mm + '/' + yyyy;
  }

  sendNotification(buddy, msg, pagename) {
    let body = {};
    if (pagename == 'chatpage') {
      body = {
        "notification": {
          "title": buddy.displayName,
          "body": msg,
          "sound": "default",
          "forceStart": "1",
          "icon": "https://s3.envato.com/files/244032374/ICON.jpg"
        },
        "to": buddy.deviceToken,
        "priority": "high"
      }
    } else if (pagename == 'Grouppage') {
      body = {
        "notification": {
          "title": buddy.displayName,
          "body": msg,
          "sound": "default",
          "forceStart": "1",
          "icon": "https://s3.envato.com/files/244032374/ICON.jpg"
        },
        "to": buddy.deviceToken,
        "priority": "high"
      }
    } else if (pagename == 'sendreq') {
      body = {
        "notification": {
          "title": '',
          "body": msg,
          "forceStart": "1",
          "sound": "default",
          "icon": "https://s3.envato.com/files/244032374/ICON.jpg"
        },
        "to": buddy.deviceToken,
        "priority": "high"
      }
    }
    let headers = new Headers({ 'Authorization': 'key=' + server_key.key, 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    this.http.post("https://fcm.googleapis.com/fcm/send", body, options).subscribe(res => {
    }, error => {
    });
  }

}
