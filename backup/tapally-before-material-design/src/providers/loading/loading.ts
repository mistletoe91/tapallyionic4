import { Injectable } from '@angular/core';
import { LoadingController, ToastController } from 'ionic-angular';


@Injectable()
export class LoadingProvider {

	loading:any;

	constructor(public toastCtrl: ToastController,
				public loadingCtrl: LoadingController) {


	}

	// For present loading
	presentLoading() {

		/*
			this.loading = this.loadingCtrl.create({
				content: 'Please wait...'
			});

			this.loading.present().then(()=>{
			   //setTimeout(()=>{
			      this.loading.dismiss();
			   //},300);
			});
			*/

	}

	dismissMyLoading (){
		//this.loading.dismiss();
	}

	// Toast message
	presentToast(text) {
		let toast = this.toastCtrl.create({
			message: text,
			duration: 3000,
			position: 'top'
		});
		toast.present();
	}

}
