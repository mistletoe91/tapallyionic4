import { Injectable } from '@angular/core';
import { connreq } from '../../models/interfaces/request';
import firebase from 'firebase';
import { UserProvider } from '../user/user';
import { Events } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';



@Injectable()
export class RequestsProvider {
  firebuddychats = firebase.database().ref('/buddychats');

  firedata = firebase.database().ref('/chatusers');
  firereq = firebase.database().ref('/requests');
  firefriends = firebase.database().ref('/friends');
  userdetails;
  myfriends;
  userId;
  blockRequest: any = [];
  messages;
  constructor(
    public userservice: UserProvider,
    public events: Events,
    public storage: Storage
  ) {
    let userID = localStorage.getItem('userUID');

    if (userID != undefined) {
      this.userId = userID;
    } else {
      this.userId = firebase.auth().currentUser.uid;
    }

  }


  sendrequest(req: connreq) {
    var promise = new Promise((resolve, reject) => {
      this.firereq.child(req.recipient).push({ sender: req.sender, isBlock: false }).then(() => {
        resolve({ success: true });
      })
    })
    return promise;
  }
  getmyrequests() {
    let allmyrequests;
    var myrequests = [];
    this.firereq.child(this.userId).on('value', (snapshot) => {
      allmyrequests = snapshot.val();
      myrequests = [];
      for (var i in allmyrequests) {
        myrequests.push(allmyrequests[i]);
      }
      this.userservice.getallusers().then((res) => {
        var allusers = res;
        this.userdetails = [];
        for (var j in myrequests)
          for (var key in allusers) {
            if (myrequests[j].sender === allusers[key].uid && myrequests[j].isBlock == false) {
              this.userdetails.push(allusers[key]);
            }
          }
        this.events.publish('gotrequests');
      })

    })
  }
  acceptrequest(buddy) {
    var promise = new Promise((resolve, reject) => {
      this.firefriends.child(this.userId).push({
        uid: buddy.uid,
        isActive: 1,
        isBlock: false
      }).then(() => {
        this.firefriends.child(buddy.uid).push({
          uid: this.userId,
          isActive: 1,
          isBlock: false
        }).then(() => {
          this.deleterequest(buddy).then(() => {
            resolve(true);
          })

        })
      })
    })
    return promise;
  }
  deleterequest(buddy) {
    var promise = new Promise((resolve, reject) => {
      this.firereq.child(this.userId).orderByChild('sender').equalTo(buddy.uid).once('value', (snapshot) => {
        let somekey;
        for (var key in snapshot.val())
          somekey = key;
        this.firereq.child(this.userId).child(somekey).remove().then(() => {
          resolve(true);
        })
      })
        .then(() => {

        }).catch((err) => {
          reject(err);
        })
    })
    return promise;
  }
  getmyfriends() {
    let friendsuid = [];
    this.firefriends.child(this.userId).on('value', (snapshot) => {
      let allfriends = snapshot.val();
      if (allfriends != null) {
        for (var i in allfriends) {
          friendsuid.push(allfriends[i]);
        }
      }
      this.userservice.getallusers().then((users) => {
        this.myfriends = [];
        let counter = 0;
        if (friendsuid.length > 0) {
          for (var j in friendsuid) {
            for (var key in users) {
              if (friendsuid[j].uid === users[key].uid) {
                users[key].isActive = friendsuid[j].isActive;
                users[key].userprio = friendsuid[j].userprio ? friendsuid[j].userprio : null;
                let userKey;
                userKey = users[key];
                let flag1 = false;
                let flag2 = false;
                let flag3 = false;
                this.userservice.getuserblock(users[key]).then((res) => {
                  userKey.isBlock = res;
                  flag1 = true;
                })
                this.get_last_unreadmessage(users[key]).then(responce => {
                  userKey.unreadmessage = responce;
                  flag2 = true;
                })
                this.get_last_msg(users[key]).then(resp => {
                  userKey.lastMessage = resp;
                  flag3 = true;
                  this.myfriends.push(userKey);
                  counter++;
                  if (counter == friendsuid.length) {
                    this.events.publish('friends');
                  }
                })
              }
            }
          }
        } else {
          if (friendsuid.length == 0) {
            this.events.publish('friends');
          }
        }
      }).catch((err) => {
      })

    })
  }
  get_last_msg(buddy) {
    return new Promise((resolve, reject) => {
      this.firebuddychats.child(this.userId).child(buddy.uid).orderByChild('uid').once('value', (snapshot) => {
        let allmsg = snapshot.val();
        if (allmsg != null) {
          let lastmsg = allmsg[Object.keys(allmsg)[Object.keys(allmsg).length - 1]]
          resolve(lastmsg)
        } else {
          resolve('')
        }
      })
    })
  }
  get_last_unreadmessage(buddy) {
    return new Promise((resolve, reject) => {
      let allunreadmessage = [];
      this.firebuddychats.child(buddy.uid).child(this.userId).orderByChild('uid').once('value', (snapshot) => {
        let allmsg = snapshot.val();
        if (allmsg != null) {
          for (let tmpkey in allmsg) {
            if (allmsg[tmpkey].isRead == false) {
              allunreadmessage.push(allmsg);
            }
          }
        }
        if (allunreadmessage.length > 0) {
          resolve(allunreadmessage.length)
        } else {
          resolve(allunreadmessage.length)
        }
      })
    })
  }
  pendingetmyrequests(userd) {
    return new Promise((resolve, reject) => {
      let allmyrequests;
      var myrequests = [];

      this.firereq.child(userd).on('value', (snapshot) => {
        allmyrequests = snapshot.val();
        myrequests = [];
        for (var i in allmyrequests) {
          myrequests.push(allmyrequests[i].sender);
        }
        this.userservice.getallusers().then((res) => {
          var allusers = res;
          let pendinguserdetails = [];
          for (var j in myrequests)
            for (var key in allusers) {
              if (myrequests[j] == allusers[key].uid) {
                pendinguserdetails.push(allusers[key]);
              }
            }
          resolve(pendinguserdetails);
        })

      })
    })
  }
  blockuserprof(buddy) {
    return new Promise((resolve, reject) => {
      this.firereq.child(this.userId).orderByChild('uid').once('value', (snapshot) => {
        let requestFriends = snapshot.val();
        for (let tmpkey in requestFriends) {
          if (requestFriends[tmpkey].sender == buddy.uid) {
            this.firereq.child(this.userId).child(tmpkey).update({ isBlock: true })
            resolve(true);
          }
        }
      })
    })
  }
  getAllBlockRequest() {
    this.firereq.child(this.userId).orderByChild('uid').once('value', (snapshot) => {
      let allrequest = snapshot.val();
      let blockuser = []
      for (var key in allrequest) {
        if (allrequest[key].isBlock) {
          this.firedata.child(allrequest[key].sender).once('value', (snapsho) => {
            blockuser.push(snapsho.val());
          }).catch((err) => {
          })
        }
        this.blockRequest = [];
        this.blockRequest = blockuser;
      }
      this.events.publish('block-request');
    })
  }
  unblockRequest(buddy) {
    return new Promise((resolve, reject) => {
      this.firereq.child(this.userId).orderByChild('uid').once('value', (snapshot) => {
        let allfriends = snapshot.val();
        for (var key in allfriends) {
          if (allfriends[key].sender == buddy.uid) {
            this.firereq.child(this.userId).child(key).update({ isBlock: false });
            resolve(true);
          }
        }
      })
    })
  }


}
