import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';
import firebase from 'firebase';
import { Storage } from '@ionic/storage';
import { UserProvider } from '../user/user';
@Injectable()
export class ChatProvider {
  firedata = firebase.database().ref('/chatusers');
  firebuddychats = firebase.database().ref('/buddychats');
  firebuddymessagecounter = firebase.database().ref('/buddychats');
  fireuserStatus = firebase.database().ref('/userstatus');
  fireStar = firebase.database().ref('/starredmessage');
  firefriends = firebase.database().ref('/friends');

  buddy: any;
  buddymessages = [];
  msgcount = 0;
  buddyStatus: any;
  userId;
  constructor(public events: Events, public storage: Storage, public userservice: UserProvider) {
    let userID = localStorage.getItem('userUID');

    if (userID != undefined) {
      this.userId = userID;
    } else {
      this.userId = firebase.auth().currentUser.uid;
    }
  }
  buddymessageRead() {
    this.firebuddychats.child(this.buddy.uid).child(this.userId)
      .orderByChild('uid').once('value', (snapshot) => {
        let allmessahes = snapshot.val();
        for (var key in allmessahes) {
          if (allmessahes[key].isRead == false) {
            this.firebuddychats.child(this.buddy.uid).child(this.userId).child(key).update({ isRead: true })
          }
        }
      })
  }
  initializebuddy(buddy) {
    this.buddy = buddy;
    this.buddymessageRead();

  }
  formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
  }
  formatDate(date) {
    var dd = date.getDate();
    var mm = date.getMonth() + 1;
    var yyyy = date.getFullYear();
    if (dd < 10)
      dd = '0' + dd;
    if (mm < 10)
      mm = '0' + mm;
    return dd + '/' + mm + '/' + yyyy;
  }
  addnewmessageBroadcast(msg, type, buddy, selectCatId ) {
    let time = this.formatAMPM(new Date());
    let date = this.formatDate(new Date());
    if (buddy) {
      var promise = new Promise((resolve, reject) => {
        this.firebuddychats.child(this.userId).child(buddy.uid).push({
          sentby: this.userId,
          message: msg,
          type: type,
          timestamp: firebase.database.ServerValue.TIMESTAMP,
          timeofmsg: time,
          dateofmsg: date,
          messageId: '',
          selectCatId: selectCatId,
          multiMessage: false,
          isStarred: false,
          userprio:''
        })
          .then((item) => {
            this.getfirendlist(this.userId).then((res) => {
              let friends = res;
              for (var tmpkey in friends) {
                if (friends[tmpkey].uid == buddy.uid) {
                  this.firefriends.child(this.userId).child(tmpkey).update({ isActive: 1, userprio: new Date() });
                  resolve(true);
                }
              }
            })
            this.firebuddychats.child(this.userId).child(buddy.uid).child(item.key).update({ messageId: item.key , userprio:new Date()})
            this.userservice.getstatusblock(buddy).then((res) => {
              if (res == false) {

                //mytodo : check if buddystatus is online
                //if (buddystatus == "online") {
                if (false) {
                  //this.firebuddychats.child(this.userId).child(buddy.uid).child(item.key).update({ isRead: true });
                } else {
                  this.firebuddychats.child(this.userId).child(buddy.uid).child(item.key).update({ isRead: false });
                }

              }
              else {
                this.firebuddychats.child(this.userId).child(buddy.uid).child(item.key).update({ isRead: false });
              }
            })
            /*
            //mytodo : why following code does not work : it create 4-6 additional chat messages which i dont understand why
            this.userservice.getstatusblock(buddy).then((res) => {
              if (res == false) {
                this.firebuddychats.child(buddy.uid).child(this.userId).push({
                  sentby: this.userId,
                  message: msg,
                  type: type,
                  timestamp: firebase.database.ServerValue.TIMESTAMP,
                  timeofmsg: time,
                  dateofmsg: date,
                  messageId: '',
                  isRead: true,
                  isStarred: false,
                  userprio:''
                }).then((items) => {
                  this.firebuddychats.child(buddy.uid).child(this.userId).child(items.key).update({ messageId: items.key , userprio:new Date()}).then(() => {

                    this.getfirendlist(buddy.uid).then((res) => {
                      let friends = res;
                      for (var tmpkey in friends) {
                        if (friends[tmpkey].uid == this.userId) {
                          this.firefriends.child(buddy.uid).child(tmpkey).update({ isActive: 1, userprio: new Date() });
                          resolve(true);
                        }
                      }
                    })
                  })
                })
              } else {
                resolve(true);
              }
            })
            */
          })
      })
      return promise;
    }
  }
  addnewmessage(msg, type, buddystatus) {

    let time = this.formatAMPM(new Date());
    let date = this.formatDate(new Date());
    if (this.buddy) {
      var promise = new Promise((resolve, reject) => {
        this.firebuddychats.child(this.userId).child(this.buddy.uid).push({
          sentby: this.userId,
          message: msg,
          type: type,
          timestamp: firebase.database.ServerValue.TIMESTAMP,
          timeofmsg: time,
          dateofmsg: date,
          messageId: '',
          multiMessage: false,
          isStarred: false,
          userprio:''
        })
          .then((item) => {
            this.getfirendlist(this.userId).then((res) => {
              let friends = res;
              for (var tmpkey in friends) {
                if (friends[tmpkey].uid == this.buddy.uid) {
                  this.firefriends.child(this.userId).child(tmpkey).update({ isActive: 1, userprio: new Date() });
                  resolve(true);
                }
              }
            })
            this.firebuddychats.child(this.userId).child(this.buddy.uid).child(item.key).update({ messageId: item.key , userprio:new Date()})
            this.userservice.getstatusblock(this.buddy).then((res) => {
              if (res == false) {
                if (buddystatus == "online") {
                  this.firebuddychats.child(this.userId).child(this.buddy.uid).child(item.key).update({ isRead: true });
                } else {
                  this.firebuddychats.child(this.userId).child(this.buddy.uid).child(item.key).update({ isRead: false });
                }

              }
              else {
                this.firebuddychats.child(this.userId).child(this.buddy.uid).child(item.key).update({ isRead: false });
              }
            })
            this.userservice.getstatusblock(this.buddy).then((res) => {
              if (res == false) {
                this.firebuddychats.child(this.buddy.uid).child(this.userId).push({
                  sentby: this.userId,
                  message: msg,
                  type: type,
                  timestamp: firebase.database.ServerValue.TIMESTAMP,
                  timeofmsg: time,
                  dateofmsg: date,
                  messageId: '',
                  isRead: true,
                  isStarred: false,
                  userprio:''
                }).then((items) => {
                  this.firebuddychats.child(this.buddy.uid).child(this.userId).child(items.key).update({ messageId: items.key , userprio:new Date()}).then(() => {

                    this.getfirendlist(this.buddy.uid).then((res) => {
                      let friends = res;
                      for (var tmpkey in friends) {
                        if (friends[tmpkey].uid == this.userId) {
                          this.firefriends.child(this.buddy.uid).child(tmpkey).update({ isActive: 1, userprio: new Date() });
                          resolve(true);
                        }
                      }
                    })
                  })
                })
              } else {
                resolve(true);
              }

            })
          })
      })
      return promise;
    }
  }
  addnewmessagemultiple(msg, type, buddystatus) {
    let time = this.formatAMPM(new Date());
    let date = this.formatDate(new Date());
    if (this.buddy) {
      var promise = new Promise((resolve, reject) => {
        this.firebuddychats.child(this.userId).child(this.buddy.uid).push({
          sentby: this.userId,
          message: msg,
          type: type,
          timestamp: firebase.database.ServerValue.TIMESTAMP,
          timeofmsg: time,
          dateofmsg: date,
          messageId: '',
          isRead: true,
          multiMessage: true,
          isStarred: false,
          userprio:''
        })
          .then((item) => {
            this.getfirendlist(this.userId).then((res) => {
              let friends = res;
              for (var tmpkey in friends) {
                if (friends[tmpkey].uid == this.buddy.uid) {
                  this.firefriends.child(this.userId).child(tmpkey).update({ isActive: 1, userprio: new Date() });
                  resolve(true);
                }
              }
            })
            this.firebuddychats.child(this.userId).child(this.buddy.uid).child(item.key).update({ messageId: item.key , userprio:new Date()})
            if (buddystatus == "online") {
              this.firebuddychats.child(this.userId).child(this.buddy.uid).child(item.key).update({ isRead: true });
            } else {
              this.userservice.getstatusblock(this.buddy).then((res) => {
                if (res == false) {
                  this.firebuddychats.child(this.userId).child(this.buddy.uid).child(item.key).update({ isRead: false });
                } else {
                  this.firebuddychats.child(this.userId).child(this.buddy.uid).child(item.key).update({ isRead: true });
                }
              })
            }
            this.userservice.getstatusblock(this.buddy).then((res) => {
              if (res == false) {
                this.firebuddychats.child(this.buddy.uid).child(this.userId).push({
                  sentby: this.userId,
                  message: msg,
                  type: type,
                  timestamp: firebase.database.ServerValue.TIMESTAMP,
                  timeofmsg: time,
                  dateofmsg: date,
                  messageId: '',
                  multiMessage: true,
                  isStarred: false,
                  userprio:''
                }).then((items) => {
                  this.firebuddychats.child(this.buddy.uid).child(this.userId).child(items.key).update({ messageId: items.key , userprio:new Date()}).then(() => {
                    if (buddystatus == "online") {
                      this.firebuddychats.child(this.buddy.uid).child(this.userId).child(items.key).update({ isRead: true });
                    } else {
                    }
                    this.getfirendlist(this.buddy.uid).then((res) => {
                      let friends = res;
                      for (var tmpkey in friends) {
                        if (friends[tmpkey].uid == this.userId) {
                          this.firefriends.child(this.buddy.uid).child(tmpkey).update({ isActive: 1, userprio: new Date() });
                          resolve(true);
                        }
                      }
                    })
                  })
                })
              } else {
                resolve(true);
              }

            })
          })
      })
      return promise;
    }
  }
  deleteMessages(items) {
    return new Promise((resolve, reject) => {
      for (let i = 0; i < items.length; i++) {
        this.firebuddychats.child(this.userId).child(this.buddy.uid).child(items[i].messageId).remove()
          .then((res) => {
                this.getfirendlist(this.userId).then((res) => {
                  let friends = res;
                  for (var tmpkey in friends) {
                    if (friends[tmpkey].uid == this.buddy.uid) {
                      this.firebuddychats.child(this.userId).child(this.buddy.uid).once('value',(snapshot)=>{
                        var tempdata =this.converanobj(snapshot.val());
                        if(tempdata.length>0){
                          var lastMsg = tempdata[tempdata.length-1];
                          this.firefriends.child(this.userId).child(tmpkey).update({  userprio: lastMsg.userprio });
                          resolve(true);
                        }else{
                          this.firefriends.child(this.userId).child(tmpkey).update({  userprio: '' });
                          resolve(true);
                        }
                      })
                    }
                  }
                })

          }).catch((err) => {
            reject(false);
          })
      }
    })
  }
  deleteUserMessages(allbuddyuser) {
    return new Promise((resolve, reject) => {
      if (allbuddyuser.length > 0) {
        for (let i = 0; i < allbuddyuser.length; i++) {
          this.firebuddychats.child(this.userId).child(allbuddyuser[i].uid).remove()
            .then((res) => {
              this.getfirendlist(this.userId).then((res) => {
                let friends = res;
                for (var tmpkey in friends) {
                  if (friends[tmpkey].uid == allbuddyuser[i].uid) {
                    this.firefriends.child(this.userId).child(tmpkey).update({ isActive: 0 }).then((res) => {
                      resolve(true);
                      this.events.publish('friends');
                    })
                  }
                }
              })
            })
        }
      }
    })
  }
  getbuddymessages() {
    let temp;
    this.firebuddychats.child(this.userId).child(this.buddy.uid).on('value', (snapshot) => {
      this.buddymessages = [];
      temp = snapshot.val();
      for (var tempkey in temp) {
        this.buddymessages.push(temp[tempkey]);
      }
      this.events.publish('newmessage');
    })
  }
  getbuddyStatus() {
    let tmpStatus;

    this.fireuserStatus.child(this.buddy.uid).on('value', (statuss) => {
      tmpStatus = statuss.val();
      if (tmpStatus.status == 1) {
        this.buddyStatus = tmpStatus.data
      } else {
        let date = tmpStatus.timestamp;
        this.buddyStatus = date
      }
      this.events.publish('onlieStatus');
    })
  }
  setstatusUser() {
    let time = this.formatAMPM(new Date());
    let date = this.formatDate(new Date());
    var promise = new Promise((resolve, reject) => {
      this.fireuserStatus.child(this.userId).set({
        status: 1,
        data: 'online',
        timestamp: date + ' at ' + time
      }).then(() => {
        resolve(true);
      }).catch((err) => {
        reject(err);
      })
    })
    return promise;
  }
  setStatusOffline() {
    let time = this.formatAMPM(new Date());
    let date = this.formatDate(new Date());

    var promise = new Promise((resolve, reject) => {
      this.fireuserStatus.child(this.userId).update({
        status: 0,
        data: 'offline',
        timestamp: date + ' at ' + time
      }).then(() => {
        resolve(true);
      }).catch((err) => {
        reject(err);
      })

    })
    return promise;
  }
  getfirendlist(uid) {
    return new Promise((resolve, reject) => {
      this.firefriends.child(uid).on('value', (snapshot) => {
        let friendsAll = snapshot.val();
        resolve(friendsAll);
      })
    })
  }
  converanobj(obj) {
    let tmp = []
    for (var key in obj) {
      tmp.push(obj[key]);
    }
    return tmp;
  }
}
