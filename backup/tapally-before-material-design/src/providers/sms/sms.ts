import { Injectable } from '@angular/core';
import { SMS } from '@ionic-native/sms';
@Injectable()
export class SmsProvider {

  simulateSMS;

  constructor(public sms: SMS) {
      this.simulateSMS = false;
  }

  sendSmsCustomMsg(mobile,msg){
    return new Promise((resolve,reject)=>{
      if(this.simulateSMS){
          //Simulate msg sent
          resolve(true);
      } else {
          //send message in actual
          console.log ("Trying to send sms");
          try{
            this.sms.send(mobile,msg).then((res)=>{
              resolve(true)
            }).catch((err)=>{
              reject(err);
            })
          }catch(e){
          }
      }//endif
    })
  }//end function

  sendSms(mobile){
    let msg = "Install TapAlly App from https://googlestore.com"
    return new Promise((resolve,reject)=>{
      if(this.simulateSMS){
          //Simulate msg sent
          resolve(true);
      } else {
          //send message in actual
          try{
            this.sms.send(mobile,msg).then((res)=>{
              resolve(true)
            }).catch((err)=>{
              reject(err);
            })
          }catch(e){
          }
      }//endif
    })
  }//end function
}



  /*
  let contactsInvitedViaSMS = localStorage.getItem('contactsInvitedViaSMS');
  if(!contactsInvitedViaSMS || contactsInvitedViaSMS== "0" || contactsInvitedViaSMS=0){
    contactsInvitedViaSMS = 1;
  } else {
    contactsInvitedViaSMS = contactsInvitedViaSMS + 1;
  }
  this.storage.set('contactsInvitedViaSMS', contactsInvitedViaSMS);
  */
