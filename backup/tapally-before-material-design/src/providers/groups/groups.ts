import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';
import firebase from 'firebase';
import { UserProvider } from '../user/user';
import { FcmProvider } from '../fcm/fcm';

@Injectable()
export class GroupsProvider {


    firedata = firebase.database().ref('/chatusers');
    firegroup = firebase.database().ref('/groups');
    fireuserStatus = firebase.database().ref('/userstatus');
    mygroups: any = [];
    userId: any;
    currentgroup: any = [];
    currentgroupname: any;
    grouppic;
    flag = 0;
    groupmsgs;
    groupmemeberStatus = [];
    constructor(
        public http: HttpClient,
        public events: Events,
        public userservice: UserProvider,
        public fcm: FcmProvider,
    ) {
        let userID = localStorage.getItem('userUID');
        if (userID != undefined) {
            this.userId = userID;

        } else {
            this.userId = firebase.auth().currentUser.uid;
        }
    }
    formatAMPM(date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    }
    formatDate(date) {
        var dd = date.getDate();
        var mm = date.getMonth() + 1;
        var yyyy = date.getFullYear();
        if (dd < 10)
            dd = '0' + dd;
        if (mm < 10)
            mm = '0' + mm;
        return dd + '/' + mm + '/' + yyyy;
    }
    getmygroups() {
        this.firegroup.child(this.userId).once('value', (snapshot) => {
            this.mygroups = [];
            if (snapshot.val() != null) {
                var temp = snapshot.val();
                for (var key in temp) {
                    var newgroup = {
                        groupName: key,
                        groupimage: temp[key].groupimage,
                        timestamp: temp[key].timestamp,
                        userprio: temp[key].userprio ? temp[key].userprio : null,
                        lastmsg: temp[key].lastmsg,
                        dateofmsg: temp[key].dateofmsg,
                        timeofmsg: temp[key].timeofmsg,


                    }
                    this.mygroups.push(newgroup);
                }
            }
            this.events.publish('allmygroups');
        })

    }

    deletegroup() {
        return new Promise((resolve, reject) => {
            this.firegroup.child(this.userId).child(this.currentgroupname).child('members').once('value', (snapshot) => {
                var tempmembers = snapshot.val();

                for (var key in tempmembers) {
                    this.firegroup.child(tempmembers[key].uid).child(this.currentgroupname).remove();
                }
                this.firegroup.child(this.userId).child(this.currentgroupname).remove().then(() => {
                    resolve(true);
                }).catch((err) => {
                    reject(err);
                })

            })
        })
    }
    addMeaasge(newmessage, loginUser, tempowner, type, groupname) {
        let date = this.formatDate(new Date());
        let timeofmsg = this.formatAMPM(new Date());
        return new Promise((resolve) => {
            this.firegroup.child(tempowner).child(groupname).child('msgboard').push({
                sentby: this.userId,
                displayName: loginUser.displayName,
                photoURL: loginUser.photoURL,
                message: newmessage,
                timestamp: firebase.database.ServerValue.TIMESTAMP,
                multiMessage: false,
                type: type,
                messageId: '',
                timeofmsg: timeofmsg,
                dateofmsg: date,
                userprio: '',
            }).then((res) => {
                this.firegroup.child(tempowner).child(groupname).child('msgboard').child(res.key).update({ messageId: res.key, userprio: new Date() });
                let notifyMessage = 'You have received a message in ' + groupname + ' by ' + loginUser.displayName;
                if (this.groupmemeberStatus[tempowner] != this.userId) {
                    if (this.groupmemeberStatus[tempowner].status == 'offline') {
                        this.userservice.getbuddydetails(tempowner).then((res) => {
                            let buddydata = res;
                            this.fcm.sendNotification(buddydata, notifyMessage, 'Grouppage')
                        })
                    }
                }
                resolve(true);
            })
        })
    }

    addMultiMeaasge(newmessage, loginUser, tempowner, type, cgroupname) {
        let timeofmsg = this.formatAMPM(new Date());
        let date = this.formatDate(new Date());
        return new Promise((resolve) => {
            this.firegroup.child(tempowner).child(cgroupname).child('msgboard').push({
                sentby: this.userId,
                displayName: loginUser.displayName,
                photoURL: loginUser.photoURL,
                message: newmessage,
                messageId: '',
                timestamp: firebase.database.ServerValue.TIMESTAMP,
                multiMessage: true,
                type: type,
                timeofmsg: timeofmsg,
                dateofmsg: date,
                userprio: '',
            }).then((res) => {
                this.firegroup.child(tempowner).child(cgroupname).child('msgboard').child(res.key).update({ messageId: res.key, userprio: new Date() });
                let notifyMessage = 'You have received a message in ' + this.currentgroupname + ' by ' + loginUser.displayName;
                if (this.groupmemeberStatus[tempowner] != this.userId) {
                    if (this.groupmemeberStatus[tempowner].status == 'offline') {
                        this.userservice.getbuddydetails(tempowner).then((res) => {
                            let buddydata = res;
                            this.fcm.sendNotification(buddydata, notifyMessage, 'Grouppage')
                        })
                    }
                }
                resolve(true);
            })
        })
    }
    addgroupmsg(newmessage, loginUser, type, groupname) {
        let time = this.formatAMPM(new Date());
        let date = this.formatDate(new Date());
        let cgroupname = '';
        if (this.currentgroupname == undefined || this.currentgroupname == null) {
            cgroupname = groupname;
        } else {
            cgroupname = this.currentgroupname;
        }
        return new Promise((resolve) => {
            this.firegroup.child(this.userId).child(cgroupname).child('owner').once('value', (snapshots) => {
                let own = this.converanobj(snapshots.val());
                this.firegroup.child(own[0].uid).child(cgroupname).child('members').once('value', (snapshot) => {
                    var tempowners = this.converanobj(snapshot.val());
                    let flag = 0;
                    for (let i = 0; i < tempowners.length; i++) {
                        this.addMeaasge(newmessage, loginUser, tempowners[i].uid, type, cgroupname).then((res) => {
                            let msgtype = type;
                            let msg = newmessage;
                            this.firegroup.child(tempowners[i].uid).child(cgroupname).update({ userprio: new Date() });
                            this.firegroup.child(tempowners[i].uid).child(cgroupname).update({ dateofmsg: date });
                            this.firegroup.child(tempowners[i].uid).child(cgroupname).update({ timeofmsg: time });
                            if (msgtype == 'message') {
                                this.firegroup.child(tempowners[i].uid).child(cgroupname).update({ lastmsg: msg });
                            } else {
                                this.firegroup.child(tempowners[i].uid).child(cgroupname).update({ lastmsg: msgtype });
                            }
                            flag++;
                            if (tempowners.length == flag) {
                                resolve(true);
                            }
                        })
                    }            
                })
            })
        })
    }
    deleteMessages(item) {
        let cgroupname = this.currentgroupname;
        return new Promise((resolve, reject) => {
            for (let i = 0; i < item.length; i++) {
                this.firegroup.child(this.userId).child(cgroupname).child('msgboard').child(item[i].messageId).remove().then((res) => {
                    this.firegroup.child(this.userId).child(cgroupname).child('msgboard').once('value', (snapshot) => {
                        var tempData = this.converanobj(snapshot.val());
                        if (tempData.length > 0) {
                            var lastMsg = tempData[tempData.length - 1]
                            this.firegroup.child(this.userId).child(cgroupname).update({
                                timeofmsg: lastMsg.timeofmsg,
                                timestamp: lastMsg.timestamp,
                                userprio: lastMsg.userprio,
                                dateofmsg: lastMsg.dateofmsg,
                            })
                            if (lastMsg.type == 'message') {
                                this.firegroup.child(this.userId).child(cgroupname).update({ lastmsg: lastMsg.message });
                            }
                            else {
                                this.firegroup.child(this.userId).child(cgroupname).update({ lastmsg: lastMsg.type });
                            }
                        }
                        else {
                            this.firegroup.child(this.userId).child(cgroupname).update({
                                timeofmsg: '',
                                timestamp: '',
                                userprio: '',
                                lastmsg: '',
                                dateofmsg: '',
                            })
                        }

                    }).then((res) => {
                        resolve(true)
                    }, (err) => {
                        reject(err)
                    })
                }, (err) => {
                    reject(err);
                })
            }
        })
    }


    addgroupmsgmultiple(newmessage, loginUser, type, groupname) {
        let time = this.formatAMPM(new Date());
        let date = this.formatDate(new Date());
        let cgroupname = '';
        if (this.currentgroupname == undefined || this.currentgroupname == null) {
            cgroupname = groupname;
        } else {
            cgroupname = this.currentgroupname;
        }
        return new Promise((resolve) => {
            this.firegroup.child(this.userId).child(cgroupname).child('owner').once('value', (snapshots) => {
                let own = this.converanobj(snapshots.val());
                this.firegroup.child(this.userId).child(cgroupname).child('msgboard').once('value', (snapshots) => {
                })
                this.firegroup.child(own[0].uid).child(cgroupname).child('members').once('value', (snapshot) => {

                    var tempowners = this.converanobj(snapshot.val());
                    let flag = 0;
                    for (let i = 0; i < tempowners.length; i++) {
                        this.firegroup.child(tempowners[i].uid).child(cgroupname).update({ userprio: new Date() });
                        this.firegroup.child(tempowners[i].uid).child(cgroupname).update({ dateofmsg: date });
                        this.firegroup.child(tempowners[i].uid).child(cgroupname).update({ timeofmsg: time });
                        if (type == 'message') {
                            this.firegroup.child(tempowners[i].uid).child(cgroupname).update({ lastmsg: newmessage });
                        } else {
                            this.firegroup.child(tempowners[i].uid).child(cgroupname).update({ lastmsg: type });
                        }
                        this.addMultiMeaasge(newmessage, loginUser, tempowners[i].uid, type, cgroupname).then((res) => {
                            if (res) {
                                flag++;
                            }
                            if (tempowners.length == flag) {
                                resolve(true);

                            }
                        })

                    }

                })
            })
        })

    }
    postmsgs(member, msg, cb, loginUser) {
        this.firegroup.child(member.uid).child(this.currentgroupname).child('msgboard').push({
            sentby: this.userId,
            displayName: loginUser.displayName,
            photoURL: loginUser.photoURL,
            message: msg,
            timestamp: firebase.database.ServerValue.TIMESTAMP
        }).then(() => {
            cb();
        })
    }

    converanobj(obj) {
        let tmp = []
        for (var key in obj) {
            tmp.push(obj[key]);
        }
        return tmp;
    }
    getownership(groupname) {
        var promise = new Promise((resolve, reject) => {
            this.firegroup.child(this.userId).child(groupname).once('value', (snapshot) => {
                let temp = this.converanobj(snapshot.val().owner);
                if (temp.length > 0) {
                    let flag = 0
                    for (let i = 0; i < temp.length; i++) {
                        if (temp[i].uid == this.userId) {
                            flag = 1;
                            break;
                        }
                    }
                    if (flag == 1) {
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                }
            }).catch((err) => {
                reject(err);
            })
        })
        return promise;
    }

    getgroupimage() {
        return new Promise((resolve, reject) => {
            this.firegroup.child(this.userId).child(this.currentgroupname).once('value', (snapshot) => {
                if (snapshot.val() != null) {
                    this.grouppic = snapshot.val().groupimage;
                    resolve(true);
                } else {
                    resolve(true);
                }
            })
        })

    }
    getintogroup(groupname) {
        if (groupname != null) {
            this.currentgroupname = groupname;
            this.firegroup.child(this.userId).child(groupname).once('value', (snapshot) => {
                if (snapshot.val() != null) {
                    var temp = snapshot.val().members;
                    this.currentgroup = [];
                    for (var key in temp) {
                        this.currentgroup.push(temp[key]);
                    }
                    this.currentgroupname = groupname;
                    this.events.publish('gotintogroup')
                }
            })
        } else {
            this.currentgroupname = undefined;
        }
    }
    getgroupmember(gpname) {
        return new Promise((resolve, reject) => {
            this.firegroup.child(this.userId).child(gpname).once('value', (snap) => {
                let groupqwner = this.converanobj(snap.val().owner);

                this.firegroup.child(groupqwner[0].uid).child(gpname).once('value', (snapshot) => {
                    if (snapshot.val() != null) {
                        let temp = snapshot.val().members;
                        let currentgroup = []
                        for (var key in temp) {
                            currentgroup.push(temp[key]);
                        }
                        resolve(currentgroup);
                    }
                })
            })
        })
    }
    addgroup(newGroup, members) {
        return new Promise((resolve, reject) => {
            this.userservice.getbuddydetails(this.userId).then((owendetail) => {
                this.firegroup.child(this.userId).child(newGroup.groupName).set({
                    timestamp: firebase.database.ServerValue.TIMESTAMP,
                    groupimage: newGroup.groupPic,
                    msgboard: '',
                    owner: '',


                }).then(() => {
                    this.firegroup.child(this.userId).child(newGroup.groupName).child('owner').push(owendetail);
                    this.tempAddMember(members, newGroup).then((res) => {
                        if (res == true) {
                            this.firegroup.child(this.userId).child(newGroup.groupName).child('members').push(owendetail)
                            resolve(true)
                        }
                    });

                }).catch((err) => {
                    reject(err);
                })
            })
        });
    }

    tempAddMember(members, newGroup) {
        return new Promise((resolve, reject) => {
            this.flag = 0;
            for (let i = 0; i < members.length; i++) {
                this.addmember(members[i], newGroup.groupName).then((res) => {
                    if (res == true) {
                        this.flag++;
                        if (this.flag == members.length) {
                            resolve(true);
                        }
                    }
                })
            }
        });
    }

    tempAddMembers(members, groupName) {
        return new Promise((resolve, reject) => {
            this.flag = 0;
            for (let i = 0; i < members.length; i++) {
                this.tempaddmember(members[i], groupName).then((res) => {
                    if (res == true) {
                        this.flag++;
                        if (this.flag == members.length) {
                            resolve(true);
                        }
                    }
                })
            }
        });
    }
    tempaddmember(newmember, groupName) {
        return new Promise((resolve, reject) => {
            this.firegroup.child(this.userId).child(groupName).child('owner').once('value', (snapshot) => {
                let owners = snapshot.val();
                for (var key in owners) {
                    this.firegroup.child(owners[key].uid).child(groupName).child('members').push(newmember).then(() => {
                        this.getgroupimage().then(() => {
                            this.firegroup.child(newmember.uid).child(groupName).set({
                                timestamp: firebase.database.ServerValue.TIMESTAMP,
                                groupimage: this.grouppic,
                                owner: owners,
                                msgboard: ''
                            }).then(() => {
                                resolve(true)
                            }).catch((err) => {
                                resolve(false)
                            })
                        })
                    })
                }
            })
        })
    }

    addmember(newmember, groupName) {
        return new Promise((resolve, reject) => {
            this.currentgroupname = groupName;
            this.userservice.getbuddydetails(this.userId).then((owendetail) => {

                this.firegroup.child(this.userId).child(groupName).child('members').push(newmember).then(() => {
                    this.getgroupimage().then(() => {
                        this.firegroup.child(newmember.uid).child(groupName).set({
                            timestamp: firebase.database.ServerValue.TIMESTAMP,
                            groupimage: this.grouppic,
                            owner: '',
                            msgboard: ''
                        }).then(() => {
                            this.firegroup.child(newmember.uid).child(groupName).child('owner').push(owendetail);
                            resolve(true)
                        }).catch((err) => {
                            resolve(false)
                        })
                    })
                })
            })
        });
    }

    deletemember(member) {
        this.firegroup.child(this.userId).child(this.currentgroupname).child('owner').once('value', (snapshot) => {
            let owners = snapshot.val();
            let ownersmember = [];
            for (var keys in owners) {
                ownersmember.push(owners[keys]);
            }
            let flag = 0;
            for (let i = 0; i < ownersmember.length; i++) {
                this.firegroup.child(ownersmember[i].uid).child(this.currentgroupname).child('members').once('value', (snapshot) => {
                    let buddy = snapshot.val();
                    for (var kk in buddy) {
                        if (buddy[kk].uid == member.uid) {
                            this.firegroup.child(ownersmember[i].uid).child(this.currentgroupname).child('members').child(kk).remove();
                            flag++;
                            if (flag == ownersmember.length) {
                                this.firegroup.child(member.uid).child(this.currentgroupname).remove().then(() => {
                                    this.getintogroup(this.currentgroupname);
                                    this.getgroupmembers()
                                })
                            }
                        }
                    }
                })
            }
        })

    }
    getgroupmsgs(groupname) {
        this.firegroup.child(this.userId).child(groupname).child('msgboard').on('value', (snapshot) => {
            var tempmsgholder = snapshot.val();
            this.groupmsgs = [];
            for (var key in tempmsgholder)
                this.groupmsgs.push(tempmsgholder[key]);
            this.events.publish('newgroupmsg');
        })
    }
    getgroupmembers() {
        this.firegroup.child(this.userId).child(this.currentgroupname).once('value', (snapshots) => {
            var tempdata = this.converanobj(snapshots.val().owner);
            this.firegroup.child(tempdata[0].uid).child(this.currentgroupname).child('members').once('value', (snapshot) => {
                var tempvar = snapshot.val();
                this.currentgroup = [];
                for (var key in tempvar) {
                    this.currentgroup.push(tempvar[key]);
                }
                this.events.publish('gotmembers');
            })
        })
    }

    getgroupmemebrstatus(member) {
        for (let i = 0; i < member.length; i++) {
            let tmpStatus;
            this.fireuserStatus.child(member[i].uid).on('value', (statuss) => {
                tmpStatus = statuss.val();
                if (tmpStatus.status == 1) {
                    this.groupmemeberStatus[member[i].uid] = {
                        uid: member[i].uid,
                        status: tmpStatus.data,
                        sdate: tmpStatus.timestamp
                    };
                } else {
                    this.groupmemeberStatus[member[i].uid] = {
                        uid: member[i].uid,
                        status: tmpStatus.data,
                        sdate: tmpStatus.timestamp
                    };
                }

            })
        }
        this.events.publish('groupmemberstatus');
    }

    leavegroup() {
        return new Promise((resolve, reject) => {
            this.firegroup.child(this.userId).child(this.currentgroupname).once('value', (snapshot) => {
                var tempowner = this.converanobj(snapshot.val().owner);
                this.firegroup.child(tempowner[0].uid).child(this.currentgroupname).child('members').once('value', (snapshots) => {

                    let cuser = snapshots.val();
                    for (let key in cuser) {
                        if (cuser[key].uid == this.userId) {
                            this.firegroup.child(tempowner[0].uid).child(this.currentgroupname).child('members').child(key).remove();
                        }
                    }
                    this.firegroup.child(this.userId).child(this.currentgroupname).remove().then(() => {
                        resolve(true);
                    }).catch((err) => {
                        reject(err);
                    })

                })
            })
        })
    }
    getgroupInfo(groupname) {
        return new Promise((resolve, reject) => {
            this.firegroup.child(this.userId).child(groupname).once('value', (snapshot) => {
                resolve(snapshot.val());
            })
        })
    }
    updategroupname(oldgpname, newgpname) {
        for (let i = 0; i < this.currentgroup.length; i++) {
            setTimeout(() => {
                let child = this.firegroup.child(this.currentgroup[i].uid).child(oldgpname);
                this.firegroup.child(this.currentgroup[i].uid).child(oldgpname).once('value', (snapshot) => {
                    this.firegroup.child(this.currentgroup[i].uid).child(newgpname).set(snapshot.val());
                    child.remove();
                })
            }, 1000);

        }
    }
    updategroupImage(res, gpname) {
        for (let i = 0; i < this.currentgroup.length; i++) {
            this.firegroup.child(this.currentgroup[i].uid).child(gpname).update({ groupimage: res })
        }
        this.firegroup.child(this.userId).child(gpname).update({ groupimage: res })
    }
    addGroupAdmin(buddy, gpname) {
        for (let i = 0; i < this.currentgroup.length; i++) {
            this.firegroup.child(this.currentgroup[i].uid).child(gpname).child('owner').push(buddy);
        }
        this.firegroup.child(this.userId).child(gpname).child('members').once('value', (snapshot) => {
            this.firegroup.child(buddy.uid).child(gpname).child('members').set(snapshot.val());
        })
    }
    removefromAdmin(member) {
        this.firegroup.child(member.uid).child(this.currentgroupname).child('members').once('value', (snapshots) => {
            let members = snapshots.val();
            let allmember = [];
            for (var keys in members) {
                allmember.push(members[keys]);
            }
            if (allmember.length > 1) {
                let flag = 0;
                for (let i = 0; i < allmember.length; i++) {
                    this.firegroup.child(allmember[i].uid).child(this.currentgroupname).child('owner').once('value', (snapshot) => {
                        const owner = snapshot.val();
                        flag++;
                        let keyofmember;
                        for (let keyss in owner) {
                            if (owner[keyss].uid == member.uid) {
                                keyofmember = keyss;
                            }
                        }
                        this.firegroup.child(allmember[i].uid).child(this.currentgroupname).child('owner').child(keyofmember).remove();
                        if (flag == allmember.length) {
                            this.firegroup.child(member.uid).child(this.currentgroupname).child('members').remove();
                            this.getgroupmembers();
                        }
                    })

                }
            }
        })
    }
    removeAdmin(member) {
        this.deletemember(member);
        this.removefromAdmin(member);
    }
    deletegroups(groupname) {
        return new Promise((resolve, reject) => {
            this.firegroup.child(this.userId).child(groupname).child('members').once('value', (snapshot) => {
                var tempmembers = snapshot.val();

                for (var key in tempmembers) {
                    this.firegroup.child(tempmembers[key].uid).child(groupname).remove();
                }
                this.firegroup.child(this.userId).child(groupname).remove().then(() => {
                    resolve(true);
                }).catch((err) => {
                    reject(err);
                })

            })
        })
    }

    leavegroups(groupname) {
        return new Promise((resolve, reject) => {
            this.firegroup.child(this.userId).child(groupname).once('value', (snapshot) => {
                var tempowner = this.converanobj(snapshot.val().owner);
                this.firegroup.child(tempowner[0].uid).child(groupname).child('members').once('value', (snapshots) => {

                    let cuser = snapshots.val();
                    for (let key in cuser) {
                        if (cuser[key].uid == this.userId) {
                            this.firegroup.child(tempowner[0].uid).child(groupname).child('members').child(key).remove();
                        }
                    }
                    this.firegroup.child(this.userId).child(groupname).remove().then(() => {
                        resolve(true);
                    }).catch((err) => {
                        reject(err);
                    })

                })
            })
        })
    }

    leavegroupsowner(groupname) {
        return new Promise((resolve, reject) => {
            this.firegroup.child(this.userId).child(groupname).once('value', (snapshot) => {
                this.firegroup.child(this.userId).child(groupname).child('members').once('value', (snapshot) => {
                    let members = snapshot.val();
                    let allmember = [];
                    for (var keys in members) {
                        allmember.push(members[keys]);
                    }
                    if (allmember.length > 1) {
                        let flag = 0;
                        for (let i = 0; i < allmember.length; i++) {
                            this.firegroup.child(allmember[i].uid).child(groupname).child('owner').once('value', (snapshot) => {
                                const owner = snapshot.val();
                                flag++;
                                let keyofmember;
                                for (let keyss in owner) {
                                    if (owner[keyss].uid == this.userId) {
                                        keyofmember = keyss;
                                    }
                                }
                                this.firegroup.child(allmember[i].uid).child(groupname).child('owner').child(keyofmember).remove();
                                if (flag == allmember.length) {
                                    this.firegroup.child(this.userId).child(groupname).child('members').remove();

                                }
                            })
                            this.firegroup.child(allmember[i].uid).child(groupname).child('members').once('value', (snapshots) => {

                                let cuser = snapshots.val();
                                for (let key in cuser) {
                                    if (cuser[key].uid == this.userId) {
                                        this.firegroup.child(allmember[i].uid).child(groupname).child('members').child(key).remove();
                                    }


                                }
                            })

                            this.firegroup.child(this.userId).child(groupname).remove().then(() => {
                                resolve(true);
                            }).catch((err) => {
                                reject(err);
                            })

                        }

                    }

                })
            })

        })
    }
}
