webpackJsonp([56],{

/***/ 1025:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DiscPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DiscPage = (function () {
    function DiscPage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.max = 5;
        this.longTxt = 250;
        this.disc = this.navParams.get('disc');
        this.disc = this.disc.replace(new RegExp('<br />', 'g'), "\n");
    }
    DiscPage.prototype.limitText = function (event) {
        var limitCount = (+(this.disc.length));
        if (limitCount > this.longTxt) {
            this.disc = this.disc.slice(0, this.longTxt);
        }
        else {
            limitCount = this.longTxt - limitCount;
        }
    };
    DiscPage.prototype.ionViewDidLoad = function () {
    };
    DiscPage.prototype.save = function () {
        var data = { disc: this.disc };
        this.viewCtrl.dismiss(data);
    };
    DiscPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    DiscPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-disc',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/disc/disc.html"*/'<ion-header>\n\n  <ion-navbar color="dark">\n    <div class="header-wrap">\n      <div class="flex-start">\n          <ion-buttons left>\n              <button ion-button icon-only (click)="dismiss()"><ion-icon ios="md-close" md="md-close" ></ion-icon></button>\n          </ion-buttons>\n      </div>\n      <div class="header-title">Description</div>\n      <div class="flex-end">\n          <ion-buttons right>\n              <button  ion-button icon-only (click)="save()" ><ion-icon ios="md-checkmark" md="md-checkmark" ></ion-icon></button>\n          </ion-buttons>\n      </div>\n    </div>\n    \n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n<ion-card>\n  <ion-card-content>\n    <ion-list>\n      <ion-item>\n\n        <ion-textarea (keydown)="limitText($event)" (keydown.enter)="false" (keyup)=\'limitText($event);\' ng-trim="false" maxlength="250" id="text" placeholder="Enter a description" rows="10" cols="10" [(ngModel)]="disc"></ion-textarea>\n\n\n        \n      </ion-item>\n\n     \n    </ion-list>\n  </ion-card-content>\n</ion-card>\n\n</ion-content>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/disc/disc.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["B" /* ViewController */]])
    ], DiscPage);
    return DiscPage;
}());

//# sourceMappingURL=disc.js.map

/***/ }),

/***/ 954:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DiscPageModule", function() { return DiscPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__disc__ = __webpack_require__(1025);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DiscPageModule = (function () {
    function DiscPageModule() {
    }
    DiscPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__disc__["a" /* DiscPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__disc__["a" /* DiscPage */]),
            ],
        })
    ], DiscPageModule);
    return DiscPageModule;
}());

//# sourceMappingURL=disc.module.js.map

/***/ })

});
//# sourceMappingURL=56.js.map