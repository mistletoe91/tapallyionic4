webpackJsonp([43],{

/***/ 1039:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_groups_groups__ = __webpack_require__(496);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_imagehandler_imagehandler__ = __webpack_require__(499);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_loading_loading__ = __webpack_require__(240);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_app_angularfireconfig__ = __webpack_require__(142);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var GroupsPage = (function () {
    function GroupsPage(navCtrl, navParams, alertCtrl, groupservice, loadingCtrl, imghandler, events, loadingProvider, platform) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.groupservice = groupservice;
        this.loadingCtrl = loadingCtrl;
        this.imghandler = imghandler;
        this.events = events;
        this.loadingProvider = loadingProvider;
        this.platform = platform;
        this.newgroup = {
            groupName: 'GroupName',
            groupPic: __WEBPACK_IMPORTED_MODULE_5__app_app_angularfireconfig__["c" /* env */].userPic
        };
        this.selectedBuddy = [];
        this.selectedBuddy = navParams.get('buddy');
    }
    GroupsPage.prototype.ionViewDidLoad = function () {
    };
    GroupsPage.prototype.chooseimage = function () {
        var _this = this;
        this.imghandler.gpuploadimage(this.newgroup.groupName).then(function (res) {
            _this.loadingProvider.dismissMyLoading();
            if (res) {
                _this.newgroup.groupPic = res;
            }
            else {
                _this.loadingProvider.dismissMyLoading();
            }
        }).catch(function (err) {
            _this.loadingProvider.dismissMyLoading();
        });
    };
    GroupsPage.prototype.creategroup = function () {
        var _this = this;
        if (this.newgroup.groupName == "" || this.newgroup.groupName == "GroupName") {
            var namealert = this.alertCtrl.create({
                buttons: ['okay'],
                message: 'Please enter the Group name.'
            });
            namealert.present();
        }
        else {
            if (this.newgroup.groupName.length < 26) {
                if (this.newgroup.groupName.trim()) {
                    this.groupservice.addgroup(this.newgroup, this.selectedBuddy).then(function (res) {
                        _this.navCtrl.setRoot('ChatsPage');
                    }).catch(function (err) {
                        alert(JSON.stringify(err));
                    });
                }
                else {
                    var namealert = this.alertCtrl.create({
                        buttons: ['okay'],
                        message: 'Please Enter Valid Groupname!'
                    });
                    namealert.present();
                    this.newgroup.groupName = 'GroupName';
                }
            }
            else {
                var namealert = this.alertCtrl.create({
                    buttons: ['okay'],
                    message: 'Group name maximum length is 25 characters'
                });
                namealert.present();
            }
        }
    };
    GroupsPage.prototype.editgroupname = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Group Name',
            inputs: [{
                    name: 'groupname',
                    placeholder: 'Give a new groupname',
                }],
            buttons: [{
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function (data) {
                    }
                }, {
                    text: 'Set',
                    handler: function (data) {
                        if (data.groupname) {
                            _this.newgroup.groupName = data.groupname;
                        }
                        else {
                            _this.newgroup.groupName = 'GroupName';
                        }
                    }
                }
            ]
        });
        alert.present();
    };
    GroupsPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    GroupsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-groups',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/groups/groups.html"*/'<ion-header>\n\n  <ion-navbar >\n    <div class="header-wrap">\n        <!-- <ion-buttons class="left-arrow" left>\n            <button ion-button icon-only (click)="goBack()">\n              <ion-icon ios="md-arrow-round-back" md="md-arrow-round-back"></ion-icon>\n            </button>\n        </ion-buttons> -->\n       <div class="header-title">Add a New Group</div>\n    </div>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n<div class="profile-image" (click)="chooseimage()">\n  <img src="{{newgroup.groupPic}}">\n</div>\n<div>\n  <h2 (click)="editgroupname()">{{newgroup.groupName}}</h2>\n</div>\n<div>\n  Tap on the pic or group name to change it.\n</div>\n<div class="spacer" style="height: 10px;"></div>\n<div>\n  <button ion-button round outline (click)="creategroup()">Create</button>\n</div>\n</ion-content>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/groups/groups.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_groups_groups__["a" /* GroupsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_imagehandler_imagehandler__["a" /* ImagehandlerProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_4__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */]])
    ], GroupsPage);
    return GroupsPage;
}());

//# sourceMappingURL=groups.js.map

/***/ }),

/***/ 968:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupsPageModule", function() { return GroupsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__groups__ = __webpack_require__(1039);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var GroupsPageModule = (function () {
    function GroupsPageModule() {
    }
    GroupsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__groups__["a" /* GroupsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__groups__["a" /* GroupsPage */]),
            ],
        })
    ], GroupsPageModule);
    return GroupsPageModule;
}());

//# sourceMappingURL=groups.module.js.map

/***/ })

});
//# sourceMappingURL=43.js.map