webpackJsonp([8],{

/***/ 1002:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingPageModule", function() { return SettingPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__setting__ = __webpack_require__(1073);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SettingPageModule = (function () {
    function SettingPageModule() {
    }
    SettingPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__setting__["a" /* SettingPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__setting__["a" /* SettingPage */]),
            ],
        })
    ], SettingPageModule);
    return SettingPageModule;
}());

//# sourceMappingURL=setting.module.js.map

/***/ }),

/***/ 1073:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_user_user__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_requests_requests__ = __webpack_require__(495);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_app_angularfireconfig__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_chat_chat__ = __webpack_require__(497);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var SettingPage = (function () {
    function SettingPage(navCtrl, navParams, zone, events, storage, userservice, requestservice, chatservice) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.zone = zone;
        this.events = events;
        this.storage = storage;
        this.userservice = userservice;
        this.requestservice = requestservice;
        this.chatservice = chatservice;
        this.imgurl = __WEBPACK_IMPORTED_MODULE_6__app_app_angularfireconfig__["c" /* env */].userPic;
        this.isDisabled = false;
        this.isNotify = true;
        this.profileData = {
            username: '',
            disc: '',
            imgurl: __WEBPACK_IMPORTED_MODULE_6__app_app_angularfireconfig__["c" /* env */].userPic
        };
        this.blockcounter = 0;
        this.loaduserdetails();
        events.subscribe('block-users-counter', function () {
            _this.blockcounter = _this.userservice.blockUsersCounter;
        });
        this.userservice.initializeItem().then(function (res) {
            if (res == true) {
                _this.isNotify = true;
            }
            else {
                _this.isNotify = false;
            }
        });
    }
    SettingPage.prototype.ionViewDidLoad = function () {
        this.userservice.getAllBlockUsersCounter();
    };
    SettingPage.prototype.goBack = function () {
        this.navCtrl.setRoot('TabsPage');
    };
    SettingPage.prototype.loaduserdetails = function () {
        var _this = this;
        var userId;
        if (__WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser) {
            userId = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid;
        }
        else {
            userId = localStorage.getItem('userUID');
        }
        if (userId != undefined) {
            this.isDisabled = true;
            this.userservice.getuserdetails().then(function (res) {
                if (res) {
                    var desc = res['disc'].replace(new RegExp('\n', 'g'), "<br />");
                    if (res['displayName'] != undefined) {
                        _this.profileData.username = res['displayName'];
                    }
                    if (res['photoURL'] != undefined) {
                        _this.profileData.imgurl = res['photoURL'];
                    }
                    if (desc != undefined) {
                        _this.profileData.disc = desc;
                    }
                }
            }).catch(function (err) {
            });
        }
        else {
            this.profileData = {
                username: '',
                disc: '',
                imgurl: __WEBPACK_IMPORTED_MODULE_6__app_app_angularfireconfig__["c" /* env */].userPic
            };
        }
    };
    SettingPage.prototype.goBlockuser = function () {
        this.navCtrl.push('BlockUserPage');
    };
    SettingPage.prototype.openProfilePage = function () {
        this.navCtrl.push('ProfilepicPage');
    };
    SettingPage.prototype.inviteFriends = function () {
        this.navCtrl.push('InviteFriendPage');
    };
    SettingPage.prototype.notify = function () {
        this.isNotify != this.isNotify;
        this.userservice.notifyUser(this.isNotify);
    };
    SettingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-setting',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/setting/setting.html"*/'\n<ion-header>\n  <ion-navbar>\n    <div class="header-wrap">\n      <!--\n     <ion-buttons class="left-arrow" left>\n        <button ion-button icon-only (click)="goBack()">\n          <ion-icon ios="md-arrow-round-back" md="md-arrow-round-back"></ion-icon>\n        </button>\n      </ion-buttons>\n    -->\n      <div class="header-title">Settings</div>\n   </div>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content  class="bg-grey" padding>\n  <ion-list>\n      <div class="border-bottom" (click)="openProfilePage()">\n          <ion-item no-lines>\n              <ion-avatar item-start >\n                <img src="{{ profileData.imgurl ||assets/imgs/user.png}}">\n              </ion-avatar>\n              <h2>{{profileData.username}}</h2>\n              <p>{{profileData.disc}}</p>\n          </ion-item>\n      </div>\n\n      <ion-item (click)="goBlockuser()">\n        <ion-avatar item-start >\n          <ion-icon name="people"></ion-icon>\n        </ion-avatar>\n        <h2>Blocked Contacts<span *ngIf="blockcounter > 0">:{{blockcounter}}</span></h2>\n      </ion-item>\n\n      <!-- <ion-item>\n        <ion-avatar item-start >\n          <ion-icon name="notifications"></ion-icon>\n        </ion-avatar>\n        <ion-label>Notification</ion-label>\n        <ion-toggle [(ngModel)]="isNotify" (ionChange)="notify()"></ion-toggle>\n      </ion-item>\n\n      <ion-item>\n        <ion-avatar item-start >\n          <ion-icon name="help"></ion-icon>\n        </ion-avatar>\n        <h2>Help</h2>\n      </ion-item>\n\n      <ion-item (click)="inviteFriends()">\n        <ion-avatar item-start >\n          <ion-icon name="add"></ion-icon>\n        </ion-avatar>\n        <h2>Invite Friend</h2>\n      </ion-item> -->\n\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/setting/setting.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_4__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_requests_requests__["a" /* RequestsProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_chat_chat__["a" /* ChatProvider */]])
    ], SettingPage);
    return SettingPage;
}());

//# sourceMappingURL=setting.js.map

/***/ })

});
//# sourceMappingURL=8.js.map