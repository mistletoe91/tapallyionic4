webpackJsonp([6],{

/***/ 1003:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatsPageModule", function() { return StatsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__stats__ = __webpack_require__(1074);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var StatsPageModule = (function () {
    function StatsPageModule() {
    }
    StatsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__stats__["a" /* StatsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__stats__["a" /* StatsPage */]),
            ],
        })
    ], StatsPageModule);
    return StatsPageModule;
}());

//# sourceMappingURL=stats.module.js.map

/***/ }),

/***/ 1074:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StatsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the StatsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var StatsPage = (function () {
    function StatsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    StatsPage.prototype.gotoPage = function () {
        this.navCtrl.setRoot("RequestStatusPage");
    };
    StatsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad StatsPage');
    };
    StatsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-stats',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/stats/stats.html"*/'<ion-header>\n  <ion-navbar color="primary" >\n    <ion-title>\n      Your Earnings & Activity\n    </ion-title>\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name=\'menu\'></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content >\n  <ion-list  >\n    <ion-list-header  no-lines>\n      <ion-item   item-end class="text_color_sec"  style="text-align:right;">Balance : $220</ion-item>\n    </ion-list-header>\n    <ion-list-header color="light" no-lines>\n      Your Requests\n    </ion-list-header>\n    <ion-item (click)="gotoPage()">\n      <h2>Need <span class="text_color_primary strongwords">Website Designer</span></h2>\n      <h3><ion-badge color="secondary">4 Referrals Recieved</ion-badge> </h3>\n\n      <p>Need some website re-do work...</p>\n      <h3 item-end><ion-badge color="primary">Claim Referral Fee</ion-badge> </h3>\n    </ion-item>\n    <ion-item (click)="gotoPage()">\n      <h2>Need <span class="text_color_primary strongwords">Blockchain Expert</span></h2>\n      <h3><ion-badge color="secondary_shade">View Request</ion-badge> </h3>\n\n      <p>Doing some ICO, need to build crypto...</p>\n      <h3 item-end><ion-badge color="primary">$90 earned</ion-badge> </h3>\n    </ion-item>\n    <ion-item (click)="gotoPage()">\n      <h2>Need <span class="text_color_primary strongwords">Teacher</span></h2>\n      <h3><ion-badge color="secondary">1 Referral Recieved</ion-badge> </h3>\n      <h3 item-end><ion-badge color="primary">$10 earned</ion-badge> </h3>\n      <p>Kids in Grade 3, need STEM teacher...</p>\n    </ion-item>\n    <ion-item (click)="gotoPage()">\n      <h2>Need <span class="text_color_primary strongwords">Other</span></h2>\n      <h3><ion-badge color="secondary_shade">View Request</ion-badge> </h3>\n        <h3 item-end><ion-badge color="primary">Claim Referral Fee</ion-badge> </h3>\n      <p>I just need someone to redefine myself...</p>\n    </ion-item>\n    <ion-item (click)="gotoPage()">\n      <h2>Need <span class="text_color_primary strongwords">Plumber</span></h2>\n      <h3><ion-badge color="secondary_shade">View Request</ion-badge> </h3>\n\n        <h3 item-end><ion-badge color="primary">$10 earned</ion-badge> </h3>\n      <p>Need 2 tap changes, showerhead, some...</p>\n    </ion-item>\n    <ion-item class="pull-center" no-lines>\n      <p>Load Older Requests</p>\n    </ion-item>\n  </ion-list>\n\n  <ion-list  >\n    <ion-list-header color="light" no-lines>\n      Other Activity\n    </ion-list-header>\n    <ion-item>\n      <h2><span class="text_color_primary strongwords">New Member Invited</span></h2>\n      <h3 item-end><ion-badge color="secondary">$50 earned</ion-badge> </h3>\n      <p>7 x 21 point each</p>\n    </ion-item>\n    <ion-item>\n      <h2><span class="text_color_primary strongwords">New Businesses Invited</span></h2>\n      <h3 item-end><ion-badge color="secondary">$0 earned</ion-badge> </h3>\n      <p>0 x 5 point each</p>\n    </ion-item>\n    <ion-item>\n      <h2><span class="text_color_primary strongwords">Referral Send</span></h2>\n      <h3 item-end><ion-badge color="secondary">$60 earned</ion-badge> </h3>\n      <p>0 x 5 point each</p>\n    </ion-item>\n  </ion-list>\n\n  <ion-list  >\n    <ion-list-header color="light" no-lines>\n      Amount Paid\n      <br><small>(Automatically paid for year end)</small>\n    </ion-list-header>\n\n    <ion-item>\n      <h2><span class="text_color_primary strongwords">Paid for year 2018 </span></h2>\n      <h3 item-end><ion-badge color="secondary">Not Paid Yet</ion-badge> </h3>\n      <p>Auto Redeemable on 2019 Jan 01</p>\n    </ion-item>\n    <ion-item>\n      <h2><span class="text_color_primary strongwords">Paid for year 2017 </span></h2>\n      <h3 item-end><ion-badge color="secondary">$328</ion-badge> </h3>\n      <p>Auto Redeemable on 2018 Jan 01</p>\n    </ion-item>\n    <ion-item>\n      <h2><span class="text_color_primary strongwords">Paid for year 2016</span></h2>\n      <h3 item-end><ion-badge color="secondary">$0</ion-badge> </h3>\n      <p>Auto Redeemable on 2017 Jan 01</p>\n    </ion-item>\n  </ion-list>\n\n  <ion-list  >\n    <ion-list-header color="light" no-lines>\n      Your Subscriptions\n    </ion-list-header>\n    <ion-item>\n      <h2><span class="text_color_primary strongwords">Business Plan </span></h2>\n      <h3 item-end><ion-badge color="dark">$30/mo</ion-badge> </h3>\n      <p>View/Update Payments On Website </p>\n    </ion-item>\n  </ion-list>\n\n</ion-content>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/stats/stats.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */]])
    ], StatsPage);
    return StatsPage;
}());

//# sourceMappingURL=stats.js.map

/***/ })

});
//# sourceMappingURL=6.js.map