webpackJsonp([52],{

/***/ 1029:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FriendAskingReferralPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_cats_cats__ = __webpack_require__(502);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FriendAskingReferralPage = (function () {
    function FriendAskingReferralPage(navCtrl, navParams, catservice) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.catservice = catservice;
        this.cats = this.catservice.getCats();
        this.friend_asking_referral_uid = navParams.get('friend_asking_referral_uid');
        this.friend_asking_referral_displayName = navParams.get('friend_asking_referral_displayName');
        for (var i = 0; i < this.cats.length; i++) {
            if (this.cats[i].id == navParams.get('catid')) {
                this.friend_asking_referral_catId = this.cats[i].id;
                this.friend_asking_referral_catName = this.cats[i].name;
                break;
            }
        }
    }
    FriendAskingReferralPage.prototype.sendPickContactTo = function () {
        this.navCtrl.setRoot("PickContactFromPage", { friend_asking_referral_uid: this.friend_asking_referral_uid, friend_asking_referral_displayName: this.friend_asking_referral_displayName, friend_asking_referral_catId: this.friend_asking_referral_catId, friend_asking_referral_catName: this.friend_asking_referral_catName });
    };
    FriendAskingReferralPage.prototype.ionViewDidLoad = function () {
    };
    FriendAskingReferralPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-friend-asking-referral',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/friend-asking-referral/friend-asking-referral.html"*/'<ion-header>\n  <ion-navbar color="primary" >\n    <ion-title>\n      Referral Request\n    </ion-title>\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name=\'menu\'></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n\n  <div class="pull-center givepadding">\n      <h3>{{friend_asking_referral_displayName}} is asking you a referral for {{friend_asking_referral_catName}}</h3>\n      <p>Do you know any contact which can help him for {{friend_asking_referral_catName}} related work?\n      </p>\n<br />\n        <button ion-button  (click)="sendPickContactTo()" color="primary" block>Send Referral</button>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/friend-asking-referral/friend-asking-referral.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_cats_cats__["a" /* CatProvider */]])
    ], FriendAskingReferralPage);
    return FriendAskingReferralPage;
}());

//# sourceMappingURL=friend-asking-referral.js.map

/***/ }),

/***/ 958:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FriendAskingReferralPageModule", function() { return FriendAskingReferralPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__friend_asking_referral__ = __webpack_require__(1029);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FriendAskingReferralPageModule = (function () {
    function FriendAskingReferralPageModule() {
    }
    FriendAskingReferralPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__friend_asking_referral__["a" /* FriendAskingReferralPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__friend_asking_referral__["a" /* FriendAskingReferralPage */]),
            ],
        })
    ], FriendAskingReferralPageModule);
    return FriendAskingReferralPageModule;
}());

//# sourceMappingURL=friend-asking-referral.module.js.map

/***/ })

});
//# sourceMappingURL=52.js.map