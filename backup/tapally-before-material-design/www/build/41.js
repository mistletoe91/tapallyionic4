webpackJsonp([41],{

/***/ 1040:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_requests_requests__ = __webpack_require__(495);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HomePage = (function () {
    function HomePage(navCtrl, events, navParams, requestservice) {
        this.navCtrl = navCtrl;
        this.events = events;
        this.navParams = navParams;
        this.requestservice = requestservice;
        this.myrequests = [];
        this.requestcounter = null;
        this.total_connections = 0;
        this.total_referral_send = 0;
        this.total_referral_recieved = 0;
        this.total_earned = 0;
    }
    HomePage.prototype.presentPopover = function (myEvent) {
        this.navCtrl.push("NotificationPage");
        this.requestcounter = this.myrequests.length;
    };
    HomePage.prototype.ionViewDidLoad = function () {
    };
    HomePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.requestservice.getmyrequests();
        this.events.subscribe('gotrequests', function () {
            _this.myrequests = [];
            _this.myrequests = _this.requestservice.userdetails;
            if (_this.myrequests) {
                _this.requestcounter = _this.myrequests.length;
            }
        });
    };
    HomePage.prototype.fnGoToSearchNextPage = function () {
        this.navCtrl.setRoot('ContactPage');
    };
    HomePage.prototype.addnewContact = function () {
        this.navCtrl.setRoot('ContactPage');
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar >\n    <ion-title>\n      TapAlly\n    </ion-title>\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name=\'menu\'></ion-icon>\n      </button>\n    </ion-buttons>\n    <div class="flex-end-notif">\n      <ion-buttons end>\n          <button ion-button icon-only (click)="presentPopover($event)">\n            <ion-icon name="notifications" class="notification"></ion-icon>\n            <ion-badge class="notification" color="danger" *ngIf="requestcounter != null">{{requestcounter}}</ion-badge>\n          </button>\n      </ion-buttons>\n    </div>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n  <h3>{{total_connections}} Connections</h3>\n  <h3>{{total_referral_send}} Referral Send</h3>\n  <h3>{{total_referral_recieved}} Referral Recieved</h3>\n  <h3>{{total_earned}} Points Earned</h3>\n\n  <ion-fab right bottom >\n    <button (click)="addnewContact()" ion-fab color="danger"><ion-icon name="contacts"></ion-icon></button>\n  </ion-fab>\n\n\n\n</ion-content>\n<ion-footer>\n  <div style="border:1px solid #ddd;">\n    <ion-item style="height:50px;background-color:#dedede;color:#999;">\n      Sponsered Business\n    </ion-item>\n    <ion-item (click)="fnGoToSearchNextPage()" style="text-align:left;">\n      <h2>\n        <span class="text_color_primary strongwords">John Smith</span>\n        <span    style="text-align:right;width:100%;color:#32db64;font-size:1.4rem;"></span>\n      </h2>\n      <p>ABC Corporation  <span class="hideme" style="color:#32db64"></span></p>\n       <ion-icon end item-end name=\'arrow-dropright-circle\'> </ion-icon>\n    </ion-item>\n  </div>\n</ion-footer>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/home/home.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_requests_requests__["a" /* RequestsProvider */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 969:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home__ = __webpack_require__(1040);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var HomePageModule = (function () {
    function HomePageModule() {
    }
    HomePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */]),
            ],
        })
    ], HomePageModule);
    return HomePageModule;
}());

//# sourceMappingURL=home.module.js.map

/***/ })

});
//# sourceMappingURL=41.js.map