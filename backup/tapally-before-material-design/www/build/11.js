webpackJsonp([11],{

/***/ 1000:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SendReferralFilterPageModule", function() { return SendReferralFilterPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__send_referral_filter__ = __webpack_require__(1071);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SendReferralFilterPageModule = (function () {
    function SendReferralFilterPageModule() {
    }
    SendReferralFilterPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__send_referral_filter__["a" /* SendReferralFilterPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__send_referral_filter__["a" /* SendReferralFilterPage */]),
            ],
        })
    ], SendReferralFilterPageModule);
    return SendReferralFilterPageModule;
}());

//# sourceMappingURL=send-referral-filter.module.js.map

/***/ }),

/***/ 1071:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SendReferralFilterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the SendReferralFilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SendReferralFilterPage = (function () {
    function SendReferralFilterPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    SendReferralFilterPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SendReferralFilterPage');
    };
    SendReferralFilterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-send-referral-filter',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/send-referral-filter/send-referral-filter.html"*/'<ion-header>\n  <ion-navbar color="primary" >\n    <ion-title>\n      Set Filter\n    </ion-title>\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name=\'menu\'></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <h3 padding>Filters For Results</h3>\n  <ion-list  >\n    <ion-item>\n      <ion-label> Only Display Known Contacts </ion-label>\n      <ion-toggle  ></ion-toggle>\n    </ion-item>\n\n\n    <ion-item>\n      <ion-label>Show Results Within</ion-label>\n      <ion-select  >\n        <ion-option value="1"  checked="true" selected>  25Km</ion-option>\n        <ion-option value="2"  >  50km</ion-option>\n        <ion-option value="3" >  100Km</ion-option>\n        <ion-option value="4">  Province/State</ion-option>\n        <ion-option value="5">  Country</ion-option>\n      </ion-select>\n    </ion-item>\n\n\n\n  </ion-list>\n\n\n</ion-content>\n\n<ion-footer >\n  <ion-toolbar>\n\n    <div  >\n      <button ion-button color="primary" block>Done</button>\n    </div>\n\n  </ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/send-referral-filter/send-referral-filter.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */]])
    ], SendReferralFilterPage);
    return SendReferralFilterPage;
}());

//# sourceMappingURL=send-referral-filter.js.map

/***/ })

});
//# sourceMappingURL=11.js.map