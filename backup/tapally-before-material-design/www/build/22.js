webpackJsonp([22],{

/***/ 1059:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReferralPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_cats_cats__ = __webpack_require__(502);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_requests_requests__ = __webpack_require__(495);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ReferralPage = (function () {
    function ReferralPage(platform, navCtrl, navParams, catservice, events, requestservice, viewCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.catservice = catservice;
        this.events = events;
        this.requestservice = requestservice;
        this.viewCtrl = viewCtrl;
        this.myrequests = [];
        this.requestcounter = null;
        this.isVirgin = false;
        platform.ready().then(function () {
            _this.initializeItems();
        });
    }
    ReferralPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ReferralPage.prototype.presentPopover = function (myEvent) {
        this.navCtrl.push("NotificationPage");
        this.requestcounter = this.myrequests.length;
    };
    ReferralPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.requestservice.getmyrequests();
        this.events.subscribe('gotrequests', function () {
            _this.myrequests = [];
            _this.myrequests = _this.requestservice.userdetails;
            if (_this.myrequests) {
                _this.requestcounter = _this.myrequests.length;
            }
        });
    };
    ReferralPage.prototype.sendreferraldirectly = function () {
        this.navCtrl.setRoot("SearchNextPage", { src: 1 });
    };
    ReferralPage.prototype.getItems = function (ev) {
        // Reset items back to all of the items
        this.initializeItems();
        // set val to the value of the ev target
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.items = this.items.filter(function (item) {
                return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    };
    ReferralPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ReferralPage');
    };
    //not used
    ReferralPage.prototype.fnGoToSearchNextPage = function () {
        this.navCtrl.setRoot("SearchNextPage", { src: 1 });
    };
    ReferralPage.prototype.fnGoToHomeAfterINeedTodayPage = function (item_id) {
        this.navCtrl.setRoot("HomeAfterINeedTodayPage", { cat_id: item_id });
    };
    ReferralPage.prototype.initializeItems = function () {
        this.items = this.catservice.getCats();
    };
    ReferralPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-referral',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/referral/referral.html"*/'<ion-header>\n  <ion-navbar >\n    <ion-title>\n      Exchange Referral\n    </ion-title>\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name=\'menu\'></ion-icon>\n      </button>\n    </ion-buttons>\n    <div class="flex-end-notif">\n      <ion-buttons end>\n          <button ion-button icon-only (click)="presentPopover($event)">\n            <ion-icon name="notifications" class="notification"></ion-icon>\n            <ion-badge class="notification" color="danger" *ngIf="requestcounter != null">{{requestcounter}}</ion-badge>\n          </button>\n      </ion-buttons>\n    </div>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n\n<div>\n      <div class="pull-center" style="margin:20px 0px;">\n        <h4 class="text_color_primary" >Get Trusted Business Referrals</h4>\n\n        <div style="height:408px;overflow:hidden;border:1px solid #ddd;">\n          <ion-searchbar (ionInput)="getItems($event)" placeholder="Search Services"></ion-searchbar>\n          <ion-list>\n            <ion-item  *ngFor="let item of items">\n                 <p (click)="fnGoToHomeAfterINeedTodayPage()">{{item.name}}</p>\n                 <ion-icon end item-end (click)="fnGoToHomeAfterINeedTodayPage(item.id)" name=\'arrow-dropright-circle\'> </ion-icon>\n            </ion-item>\n          </ion-list>\n        </div>\n        </div>\n</div>\n\n        <!--\n        <button clear (click)="sendreferraldirectly()" ion-button color="primary" block>or send Referral Directly To Contact</button>\n      -->\n\n</ion-content>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/referral/referral.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_cats_cats__["a" /* CatProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_3__providers_requests_requests__["a" /* RequestsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["B" /* ViewController */]])
    ], ReferralPage);
    return ReferralPage;
}());

//# sourceMappingURL=referral.js.map

/***/ }),

/***/ 988:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReferralPageModule", function() { return ReferralPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__referral__ = __webpack_require__(1059);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ReferralPageModule = (function () {
    function ReferralPageModule() {
    }
    ReferralPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__referral__["a" /* ReferralPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__referral__["a" /* ReferralPage */]),
            ],
        })
    ], ReferralPageModule);
    return ReferralPageModule;
}());

//# sourceMappingURL=referral.module.js.map

/***/ })

});
//# sourceMappingURL=22.js.map