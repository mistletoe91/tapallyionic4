webpackJsonp([57],{

/***/ 775:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactimportprogressPageModule", function() { return ContactimportprogressPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contactimportprogress__ = __webpack_require__(849);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ContactimportprogressPageModule = (function () {
    function ContactimportprogressPageModule() {
    }
    ContactimportprogressPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__contactimportprogress__["a" /* ContactimportprogressPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__contactimportprogress__["a" /* ContactimportprogressPage */]),
            ],
        })
    ], ContactimportprogressPageModule);
    return ContactimportprogressPageModule;
}());

//# sourceMappingURL=contactimportprogress.module.js.map

/***/ }),

/***/ 849:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactimportprogressPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_contact_contact__ = __webpack_require__(402);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_tpstorage_tpstorage__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ContactimportprogressPage = (function () {
    function ContactimportprogressPage(navCtrl, navParams, contactProvider, tpStorageService, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.contactProvider = contactProvider;
        this.tpStorageService = tpStorageService;
        this.events = events;
        this.tpStorageService.getItem('userUID').then(function (res) {
            if (res) {
                _this.userId = res;
            }
        }).catch(function (e) { });
    }
    ContactimportprogressPage.prototype.ionViewDidLoad = function () {
        if (this.navParams.get('source') == "ContactPage") {
            //first run
            this.refersh('ContactPage');
        }
        else {
            if (this.navParams.get('source') == "PickContactFromPage") {
                this.refersh('TabsPage'); //mytodo : this should go to PickContactFromPage. but make sure u do .pop instead of setroot . but u have to do carefully. test it throughoutly
            }
            else {
                if (this.navParams.get('source') == "PickContactToPage") {
                    this.refersh('TabsPage'); //mytodo : this should go to PickContactFromPage. but make sure u do .pop instead of setroot . but u have to do carefully. test it throughoutly 
                }
                else {
                    this.ionViewDidLoad_();
                }
            }
        }
    };
    ContactimportprogressPage.prototype.refersh = function (nextPage) {
        var _this = this;
        //first run
        this.contactProvider.getSimJsonContacts()
            .then(function (res) {
            _this.tpStorageService.setItem('contactsImportedFromDevice', '1');
            _this.events.subscribe('contactInsertingDone', function () {
                _this.events.unsubscribe('contactInsertingDone');
                if (!_this.contactProvider.contactInsertingDone) {
                }
                else {
                    _this.navCtrl.push(nextPage, { source: "ImportpPage" });
                }
            });
        }).catch(function (e) { });
    };
    ContactimportprogressPage.prototype.ionViewDidLoad_ = function () {
        var _this = this;
        this.tpStorageService.getItem('contactsImportedFromDevice').then(function (res) {
            //contacts already imported
            if (res || res == "1") {
                _this.sendToContactsPage();
            }
        }).catch(function (e) {
            _this.refersh("ContactPage");
        });
    }; //end function
    ContactimportprogressPage.prototype.sendToContactsPage = function () {
        //this.navCtrl.push('InvitemanuallyPage');
        this.navCtrl.push("ContactPage", { source: "ImportpPage" });
    };
    ContactimportprogressPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-contactimportprogress',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/contactimportprogress/contactimportprogress.html"*/'<ion-header  >\n  <ion-navbar  hideBackButton>\n    <ion-title>Importing Contacts</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n\n  <ion-card  class="maincard_bubble " text-center padding  >\n    <ion-card-content >\n      <ion-card-title>\n        Checking Your Contacts\n        </ion-card-title>\n      <p>\n        Importing {{contactProvider.contactInsertCounter}} of {{contactProvider.contactTotalCounter}}\n      </p>\n    </ion-card-content>\n </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/contactimportprogress/contactimportprogress.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_contact_contact__["a" /* ContactProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_tpstorage_tpstorage__["a" /* TpstorageProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */]])
    ], ContactimportprogressPage);
    return ContactimportprogressPage;
}());

//# sourceMappingURL=contactimportprogress.js.map

/***/ })

});
//# sourceMappingURL=57.js.map