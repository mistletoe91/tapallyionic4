webpackJsonp([62],{

/***/ 770:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BusinessinfootherPageModule", function() { return BusinessinfootherPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__businessinfoother__ = __webpack_require__(844);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var BusinessinfootherPageModule = (function () {
    function BusinessinfootherPageModule() {
    }
    BusinessinfootherPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__businessinfoother__["a" /* BusinessinfootherPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__businessinfoother__["a" /* BusinessinfootherPage */]),
            ],
        })
    ], BusinessinfootherPageModule);
    return BusinessinfootherPageModule;
}());

//# sourceMappingURL=businessinfoother.module.js.map

/***/ }),

/***/ 844:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BusinessinfootherPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BusinessinfootherPage = (function () {
    function BusinessinfootherPage(navCtrl, navParams, app) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.app = app;
    }
    BusinessinfootherPage.prototype.backButtonClick = function () {
        this.navCtrl.pop({ animate: true, direction: "forward" });
    };
    BusinessinfootherPage.prototype.addbuddy = function () {
        this.app.getRootNav().push('ContactPage', { source: "ImportpPage" });
    };
    BusinessinfootherPage.prototype.sendToBusinessReg = function () {
        this.app.getRootNav().push("RegisterbusinessPage", { treatNewInstall: 0 });
    };
    BusinessinfootherPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-businessinfoother',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/businessinfoother/businessinfoother.html"*/'<ion-header >\n  <ion-navbar hideBackButton>\n\n    <ion-buttons left>\n        <button ion-button (click)="backButtonClick()">\n             <ion-icon class="backbtncss" name="arrow-dropleft-circle"></ion-icon>\n        </button>\n    </ion-buttons>\n\n    <ion-title>Grow Your Business</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n  <ion-card  class="maincard_bubble" text-center padding>\n    <ion-card-content >\n      <ion-card-title>\n        Grow Your Business Revenue\n        </ion-card-title>\n      <p>\n        Turn your customers into your sales agent. Everytime you send invite to your customer, they will see your ad on front page of the app. It will make them easy to send referrals to you and grow your branding\n      </p>\n\n      <button   style="margin-top:15px;" ion-button (click)="sendToBusinessReg()" color="primary" block>Let\'s Start Inviting </button>\n      <p></p>\n    </ion-card-content>\n </ion-card>\n\n\n <ion-card   text-center padding >\n   <ion-card-content >\n     <ion-card-title>\n       Your Permanent Ad\n       </ion-card-title>\n     <p>\n       Customer get your ad permanently embedded in their phone, makes it easy to send referral to you.\n     </p>\n   </ion-card-content>\n</ion-card>\n\n<ion-card   text-center padding >\n  <ion-card-content >\n    <ion-card-title>\n      Offer Them Incentives\n      </ion-card-title>\n    <p>\n      Everytime your customer refer their friends and family to you, you can offer them incentives like cash, gift card etc.\nCustomer will view the incentives and can send you message to redeem the incentives.\n    </p>\n  </ion-card-content>\n</ion-card>\n\n\n<ion-card   text-center padding >\n  <ion-card-content >\n    <ion-card-title>\n      Benefits Never Ends\n      </ion-card-title>\n    <p>\n      Send Push Notifications To Your Customers Instead of Email/Text. Run broadcasts, promotions and marketing campaigns directly to your customer’s mobile phone. Email and text messages have low engagement and high bounce rate. Achieve much better results than email and text marketing campaigns with zero or almost no chance of going in the spam. Watch your business sales and revenue growth.\n    </p>\n  </ion-card-content>\n</ion-card>\n\n\n<button   style="margin-top:15px;" ion-button (click)="sendToBusinessReg()" color="primary" block>Start Today</button>\n\n</ion-content>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/businessinfoother/businessinfoother.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* App */]])
    ], BusinessinfootherPage);
    return BusinessinfootherPage;
}());

//# sourceMappingURL=businessinfoother.js.map

/***/ })

});
//# sourceMappingURL=62.js.map