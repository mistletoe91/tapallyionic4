webpackJsonp([44],{

/***/ 788:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupinfoPageModule", function() { return GroupinfoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__groupinfo__ = __webpack_require__(862);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var GroupinfoPageModule = (function () {
    function GroupinfoPageModule() {
    }
    GroupinfoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__groupinfo__["a" /* GroupinfoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__groupinfo__["a" /* GroupinfoPage */]),
            ],
        })
    ], GroupinfoPageModule);
    return GroupinfoPageModule;
}());

//# sourceMappingURL=groupinfo.module.js.map

/***/ }),

/***/ 862:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupinfoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_groups_groups__ = __webpack_require__(400);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_user_user__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_imagehandler_imagehandler__ = __webpack_require__(403);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_loading_loading__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_app_angularfireconfig__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_tpstorage_tpstorage__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var GroupinfoPage = (function () {
    function GroupinfoPage(navCtrl, navParams, groupservice, events, userserivce, alertCtrl, zone, actionSheet, imghandler, loadingProvider, tpStorageService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.groupservice = groupservice;
        this.events = events;
        this.userserivce = userserivce;
        this.alertCtrl = alertCtrl;
        this.zone = zone;
        this.actionSheet = actionSheet;
        this.imghandler = imghandler;
        this.loadingProvider = loadingProvider;
        this.tpStorageService = tpStorageService;
        this.owner = false;
        this.groupmembers = [];
        this.userpic = __WEBPACK_IMPORTED_MODULE_6__app_app_angularfireconfig__["c" /* env */].userPic;
        this.currentgroup = [];
        this.ownergroup = [];
        this.allMembers = [];
        this.tpStorageService.getItem('userUID').then(function (userID) {
            if (userID != undefined) {
                _this.userId = userID;
            }
            _this.doInitialize();
        }).catch(function (e) { });
    }
    GroupinfoPage.prototype.doInitialize = function () {
        var _this = this;
        if (this.groupservice.currentgroupname != undefined) {
            this.groupName = this.groupservice.currentgroupname;
        }
        else {
            this.groupName = this.navParams.get('groupName');
            this.groupservice.currentgroupname = this.groupName;
        }
        this.events.subscribe('gotmembers', function () {
            _this.events.unsubscribe('gotmembers');
            _this.zone.run(function () {
                _this.groupmembers = _this.groupservice.currentgroup;
                _this.getGroupOwner();
            });
        });
        this.groupservice.getgroupimage();
        this.groupservice.getownership(this.groupName).then(function (res) {
            if (res) {
                _this.owner = true;
                _this.groupmembers = _this.groupservice.currentgroup;
                _this.getGroupOwner();
            }
            else {
                _this.groupservice.getgroupmembers();
            }
        });
        this.groupImage = this.groupservice.grouppic;
        if (this.groupName != undefined) {
            this.groupservice.getintogroup(this.groupName);
        }
    };
    GroupinfoPage.prototype.ionViewCanEnter = function () {
        var _this = this;
        this.zone.run(function () {
            _this.groupservice.getgroupInfo(_this.groupName).then(function (res) {
                _this.groupImage = res.groupimage;
            });
        });
    };
    GroupinfoPage.prototype.backButtonClick = function () {
        this.navCtrl.setRoot('TabsPage');
        //this.navCtrl.pop({animate: true, direction: "forward"});
    };
    GroupinfoPage.prototype.getGroupOwner = function () {
        var _this = this;
        if (this.groupservice.currentgroupname != undefined) {
            this.groupName = this.groupservice.currentgroupname;
        }
        else {
            this.groupName = this.navParams.get('groupName');
            this.groupservice.currentgroupname = this.groupName;
        }
        this.groupservice.getgroupInfo(this.groupName).then(function (ress) {
            var owners = _this.groupservice.converanobj(ress.owner);
            _this.ownergroup = owners;
            var currentuser = _this.userId;
            if (_this.groupmembers.length > 0 && _this.ownergroup.length > 0) {
                var Allmember_1 = [];
                var _loop_1 = function (i) {
                    if (_this.groupmembers[i].uid != currentuser) {
                        _this.userserivce.getuserblock(_this.groupmembers[i]).then(function (res) {
                            _this.groupmembers[i].blockby = res;
                        });
                    }
                    else {
                        _this.groupmembers[i].blockby = false;
                    }
                    var flag = 0;
                    for (var q = 0; q < _this.ownergroup.length; q++) {
                        if (_this.groupmembers[i].uid == _this.ownergroup[q].uid) {
                            _this.groupmembers[i].admin = true;
                            Allmember_1.push(_this.groupmembers[i]);
                            flag = 1;
                            break;
                        }
                        else {
                            flag = 0;
                        }
                    }
                    if (flag == 0) {
                        _this.groupmembers[i].admin = false;
                        Allmember_1.push(_this.groupmembers[i]);
                    }
                };
                for (var i = 0; i < _this.groupmembers.length; i++) {
                    _loop_1(i);
                }
                _this.zone.run(function () {
                    _this.allMembers = [];
                    _this.allMembers = Allmember_1;
                });
            }
            _this.currentgroup = ress;
        });
    };
    GroupinfoPage.prototype.editgroupname = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Edit Group Name',
            inputs: [{
                    name: 'groupname',
                    placeholder: 'Give a new groupname',
                    value: this.groupName
                }],
            buttons: [{
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function (data) {
                    }
                }, {
                    text: 'Set',
                    handler: function (data) {
                        if (data.groupname) {
                            if (data.groupname == "") {
                                var namealert = _this.alertCtrl.create({
                                    buttons: ['okay'],
                                    message: 'Please enter the groupname proper!'
                                });
                                namealert.present();
                            }
                            else {
                                if (data.groupname.length < 26) {
                                    if (data.groupname.trim()) {
                                        _this.groupservice.updategroupname(_this.groupName, data.groupname);
                                        _this.groupservice.getintogroup(data.groupname);
                                        _this.groupName = data.groupname;
                                    }
                                    else {
                                        var namealert = _this.alertCtrl.create({
                                            buttons: ['okay'],
                                            message: 'Please Enter Valid Groupname!'
                                        });
                                        namealert.present();
                                    }
                                }
                                else {
                                    var namealert = _this.alertCtrl.create({
                                        buttons: ['okay'],
                                        message: 'Group name maximum length is 25 characters'
                                    });
                                    namealert.present();
                                }
                            }
                        }
                    }
                }]
        });
        alert.present();
    };
    GroupinfoPage.prototype.addMemner = function () {
        this.navCtrl.push('GroupbuddiesaddPage');
    };
    GroupinfoPage.prototype.openpopover = function (buddy) {
        var _this = this;
        if (this.owner) {
            var sheet = this.actionSheet.create({
                title: 'Modification',
                buttons: [
                    {
                        text: 'Remove ' + buddy.displayName,
                        icon: 'trash',
                        handler: function () {
                            _this.removemember(buddy);
                        }
                    }, {
                        text: 'Make group admin',
                        icon: 'person-add',
                        handler: function () {
                            _this.groupservice.addGroupAdmin(buddy, _this.groupName);
                            _this.groupservice.getgroupmembers();
                        }
                    }, {
                        text: 'Cancel',
                        role: 'cancel',
                        icon: 'cancel',
                        handler: function () {
                        }
                    }
                ]
            });
            sheet.present();
        }
    };
    GroupinfoPage.prototype.removemember = function (member) {
        var _this = this;
        var index = this.allMembers.findIndex(function (o) {
            return o.uid === member.uid;
        });
        if (index !== -1) {
            this.allMembers.splice(index, 1);
        }
        if (this.allMembers.length == 0) {
            this.groupservice.deletegroup().then(function () {
                _this.navCtrl.setRoot('TabsPage');
            }).catch(function (err) {
            });
        }
        else {
            this.groupservice.deletemember(member);
        }
    };
    GroupinfoPage.prototype.updategroupimage = function () {
        var _this = this;
        this.imghandler.gpuploadimage(this.groupName).then(function (res) {
            _this.loadingProvider.dismissMyLoading();
            if (res) {
                _this.groupservice.updategroupImage(res, _this.groupName);
                _this.groupImage = res;
            }
            else {
                _this.loadingProvider.dismissMyLoading();
            }
        }).catch(function (err) {
            if (err == 20) {
                _this.navCtrl.pop();
            }
        });
    };
    GroupinfoPage.prototype.leaveGroup = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Leave Group',
            message: 'Do you want to leave this group?',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: function () {
                    }
                },
                {
                    text: 'Yes',
                    handler: function () {
                        _this.groupservice.leavegroup().then(function (res) {
                            if (res) {
                                _this.navCtrl.setRoot('TabsPage');
                            }
                        }).catch(function (err) {
                        });
                    }
                }
            ]
        });
        alert.present().then(function () {
        });
    };
    GroupinfoPage.prototype.deleteGroup = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Delete Group',
            message: 'Do you want to delete this group?',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: function () {
                    }
                },
                {
                    text: 'Yes',
                    handler: function () {
                        _this.groupservice.deletegroup().then(function () {
                            _this.navCtrl.setRoot('TabsPage');
                        }).catch(function (err) {
                        });
                    }
                }
            ]
        });
        alert.present().then(function () {
        });
    };
    GroupinfoPage.prototype.openpopoverforadmin = function (buddy) {
        var _this = this;
        if (this.owner && this.groupservice.userId != buddy.uid) {
            var sheet = this.actionSheet.create({
                title: 'Modification',
                buttons: [
                    {
                        text: 'Dismiss as Admin',
                        icon: 'trash',
                        handler: function () {
                            _this.groupservice.removefromAdmin(buddy);
                        }
                    }, {
                        text: 'Remove ' + buddy.displayName,
                        icon: 'person-add',
                        handler: function () {
                            _this.groupservice.removeAdmin(buddy);
                        }
                    }, {
                        text: 'Cancel',
                        role: 'cancel',
                        icon: 'cancel',
                        handler: function () {
                        }
                    }
                ]
            });
            sheet.present();
        }
    };
    GroupinfoPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    GroupinfoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-groupinfo',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/groupinfo/groupinfo.html"*/'<ion-header >\n  <ion-navbar hideBackButton>\n\n    <ion-buttons left>\n        <button ion-button (click)="backButtonClick()">\n             <ion-icon class="backbtncss" name="close-circle"></ion-icon>\n        </button>\n    </ion-buttons>\n\n    <ion-title>Group Info</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <div  class="main" *ngIf="groupImage">\n    <div class="back">\n    </div>\n    <img src="{{groupImage}}" (click)="updategroupimage()">\n    <div class="buddy">\n      <div class="group-name" left>\n        <h2 *ngIf="groupName">{{groupName}}</h2>\n      </div>\n      <div class="group-edit" right>\n        <button ion-button icon-only round (click)="editgroupname()">\n          <ion-icon ios="ios-create" md="md-create"></ion-icon>\n        </button>\n      </div>\n    </div>\n  </div>\n\n  <ion-list *ngIf="allMembers.length>0">\n    <ion-list-header>\n      <div item-start>{{allMembers.length}} participants</div>\n    </ion-list-header>\n    <ng-container *ngFor="let item of allMembers">\n      <ion-item *ngIf="item.admin" (press)="openpopoverforadmin(item)">\n        <ion-avatar item-left>\n            <img *ngIf="item.blockby == true" src="{{\'assets/imgs/user.png\' || userpic}}">\n            <img *ngIf="item.blockby == false" src="{{item.photoURL || userpic}}">\n        </ion-avatar>\n        <h2>{{item.displayName}}</h2>\n        <button ion-button round outline color="primary" item-right>Group Admin</button>\n      </ion-item>\n      <ion-item *ngIf="!item.admin" (press)="openpopover(item)">\n        <ion-avatar item-left>\n            <img *ngIf="item.blockby == true" src="{{\'assets/imgs/user.png\' || userpic}}">\n            <img *ngIf="item.blockby == false" src="{{item.photoURL || userpic}}">\n        </ion-avatar>\n        <h2>{{item.displayName}}</h2>\n        <p>Member</p>\n      </ion-item>\n    </ng-container>\n  </ion-list>\n  <ion-list *ngIf="!owner">\n    <ion-item (click)="leaveGroup()">\n      <ion-icon ios="ios-log-out" md="md-log-out" item-left></ion-icon>\n      Leave Group\n    </ion-item>\n  </ion-list>\n  <ion-list *ngIf="owner">\n    <ion-item (click)="deleteGroup()">\n      <ion-icon ios="ios-trash" md="md-trash" item-left></ion-icon>\n      Delete Group\n    </ion-item>\n  </ion-list>\n  <ion-fab bottom right *ngIf="owner">\n    <button ion-fab color="hcolor" (click)="addMemner()">\n      <ion-icon ios="ios-person-add" md="md-person-add"></ion-icon>\n    </button>\n  </ion-fab>\n</ion-content>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/groupinfo/groupinfo.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_groups_groups__["a" /* GroupsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_3__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_imagehandler_imagehandler__["a" /* ImagehandlerProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_tpstorage_tpstorage__["a" /* TpstorageProvider */]])
    ], GroupinfoPage);
    return GroupinfoPage;
}());

//# sourceMappingURL=groupinfo.js.map

/***/ })

});
//# sourceMappingURL=44.js.map