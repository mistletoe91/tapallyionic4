webpackJsonp([9],{

/***/ 823:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SendReferralBackwardPageModule", function() { return SendReferralBackwardPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__send_referral_backward__ = __webpack_require__(897);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SendReferralBackwardPageModule = (function () {
    function SendReferralBackwardPageModule() {
    }
    SendReferralBackwardPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__send_referral_backward__["a" /* SendReferralBackwardPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__send_referral_backward__["a" /* SendReferralBackwardPage */]),
            ],
        })
    ], SendReferralBackwardPageModule);
    return SendReferralBackwardPageModule;
}());

//# sourceMappingURL=send-referral-backward.module.js.map

/***/ }),

/***/ 897:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SendReferralBackwardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_tpstorage_tpstorage__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SendReferralBackwardPage = (function () {
    function SendReferralBackwardPage(navCtrl, navParams, tpStorageService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.tpStorageService = tpStorageService;
        this.showheader = true;
        this.thisIsMe = false;
        this.tpStorageService.get('userUID').then(function (myUID) {
            _this.buddy_displayName = navParams.get('buddy_displayName');
            _this.buddy_uid = navParams.get('buddy_uid');
            _this.sourcePage = navParams.get('source');
            if (myUID == _this.buddy_uid) {
                _this.thisIsMe = true;
            } //endif
        }).catch(function (e) {
        });
    }
    SendReferralBackwardPage.prototype.ionViewWillLeave = function () {
        this.showheader = false;
    };
    SendReferralBackwardPage.prototype.pickContact = function () {
        this.navCtrl.push("PickContactFromPage", { source: this.sourcePage, friend_asking_referral_uid: this.buddy_uid, friend_asking_referral_displayName: this.buddy_displayName, friend_asking_referral_catId: 0, friend_asking_referral_catName: 0 });
    };
    SendReferralBackwardPage.prototype.backButtonClick = function () {
        this.navCtrl.pop({ animate: true, direction: "forward" });
    };
    SendReferralBackwardPage.prototype.ionViewDidLoad = function () {
    };
    SendReferralBackwardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-send-referral-backward',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/send-referral-backward/send-referral-backward.html"*/'\n<ion-header >\n  <ion-navbar hideBackButton>\n\n    <ion-buttons left>\n        <button ion-button (click)="backButtonClick()">\n             <ion-icon class="backbtncss" name="arrow-dropleft-circle"></ion-icon>\n        </button>\n    </ion-buttons>\n\n    <ion-title>Pick Your Contact</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n    <ion-card-content *ngIf="thisIsMe"  class="herocard" text-center>\n      <ion-card-title>\n        Refer To {{buddy_displayName}}\n        </ion-card-title>\n      <p>\n        People you invite will use this button to send referral directly back to you \n      </p>\n      <button  disabled  ion-button color="primary" block>Send Referral</button>\n    </ion-card-content>\n\n    <ion-card-content *ngIf="!thisIsMe"  class="herocard" text-center>\n      <ion-card-title>\n        Refer Your Contact To {{buddy_displayName}}\n        </ion-card-title>\n      <p>\n        This will help your contact connect with {{buddy_displayName}}\n      </p>\n      <button (click)="pickContact()" ion-button color="primary" block>Pick Contact</button>\n    </ion-card-content>\n\n</ion-content>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/send-referral-backward/send-referral-backward.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_tpstorage_tpstorage__["a" /* TpstorageProvider */]])
    ], SendReferralBackwardPage);
    return SendReferralBackwardPage;
}());

//# sourceMappingURL=send-referral-backward.js.map

/***/ })

});
//# sourceMappingURL=9.js.map