webpackJsonp([24],{

/***/ 808:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PopoverChatPageModule", function() { return PopoverChatPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__popover_chat__ = __webpack_require__(882);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PopoverChatPageModule = (function () {
    function PopoverChatPageModule() {
    }
    PopoverChatPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__popover_chat__["a" /* PopoverChatPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__popover_chat__["a" /* PopoverChatPage */]),
            ],
        })
    ], PopoverChatPageModule);
    return PopoverChatPageModule;
}());

//# sourceMappingURL=popover-chat.module.js.map

/***/ }),

/***/ 882:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopoverChatPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PopoverChatPage = (function () {
    function PopoverChatPage(viewCtrl, navCtrl, navParams, app, event) {
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.app = app;
        this.event = event;
    }
    PopoverChatPage.prototype.userProfile = function () {
        this.viewCtrl.dismiss({ page: "ProfilepicPage" });
    };
    PopoverChatPage.prototype.SettingPage = function () {
        this.viewCtrl.dismiss({ page: "SettingPage" });
    };
    PopoverChatPage.prototype.userGroups = function () {
        this.viewCtrl.dismiss({ page: "GroupbuddiesPage" });
    };
    PopoverChatPage.prototype.ionViewWillLeave = function () {
    };
    PopoverChatPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-popover-chat',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/popover-chat/popover-chat.html"*/'<ion-content>\n	<ion-item (click)="userProfile()" no-line>\n		<ion-icon name="person" item-start></ion-icon>\n		<ion-label no-line>Profile</ion-label>\n	</ion-item>\n	<ion-item (click)="userGroups()" no-line>\n		<ion-icon name="people" item-start></ion-icon>\n		<ion-label no-line>Create new group</ion-label>\n	</ion-item>\n	<ion-item (click)="SettingPage()" no-line>\n		<ion-icon name="settings" item-start></ion-icon>\n		<ion-label no-line>Settings</ion-label>\n	</ion-item>\n	<!-- <ion-item (click)="StarredPage()" no-line>\n		<ion-icon name="star" item-start></ion-icon>\n		<ion-label no-line>Starred</ion-label>\n	</ion-item> -->\n</ion-content>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/popover-chat/popover-chat.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["B" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */]])
    ], PopoverChatPage);
    return PopoverChatPage;
}());

//# sourceMappingURL=popover-chat.js.map

/***/ })

});
//# sourceMappingURL=24.js.map