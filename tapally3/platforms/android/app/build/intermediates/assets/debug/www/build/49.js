webpackJsonp([49],{

/***/ 783:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GpattachmentsPageModule", function() { return GpattachmentsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__gpattachments__ = __webpack_require__(857);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var GpattachmentsPageModule = (function () {
    function GpattachmentsPageModule() {
    }
    GpattachmentsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__gpattachments__["a" /* GpattachmentsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__gpattachments__["a" /* GpattachmentsPage */]),
            ],
        })
    ], GpattachmentsPageModule);
    return GpattachmentsPageModule;
}());

//# sourceMappingURL=gpattachments.module.js.map

/***/ }),

/***/ 857:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GpattachmentsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_groups_groups__ = __webpack_require__(400);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_user_user__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_loading_loading__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_imagehandler_imagehandler__ = __webpack_require__(403);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_chat_chat__ = __webpack_require__(401);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_fcm_fcm__ = __webpack_require__(97);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var GpattachmentsPage = (function () {
    function GpattachmentsPage(navCtrl, navParams, userservice, loading, imgstore, chatservice, fcm, viewCtrl, groupservice, popoverCtrl, app) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.userservice = userservice;
        this.loading = loading;
        this.imgstore = imgstore;
        this.chatservice = chatservice;
        this.fcm = fcm;
        this.viewCtrl = viewCtrl;
        this.groupservice = groupservice;
        this.popoverCtrl = popoverCtrl;
        this.app = app;
        this.loginuserInfo = [];
    }
    GpattachmentsPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.gpname = this.navParams.get('gname');
        this.userservice.getuserdetails().then(function (res) {
            _this.loginuserInfo = res;
        }).catch(function (err) {
        });
    };
    GpattachmentsPage.prototype.sendGalleryPicMsg = function () {
        var _this = this;
        this.dismissMe();
        this.imgstore.picmsgstore().then(function (imgurl) {
            if (imgurl.length > 4) {
                var flag_1 = 0;
                _this.groupservice.addgroupmsgmultiple(imgurl, _this.loginuserInfo, 'image', _this.gpname).then(function (res) {
                    if (res) {
                        flag_1++;
                    }
                    if (flag_1 == imgurl.length) {
                    }
                });
            }
            else {
                var flag_2 = 0;
                for (var i = 0; i < imgurl.length; i++) {
                    _this.groupservice.addgroupmsg(imgurl[i], _this.loginuserInfo, 'image', _this.gpname).then(function (res) {
                        if (res) {
                            flag_2++;
                        }
                        if (flag_2 == imgurl.length) {
                        }
                    });
                }
            }
        }).catch(function (err) {
        });
    };
    GpattachmentsPage.prototype.dismissMe = function () {
        if (this.viewCtrl) {
            this.viewCtrl.dismiss();
        }
    };
    GpattachmentsPage.prototype.sendPicMsg = function () {
        var _this = this;
        this.dismissMe();
        var flag = 0;
        this.imgstore.cameraPicmsgStore().then(function (imgurl) {
            _this.groupservice.addgroupmsg(imgurl, _this.loginuserInfo, 'image', _this.gpname).then(function (res) {
                if (res) {
                    flag++;
                }
            });
        }).catch(function (err) {
        });
    };
    GpattachmentsPage.prototype.sendDocument = function () {
        var _this = this;
        this.dismissMe();
        var flag = 0;
        this.imgstore.selectDocument().then(function (imgurl) {
            _this.groupservice.addgroupmsg(imgurl, _this.loginuserInfo, 'document', _this.gpname).then(function (res) {
                if (res) {
                    flag++;
                }
            });
        }).catch(function (err) {
        });
    };
    GpattachmentsPage.prototype.sendAudioMsg = function () {
        var _this = this;
        this.dismissMe();
        var flag = 0;
        this.imgstore.recordAudio().then(function (imgurl) {
            _this.groupservice.addgroupmsg(imgurl, _this.loginuserInfo, 'audio', _this.gpname).then(function (res) {
                if (res) {
                    flag++;
                }
            });
        }).catch(function (err) {
            _this.loading.dismissMyLoading();
        });
    };
    GpattachmentsPage.prototype.openContacts = function (myEvent) {
        this.viewCtrl.dismiss({ page: "AllcontactsPage", gpname: this.gpname });
    };
    GpattachmentsPage.prototype.openMap = function (myEvent) {
        this.viewCtrl.dismiss({ page: "MapPage", gpname: this.gpname, flag: true });
    };
    GpattachmentsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-gpattachments',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/gpattachments/gpattachments.html"*/'\n  <ion-item tappable (click)="sendPicMsg()">\n    <ion-label no-line >Camera</ion-label>\n  </ion-item>\n  <ion-item  tappable (click)="sendGalleryPicMsg()" no-line>\n    <ion-label no-line >Gallery</ion-label>\n  </ion-item>\n  <!-- <ion-item no-line (click)="sendAudioMsg()">\n    <ion-label no-line >Audio</ion-label>\n  </ion-item> -->\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/gpattachments/gpattachments.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_imagehandler_imagehandler__["a" /* ImagehandlerProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_chat_chat__["a" /* ChatProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_fcm_fcm__["a" /* FcmProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["B" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_groups_groups__["a" /* GroupsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["y" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* App */]])
    ], GpattachmentsPage);
    return GpattachmentsPage;
}());

/*
Original Text in html
<ion-item no-line>
    <ion-label (click)="sendDocument()" no-line >Documents</ion-label>
  </ion-item>
  <ion-item (click)="sendPicMsg()">
    <ion-label no-line >Camera</ion-label>
  </ion-item>
  <ion-item (click)="sendGalleryPicMsg()" no-line>
    <ion-label no-line >Gallery</ion-label>
  </ion-item>
  <!-- <ion-item no-line (click)="sendAudioMsg()">
    <ion-label no-line >Audio</ion-label>
  </ion-item> -->
  <ion-item no-line (click)="openContacts($event)">
    <ion-label no-line >Contact</ion-label>
  </ion-item>
  <ion-item no-line (click)="openMap($event)">
    <ion-label no-line >Current Location</ion-label>
  </ion-item>
*/
//# sourceMappingURL=gpattachments.js.map

/***/ })

});
//# sourceMappingURL=49.js.map