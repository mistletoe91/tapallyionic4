webpackJsonp([72],{

/***/ 760:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AllbuddyContactsPageModule", function() { return AllbuddyContactsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__allbuddy_contacts__ = __webpack_require__(834);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AllbuddyContactsPageModule = (function () {
    function AllbuddyContactsPageModule() {
    }
    AllbuddyContactsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__allbuddy_contacts__["a" /* AllbuddyContactsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__allbuddy_contacts__["a" /* AllbuddyContactsPage */]),
            ],
        })
    ], AllbuddyContactsPageModule);
    return AllbuddyContactsPageModule;
}());

//# sourceMappingURL=allbuddy-contacts.module.js.map

/***/ }),

/***/ 834:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AllbuddyContactsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_contact_contact__ = __webpack_require__(402);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_groups_groups__ = __webpack_require__(400);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_user_user__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_loading_loading__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_chat_chat__ = __webpack_require__(401);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_fcm_fcm__ = __webpack_require__(97);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var AllbuddyContactsPage = (function () {
    function AllbuddyContactsPage(navCtrl, navParams, contact, chatservice, fcm, groupservice, userservice, loading, alertCtrl, platform) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.contact = contact;
        this.chatservice = chatservice;
        this.fcm = fcm;
        this.groupservice = groupservice;
        this.userservice = userservice;
        this.loading = loading;
        this.alertCtrl = alertCtrl;
        this.platform = platform;
        this.selectedMember = [];
        this.myFriends = [];
        this.loginuserInfo = [];
        this.msgstatus = false;
        this.showheader = true;
        this.tmpmyfriends = [];
    }
    AllbuddyContactsPage.prototype.ionViewWillLeave = function () {
        this.showheader = false;
    };
    AllbuddyContactsPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.buddyStatus = this.chatservice.buddyStatus;
        this.buddy = this.navParams.get('buddy');
        this.contact.getSimJsonContacts().then(function (res) {
            _this.tmpmyfriends = res["contacts"];
            _this.tmpmyfriends = _this.tmpmyfriends.sort(function (a, b) {
                if (a.displayName != undefined) {
                    var nameA = a.displayName.toUpperCase(); // ignore upper and lowercase
                }
                if (b.displayName != undefined) {
                    var nameB = b.displayName.toUpperCase(); // ignore upper and lowercase
                }
                if (nameA < nameB) {
                    return -1;
                }
                if (nameA > nameB) {
                    return 1;
                }
                // names must be equal
                return 0;
            });
            _this.myFriends = _this.tmpmyfriends;
        }).catch(function (e) { });
    };
    AllbuddyContactsPage.prototype.initializeItems = function () {
        this.myFriends = this.tmpmyfriends;
    };
    AllbuddyContactsPage.prototype.backButtonClick = function () {
        this.navCtrl.pop({ animate: true, direction: "forward" });
    };
    AllbuddyContactsPage.prototype.addContact = function (contact) {
        if (this.selectedMember.length == 0) {
            this.selectedMember.push(contact);
        }
        else {
            var flagofbuddy = false;
            var posionofbuddy = 0;
            for (var i = 0; i < this.selectedMember.length; i++) {
                if (this.selectedMember[i]._id == contact._id) {
                    flagofbuddy = true;
                    posionofbuddy = i;
                    break;
                }
                else {
                    flagofbuddy = false;
                }
            }
            if (flagofbuddy == true) {
                this.selectedMember.splice(posionofbuddy, 1);
            }
            else if (this.selectedMember.length == 6) {
                this.loading.presentToast("Maximum six contacts allowed");
            }
            else {
                this.selectedMember.push(contact);
            }
        }
    };
    AllbuddyContactsPage.prototype.searchuser = function (ev) {
        this.initializeItems();
        var val = ev.target.value;
        if (val && val.trim() != '') {
            this.myFriends = this.tmpmyfriends;
            this.myFriends = this.myFriends.filter(function (item) {
                return (item.displayName.toLowerCase().indexOf(val.toLowerCase().trim()) > -1);
            });
        }
    };
    AllbuddyContactsPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    AllbuddyContactsPage.prototype.goNext = function () {
        var _this = this;
        this.UnreadMSG = 0;
        this.userservice.getstatus(this.buddy).then(function (res) {
            if (!res) {
                _this.chatservice.addnewmessagemultiple(_this.selectedMember, 'contact', _this.buddyStatus).then(function () {
                    if (_this.buddyStatus != 'online') {
                        var newMessage = 'You have received a message';
                        _this.fcm.sendNotification(_this.buddy, newMessage, 'chatpage');
                    }
                    _this.newmessage = '';
                });
            }
            else {
                var alert_1 = _this.alertCtrl.create({
                    message: 'Unblock ' + _this.buddy.displayName + ' to send a message.',
                    buttons: [
                        {
                            text: 'CANCEL',
                            role: 'cancel',
                            handler: function () {
                            }
                        },
                        {
                            text: 'UNBLOCK',
                            handler: function () {
                                _this.userservice.unblockUser(_this.buddy).then(function (res) {
                                    if (res) {
                                    }
                                });
                            }
                        }
                    ]
                });
                alert_1.present();
            }
        });
        this.navCtrl.pop();
    };
    AllbuddyContactsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-allbuddy-contacts',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/allbuddy-contacts/allbuddy-contacts.html"*/'<ion-header  >\n  <ion-navbar hideBackButton>\n\n    <ion-buttons left>\n        <button ion-button (click)="backButtonClick()">\n             <ion-icon class="backbtncss" name="arrow-dropleft-circle"></ion-icon>\n        </button>\n    </ion-buttons>\n        <ion-title text-left>All Contacts</ion-title>\n\n  </ion-navbar>\n  <ion-searchbar placeholder="Search" class="searchbar" (ionInput)="searchuser($event)"></ion-searchbar>\n  <ng-container *ngIf="selectedMember.length > 0">\n  <ion-list  class="group-wrap" >\n    <ion-item *ngFor="let item of selectedMember">\n      <p>{{item.displayName}}</p>\n      <ion-avatar item-left><img src="assets/imgs/user.png"></ion-avatar>\n    </ion-item>\n    <!-- <ion-item *ngFor="let item of selectedMember">-->\n\n    <!-- </ion-item> -->\n  </ion-list>\n</ng-container>\n</ion-header>\n\n\n<ion-content padding [ngClass]="selectedMember.length > 0?\'group-wrap-active\':\'group-wrap-no-active\'">\n\n   <ion-list>\n    <ng-container *ngFor="let key of myFriends">\n      <ion-item (click)="addContact(key)">\n        <ion-avatar item-left><img src="assets/imgs/user.png"></ion-avatar>\n        <h2>{{key.displayName}}</h2>\n      </ion-item>\n    </ng-container>\n</ion-list>\n\n\n<ion-fab bottom right>\n  <button ion-fab [disabled]="selectedMember.length == 0" (click)="goNext()"><ion-icon ios="ios-arrow-round-forward" md="md-arrow-round-forward"></ion-icon></button>\n</ion-fab>\n\n</ion-content>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/allbuddy-contacts/allbuddy-contacts.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_contact_contact__["a" /* ContactProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_chat_chat__["a" /* ChatProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_fcm_fcm__["a" /* FcmProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_groups_groups__["a" /* GroupsProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */]])
    ], AllbuddyContactsPage);
    return AllbuddyContactsPage;
}());

//# sourceMappingURL=allbuddy-contacts.js.map

/***/ })

});
//# sourceMappingURL=72.js.map