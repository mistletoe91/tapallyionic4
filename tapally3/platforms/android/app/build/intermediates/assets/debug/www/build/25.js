webpackJsonp([25],{

/***/ 807:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PickContactToPageModule", function() { return PickContactToPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pick_contact_to__ = __webpack_require__(881);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PickContactToPageModule = (function () {
    function PickContactToPageModule() {
    }
    PickContactToPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__pick_contact_to__["a" /* PickContactToPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__pick_contact_to__["a" /* PickContactToPage */]),
            ],
        })
    ], PickContactToPageModule);
    return PickContactToPageModule;
}());

//# sourceMappingURL=pick-contact-to.module.js.map

/***/ }),

/***/ 881:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PickContactToPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_user_user__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_contact_contact__ = __webpack_require__(402);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_requests_requests__ = __webpack_require__(399);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_loading_loading__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_chat_chat__ = __webpack_require__(401);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_sms_sms__ = __webpack_require__(404);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_fcm_fcm__ = __webpack_require__(97);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_groups_groups__ = __webpack_require__(400);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_app_angularfireconfig__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_tpstorage_tpstorage__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









//import { Storage } from '@ionic/storage';



var PickContactToPage = (function () {
    function PickContactToPage(events, platform, 
        //public storage: Storage,
        loadingProvider, contactProvider, userservice, navCtrl, navParams, alertCtrl, requestservice, chatservice, zone, smsservice, fcm, modalCtrl, toastCtrl, groupservice, tpStorageService) {
        this.events = events;
        this.platform = platform;
        this.loadingProvider = loadingProvider;
        this.contactProvider = contactProvider;
        this.userservice = userservice;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.requestservice = requestservice;
        this.chatservice = chatservice;
        this.zone = zone;
        this.smsservice = smsservice;
        this.fcm = fcm;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.groupservice = groupservice;
        this.tpStorageService = tpStorageService;
        this.contactList = [];
        this.allregisteUserData = [];
        this.metchContact = [];
        this.mainArray = [];
        this.tempArray = [];
        this.userpic = __WEBPACK_IMPORTED_MODULE_10__app_app_angularfireconfig__["c" /* env */].userPic;
        this.isData = false;
        this.reqStatus = false;
        this.isInviteArray = [];
        this.showheader = true;
        this.newrequest = {};
        this.temparr = [];
        this.filteredusers = [];
        this.friend_to_forward_uid = navParams.get('friend_to_forward_uid');
        this.friend_to_forward_displayName = navParams.get('friend_to_forward_displayName');
        this.source = navParams.get('source');
        //this.friend_to_forward_catId = navParams.get('friend_to_forward_catId');
        //this.friend_to_forward_catName = navParams.get('friend_to_forward_catName');
        this.cntCounter = 0;
    }
    PickContactToPage.prototype.backButtonClick = function () {
        //this.navCtrl.pop({animate: true, direction: "forward"});
    };
    PickContactToPage.prototype.tpInitilizeFromStorage = function () {
        var _this = this;
        this.tpStorageService.getItem('userUID').then(function (res) {
            if (res) {
                _this.userId = res;
            }
        }).catch(function (e) { });
        this.tpStorageService.getItem('updated').then(function (res) {
            if (res) {
                _this.updatedRef = res;
            }
        }).catch(function (e) { });
    };
    PickContactToPage.prototype.pickContactExecute = function (buddy, isTapAllyMember) {
        //Tapally Member so send push notification
        /*
          buddy : receipent
          referral (the one being sent) :  this.friend_to_forward_uid's contact
              */
        for (var key = 0; key < this.myfriends.length; key++) {
            if (this.friend_to_forward_uid == this.myfriends[key].uid) {
                //Assuming : The referral should be your friend (mytodo : can we have it open ? )
                if (isTapAllyMember) {
                    //buddy is tapally member
                    //forwarding your referral to another
                    this.newmessage = 'Hi I am sending referral to you : ' + this.myfriends[key].displayName + " (" + this.myfriends[key].mobile + ")";
                    this.addmessage(buddy, this.myfriends[key], this.myfriends[key]);
                }
                else {
                    //buddy is not a member of tapally
                    var msg = 'Hi I send your referral to ' + this.myfriends[key].displayName + " (" + this.myfriends[key].mobile + "). Sign up & download app at https://tapally.com ";
                    this.sendSMSCustomMsg(buddy, msg, this.myfriends[key]);
                } //endif
                //also tell the one who got referred
                this.newmessage = 'Hi I send your referral to ' + buddy.displayName + " (" + buddy.mobile + ")";
                this.addmessage(this.myfriends[key], buddy, this.myfriends[key]);
                break;
            }
        } //end for
    };
    PickContactToPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.tpInitilizeFromStorage();
        this.requestservice.getmyfriends();
        this.myfriends = [];
        this.events.subscribe('friends', function () {
            _this.events.unsubscribe('friends');
            _this.myfriends = [];
            _this.myfriends = _this.requestservice.myfriends;
        });
    };
    PickContactToPage.prototype.sendToNextPage = function (originalChat) {
        this.cntCounter++;
        if (this.cntCounter >= 2) {
            this.cntCounter = 0;
            this.chatservice.initializebuddy(originalChat);
            //
            if (this.source == "buddypage") {
                //mytodo : this should go to buddychatpage as well but it was not working properly so i temporarliy sending to tabspage with a toast
                this.navCtrl.setRoot('TabsPage');
                var toast = this.toastCtrl.create({
                    message: 'Referral forwarded successfully. You can see status by clicking on your contact`s icon',
                    duration: 5000
                });
                toast.present();
            }
            else {
                this.navCtrl.setRoot('BuddychatPage', { back_button_pop_on: false });
            }
        }
    };
    //here buddy is receipient and referral is contact being sent
    PickContactToPage.prototype.addmessage = function (buddy, referral, originalChat) {
        //console.log ("aaaa");
        var _this = this;
        //Send Push notification : mytodo check if the buddy is online or not. if online then no need to send push notification
        var newMessage = 'You have received a message';
        this.fcm.sendNotification(buddy, newMessage, 'chatpage');
        //Send chat message
        this.chatservice.addnewmessageDirectReferral(this.newmessage, 'message', buddy, referral, 'forward').then(function () {
            _this.newmessage = '';
            _this.sendToNextPage(originalChat);
        });
    };
    PickContactToPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.loadingProvider.presentLoading();
        this.contactProvider.getSimJsonContacts()
            .then(function (res) {
            Object.keys(res).forEach(function (ele) {
                if (ele == "contacts") {
                    _this.mainArray = [];
                    Object.keys(res["contacts"]).forEach(function (key) {
                        _this.mainArray.push({
                            "displayName": res["contacts"][key].displayName,
                            "mobile": res["contacts"][key].mobile,
                            "mobile_formatted": res["contacts"][key].mobile_formatted,
                            "status": "",
                            "isBlock": false,
                            "isUser": 0,
                            "isdisable": false,
                            "photoURL": res["contacts"][key].photoURL,
                            "uid": res["contacts"][key].uid
                        });
                    });
                } //endif
            });
            _this.loadingProvider.dismissMyLoading();
        }).catch(function (e) { });
    }; //end function
    PickContactToPage.prototype.removeDuplicates = function (originalArray, prop) {
        var newArray = [];
        var lookupObject = {};
        originalArray.forEach(function (item, index) {
            lookupObject[originalArray[index][prop]] = originalArray[index];
        });
        Object.keys(lookupObject).forEach(function (element) {
            newArray.push(lookupObject[element]);
        });
        return newArray;
    };
    PickContactToPage.prototype.ionViewWillLeave = function () {
        //this.groupservice.getmygroups();
        //this.requestservice.getmyfriends();
        this.loadingProvider.dismissMyLoading();
    };
    PickContactToPage.prototype.initializeContacts = function () {
        this.mainArray = this.tempArray;
    };
    PickContactToPage.prototype.refreshPage = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Sync All Contacts',
            message: 'It may take few minutes to re-sync all contacts from your device. Do you agree ? ',
            buttons: [
                {
                    text: 'Cancel',
                    handler: function () {
                        //console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Agree',
                    handler: function () {
                        //console.log('Agree clicked');
                        _this.contactProvider.deleteFromTable();
                        _this.navCtrl.setRoot('ContactimportprogressPage', { source: 'PickContactToPage' });
                    }
                }
            ]
        });
        confirm.present();
    };
    PickContactToPage.prototype.searchuser = function (ev) {
        //initize the array
        if (this.tempArray.length <= 0) {
            this.tempArray = this.mainArray;
        }
        else {
            this.mainArray = this.tempArray;
        } //endif
        var val = ev.target.value;
        if (val && val.trim() != '') {
            this.mainArray = this.mainArray.filter(function (item) {
                if (item.displayName != undefined) {
                    return (item.displayName.toLowerCase().indexOf(val.toLowerCase()) > -1);
                }
            });
        }
    };
    PickContactToPage.prototype.inviteReq = function (recipient) {
        var alert = this.alertCtrl.create({
            title: 'Invitation',
            subTitle: 'Invitation already sent to ' + recipient.displayName + '.',
            buttons: ['Ok']
        });
        alert.present();
    };
    PickContactToPage.prototype.sendreq = function (recipient) {
        var _this = this;
        this.reqStatus = true;
        this.newrequest.sender = this.userId;
        this.newrequest.recipient = recipient.uid;
        if (this.newrequest.sender === this.newrequest.recipient)
            alert('You are your friend always');
        else {
            var successalert_1 = this.alertCtrl.create({
                title: 'Request sent',
                subTitle: 'Your request has been sent to ' + recipient.displayName + '.',
                buttons: ['ok']
            });
            var userName_1;
            this.userservice.getuserdetails().then(function (res) {
                userName_1 = res.displayName;
                var newMessage = userName_1 + " has sent you friend request.";
                _this.fcm.sendNotification(recipient, newMessage, 'sendreq');
            });
            this.requestservice.sendrequest(this.newrequest).then(function (res) {
                if (res.success) {
                    _this.reqStatus = false;
                    successalert_1.present();
                    var sentuser = _this.mainArray.indexOf(recipient);
                    _this.mainArray[sentuser].status = "pending";
                }
            }).catch(function (err) {
            });
        }
    };
    PickContactToPage.prototype.buddychat = function (buddy) {
        this.chatservice.initializebuddy(buddy);
        this.navCtrl.setRoot('BuddychatPage', { back_button_pop_on: false });
    };
    PickContactToPage.prototype.sendSMSCustomMsg = function (item, msg, originalChat) {
        var _this = this;
        this.loadingProvider.presentLoading();
        this.smsservice.sendSmsCustomMsg(item.mobile, msg).then(function (res) {
            _this.loadingProvider.dismissMyLoading();
            _this.sendToNextPage(originalChat);
        }).catch(function (err) {
            //console.log (err);
            _this.loadingProvider.dismissMyLoading();
            _this.sendToNextPage(originalChat);
            /*let alert = this.alertCtrl.create({
                title: 'Message Send',
                subTitle: 'Error for sending message to ' + item.displayName + '.',
                buttons: ['Ok']
            });
            alert.present();
                        */
        });
    }; //end function
    PickContactToPage.prototype.sendSMS = function (item) {
        var _this = this;
        this.loadingProvider.presentLoading();
        this.smsservice.sendSms(item.mobile).then(function (res) {
            _this.loadingProvider.dismissMyLoading();
            if (res) {
                for (var i = 0; i < _this.mainArray.length; ++i) {
                    if (_this.mainArray[i].mobile == item.mobile) {
                        _this.mainArray[i].isdisable = true;
                    }
                }
                /*let alert = this.alertCtrl.create({
                    title: 'Message Send',
                    subTitle: 'Your message has been sent to ' + item.displayName + '.',
                    buttons: ['Ok']
                });
                alert.present();//
                                */
            }
            else {
                /*let alert = this.alertCtrl.create({
                    title: 'Message Send',
                    subTitle: 'Error for sending message to ' + item.displayName + '.',
                    buttons: ['Ok']
                });
                alert.present();
                                */
            }
        }).catch(function (err) {
            _this.loadingProvider.dismissMyLoading();
            /*let alert = this.alertCtrl.create({
                title: 'Message Send',
                subTitle: 'Error for sending message to ' + item.displayName + '.',
                buttons: ['Ok']
            });
            alert.present();
                        */
        });
    };
    PickContactToPage.prototype.addnewContact = function (myEvent) {
        var profileModal = this.modalCtrl.create("AddContactPage");
        profileModal.present({
            ev: myEvent
        });
        profileModal.onDidDismiss(function (pages) {
        });
    };
    PickContactToPage.prototype.goBack = function () {
        this.navCtrl.setRoot('TabsPage');
    };
    PickContactToPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-pick-contact-to',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/pick-contact-to/pick-contact-to.html"*/'<ion-header  >\n  <ion-navbar hideBackButton>\n    <div class="header-wrap">\n        <div class="flex-start">\n          <ion-buttons left>\n              <button ion-button (click)="goBack()">\n                   <ion-icon class="backbtncss" name="arrow-dropleft-circle"></ion-icon>\n              </button>\n          </ion-buttons>\n          <div class="header-title">Forward Contact</div>\n        </div>\n        <div class="flex-end">\n          <ion-buttons end>\n              <button ion-button icon-only (click)="refreshPage()">\n                <ion-icon ios="md-refresh" md="md-refresh"></ion-icon>\n              </button>\n          </ion-buttons>\n        </div>\n    </div>\n  </ion-navbar>\n  <ion-searchbar placeholder="Search" (ionInput)="searchuser($event)"></ion-searchbar>\n  <p style="color:#000;;margin-left:10px;">Forward {{friend_to_forward_displayName}}\'s to your kown contact</p> \n</ion-header>\n<ion-content>\n\n  <ion-list [virtualScroll]="mainArray" class="contactslistion" >\n    <ion-item *virtualItem="let item" class="itemside"  >\n      <ion-avatar item-start >\n        <img *ngIf="item.isBlock == false" src="{{item.photoURL || userpic}}">\n        <img *ngIf="item.isBlock == true" src="{{\'assets/imgs/user.png\' || userpic }}">\n      </ion-avatar>\n      <ion-note class="itemnote"  item-start>\n        <h2 class="displaynamecss" *ngIf="item.displayName">{{item.displayName}} </h2>\n        <p *ngIf="item.mobile_formatted"  [innerHTML]="item.mobile_formatted" ></p>\n      </ion-note>\n\n      <button item-end  (click)="pickContactExecute(item,true)" *ngIf="item.uid">\n        <ion-icon class="mainiconcontact" name="checkmark-circle"></ion-icon>\n      </button>\n      <button item-end   (click)="pickContactExecute(item,false)" *ngIf="!item.uid && !item.isdisable">\n        <ion-icon class="mainiconcontact"  name="add-circle"></ion-icon>\n      </button>\n      <button item-end   (click)="pickContactExecute(item,false)" *ngIf="!item.uid && item.isdisable">\n        <ion-icon class="mainiconcontact"   name="time"></ion-icon>\n      </button>\n    </ion-item>\n  </ion-list>\n\n\n <div *ngIf="isData" padding-horizontal>\n        No contacts found!\n </div>\n</ion-content>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/pick-contact-to/pick-contact-to.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_5__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_contact_contact__["a" /* ContactProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_requests_requests__["a" /* RequestsProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_chat_chat__["a" /* ChatProvider */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */],
            __WEBPACK_IMPORTED_MODULE_7__providers_sms_sms__["a" /* SmsProvider */],
            __WEBPACK_IMPORTED_MODULE_8__providers_fcm_fcm__["a" /* FcmProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["z" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_9__providers_groups_groups__["a" /* GroupsProvider */],
            __WEBPACK_IMPORTED_MODULE_11__providers_tpstorage_tpstorage__["a" /* TpstorageProvider */]])
    ], PickContactToPage);
    return PickContactToPage;
}());

//# sourceMappingURL=pick-contact-to.js.map

/***/ })

});
//# sourceMappingURL=25.js.map