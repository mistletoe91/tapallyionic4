webpackJsonp([61],{

/***/ 771:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatsPageModule", function() { return ChatsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__chats__ = __webpack_require__(845);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



//import {  MatCardModule, MatTabsModule, MatChipsModule, MatIconModule, MatToolbarModule, MatDatepickerModule, MatFormFieldModule, MatNativeDateModule } from "@angular/material";
//import {MatButtonModule} from '@angular/material/button';
var ChatsPageModule = (function () {
    /*
    MatButtonModule,
    MatCardModule,
    MatTabsModule,
    MatChipsModule,
    MatIconModule,
    MatToolbarModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule
    */
    function ChatsPageModule() {
    }
    ChatsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__chats__["a" /* ChatsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__chats__["a" /* ChatsPage */]),
            ],
        })
        /*
        MatButtonModule,
        MatCardModule,
        MatTabsModule,
        MatChipsModule,
        MatIconModule,
        MatToolbarModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatNativeDateModule
        */
    ], ChatsPageModule);
    return ChatsPageModule;
}());

//# sourceMappingURL=chats.module.js.map

/***/ }),

/***/ 845:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_requests_requests__ = __webpack_require__(399);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_chat_chat__ = __webpack_require__(401);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_user_user__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_contact_contact__ = __webpack_require__(402);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_loading_loading__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_fcm_fcm__ = __webpack_require__(97);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_groups_groups__ = __webpack_require__(400);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_imagehandler_imagehandler__ = __webpack_require__(403);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_sqlite__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__app_const__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_tpstorage_tpstorage__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_firebase__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_http__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};















var ChatsPage = (function () {
    //public file: File,
    function ChatsPage(fcm, platform, loadingProvider, contactProvider, navCtrl, app, sqlite, navParams, requestservice, events, alertCtrl, chatservice, zone, http, popoverCtrl, imghandler, userservice, loadingCtrl, eleRef, modalCtrl, groupservice, tpStorageService) {
        this.fcm = fcm;
        this.platform = platform;
        this.loadingProvider = loadingProvider;
        this.contactProvider = contactProvider;
        this.navCtrl = navCtrl;
        this.app = app;
        this.sqlite = sqlite;
        this.navParams = navParams;
        this.requestservice = requestservice;
        this.events = events;
        this.alertCtrl = alertCtrl;
        this.chatservice = chatservice;
        this.zone = zone;
        this.http = http;
        this.popoverCtrl = popoverCtrl;
        this.imghandler = imghandler;
        this.userservice = userservice;
        this.loadingCtrl = loadingCtrl;
        this.eleRef = eleRef;
        this.modalCtrl = modalCtrl;
        this.groupservice = groupservice;
        this.tpStorageService = tpStorageService;
        this.myrequests = [];
        this.myfriendsList = [];
        this.tempmyfriendsList = [];
        this.debugMe = true;
        this.showPaidSign = false;
        this.showheader = true;
        this.isData = false;
        this.isNoRecord = false;
        this.requestcounter = null;
        this.username = "";
        this.selectContact = false;
        this.selectedContact = [];
        this.selectCounter = 0;
        this.allmygroups = [];
        this.tempallmygroups = [];
        this.allChatListing = [];
        this.allChatListing_HTML = [];
        this.archiveChats = [];
        this.counter = 0;
        this.messages = [];
        this.whitelabelObj = {};
        this.whitelabelObj_ShowNoBusiness = false;
        this.virginityBroken_ChatPage = true;
        this.testMode = false;
        this.tmp_StringName = '';
        this.events.unsubscribe('friends');
        this.initiateMe();
    }
    ChatsPage.prototype.formatPhoneNumber = function (phoneNumberString) {
        var cleaned = ('' + phoneNumberString).replace(/\D/g, '');
        var match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/);
        if (match) {
            var intlCode = (match[1] ? '+1 ' : '');
            return [intlCode, '(', match[2], ') ', match[3], '-', match[4]].join('');
        }
        return "";
    };
    ChatsPage.prototype.getFromDbi = function () {
        var _this = this;
        this.userservice.getuserdetails().then(function (res) {
            _this.whitelabelObj.mobile = _this.formatPhoneNumber(res['mobile']);
            _this.whitelabelObj.displayName = res['displayName'];
        }).catch(function (err) {
            _this.cleanWhiteLabelCache();
        });
    };
    ChatsPage.prototype.getWhileLabelObj = function (showMyBusiness) {
        var _this = this;
        this.whitelabelObj.buddy_uid = 0;
        if (showMyBusiness) {
            this.whitelabelObj.showMyBusiness = true;
            this.tpStorageService.get('mymobile').then(function (storage_myphone) {
                _this.tpStorageService.get('displayName').then(function (storage_myname) {
                    _this.whitelabelObj.mobile = _this.formatPhoneNumber(storage_myphone);
                    _this.whitelabelObj.displayName = storage_myname;
                }).catch(function (e) {
                    _this.getFromDbi();
                });
            }).catch(function (e) {
                _this.getFromDbi();
            });
            this.tpStorageService.get('userUID').then(function (storage_res) {
                _this.whitelabelObj.buddy_uid = storage_res;
                _this.tpStorageService.setItem('whitelabelObj_uid', storage_res);
            }).catch(function (e) {
            });
            this.tpStorageService.get('business_my_referral_type_id').then(function (storage_res) {
                _this.whitelabelObj.business_my_referral_type_id = storage_res;
            }).catch(function (e) {
                _this.whitelabelObj.business_my_referral_type_id = "";
            });
            this.tpStorageService.get('business_my_referral_detail').then(function (storage_res) {
                _this.whitelabelObj.business_my_referral_detail = storage_res;
            }).catch(function (e) {
                _this.whitelabelObj.business_my_referral_detail = "";
            });
            this.tpStorageService.get('businessName').then(function (storage_res) {
                _this.tpStorageService.setItem('whitelabelObj_sponseredbusines_name', storage_res);
                _this.whitelabelObj.sponseredbusines_businessName = storage_res;
            }).catch(function (e) {
            });
        }
        else {
            this.whitelabelObj.showMyBusiness = false;
            //use cache until we get correct result from  internet
            this.tpStorageService.get('whitelabelObj_phone').then(function (storage_myphone) {
                _this.tpStorageService.get('whitelabelObj_displayName').then(function (storage_myname) {
                    _this.whitelabelObj.mobile = _this.formatPhoneNumber(storage_myphone);
                    _this.whitelabelObj.displayName = storage_myname;
                }).catch(function (e) {
                });
            }).catch(function (e) {
            });
            this.tpStorageService.get('whitelabelObj_sponseredbusines_name').then(function (storage_res) {
                _this.whitelabelObj.sponseredbusines_businessName = storage_res;
            }).catch(function (e) {
            });
            //invited_by exists so lets call business details
            //mytodo : check once in a month : because businesses are on monthly subscription so don`t need to check everytime
            //console.log ("CHECKING INVITED BY <<<<<<<<<<:::::::<<<<<<<<");
            this.tpStorageService.getItem('invited_by').then(function (storage_res) {
                //console.log (storage_res);
                if (storage_res) {
                    _this.whitelabelObj.buddy_uid = storage_res;
                    _this.userservice.getBusinessDetailsOther(storage_res).then(function (res) {
                        if (res) {
                            if (res['paid']) {
                                _this.whitelabelObj.sponseredbusines_businessName = res['displayName'];
                                _this.tpStorageService.setItem('whitelabelObj_sponseredbusines_name', res['displayName']);
                                //replace it with my.friends to update :mytodo : save one ajax call
                                _this.userservice.getAnotherUser(storage_res).then(function (res_user) {
                                    _this.whitelabelObj.mobile = _this.formatPhoneNumber(res_user['mobile']);
                                    _this.whitelabelObj.displayName = res_user['displayName'];
                                    _this.tpStorageService.setItem('whitelabelObj_uid', storage_res);
                                    _this.tpStorageService.setItem('whitelabelObj_phone', res_user['mobile']);
                                    _this.tpStorageService.setItem('whitelabelObj_displayName', res_user['displayName']);
                                }).catch(function (err) { });
                            }
                            else {
                                _this.cleanWhiteLabelCache();
                            } //endif
                        }
                        else {
                            _this.cleanWhiteLabelCache();
                        } //endif
                    }).catch(function (err) {
                        _this.cleanWhiteLabelCache();
                    });
                } //endif
            }).catch(function (e) {
            });
            //Check if there is something to be shown at bottom bar
            /*setTimeout(() => {
                this.tpStorageService.get('whitelabelObj_sponseredbusines_name').then((r: any) => {
    
                }).catch(e => {
                });
            }, 500);
            */
            this.whitelabelObj_ShowNoBusiness = true;
        } //endif
    }; //end function
    ChatsPage.prototype.cleanWhiteLabelCache = function () {
        this.tpStorageService.setItem('whitelabelObj_uid', "");
        this.tpStorageService.setItem('whitelabelObj_phone', '');
        this.tpStorageService.setItem('whitelabelObj_displayName', '');
        this.tpStorageService.setItem('whitelabelObj_sponseredbusines_name', '');
        this.whitelabelObj.displayName = '';
        this.whitelabelObj.sponseredbusines_businessName = '';
        this.whitelabelObj.mobile = '';
    };
    ChatsPage.prototype.tpInitilizeFromStorage = function () {
        var _this = this;
        this.tpStorageService.getItem('businessName').then(function (res) {
            if (res) {
                _this.businessName = res;
            }
        }).catch(function (e) {
        });
        this.tpStorageService.getItem('virginityBroken_ChatPage').then(function (res) {
        }).catch(function (e) { _this.virginityBroken_ChatPage = false; });
    };
    ChatsPage.prototype.ionViewWillLeave = function () {
        this.events.unsubscribe('friends');
    };
    ChatsPage.prototype.ngOnDestroy = function () {
        this.events.unsubscribe('friends');
    };
    /*
    ionViewWillEnter
    ionViewDidLoad
    */
    ChatsPage.prototype.makeid = function (length) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    };
    /*
    getMyLink (){
      return 'https://tapally.com/handleotherwise.php?action=l&e='+ this.makeid(3) + btoa(this.useremail) + this.makeid(5);
    }
    Sample :
    https://tapally.com/handleotherwise.php?action=l&e=123bWlzdGxldG9lOTFAZ21haWwuY29t12345
    */
    //amFzQGlubG93cHJpY2UuY29t
    ChatsPage.prototype.openLink = function (email) {
        var str = this.makeid(3) + btoa(email) + this.makeid(5);
        window.open('https://tapally.com/handleotherwise.php?action=l&e=' + str, '_system', 'location=yes');
    };
    ChatsPage.prototype.ionViewDidLoad = function () {
        this.today = new Date();
    };
    ChatsPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        //Run Async function
        setTimeout(function () {
            _this.fnGetLocalCache();
            _this.whitelabelObj = {};
            _this.tpInitilizeFromStorage();
            _this.paidOrNot();
            _this.fcm.getToken();
            _this.events.publish('checkUserStatus');
        }, 1);
    };
    ChatsPage.prototype.initiateMe = function () {
        var _this = this;
        ////console.log ("initiateMe ()");
        this.requestservice.getmyfriends();
        this.myfriends = [];
        //if I am business
        this.tpStorageService.get('useremail').then(function (useremail_res) {
            _this.useremail = useremail_res;
        }).catch(function (e) {
            _this.useremail = "";
        });
        this.events.subscribe('friends', function () {
            ////console.log ("[[[[[[Subscribed to Firebase Friends]]]]]]");
            _this.tempallmygroups = [];
            _this.allmygroups = [];
            _this.myfriends = [];
            _this.myfriendsList = [];
            _this.myfriends = _this.sortdatalist(_this.requestservice.myfriends);
            if (_this.myfriends.length > 0) {
                //Friend found
                _this.tpStorageService.setItem('virginityBroken_ChatPage', '1');
                _this.isData = false;
                //this.convertWithContacts(this.myfriends).then((res: any) => {
                //if (res.length > 0) {
                //////console.log ("Removing Duplicate Friends");
                _this.removeDuplicates(_this.myfriends, 'mobile').then(function (responce) {
                    //////console.log ("Duplicate Friends Removed");
                    ////////console.log ("H6");
                    _this.myfriendsList = [];
                    _this.myfriendsList = responce;
                    _this.doRefresh(0); //indivial
                    /*this.events.subscribe('allmygroups', () => {
                        ////////console.log ("H7");
                        this.events.unsubscribe('allmygroups');
                        this.allmygroups = [];
                        this.allmygroups = this.groupservice.mygroups;
                        this.doRefresh(1);//groups
                    });
                    this.groupservice.getmygroups();
                    */
                });
                //}
                //  })
            }
            else {
                _this.isData = true;
                //there are no friends so let's update the database
                //but first check for the groups .
                /*
                this.events.subscribe('allmygroups', () => {
                ////////console.log ("H8");
                    this.events.unsubscribe('allmygroups');
                    this.allmygroups = [];
                    this.allmygroups = this.groupservice.mygroups;
                    this.doRefresh(1);//
                });
                ////////console.log ("H9");
                this.groupservice.getmygroups();
                */
                //this.loadingProvider.dismissMyLoading();
            }
        });
    };
    ChatsPage.prototype.referralSendForward = function (buddy) {
        // I want to send buddy's referral to someone else
        this.app.getRootNav().push("SendReferralForwardPage", { source: "chatpage", buddy_uid: buddy.uid, buddy_displayName: buddy.displayName });
    };
    ChatsPage.prototype.referralSendBackward = function (buddy) {
        this.app.getRootNav().push("SendReferralBackwardPage", { source: "chatpage", buddy_uid: buddy.uid, buddy_displayName: buddy.displayName }); //
    };
    //no need to send insert statement because contacts page will do that
    ChatsPage.prototype.updateContactsTable = function (db, allChatListing__) {
        if (!allChatListing__.mobile || allChatListing__.mobile == "undefined") {
            return;
        }
        if (allChatListing__.photoURL == null) {
            allChatListing__.photoURL = ""; //mytodo set default pic
        }
        db.executeSql("UPDATE cache_tapally_contacts SET uid='" + allChatListing__.uid + "',photoURL='" + allChatListing__.photoURL + "' where mobile = '" + allChatListing__.mobile + "'", {})
            .then(function (res) { }).catch(function (e) { });
        //https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8193%20-%20Leopard%20Face.png?alt=media&token=ded0bd0d-5400-451e-8ee7-a4a43d1329e5
        //download the images
        setTimeout(function () {
            //console.log (allChatListing__.photoURL);
            //this.downloadFile(allChatListing__.photoURL, 'mytestfile.jpg');
        }, 1000);
        //this.platform.ready().then( _ => {
        //});
    }; //end function
    /*
    https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-file/#where-to-store-files
    */
    ChatsPage.prototype.downloadFile = function (pictureUrl, fileName) {
        fileName = pictureUrl.replace(/[^a-zA-Z0-9]/g, '');
        //pictureUrl = 'https://firebasestorage.googleapis.com/v0/b/tapallytest/o/tmp%2FA-Tiger-After-Marriage-Funny-Image.jpg?alt=media&token=357bca54-7372-4754-9fc8-21ebfcf84dd5';
        var oReq = new XMLHttpRequest();
        oReq.responseType = "blob";
        oReq.open("GET", pictureUrl, true);
        oReq.onload = function (oEvent) {
            var DataBlob = oReq.response; // Note: not oReq.responseText
            if (DataBlob) {
                var myCordova = void 0;
                var defaultLocation_ = '';
                myCordova = cordova;
                if (myCordova && myCordova.file) {
                    defaultLocation_ = myCordova.file.externalDataDirectory;
                } //endif
                var folderPath = defaultLocation_; //cordova.file.dataDirectory;
                window.resolveLocalFileSystemURL(folderPath, function (dir) {
                    dir.getFile(fileName, { create: true }, function (file) {
                        file.createWriter(function (fileWriter) {
                            //fileWriter.seek(fileWriter.length);
                            fileWriter.write(DataBlob);
                            fileWriter.onwrite = function () {
                            };
                            fileWriter.onwriteend = function () {
                                //console.log(">>!!>>>>>>>>>>>>>>>>Successful file write...");
                                //read and verify
                                /*
                                file.file(function (file_) {
                                    var reader = new FileReader();
                                    reader.onloadend = function() {
                                        ////console.log("Successful file read: " + this.result);
                                        ////console.log("file.fullPath" + ": " + file.fullPath);
                                    };
                                    reader.readAsText(file_);
                                });
                                */
                            };
                            fileWriter.onerror = function (e) {
                                //console.log("!!!!!!!!!!!!!!!!Failed file write: " + e.toString());
                            };
                        }, function () {
                            alert('Unable to save file in path ' + folderPath);
                        });
                    });
                });
            }
            else {
                console.error('we didnt get an XHR response!');
            } //endif
        };
        oReq.send(null);
    }; //end function
    ChatsPage.prototype.referralSendForward_ = function (buddy) {
        // I want to send buddy's referral to someone else
        if (!buddy.displayName && buddy.sponseredbusines_businessName) {
            this.app.getRootNav().push("SendReferralForwardPage", { source: "chatpage", buddy_uid: buddy.buddy_uid, buddy_displayName: buddy.sponseredbusines_businessName });
        }
        else {
            this.app.getRootNav().push("SendReferralForwardPage", { source: "chatpage", buddy_uid: buddy.buddy_uid, buddy_displayName: buddy.displayName });
        }
    };
    ChatsPage.prototype.referralSendBackward_ = function (buddy) {
        if (!buddy.displayName && buddy.sponseredbusines_businessName) {
            this.app.getRootNav().push("SendReferralBackwardPage", { source: "chatpage", buddy_uid: buddy.buddy_uid, buddy_displayName: buddy.sponseredbusines_businessName });
        }
        else {
            this.app.getRootNav().push("SendReferralBackwardPage", { source: "chatpage", buddy_uid: buddy.buddy_uid, buddy_displayName: buddy.displayName }); //
        }
    };
    ChatsPage.prototype.sendToEarningsPage = function () {
        this.app.getRootNav().push("EarningsPage");
    };
    ChatsPage.prototype.sendToBroadcastPage = function () {
        this.app.getRootNav().push("BroadcastPage", { broadcast_retry: false });
    };
    //KEep it same as broadcast.ts :
    //If any thing change in this function make sure it is same as broadcast.ts
    ChatsPage.prototype.paidOrNot = function () {
        var _this = this;
        this.showPaidSign = false;
        //////////console.log ("01923i01923");
        this.tpStorageService.getItem('business_created').then(function (business_created_raw) {
            //business is registered
            //mytodo : uncomment following later
            _this.getWhileLabelObj(true);
            //For now this check if disabled (mytodo). But later we need to call below logic like once a day or so.
            //to avoid clogging of network.
            if (!business_created_raw) {
                business_created_raw = 1;
            }
            if (true) {
                //////////console.log ("Going to check paidOrNot");
                /* start */
                //Before we show warning to business that they have not paid yet
                var AcceptableDelayInMicroseconds = 1000 * 60 * 1; //1 minute from registration
                var timeStamp = Date.now();
                var business_created = parseInt(business_created_raw);
                //Do not check for first few minutes/hours/days ( as specified in AcceptableDelayInMicroseconds )
                //But check every single time afterwards
                if ((business_created + AcceptableDelayInMicroseconds) < timeStamp) {
                    if (true) {
                        //////console.log ("Paidornot : calling getBusinessDetails");
                        _this.userservice.getBusinessDetails().then(function (res) {
                            _this.tpStorageService.getItem('paid').then(function (paid_raw) {
                                //Paid flag set : but does not mean if user indeed paid
                                if (paid_raw && paid_raw > 0) {
                                    //paid
                                    _this.showPaidSign = false;
                                }
                                else {
                                    _this.showPaidSign = true;
                                }
                            }).catch(function (e) {
                                //Business did not paid yet
                                _this.showPaidSign = true;
                            });
                            //mytodo : need a better place to put it
                            if (res['paid']) {
                                _this.showPaidSign = false;
                            }
                            else {
                                _this.showPaidSign = true;
                            }
                        }).catch(function (err) {
                            //could not get user info
                        });
                    } //endif
                }
                else {
                } //endif
                /* end */
            }
        }).catch(function (e) {
            //no business registered for this business
            _this.getWhileLabelObj(false);
        });
    }; //end function
    ChatsPage.prototype.fnDeleteAndCreateCacheOfList = function () {
        var _this = this;
        this.sqlite.create({
            name: 'gr.db',
            location: 'default'
        }).then(function (db) {
            //Update  Local Cahce
            db.executeSql(__WEBPACK_IMPORTED_MODULE_11__app_const__["a" /* CONST */].create_table_statement_tapally_friends, {})
                .then(function (res) {
                ////console.log ("DELETE  FROM cache_tapally_friends   ");
                db.executeSql("DELETE  FROM cache_tapally_friends", {}).then(function (res) {
                    _this.isNoRecord = false;
                    var uniqueFriends = {};
                    for (var i = 0; i < _this.allChatListing.length; i++) {
                        var recGotInserted = false;
                        if (_this.allChatListing[i].gpflag > 0) {
                            //Group
                            //do not insert duplicate
                            /*

                            MY TODO : When enabling group, please uncomment this

                            if(uniqueFriends ["group"+this.allChatListing[i].groupName]){
                              continue;
                            } else {
                              uniqueFriends ["group"+this.allChatListing[i].groupName] = true;
                            }

                             db.executeSql("INSERT INTO cache_tapally_friends (gpflag       , lastMessage_message  ,lastMessage_dateofmsg  ,groupimage  ,groupName  , lastmsg  ,dateofmsg,timeofmsg,selectCatId,referral_type  ) VALUES ('"+this.allChatListing[i].gpflag+"','"+this.allChatListing[i].lastmsg+"','"+this.allChatListing[i].dateofmsg+"','"+this.allChatListing[i].groupimage+"','"+this.allChatListing[i].groupName+"','"+this.allChatListing[i].lastmsg+"','"+this.allChatListing[i].dateofmsg+"','"+this.allChatListing[i].timeofmsg+"','"+this.allChatListing[i].selectCatId+"','"+this.allChatListing[i].referral_type+"')", {})
                             .catch(e => {
                                  //////////console.log ("ERROR inserting in cache_tapally_friends");
                                  //////////console.log (e);
                             });
                             //////////console.log ("Done");

                             //Update in ARray as well
                             //mytodo : use uid in groups as well. right now its done by name
                             for(let g=0;g<this.allChatListing_HTML.length;g++){
                                 if(this.allChatListing_HTML[g].groupName == this.allChatListing[i].groupName){

                                    //update photo url
                                    if(this.allChatListing_HTML[g].groupimage != this.allChatListing[i].groupimage){
                                         this.allChatListing_HTML[g].groupimage = this.allChatListing[i].groupimage;
                                    }

                                    this.allChatListing_HTML[g].lastmsg = this.allChatListing[i].lastmsg;
                                    this.allChatListing_HTML[g].dateofmsg = this.allChatListing[i].dateofmsg;
                                    this.allChatListing_HTML[g].timeofmsg = this.allChatListing[i].timeofmsg;
                                    this.allChatListing_HTML[g].selectCatId = this.allChatListing[i].selectCatId;
                                    this.allChatListing_HTML[g].referral_type = this.allChatListing[i].referral_type;

                                    recGotInserted = true;
                                    break;
                                 }
                             }//endfor
                             */
                        }
                        else {
                            //Individual
                            //do not insert duplicate
                            if (uniqueFriends["indivial" + _this.allChatListing[i].uid]) {
                                continue;
                            }
                            else {
                                uniqueFriends["indivial" + _this.allChatListing[i].uid] = true;
                            }
                            ////////console.log ("this.allChatListing[i].gpflag"+this.allChatListing[i].gpflag);
                            //Individual
                            var isactive_ = 0;
                            if (_this.allChatListing[i].isActive) {
                                isactive_ = 1;
                            }
                            var isBlock_ = 0;
                            if (_this.allChatListing[i].isBlock) {
                                isBlock_ = 1;
                            }
                            var selection_ = 0;
                            if (_this.allChatListing[i].selection) {
                                selection_ = 1;
                            }
                            //console.log ("INSERTING INTO cache_tapally_friends");
                            db.executeSql("INSERT INTO cache_tapally_friends (isactive,uid,gpflag,selection,isBlock, unreadmessage  , displayName  ,mobile  ,photoURL   ,                                      lastMessage_message  ,lastMessage_type  ,lastMessage_dateofmsg  ,groupimage  ,groupName  , lastmsg  ,dateofmsg,timeofmsg,selectCatId,referral_type,deviceToken  ) VALUES (" + isactive_ + ",'" + _this.allChatListing[i].uid + "','" + _this.allChatListing[i].gpflag + "','" + selection_ + "'," + isBlock_ + ",0,'" + _this.allChatListing[i].displayName + "','" + _this.allChatListing[i].mobile + "','" + _this.allChatListing[i].photoURL + "','" + _this.allChatListing[i].lastMessage.message + "','" + _this.allChatListing[i].lastMessage.type + "','" + _this.allChatListing[i].lastMessage.dateofmsg + "','" + _this.allChatListing[i].groupimage + "','" + _this.allChatListing[i].groupName + "','" + _this.allChatListing[i].lastmsg + "','" + _this.allChatListing[i].dateofmsg + "','" + _this.allChatListing[i].timeofmsg + "','" + _this.allChatListing[i].selectCatId + "','" + _this.allChatListing[i].referral_type + "','" + _this.allChatListing[i].deviceToken + "')", {})
                                .catch(function (e) {
                                ////////console.log ("ERROR inserting in cache_tapally_friends");
                                ////////console.log (e);
                            });
                            //Also update cache_tapally_contacts
                            _this.updateContactsTable(db, _this.allChatListing[i]);
                            //Update in ARray as well
                            for (var g = 0; g < _this.allChatListing_HTML.length; g++) {
                                if (_this.allChatListing_HTML[g].uid == _this.allChatListing[i].uid) {
                                    //update photo url
                                    if (_this.allChatListing_HTML[g].photoURL != _this.allChatListing[i].photoURL) {
                                        _this.allChatListing_HTML[g].photoURL = _this.allChatListing[i].photoURL;
                                    } //endif
                                    //this.allChatListing_HTML[g].localPhotoURL = '';
                                    //this.getLocalImageIfAvial(res.rows.item(i).photoURL,g);
                                    _this.allChatListing_HTML[g].displayName = _this.allChatListing[i].displayName;
                                    _this.allChatListing_HTML[g].lastMessage = _this.allChatListing[i].lastMessage;
                                    recGotInserted = true;
                                    break;
                                }
                            } //endfor
                        } //endif
                        if (!recGotInserted) {
                            //console.log ("Pushing in allChatListing_HTML");
                            _this.allChatListing_HTML.push(_this.allChatListing[i]);
                        }
                    } //end for
                    ////console.log ("this.allChatListing_HTML.length");
                    ////console.log (this.allChatListing_HTML.length);
                    if (_this.allChatListing_HTML.length == 0) {
                        _this.isNoRecord = true;
                    }
                    //Subscribe to new msg  jaswinder
                    _this.requestservice.subscribeToReadingLastMsg();
                    _this.events.subscribe('new_chat_message', function () {
                        ////console.log ("new_chat_messagenew_chat_messagenew_chat_messagenew_chat_messagenew_chat_message");
                        //mytodo : this event get called too many times. Try switching between tabs "My contacts" & "Get Referral"
                        for (var x in _this.requestservice.lastMsgReader) {
                            for (var g = 0; g < _this.allChatListing_HTML.length; g++) {
                                //////console.log (this.allChatListing_HTML[g]['uid'] + ":"+x);
                                if (_this.allChatListing_HTML[g]['uid'] == x) {
                                    //mytodo : why this is so slow. It takes forever to show on html page
                                    if (_this.requestservice.lastMsgReader[x].request_to_release_incentive) {
                                        _this.requestservice.lastMsgReader[x].message = "Request For Incentive";
                                    }
                                    ////console.log ("GOING TO SET LAST MSG<<<<<<<<<<<");
                                    ////console.log (this.requestservice.lastMsgReader[x].message);
                                    _this.allChatListing_HTML[g].lastMessage = _this.requestservice.lastMsgReader[x];
                                    break;
                                } //endif
                            } //end for
                        } //end for
                    });
                }).catch(function (e) {
                    //Error in operation
                });
            })
                .catch(function (e) {
            }); //create table
        }); //create database
    }; //end function
    ChatsPage.prototype.fnGetLocalCache = function () {
        var _this = this;
        this.sqlite.create({
            name: 'gr.db',
            location: 'default'
        }).then(function (db) {
            //Get Friends
            db.executeSql(__WEBPACK_IMPORTED_MODULE_11__app_const__["a" /* CONST */].create_table_statement_tapally_friends, {})
                .then(function (res) {
                db.executeSql("SELECT * FROM cache_tapally_friends", {})
                    .then(function (res) {
                    ////console.log ("Going to read from  cache_tapally_friends <--------<<<<<-------------");
                    ////console.log (res.rows.length + " Records");
                    if (res.rows.length > 0) {
                        //if row exists
                        ////console.log ("RESETTING allChatListing_HTML = []");
                        _this.allChatListing_HTML = [];
                        var uniqueFriends = {};
                        _this.isNoRecord = false;
                        for (var i = 0; i < res.rows.length; i++) {
                            ////////console.log (res.rows.item(i));
                            var isBlock_ = false;
                            if (res.rows.item(i).isBlock) {
                                isBlock_ = true;
                            }
                            var isActive_ = true;
                            if (!res.rows.item(i).isactive) {
                                isActive_ = false;
                            }
                            var record = {};
                            if (res.rows.item(i).gpflag > 0) {
                                //Group
                                var lastMssg = '';
                                if (res.rows.item(i).lastmsg && res.rows.item(i).lastmsg != "undefined") {
                                    lastMssg = res.rows.item(i).lastmsg;
                                }
                                //Group
                                var tmofmsg = '';
                                if (res.rows.item(i).timeofmsg) {
                                    tmofmsg = res.rows.item(i).timeofmsg;
                                }
                                record = {
                                    uid: res.rows.item(i).uid,
                                    displayName: res.rows.item(i).displayName,
                                    gpflag: res.rows.item(i).gpflag,
                                    isActive: isActive_,
                                    isBlock: isBlock_,
                                    lastmsg: lastMssg,
                                    mobile: res.rows.item(i).mobile,
                                    selection: res.rows.item(i).selection,
                                    unreadmessage: 0,
                                    photoURL: res.rows.item(i).photoURL,
                                    groupimage: res.rows.item(i).groupimage,
                                    groupName: res.rows.item(i).groupName,
                                    dateofmsg: res.rows.item(i).dateofmsg,
                                    timeofmsg: tmofmsg
                                };
                            }
                            else {
                                //////////console.log ("gpflaggpflaggpflaggpflag= 0");
                                //Individual
                                var lastMssg = '';
                                if (res.rows.item(i).lastMessage_message && res.rows.item(i).lastMessage_message != "undefined") {
                                    lastMssg = res.rows.item(i).lastMessage_message;
                                }
                                var tmofmsg = '';
                                if (res.rows.item(i).timeofmsg) {
                                    tmofmsg = res.rows.item(i).timeofmsg;
                                }
                                record = {
                                    uid: res.rows.item(i).uid,
                                    displayName: res.rows.item(i).displayName,
                                    gpflag: res.rows.item(i).gpflag,
                                    isActive: isActive_,
                                    isBlock: isBlock_,
                                    lastMessage: {
                                        message: lastMssg,
                                        type: res.rows.item(i).lastMessage_type,
                                        dateofmsg: res.rows.item(i).lastMessage_dateofmsg,
                                        selectCatId: res.rows.item(i).selectCatId,
                                        referral_type: res.rows.item(i).referral_type,
                                        timeofmsg: tmofmsg,
                                        isRead: false,
                                        isStarred: false
                                    },
                                    mobile: res.rows.item(i).mobile,
                                    selection: res.rows.item(i).selection,
                                    unreadmessage: 0,
                                    photoURL: res.rows.item(i).photoURL,
                                    localphotoURL: '',
                                    groupimage: res.rows.item(i).groupimage,
                                    groupName: res.rows.item(i).groupName,
                                    deviceToken: res.rows.item(i).deviceToken
                                };
                            }
                            ////console.log ("Check1 ");
                            if (res.rows.item(i).gpflag > 1) {
                                //group
                                if (!uniqueFriends["group" + res.rows.item(i).groupimage]) {
                                    ////console.log ("Pushing in this.allChatListing_HTML <");
                                    _this.allChatListing_HTML.push(record);
                                    uniqueFriends["group" + res.rows.item(i).groupimage] = true;
                                }
                            }
                            else {
                                //individual
                                if (!uniqueFriends["individual" + res.rows.item(i).uid]) {
                                    ////console.log ("Pushing in this.allChatListing_HTML <");
                                    var lengthOfArr = _this.allChatListing_HTML.push(record);
                                    //this.getLocalImageIfAvial(res.rows.item(i).photoURL,lengthOfArr-1);
                                    uniqueFriends["individual" + res.rows.item(i).uid] = true;
                                }
                            }
                            //
                            //tmp.push (record);
                        } //end for
                        if (_this.allChatListing_HTML.length == 0) {
                            _this.isNoRecord = true;
                        }
                        //just for testing
                        if (_this.testMode) {
                            _this.fnMakeSomeTestData();
                        }
                        //this.loadingProvider.dismissMyLoading();
                        //console.log ("------++++++--+> The HTML should load by now ");
                        // ////////console.log (tmp );
                        //console.log ("this.allChatListing_HTML");
                        ////console.log (this.allChatListing_HTML);
                    }
                })
                    .catch(function (e) {
                    //Error in operation
                });
            })
                .catch(function (e) {
            }); //create table
        }); //create database
    }; //end function
    //mytodo : test if this load the photourl onload or not.
    //the whole is to read file from localstorage and not use internet to download profile pics
    //but if it still does it then its bad
    ChatsPage.prototype.getMyPhotoURL = function (item) {
        return item.photoURL;
    };
    ChatsPage.prototype.getLocalImageIfAvial = function (remoteImageUrl, index) {
        var localImageUrl = remoteImageUrl;
        var fileName = remoteImageUrl.replace(/[^a-zA-Z0-9]/g, '');
        //these type of logic is required to escape build errors when cordova.file does not exist
        var myCordova;
        var defaultLocation_ = '';
        myCordova = cordova;
        if (myCordova && myCordova.file) {
            defaultLocation_ = myCordova.file.externalDataDirectory;
        } //endif
        //fileName = "mytestfile.jpg";
        var loc_ = defaultLocation_ + "" + fileName;
        loc_ = Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["C" /* normalizeURL */])(loc_);
        /*
        file:///storage/emulated/0/Android/data/com.tapally.tapally/files/mytestfile.jpg
        */
        this.allChatListing_HTML[index].localPhotoURL = loc_;
        //console.log ("*****this.allChatListing_HTML[g].localPhotoURL");
        //console.log (this.allChatListing_HTML[index].localPhotoURL);
        //The following line works perfectly
        //(<any>window).resolveLocalFileSystemURL(defaultLocation_ + "/"+fileName, this.fileExists, this.fileDoesNotExist);
        //return localImageUrl;
    };
    ChatsPage.prototype.fileExists = function (fileEntry) {
        ////console.log (">>>>>>>>File EXISTS : " );
        //this.allChatListing_HTML[g].localPhotoURL = defaultLocation_ + "" +fileEntry.fullPath;
        ////console.log (this.allChatListing_HTML[g].localPhotoURL );
        ////console.log (">>File " + fileEntry.fullPath + " exists!");
    };
    ChatsPage.prototype.fileDoesNotExist = function () {
        //console.log(">>file does not exist");
    };
    ChatsPage.prototype.fnMakeSomeTestData = function () {
        this.allChatListing_HTML = [];
        /*Some Test Data */
        var record1 = {
            uid: "test1",
            displayName: "John Wilson",
            gpflag: 0,
            isActive: 1,
            isBlock: 0,
            lastMessage: {
                message: "Can I send you referral",
                type: "message",
                dateofmsg: "05-02-2019",
                timeofmsg: "",
                isRead: false,
                isStarred: false
            },
            mobile: "1231231234",
            selection: "",
            unreadmessage: 0,
            photoURL: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8198%20-%20Octopus%20Face.png?alt=media&token=8ad94d18-0ef1-4cc0-8bb0-7e319be9c11b"
        };
        var record2 = {
            uid: "test2",
            displayName: "Jack Wong",
            gpflag: 0, isActive: 1, isBlock: 0,
            lastMessage: {
                message: "Send me draft tomorrow",
                type: "message",
                dateofmsg: "", timeofmsg: "", isRead: false, isStarred: false
            },
            mobile: "", selection: "", unreadmessage: 0,
            photoURL: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8189%20-%20Horse%20Face.png?alt=media&token=f68d155d-9ca7-46d3-881c-30d0f41103dc"
        };
        var record3 = {
            uid: "test2",
            displayName: "Amy Johnson",
            gpflag: 0, isActive: 1, isBlock: 0,
            lastMessage: {
                message: "I got a new lead and she seems intersting",
                type: "message",
                dateofmsg: "", timeofmsg: "", isRead: false, isStarred: false
            },
            mobile: "", selection: "", unreadmessage: 0,
            photoURL: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8185%20-%20Giraffe%20Face.png?alt=media&token=6a7d3c69-89ce-411d-8649-cfb7211cc9b6"
        };
        var record4 = {
            uid: "test2",
            displayName: "Peter Jack",
            gpflag: 0, isActive: 1, isBlock: 0,
            lastMessage: {
                message: "Come at 7pm next thursday",
                type: "message",
                dateofmsg: "", timeofmsg: "", isRead: false, isStarred: false
            },
            mobile: "", selection: "", unreadmessage: 0,
            photoURL: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8209%20-%20Turtle%20Face.png?alt=media&token=bdaf2962-5a6a-4823-8388-4f7b9d1ab831"
        };
        var record5 = {
            uid: "test2",
            displayName: "Michael Williams",
            gpflag: 0, isActive: 1, isBlock: 0,
            lastMessage: {
                message: "I have question regarding your product",
                type: "message",
                dateofmsg: "", timeofmsg: "", isRead: false, isStarred: false
            },
            mobile: "", selection: "", unreadmessage: 0,
            photoURL: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8211%20-%20Wolf%20Face.png?alt=media&token=d8654953-fdd6-46f1-b50a-cbceaf3d5cca"
        };
        this.allChatListing_HTML.push(record1);
        this.allChatListing_HTML.push(record2);
        this.allChatListing_HTML.push(record3);
        this.allChatListing_HTML.push(record4);
        this.allChatListing_HTML.push(record5);
        this.allChatListing_HTML.push(record1);
        this.allChatListing_HTML.push(record2);
        this.allChatListing_HTML.push(record3);
        this.allChatListing_HTML.push(record4);
        this.allChatListing_HTML.push(record5);
        this.allChatListing_HTML.push(record1);
        this.allChatListing_HTML.push(record2);
        this.allChatListing_HTML.push(record3);
        this.allChatListing_HTML.push(record4);
        this.allChatListing_HTML.push(record5);
        //this.testData.company = 'Home4You Realtor';
        //this.testData.person = 'John Wilson';
        //this.testData.phone = '416-123-1234';
    };
    ChatsPage.prototype.sendto_invite = function () {
        if (this.businessName) {
            this.app.getRootNav().push("OutreachForBusinessPage");
        }
        else {
            this.app.getRootNav().push("RegisterbusinessPage", { treatNewInstall: 0 });
        }
    };
    ChatsPage.prototype.goToAInfo = function () {
        this.app.getRootNav().push("BusinessinfoPage", { showPaidSign: this.showPaidSign });
    };
    ChatsPage.prototype.goToBInfo = function () {
        this.app.getRootNav().push("BusinessinfootherPage");
    };
    ChatsPage.prototype.goToConsumerInfo = function () {
        this.app.getRootNav().push("ConsumerinfoPage");
    };
    ChatsPage.prototype.checkReferralIncentive = function () {
        this.app.getRootNav().push("CheckreferralincentivePage", { whitelabelObj: this.whitelabelObj });
    };
    ChatsPage.prototype.removeDuplicates = function (originalArray, prop) {
        return new Promise(function (resolve) {
            var newArray = [];
            var lookupObject = {};
            for (var i in originalArray) {
                lookupObject[originalArray[i][prop]] = originalArray[i];
            }
            for (i in lookupObject) {
                newArray.push(lookupObject[i]);
            }
            resolve(newArray);
        }).catch(function (error) {
            ////////console.log ("Error 2938");////////console.log (error)
        });
    };
    ChatsPage.prototype.doRefresh = function (gpFlag__) {
        //////console.log ("doRefresh ()");
        /*this.tpStorageService.getItem('invited_by').then((invited_by_raw: any) => {
            this.doRefreshRaw(gpFlag__,invited_by_raw);
          }).catch(e => {
          this.doRefreshRaw(gpFlag__,"");
        });
        */
        this.doRefreshRaw(gpFlag__);
    };
    ChatsPage.prototype.doRefreshRaw = function (gpFlag__) {
        var _this = this;
        ////console.log ("doRefreshRaw");
        ////////console.log ("doRefreshRaw gpFlag__ = "+gpFlag__);
        //////////console.log ("Do Refersh() - one for contacts and other for groups");
        var tmpalllist = [];
        if (this.allmygroups.length > 0) {
            for (var i_1 = 0; i_1 < this.allmygroups.length; i_1++) {
                this.allmygroups[i_1].gpflag = 1;
                tmpalllist.push(this.allmygroups[i_1]);
            }
        }
        ////////console.log ("this.myfriendsListthis.myfriendsList");
        ////////console.log (this.myfriendsList);
        if (this.myfriendsList.length > 0) {
            for (var i_2 = 0; i_2 < this.myfriendsList.length; i_2++) {
                this.myfriendsList[i_2].gpflag = 0;
                this.myfriendsList[i_2].whitelabelcss = '';
                /*
                if(invited_by_var){
                    if( this.myfriendsList[i].uid == invited_by_var){
                        //this.tpStorageService.setItem('sponseredbusines_name',this.myfriendsList[i].displayName);
                        //this.tpStorageService.setItem('sponseredbusines_mobile',this.myfriendsList[i].mobile);
                    }
                }
                */
                tmpalllist.push(this.myfriendsList[i_2]);
            }
        }
        if (this.selectedContact.length > 0) {
            this.selectedContact = [];
            this.selectCounter = 0;
            this.selectContact = false;
            for (var i = 0; i < this.allChatListing.length; i++) {
                this.allChatListing[i].selection = false;
            }
        }
        setTimeout(function () {
            //////console.log ("Settimeout inside refersh function ");
            if (_this.myfriendsList.length > 0 || _this.allmygroups.length > 0) {
                _this.zone.run(function () {
                    _this.archiveChats = [];
                    _this.allChatListing = [];
                    _this.tempallmygroups = [];
                    _this.counter = [];
                    ////////console.log ("tmpalllist");
                    ////////console.log (tmpalllist);
                    //jaswinder
                    for (var x = 0; x < tmpalllist.length; x++) {
                        if (tmpalllist[x].gpflag > 0) {
                            var ifMatched = false;
                            for (var y = 0; y < _this.allChatListing.length; y++) {
                                //Group
                                if (tmpalllist[x].groupName == _this.allChatListing[y].groupName) {
                                    //this.allChatListing[y].lastmsg = tmpalllist [x].lastmsg;
                                    _this.allChatListing[y] = tmpalllist[x];
                                    ifMatched = true;
                                    break;
                                }
                            } //endfor
                            if (!ifMatched) {
                                _this.allChatListing.push(tmpalllist[x]);
                            }
                        }
                        else {
                            var ifMatched = false;
                            for (var y = 0; y < _this.allChatListing.length; y++) {
                                //Individual
                                if (tmpalllist[x].uid == _this.allChatListing[y].uid) {
                                    //this.allChatListing[y].lastMessage = tmpalllist [x].lastMessage;
                                    _this.allChatListing[y] = tmpalllist[x];
                                    ifMatched = true;
                                    break;
                                }
                            } //endfor
                            if (!ifMatched) {
                                _this.allChatListing.push(tmpalllist[x]);
                            }
                        } //endif
                    } //end for
                    _this.tempallmygroups = _this.allChatListing;
                    _this.fnDeleteAndCreateCacheOfList();
                    //this.loadingProvider.dismissMyLoading();
                });
            }
            else {
                //so no group or friend found. Might be first time or deleted friends/groups. LEts make sure we put that in database
                _this.fnDeleteAndCreateCacheOfList();
            }
        }, 1000);
    };
    ChatsPage.prototype.closeButtonClick = function () {
        //////////console.log ("CloseButton Click");
        if (this.selectedContact.length > 0) {
            this.selectedContact = [];
            this.selectCounter = 0;
            this.selectContact = false;
            for (var i = 0; i < this.allChatListing.length; i++) {
                this.allChatListing[i].selection = false;
            }
        }
    };
    ChatsPage.prototype.CheckLastMSG = function (lastmsg, msg) {
        if (!(msg instanceof Array) && msg.match("^(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|www\\.){1}([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?")) {
            return false;
        }
        else if (!(msg instanceof Array) && lastmsg) {
            return true;
        }
        else {
            return false;
        }
    };
    ChatsPage.prototype.CheckLastMsgUrl = function (url) {
        if (!(url instanceof Array) && url.match("^(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|www\\.){1}([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?")) {
            return true;
        }
        else if ((url instanceof Array) && url.length > 0) {
            return true;
        }
    };
    ChatsPage.prototype.sortdatalist = function (data) {
        return data.sort(function (a, b) {
            var result = null;
            if (a.userprio != null) {
                var dateA = new Date(a.userprio).getTime();
                var dateB = new Date(b.userprio).getTime();
            }
            result = dateA < dateB ? 1 : -1;
            return result;
        });
    };
    ChatsPage.prototype.refreshPage = function (refresher) {
        this.ionViewWillEnter();
        setTimeout(function () {
            refresher.complete();
        }, 500);
    };
    ChatsPage.prototype.SortByDate = function (array) {
        array.sort(function (a, b) {
            if (a === void 0) { a = []; }
            if (b === void 0) { b = []; }
            if (a.lastMessage.timestamp > b.lastMessage.timestamp) {
                return -1;
            }
            else if (a.lastMessage.timestamp < b.lastMessage.timestamp) {
                return 1;
            }
            else {
                return 0;
            }
        });
        return array;
    };
    ChatsPage.prototype.viewPofile = function (url, name) {
        if (this.selectedContact.length == 0) {
            var modal = this.modalCtrl.create("ProfileViewPage", { img: url, name: name });
            modal.present();
        }
    };
    ChatsPage.prototype.initializeContacts = function () {
        //////////console.log ("initializeContacts ()");
        this.allChatListing = this.tempallmygroups;
    };
    ChatsPage.prototype.searchuser = function (ev) {
        this.initializeContacts();
        this.isNoRecord = false;
        var val = ev.target.value;
        if (val && val.trim() != '') {
            this.allChatListing = this.allChatListing.filter(function (item) {
                if (item.displayName) {
                    return (item.displayName.toLowerCase().indexOf(val.toLowerCase().trim()) > -1);
                }
                else if (item.groupName) {
                    return (item.groupName.toLowerCase().indexOf(val.toLowerCase().trim()) > -1);
                }
            });
            if (this.allChatListing.length == 0) {
                this.isNoRecord = true;
            }
        }
    };
    ChatsPage.prototype.setStatus = function () {
        this.chatservice.setstatusUser().then(function (res) {
            if (res) {
            }
        }).catch(function (err) {
        });
    };
    ChatsPage.prototype.presentPopover = function (myEvent) {
        this.app.getRootNav().push("NotificationPage");
        //this.requestcounter = this.myrequests.length;
    };
    ChatsPage.prototype.viewPopover = function (myEvent) {
        var _this = this;
        var popover = this.popoverCtrl.create('PopoverChatPage', { page: '0' });
        popover.present({
            ev: myEvent
        });
        popover.onDidDismiss(function (pages) {
            if (pages != 0 && pages != undefined && pages != "undefined" && pages != null) {
                if (pages.page) {
                    _this.app.getRootNav().push(pages.page);
                }
            }
        });
    };
    ChatsPage.prototype.addbuddy = function () {
        this.app.getRootNav().push('ContactPage', { source: "ImportpPage" });
    };
    /*
    checkInvitedBy (invited_by_business_paid_flag_checked_date_raw){
      this.userservice.getuserdetails().then((res: any) => {
          if(res){
            this.username = res.displayName;
            //////////console.log ("resresresresresresresresresresresresresresresresres");
            //////////console.log (res);
            this.tpStorageService.setItem('invited_by', res.invited_by);
            if(res.invited_by){
                  let AcceptableDelayInMicroseconds:number = 1000; //1 day : Before we check if another business is paid member or not
                  let invited_by_business_paid_flag_checked_date:number = parseInt(invited_by_business_paid_flag_checked_date_raw);
                  let timeStamp:number = Date.now();
                  if(!invited_by_business_paid_flag_checked_date) {
                    invited_by_business_paid_flag_checked_date = 0;
                  }

                  if((invited_by_business_paid_flag_checked_date+AcceptableDelayInMicroseconds)<timeStamp){
                      if(!res.invited_by || res.invited_by != "none"){
                          this.userservice.checkIfBusinessIsPaid(res.invited_by).then((res_invitedby: any) => {
                                  ////////////console.log ("---->");
                                  ////////////console.log (res_invitedby);
                                  this.tpStorageService.setItem('sponseredbusines_businessName',res_invitedby.displayName);
                                  this.tpStorageService.setItem('invited_by_business_paid_flag_checked_date', String(Date.now()));//jaswinder
                                  if(res_invitedby.paid && res_invitedby.paid == "1" ){
                                      this.tpStorageService.setItem('invited_by', res.invited_by);
                                  } else {
                                      //member is not paid so lets not do whitelabeling
                                      this.tpStorageService.setItem('invited_by', "none");
                                      this.tpStorageService.setItem('sponseredbusines_businessName', "");

                                  }
                          });
                      } else {
                              this.tpStorageService.setItem('invited_by', "none");
                              this.tpStorageService.setItem('sponseredbusines_businessName', "");
                      }
                  }//endif
            }
            this.zone.run(() => {
                this.avatar = res.photoURL;
            })
          }
      })
    }
    */
    //profile data
    ChatsPage.prototype.loaduserdetails = function () {
        /*
        this.tpStorageService.getItem('invited_by_business_paid_flag_checked_date').then((invited_by_business_paid_flag_checked_date_raw: any) => {
            this.checkInvitedBy (invited_by_business_paid_flag_checked_date_raw);
            }).catch(e => { this.checkInvitedBy (0); });
        */
    };
    ChatsPage.prototype.logout = function () {
        var _this = this;
        this.chatservice.setStatusOffline().then(function (res) {
            if (res) {
                __WEBPACK_IMPORTED_MODULE_13_firebase___default.a.auth().signOut().then(function () {
                    _this.navCtrl.setRoot('LoginPage');
                });
            }
        });
    };
    ChatsPage.prototype.editname = function () {
        var _this = this;
        var statusalert = this.alertCtrl.create({
            buttons: ['okay']
        });
        var alert = this.alertCtrl.create({
            title: 'Edit Nickname',
            inputs: [{
                    name: 'nickname',
                    placeholder: 'Nickname'
                }],
            buttons: [{
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function (data) {
                    }
                },
                {
                    text: 'Edit',
                    handler: function (data) {
                        if (data.nickname) {
                            _this.userservice.updatedisplayname(data.nickname).then(function (res) {
                                if (res.success) {
                                    statusalert.setTitle('Updated');
                                    statusalert.setSubTitle('Your username has been changed successfully!!');
                                    statusalert.present();
                                    _this.zone.run(function () {
                                        _this.username = data.nickname;
                                    });
                                }
                                else {
                                    statusalert.setTitle('Failed');
                                    statusalert.setSubTitle('Your username was not changed');
                                    statusalert.present();
                                }
                            });
                        }
                    }
                }]
        });
        alert.present();
    };
    ChatsPage.prototype.editimage = function () {
        var _this = this;
        var statusalert = this.alertCtrl.create({
            buttons: ['okay']
        });
        this.imghandler.uploadimage().then(function (url) {
            _this.userservice.updateimage(url).then(function (res) {
                if (res.success) {
                    statusalert.setTitle('Updated');
                    statusalert.setSubTitle('Your profile pic has been changed successfully!!');
                    statusalert.present();
                    _this.zone.run(function () {
                        _this.avatar = url;
                    });
                }
            }).catch(function (err) {
                statusalert.setTitle('Failed');
                statusalert.setSubTitle('Your profile pic was not changed');
                statusalert.present();
            });
        });
    };
    ChatsPage.prototype.deleteAllMessage = function () {
        var _this = this;
        var title;
        var message;
        if (this.selectedContact[0].uid) {
            title = 'Clear Chat';
            message = 'Do you want to clear this chat ?';
        }
        else {
            title = 'Delete Group';
            message = 'Do you want to Delete this group ?';
        }
        var alert = this.alertCtrl.create({
            title: title,
            message: message,
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                    }
                },
                {
                    text: 'Ok',
                    handler: function () {
                        if (_this.selectedContact.length > 0) {
                            for (var j = 0; j < _this.selectedContact.length; j++) {
                                if (_this.selectedContact[j].uid) {
                                    for (var i = 0; i < _this.selectedContact.length; i++) {
                                        _this.chatservice.deleteUserMessages(_this.selectedContact).then(function (res) {
                                            if (res) {
                                                _this.selectedContact = [];
                                                if (_this.selectedContact.length == 0) {
                                                    _this.selectContact = false;
                                                }
                                                for (var k = 0; k < _this.allChatListing.length; k++) {
                                                    _this.allChatListing[k].selection = false;
                                                }
                                            }
                                        });
                                    }
                                }
                                else {
                                    var _loop_1 = function (i) {
                                        var groupname = _this.selectedContact[i].groupName;
                                        _this.groupservice.getgroupInfo(groupname).then(function (ress) {
                                            var owners = _this.groupservice.converanobj(ress.owner);
                                            if (owners.length == 1) {
                                                _this.groupservice.getownership(groupname).then(function (res) {
                                                    if (res) {
                                                        _this.groupservice.deletegroups(groupname).then(function (res) {
                                                            if (res) {
                                                                _this.selectedContact = [];
                                                                if (_this.selectedContact.length == 0) {
                                                                    _this.selectContact = false;
                                                                }
                                                                for (var k = 0; k < _this.allChatListing.length; k++) {
                                                                    _this.allChatListing[k].selection = false;
                                                                }
                                                            }
                                                        });
                                                    }
                                                    else {
                                                        _this.groupservice.leavegroups(groupname).then(function (res) {
                                                            if (res) {
                                                                _this.selectedContact = [];
                                                                if (_this.selectedContact.length == 0) {
                                                                    _this.selectContact = false;
                                                                }
                                                                for (var m = 0; m < _this.allChatListing.length; m++) {
                                                                    _this.allChatListing[m].selection = false;
                                                                }
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                            else {
                                                _this.groupservice.getownership(groupname).then(function (res) {
                                                    if (res) {
                                                        _this.groupservice.leavegroupsowner(groupname).then(function (res) {
                                                            if (res) {
                                                                _this.selectedContact = [];
                                                                if (_this.selectedContact.length == 0) {
                                                                    _this.selectContact = false;
                                                                }
                                                                for (var p = 0; p < _this.allChatListing.length; p++) {
                                                                    _this.allChatListing[p].selection = false;
                                                                }
                                                            }
                                                        });
                                                    }
                                                    else {
                                                        _this.groupservice.leavegroups(groupname).then(function (res) {
                                                            if (res) {
                                                                _this.selectedContact = [];
                                                                if (_this.selectedContact.length == 0) {
                                                                    _this.selectContact = false;
                                                                }
                                                                for (var o = 0; o < _this.allChatListing.length; o++) {
                                                                    _this.allChatListing[o].selection = false;
                                                                }
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    };
                                    for (var i = 0; i < _this.selectedContact.length; i++) {
                                        _loop_1(i);
                                    }
                                }
                            }
                        }
                    }
                }
            ]
        });
        alert.present();
    };
    //(press)="popoveroncontact(item,$event)"
    ChatsPage.prototype.popoveroncontact = function (item, event) {
        if (this.selectedContact.length == 0) {
            item.selection = true;
            this.selectedContact.push(item);
            if (this.selectContact == true) {
                this.selectContact = false;
            }
            else {
                this.selectContact = true;
            }
        }
        this.selectCounter = this.selectedContact.length;
        if (this.selectedContact.length == 0) {
            this.selectContact = false;
            item.selection = false;
        }
    };
    ChatsPage.prototype.testfunction = function () {
        //////console.log ("test123");
    };
    ChatsPage.prototype.buddychat = function (item, event, UnreadCount) {
        //////console.log ("BuddyChat Clicked");
        var animationsOptions = {
            animate: true, direction: "forward"
        };
        if (event.target.classList.contains('avtar-class') || event.target.classList.contains('avtar-img-list')) {
        }
        else {
            if (this.selectedContact.length == 0) {
                this.tpStorageService.setItem('UnreadCount', UnreadCount);
                this.chatservice.initializebuddy(item);
                this.app.getRootNav().push('BuddychatPage', { back_button_pop_on: true }, animationsOptions); //animationsOptions
            }
            else if (this.selectedContact.length == 1) {
                if (this.selectedContact[0].uid == item.uid) {
                    item.selection = false;
                    this.selectedContact = [];
                    this.selectCounter = 0;
                    this.selectContact = false;
                }
                else {
                    this.selectedContact = [];
                    this.selectCounter = 0;
                    this.selectContact = false;
                    for (var i = 0; i < this.allChatListing.length; i++) {
                        this.allChatListing[i].selection = false;
                    }
                    item.selection = true;
                    this.selectedContact.push(item);
                    this.selectCounter = this.selectedContact.length;
                    this.selectContact = true;
                }
            }
            else {
                this.popoveroncontact(item, event);
            }
        }
    };
    ChatsPage.prototype.openchat = function (item, event) {
        //////////console.log ("Open Chat");
        if (this.selectedContact.length == 0) {
            this.groupservice.getintogroup(item.groupName);
            this.app.getRootNav().push('GroupchatPage', { groupName: item.groupName, groupImage: item.groupimage });
        }
        else if (this.selectedContact.length == 1) {
            if (this.selectedContact[0].groupName == item.groupName) {
                item.selection = false;
                this.selectedContact = [];
                this.selectCounter = 0;
                this.selectContact = false;
            }
            else {
                this.selectedContact = [];
                this.selectCounter = 0;
                this.selectContact = false;
                for (var i = 0; i < this.allChatListing.length; i++) {
                    this.allChatListing[i].selection = false;
                }
                item.selection = true;
                this.selectedContact.push(item);
                this.selectCounter = this.selectedContact.length;
                this.selectContact = true;
            }
        }
        else {
            this.popoveroncontact(item, event);
        }
    };
    ChatsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-chats',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/chats/chats.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title  >TapAlly</ion-title>\n\n    <ion-buttons end class="btnInHeaderVeryLast">\n      <button ion-button icon-only (click)="sendToBroadcastPage()">\n        <ion-icon name="megaphone"   class="megaphone"></ion-icon>\n      </button>\n      <button ion-button icon-only (click)="sendToEarningsPage()">\n        <ion-icon  name="checkmark-circle" class="notification"></ion-icon>\n      </button>\n    </ion-buttons>\n\n  </ion-navbar>\n</ion-header>\n<ion-content  >\n\n\n  <ion-refresher (ionRefresh)="refreshPage($event)">\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="Pull to refresh" refreshingSpinner="circles"\n      refreshingText="Refreshing...">\n    </ion-refresher-content>\n  </ion-refresher>\n\n  <ion-list  >\n    <ion-card  class="maincard_bubble" text-center padding *ngIf="!virginityBroken_ChatPage && allChatListing_HTML.length <= 0">\n      <ion-card-content >\n        <ion-card-title>\n          Let\'s Get Started\n          </ion-card-title>\n        <p>\n          Invite Friends and <br>get started exchanging referrals<br>\n        </p>\n        <button style="margin-top:15px;" ion-button block (click)="addbuddy()"> Let\'s Get Started  </button>\n      </ion-card-content>\n   </ion-card>\n\n    <ng-container *ngIf="allChatListing_HTML.length > 0">\n      <div tappable *ngFor="let item of allChatListing_HTML" class="list  "> <!-- (press)="popoveroncontact(item,$event)"  -->\n        <ng-container *ngIf="item.gpflag == 0" class="list  ">\n          <ion-item  class="list-item mainitemlist rippleeffect"\n            [ngClass]="{ \'select-contact\': item.selection}">\n            <ion-avatar tappable item-start class="avtar-class" (click)="buddychat(item,$event,item.unreadmessage)">\n\n              <img *ngIf="item.isBlock == false" class="avtar-img-list"\n                src="{{item.photoURL || getMyPhotoURL(item)}}">\n              <img *ngIf="item.isBlock == true" class="avtar-img-list"\n                src="{{\'assets/imgs/user.png\'}}">\n\n              <!--\n              <img *ngIf="item.isBlock == false" class="avtar-img-list" (click)="viewPofile(item.photoURL || \'assets/imgs/user.png\',item.displayName )"\n                src="{{item.photoURL || \'assets/imgs/user.png\'}}">\n              <img *ngIf="item.isBlock == true" class="avtar-img-list" (click)="viewPofile(\'assets/imgs/user.png\',item.displayName )"\n                src="{{\'assets/imgs/user.png\'}}">\n              -->\n            </ion-avatar>\n            <ion-label class="list " tappable (click)="buddychat(item,$event,item.unreadmessage)"><!-- (click)="buddychat(item,$event,item.unreadmessage)" -->\n              <h2 tappable>{{item.displayName}}</h2>\n              <ng-container *ngIf="item.lastMessage" class="list">\n                <span *ngIf="item.lastMessage.type == \'message\' && !item.lastMessage.message && ( item.lastMessage.selectCatId || item.lastMessage.referral_type)" class="lastMSG">Referral Request</span>\n                <span *ngIf="item.lastMessage.type == \'message\' && item.lastMessage.message" class="lastMSG">{{item.lastMessage.message}}</span>\n                <span *ngIf="item.lastMessage.type == \'image\'" class="lastMSG">\n                  <ion-icon name="camera" class="list"></ion-icon> Photo\n                </span>\n                <span *ngIf="item.lastMessage.type == \'contact\'" class="lastMSG">\n                  <ion-icon name="contact" class="list"></ion-icon> Contact\n                </span>\n                <span *ngIf="item.lastMessage.type == \'audio\'" class="lastMSG">\n                  <ion-icon name="musical-note" class="list"></ion-icon> Audio\n                </span>\n                <span *ngIf="item.lastMessage.type == \'document\'" class="lastMSG">\n                  <ion-icon name="document" class="list"></ion-icon> document\n                </span>\n                <span *ngIf="item.lastMessage.type == \'location\'" class="lastMSG">\n                  <ion-icon name="locate" class="list"></ion-icon> location\n                </span>\n              </ng-container>\n            </ion-label>\n\n            <div tappable class="endItem btnsonthelast " item-end outline>\n                <button ion-button (click)="referralSendBackward(item)" clear item-start> <ion-icon color="dark" class="referralicon"  name="arrow-dropleft-circle"></ion-icon> </button>\n                <button  ion-button (click)="referralSendForward(item)" clear  item-end><ion-icon color="dark" class="referralicon"  name="arrow-dropright-circle"></ion-icon></button>\n            </div>\n            <!--\n            Some other options for later on\n            <div class="endItem hide" item-end outline>\n              <span *ngIf="item.lastMessage" class="list">\n                <p *ngIf="(today |  date:\'dd/MM/yyyy\') != item.lastMessage.dateofmsg" class="list">\n                  {{item.lastMessage.dateofmsg}}</p>\n                <p *ngIf="(today |  date:\'dd/MM/yyyy\') == item.lastMessage.dateofmsg" class="list">\n                  {{item.lastMessage.timeofmsg}}</p>\n              </span>\n              <button class="list">\n                <ion-badge item-end *ngIf="item.unreadmessage " class="list">{{item.unreadmessage}}</ion-badge>\n              </button>\n            </div>\n            -->\n          </ion-item>\n        </ng-container>\n\n        <ion-item *ngIf="item.gpflag == 1" class="list mainitemlist" [ngClass]="{ \'select-contact\': item.selection}">\n          <ion-avatar tappable item-start class="avtar-class">\n            <img class="list" src="{{item.groupimage || \'assets/imgs/user.png\'}}">\n          </ion-avatar>\n          <ion-label tappable class="list" (click)="openchat(item, $event)" >\n            <h2>{{item.groupName}} </h2>\n\n            <ng-container   *ngIf="item.lastmsg" class="list">\n\n              <span *ngIf="item.lastmsg !== \'image\' && item.lastmsg !== \'contact\' && item.lastmsg !== \'audio\' && item.lastmsg !== \'document\' && item.lastmsg !== \'location\' "\n                class="lastMSG">{{item.lastmsg}}</span>\n              <span *ngIf="item.lastmsg == \'image\'" class="lastMSG">\n                <ion-icon name="camera" class="list"></ion-icon> Photo\n              </span>\n              <span *ngIf="item.lastmsg == \'contact\'" class="lastMSG">\n                <ion-icon name="contact" class="list"></ion-icon> Contact\n              </span>\n              <span *ngIf="item.lastmsg == \'audio\'" class="lastMSG">\n                <ion-icon name="musical-note" class="list"></ion-icon> Audio\n              </span>\n              <span *ngIf="item.lastmsg == \'document\'" class="lastMSG">\n                <ion-icon name="document" class="list"></ion-icon> document\n              </span>\n              <span *ngIf="item.lastmsg == \'location\'" class="lastMSG">\n                <ion-icon name="locate" class="list"></ion-icon> location\n              </span>\n            </ng-container>\n\n          </ion-label>\n            <!--\n            <div class="endItem btnsonthelast  " item-end outline>\n                <button ion-button (click)="referralSendBackward(item)" clear item-start> <ion-icon color="dark" class="referralicon"  name="arrow-dropleft-circle"></ion-icon> </button>\n                <button  ion-button (click)="referralSendForward(item)" clear  item-end><ion-icon color="dark" class="referralicon"  name="arrow-dropright-circle"></ion-icon></button>\n            </div>\n            -->\n          <div  class="endItem  " item-end outline>\n            <span *ngIf="item.lastmsg" class="list">\n              <p class="list" *ngIf="(today |  date:\'dd/MM/yyyy\') != item.dateofmsg"> {{item.dateofmsg}}</p>\n              <p class="list" *ngIf="(today |  date:\'dd/MM/yyyy\') == item.dateofmsg"> {{item.timeofmsg}}</p>\n            </span>\n          </div>\n        </ion-item>\n      </div>\n    </ng-container>\n  </ion-list>\n\n\n\n\n</ion-content>\n<ion-footer class="myfooter" tappable  *ngIf="whitelabelObj.buddy_uid && this.whitelabelObj.buddy_uid!=\'undefined\' && whitelabelObj.sponseredbusines_businessName && whitelabelObj.sponseredbusines_businessName!=\'undefined\'" >\n\n  <ion-fab *ngIf="whitelabelObj.buddy_uid && this.whitelabelObj.buddy_uid!=\'undefined\' && whitelabelObj.sponseredbusines_businessName && whitelabelObj.sponseredbusines_businessName!=\'undefined\'"\n  [ngClass]="showPaidSign?\'liftupfab_not_paid\':\'liftupfab_paid\'"  bottom right>\n    <button ion-fab (click)="addbuddy()">\n      <ion-icon ios="md-person-add" md="md-person-add"></ion-icon>\n    </button>\n  </ion-fab>\n\n  <ion-list tappable class="sponseredbusines_ionlist fadeInAnimation_Slow" >\n     <ion-item tappable class="list-item sponseredbusines_ionitem">\n       <div tappable class="sponseredbusines_bottom">\n          <div tappable *ngIf="whitelabelObj.showMyBusiness && showPaidSign" (click)="goToAInfo()" ><p >People you invite can see your ad <ion-icon   class="sponseredbusines_iconhelp" name="information-circle"></ion-icon></p></div>\n          <div tappable *ngIf="whitelabelObj.showMyBusiness && !showPaidSign" (click)="goToAInfo()" ><p >This app is now co-branded as <ion-icon   class="sponseredbusines_iconhelp" name="information-circle"></ion-icon></p></div>\n          <div tappable *ngIf="!whitelabelObj.showMyBusiness" (click)="checkReferralIncentive()" ><p >This app is brought to you by <ion-icon   class="sponseredbusines_iconhelp" name="information-circle"></ion-icon></p></div>\n\n          <!-- This is test data -->\n          <div *ngIf="testMode">Home4You Realtor</div>\n          <div *ngIf="testMode">John Wilson 416-123-1234</div>\n\n          <div *ngIf="!testMode"  >{{whitelabelObj.sponseredbusines_businessName}} </div>\n          <div *ngIf="!testMode"  >{{whitelabelObj.displayName}} {{whitelabelObj.mobile}}</div>\n\n          <div class=" " tappable (click)="checkReferralIncentive()"  *ngIf="whitelabelObj.business_my_referral_type_id && whitelabelObj.business_my_referral_type_id>0"> <ion-badge>Check Referral Incentive </ion-badge> </div>\n\n       </div>\n       <button   ion-button (click)="referralSendBackward_(whitelabelObj)" clear item-end> <ion-icon color="dark" class="referralicon_s"  name="arrow-dropleft-circle"></ion-icon> </button>\n       <button   ion-button (click)="referralSendForward_(whitelabelObj)" clear  item-end><ion-icon color="dark" class="referralicon_s"  name="arrow-dropright-circle"></ion-icon></button>\n     </ion-item>\n  </ion-list>\n  <div *ngIf="showPaidSign" tappable class="notpaidbar   fadeInAnimation_Slow" (click)="openLink(useremail)">In order for co-branding and other features to work, tap here to finish your business registration</div>\n</ion-footer>\n\n<ion-footer  class="myfooter_register"  *ngIf="whitelabelObj_ShowNoBusiness && !(whitelabelObj.buddy_uid && this.whitelabelObj.buddy_uid!=\'undefined\' && whitelabelObj.sponseredbusines_businessName && whitelabelObj.sponseredbusines_businessName!=\'undefined\')" (click)="goToBInfo()" tappable  >\n\n  <ion-fab  [ngClass]="showPaidSign?\'c_liftupfab_paid\':\'c_liftupfab_paid\'"   bottom right>\n    <button ion-fab (click)="addbuddy()">\n      <ion-icon ios="md-person-add" md="md-person-add"></ion-icon>\n    </button>\n  </ion-fab>\n\n  <ion-list class="sponseredbusines_ionlist fadeInAnimation_Slow" >\n     <ion-item class="list-item sponseredbusines_ionitem">\n       <div class="sponseredbusines_bottom">\n          <div tappable ><p>Register your business for greater exposure <ion-icon   class="sponseredbusines_iconhelp" name="information-circle"></ion-icon></p></div>\n       </div>\n     </ion-item>\n  </ion-list>\n\n  <!-- we shouldn`t show the bottom red tap for people who are not businesses -->\n  <div *ngIf="false && showPaidSign" tappable class="notpaidbar   fadeInAnimation_Slow" (click)="openLink(useremail)">In order for co-branding and other features to work, tap here to finish your business registration</div>\n\n</ion-footer>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/chats/chats.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_7__providers_fcm_fcm__["a" /* FcmProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_6__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_contact_contact__["a" /* ContactProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* App */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_sqlite__["a" /* SQLite */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_requests_requests__["a" /* RequestsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_chat_chat__["a" /* ChatProvider */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */],
            __WEBPACK_IMPORTED_MODULE_14__angular_http__["c" /* Http */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["y" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_9__providers_imagehandler_imagehandler__["a" /* ImagehandlerProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_8__providers_groups_groups__["a" /* GroupsProvider */],
            __WEBPACK_IMPORTED_MODULE_12__providers_tpstorage_tpstorage__["a" /* TpstorageProvider */]])
    ], ChatsPage);
    return ChatsPage;
}());

/*
function checkIfFileExists(path){
    // path is the full absolute path to the file.
    (<any>window).resolveLocalFileSystemURL(path, fileExists, fileDoesNotExist);
}
function fileExists(fileEntry){
    //console.log ("File " + fileEntry.fullPath + " exists!");
}
function fileDoesNotExist(){
    //console.log("file does not exist");
}
*/
//# sourceMappingURL=chats.js.map

/***/ })

});
//# sourceMappingURL=61.js.map