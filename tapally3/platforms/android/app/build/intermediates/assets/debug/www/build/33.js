webpackJsonp([33],{

/***/ 799:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapPageModule", function() { return MapPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__map__ = __webpack_require__(873);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MapPageModule = (function () {
    function MapPageModule() {
    }
    MapPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__map__["a" /* MapPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__map__["a" /* MapPage */]),
            ],
        })
    ], MapPageModule);
    return MapPageModule;
}());

//# sourceMappingURL=map.module.js.map

/***/ }),

/***/ 873:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__ = __webpack_require__(409);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_groups_groups__ = __webpack_require__(400);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_user_user__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_chat_chat__ = __webpack_require__(401);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_fcm_fcm__ = __webpack_require__(97);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_loading_loading__ = __webpack_require__(182);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var MapPage = (function () {
    function MapPage(navCtrl, geolocation, groupservice, userservice, viewCtrl, navParams, chatservice, alertCtrl, fcm, loading, platform) {
        this.navCtrl = navCtrl;
        this.geolocation = geolocation;
        this.groupservice = groupservice;
        this.userservice = userservice;
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.chatservice = chatservice;
        this.alertCtrl = alertCtrl;
        this.fcm = fcm;
        this.loading = loading;
        this.platform = platform;
        this.loginuserInfo = [];
        this.location = { lat: "", long: "" };
        this.showheader = true;
        this.buddy = this.navParams.get('buddy');
    }
    MapPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.loading.presentLoading();
        this.gpname = this.navParams.get('gpname');
        this.buddyStatus = this.chatservice.buddyStatus;
        this.userservice.getuserdetails().then(function (res) {
            _this.loginuserInfo = res;
            _this.loading.dismissMyLoading();
        }).catch(function (err) {
        });
        if (this.navParams.get('location')) {
            this.flag = false;
            var latlong = this.navParams.get('location');
            this.latLng = new google.maps.LatLng(latlong.lat, latlong.long);
            this.location.lat = latlong.lat;
            this.location.long = latlong.long;
            setTimeout(function () {
                _this.loadMap();
            }, 300);
        }
        else {
            this.flag = true;
            this.geolocation.getCurrentPosition().then(function (position) {
                _this.location.lat = position.coords.latitude;
                _this.location.long = position.coords.longitude;
                _this.latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                setTimeout(function () {
                    _this.loadMap();
                }, 300);
            }, function (err) {
                if (err.code == 1) {
                    _this.navCtrl.pop();
                }
            });
        }
    };
    MapPage.prototype.backButtonClick = function () {
        this.navCtrl.pop({ animate: true, direction: "forward" });
    };
    MapPage.prototype.ionViewWillLeave = function () {
        this.showheader = false;
    };
    MapPage.prototype.loadMap = function () {
        var mapOptions = {
            center: this.latLng,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
        var marker = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: {
                lat: this.location.lat,
                lng: this.location.long
            }
        });
        console.log(marker);
        this.loading.dismissMyLoading();
    };
    MapPage.prototype.goBack = function () {
        this.loading.dismissMyLoading();
        this.navCtrl.pop();
    };
    MapPage.prototype.share = function () {
        var _this = this;
        var temp = this.navParams.get('flag');
        if (temp == true) {
            this.loading.presentLoading();
            var flag_1 = 0;
            this.groupservice.addgroupmsgmultiple(this.location, this.loginuserInfo, 'location', this.gpname).then(function (res) {
                _this.loading.dismissMyLoading();
                if (res) {
                    flag_1++;
                }
                _this.navCtrl.pop();
            }).catch(function (err) {
                _this.loading.dismissMyLoading();
            });
        }
        else if (temp == false) {
            this.loading.presentLoading();
            this.UnreadMSG = 0;
            this.userservice.getstatus(this.buddy).then(function (res) {
                if (!res) {
                    _this.chatservice.addnewmessagemultiple(_this.location, 'location', _this.buddyStatus).then(function (res) {
                        _this.viewCtrl.dismiss();
                        if (_this.buddyStatus != 'online') {
                            var newMessage = 'You have received a message';
                            _this.fcm.sendNotification(_this.buddy, newMessage, 'chatpage');
                        }
                        if (res) {
                        }
                        _this.loading.dismissMyLoading();
                        _this.navCtrl.pop();
                    }).catch(function (err) {
                        _this.loading.dismissMyLoading();
                        alert(err);
                    });
                }
                else {
                    var alert_1 = _this.alertCtrl.create({
                        message: 'Unblock ' + _this.buddy.displayName + ' to send a message.',
                        buttons: [
                            {
                                text: 'CANCEL',
                                role: 'cancel',
                                handler: function () {
                                }
                            },
                            {
                                text: 'UNBLOCK',
                                handler: function () {
                                    _this.userservice.unblockUser(_this.buddy).then(function (res) {
                                    });
                                }
                            }
                        ]
                    });
                    alert_1.present();
                }
                _this.viewCtrl.dismiss();
            });
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], MapPage.prototype, "mapElement", void 0);
    MapPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-map',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/map/map.html"*/'<ion-header  >\n  <ion-navbar hideBackButton>\n    <div class="header-wrap">\n        <div class="flex-start">\n          <ion-buttons left>\n              <button ion-button (click)="backButtonClick()">\n                   <ion-icon class="backbtncss" name="arrow-dropleft-circle"></ion-icon>\n              </button>\n          </ion-buttons>\n            <div class="header-title">Map</div>\n        </div>\n        <div class="flex-end">\n            <ion-buttons end *ngIf="flag==true">\n                <button ion-button [disabled]="!this.latLng" (click)="share()">Share</button>\n            </ion-buttons>\n        </div>\n    </div>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <div #map id="map"></div>\n</ion-content>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/map/map.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_3__providers_groups_groups__["a" /* GroupsProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["B" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_5__providers_chat_chat__["a" /* ChatProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_6__providers_fcm_fcm__["a" /* FcmProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */]])
    ], MapPage);
    return MapPage;
}());

//# sourceMappingURL=map.js.map

/***/ })

});
//# sourceMappingURL=33.js.map