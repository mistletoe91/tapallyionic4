webpackJsonp([46],{

/***/ 786:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupbuddiesaddPageModule", function() { return GroupbuddiesaddPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__groupbuddiesadd__ = __webpack_require__(860);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var GroupbuddiesaddPageModule = (function () {
    function GroupbuddiesaddPageModule() {
    }
    GroupbuddiesaddPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__groupbuddiesadd__["a" /* GroupbuddiesaddPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__groupbuddiesadd__["a" /* GroupbuddiesaddPage */]),
            ],
        })
    ], GroupbuddiesaddPageModule);
    return GroupbuddiesaddPageModule;
}());

//# sourceMappingURL=groupbuddiesadd.module.js.map

/***/ }),

/***/ 860:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupbuddiesaddPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_requests_requests__ = __webpack_require__(399);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_groups_groups__ = __webpack_require__(400);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var GroupbuddiesaddPage = (function () {
    function GroupbuddiesaddPage(navCtrl, navParams, requestservice, groupservice, platform) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.requestservice = requestservice;
        this.groupservice = groupservice;
        this.platform = platform;
        this.myfriends = [];
        this.groupmembers = [];
        this.tempmyfriends = [];
        this.selectedMember = [];
        this.showheader = true;
    }
    GroupbuddiesaddPage.prototype.backButtonClick = function () {
        this.navCtrl.pop({ animate: true, direction: "forward" });
    };
    GroupbuddiesaddPage.prototype.ionViewDidLoad = function () {
    };
    GroupbuddiesaddPage.prototype.ionViewWillLeave = function () {
    };
    GroupbuddiesaddPage.prototype.ionViewWillEnter = function () {
        this.myfriends = this.requestservice.myfriends;
        this.groupmembers = this.groupservice.currentgroup;
        for (var key in this.groupmembers) {
            for (var friend in this.myfriends) {
                if (this.groupmembers[key].uid === this.myfriends[friend].uid)
                    this.myfriends.splice(this.myfriends.indexOf(this.myfriends[friend]), 1);
            }
        }
        this.tempmyfriends = this.myfriends;
    };
    GroupbuddiesaddPage.prototype.addbuddy = function (buddy) {
        if (this.selectedMember.length == 0) {
            this.selectedMember.push(buddy);
        }
        else {
            var flagofbuddy = false;
            var posionofbuddy = 0;
            for (var i = 0; i < this.selectedMember.length; i++) {
                if (this.selectedMember[i].uid == buddy.uid) {
                    flagofbuddy = true;
                    posionofbuddy = i;
                    break;
                }
                else {
                    flagofbuddy = false;
                }
            }
            if (flagofbuddy == true) {
                this.selectedMember.splice(posionofbuddy, 1);
            }
            else {
                this.selectedMember.push(buddy);
            }
        }
    };
    GroupbuddiesaddPage.prototype.initializeItems = function () {
        this.myfriends = this.tempmyfriends;
    };
    GroupbuddiesaddPage.prototype.searchuser = function (ev) {
        this.initializeItems();
        var val = ev.target.value;
        if (val && val.trim() != '') {
            this.myfriends = this.myfriends.filter(function (item) {
                return (item.displayName.toLowerCase().indexOf(val.toLowerCase().trim()) > -1);
            });
        }
    };
    GroupbuddiesaddPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    GroupbuddiesaddPage.prototype.goNext = function () {
        this.groupservice.tempAddMembers(this.selectedMember, this.groupservice.currentgroupname);
        this.groupservice.getintogroup(this.groupservice.currentgroupname);
        this.groupservice.getgroupmembers();
        this.navCtrl.pop();
    };
    GroupbuddiesaddPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-groupbuddiesadd',template:/*ion-inline-start:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/groupbuddiesadd/groupbuddiesadd.html"*/'<ion-header  >\n  <ion-navbar hideBackButton>\n      <div class="header-wrap">\n        <ion-buttons left>\n            <button ion-button (click)="backButtonClick()">\n                 <ion-icon class="backbtncss" name="arrow-dropleft-circle"></ion-icon>\n            </button>\n        </ion-buttons>\n          <div class="header-title">Add new members</div>\n      </div>\n  </ion-navbar>\n  <ion-searchbar placeholder="Search" class="searchbar" (ionInput)="searchuser($event)">\n  </ion-searchbar>\n  <ng-container *ngIf="selectedMember.length > 0">\n      <ion-list class="group-wrap">\n        <ion-item *ngFor="let item of selectedMember">\n            <ion-avatar class="buddy-pic" item-left>\n                <img src="{{item.photoURL}}">\n            </ion-avatar>\n          <p>{{item.displayName}}</p>\n        </ion-item>\n      </ion-list>\n  </ng-container>\n</ion-header>\n\n\n<!-- <ion-content padding>\n\n  <ion-list class="group-wrap" *ngIf="selectedMember.length > 0">\n    <ion-item *ngFor="let item of selectedMember">\n      <ion-avatar item-left><img src="{{item.photoURL}}"></ion-avatar>\n    </ion-item>\n  </ion-list>\n\n  <ion-list>\n    <ng-container *ngFor="let key of myfriends">\n      <ion-item (click)="addbuddy(key)">\n        <ion-avatar item-left><img src="{{key.photoURL}}"></ion-avatar>\n        <h2>{{key.displayName}}</h2>\n      </ion-item>\n    </ng-container>\n  </ion-list> -->\n  <ion-content padding [ngClass]="selectedMember.length != 0 ?\'group-wrap-active\':\'group-wrap-no-active\'">\n  <ion-list>\n      <ng-container *ngFor="let key of myfriends">\n        <ion-item (click)="addbuddy(key)">\n          <ion-avatar item-left>\n              <img src="{{key.photoURL}}">\n          </ion-avatar>\n          <h2>{{key.displayName}}</h2>\n        </ion-item>\n      </ng-container>\n    </ion-list>\n  <div *ngIf="myfriends?.length == 0" align="center">\n    No result found!\n  </div>\n\n  <ion-fab bottom right>\n    <button ion-fab [disabled]="selectedMember.length == 0" (click)="goNext()">\n      <ion-icon ios="ios-checkmark" md="md-checkmark"></ion-icon>\n    </button>\n  </ion-fab>\n\n</ion-content>\n'/*ion-inline-end:"/Users/macbookpro/Documents/code/app/tapallyionic4/tapally3/src/pages/groupbuddiesadd/groupbuddiesadd.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_requests_requests__["a" /* RequestsProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_groups_groups__["a" /* GroupsProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */]])
    ], GroupbuddiesaddPage);
    return GroupbuddiesaddPage;
}());

//# sourceMappingURL=groupbuddiesadd.js.map

/***/ })

});
//# sourceMappingURL=46.js.map