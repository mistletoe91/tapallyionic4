import { Component, ViewChild } from '@angular/core';

// Ionic native
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// Ionic-angular
import { Nav,Platform, NavController, IonicApp, Events,App } from 'ionic-angular';
import { Subscription } from 'rxjs';

import { CONST} from './const';

// Providers
import { FcmProvider } from '../providers/fcm/fcm';
import { UserProvider } from '../providers/user/user';
import { SQLite,SQLiteObject } from '@ionic-native/sqlite';
import { TpstorageProvider } from '../providers/tpstorage/tpstorage';
//import { tap } from 'rxjs/operators'; //uncomment for push notification

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  rootPage: any;
  @ViewChild('myNav') navCtrl: NavController;
  @ViewChild(Nav) nav: Nav;
  pages: Array<{title: string, component: any}>;
  public onResumeSubscription: Subscription;
  public onPauseSubscription: Subscription;
  public alertShown: boolean = false;

  constructor(public events: Events,
    public fcm: FcmProvider,
    public platform: Platform,
    public statusBar: StatusBar,
    public app: App,
    public ionicApp: IonicApp,
    public splashScreen: SplashScreen,
    private sqlite: SQLite,
    public tpStorageService: TpstorageProvider,
    public userService: UserProvider) {



    // Okay, so the platform is ready and our plugins are available.
    // Here you can do any higher level native things you might need.
    this.platform.ready().then((readySource) => {

    // used for an example of ngFor and navigation
    this.pages = [
        { title: 'Settings', component: "SettingPage" },
        { title: 'Business Settings', component: "RegisterbusinessPage" },
    ];


    //hardware back button
    this.platform.registerBackButtonAction(() => {
           //console.log ("Hardware Back Button");
           const overlayView = this.app._appRoot._overlayPortal._views[0];
           if (overlayView && overlayView.dismiss) {
             overlayView.dismiss();
             return;
           }
           if (this.nav.canGoBack()) {
             this.nav.pop();
           }
           else {
             let view = this.nav.getActive();
              //console.log ("viewview");
              //console.log (view.component);
           }//endif
    });


      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.statusBar.overlaysWebView(false);

      // Listen to incoming messages .
      // Get a FCM token
      //this.fcm.getToken()

      // Listen to incoming messages
      //uncomment for push notification . Also uncomment above required line in header of this file :  for "pipe"
    /*  this.fcm.listenToNotifications().pipe(
        tap(msg => {
          //console.log (" PUSH NOTIFICATION DETECTED");
        })
     );
     */

      //jaswinder disabled for now because of error
      /*
      this.fcm.listenToNotifications().subscribe(data => {
        if (data.wasTapped) {
          //Notification was received on device tray and tapped by the user.
          this.navCtrl.setRoot('ChatsPage');
        } else {
          //Notification was received in foreground. Maybe the user needs to be notified.
        }
      },(err) => {////console.log("error in fcm")});
      */
      this.events.subscribe('checkUserStatus', () => {
        this.onResumeSubscription = platform.resume.subscribe(() => {
          this.fcm.setstatusUser().then((data) => {
          }).catch((error) => {
          });
        },(err) => {});
        this.onPauseSubscription = platform.pause.subscribe(() => {
          this.fcm.setStatusOffline().then((data) => {
          }).catch((error) => {
          });
        },(err) => {});
      });

      //this.dropAllStorage (); 
      this.checkUserAndRedirectUser ();

    });
  }

 dropAllStorage (){
   ////console.log("DROP Everything and start fresh ");
   this.dropTable ('cache_users_local');
   this.dropTable ('cache_tapally_contacts');
   this.dropTable ('cache_tapally_friends');
   this.dropTable ('key_val');
   this.tpStorageService.clear();
 }

    dropTable(tbl) {
        this.sqlite.create({
          name: 'gr.db',
          location: 'default'
        }).then((db: SQLiteObject) => {
          console.log ("DROPPED TABLE "+tbl);
          db.executeSql( "DROP TABLE "+tbl, {})
          .then(res => {
          })
          .catch(e => {
              //Error in operation
          });//create table

        });//create database
   }//end function

  deleteFromTable() {
      this.sqlite.create({
        name: 'gr.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        db.executeSql(CONST.create_table_statement, {})
        .then(res => {
            db.executeSql("DELETE   FROM cache_users_local", {})
            .then(res => {
            })
            .catch(e => {
                //Error in operation
            });
        })
        .catch(e => {
            //Error in operation
        });//create table

      });//create database
 }//end function

 checkUserAndRedirectUser() {
     ////console.log ("CHECK1");

     this.sqlite.create({
       name: 'gr.db',
       location: 'default'
     }).then((db: SQLiteObject) => {
       ////console.log ("CHECK2");
       db.executeSql(CONST.create_table_statement, {})
       .then(res => {
           db.executeSql("SELECT *  FROM cache_users_local", {})
           .then(res => {
                ////console.log ("CHECK 4");
               if(res.rows.length>0){
                       ////console.log (res.rows.item(0));

                       //User exists so go to next page
                       if(!res.rows.item(0).uid || res.rows.item(0).uid == undefined){
                           this.rootPage = 'TutorialPage';
                       }

                       ////console.log ("App component Trying to get userUID");
                       this.tpStorageService.getItem('userUID').then((res: any) => {
                          ////console.log ("??Inside this.tpStorageService.getItem");
                          if (res && res != undefined) {
                            ////console.log ("userUID Found in tpStorageService");
                          } else {
                            ////console.log ("userUID NOT Found in tpStorageService so setting to "+ res.rows.item(0).uid);
                            this.tpStorageService.setItem('userUID', res.rows.item(0).uid);
                          }
                   		}).catch(e => { });
                      this.tpStorageService.getItem('newname').then((res: any) => {
                         if (res && res != undefined) {
                         } else {
                           this.tpStorageService.setItem('newname', res.rows.item(0).user_name);
                         }
                     }).catch(e => { });
                     this.tpStorageService.getItem('useremail').then((res: any) => {
                        if (res && res != undefined) {
                        } else {
                          this.tpStorageService.setItem('useremail', res.rows.item(0).useremail);
                        }
                    }).catch(e => { });

                       //When everything is fine
                       //this.dropTable ('cache_tapally_contacts');
                       this.rootPage = 'TabsPage';  //TabsPage  TutorialPage
                       /*
                       Workflow goes like this --->
                       PhonePage
                       PhoneverfyPage
                       DisplaynamePage
                       TabsPage
                       RegisterbusinessPage
                       */

                       ////console.log ("++++--->");
                       ////console.log (res.rows.item(0).user_name);


                       if(!res.rows.item(0).user_name || res.rows.item(0).user_name == undefined){
                           this.rootPage = 'DisplaynamePage';
                       }
               } else {

                       this.tpStorageService.getItem('userUID').then((userId__: any) => {
                          if(userId__){
                              //A situation when we have userId in storage but not in sqlite
                              this.userService.saveUserIdInSqlLite (userId__);
                              this.rootPage = 'TabsPage';
                          } else {
                              ////console.log ("RECORD DOES NOT EXISTS");
                              this.rootPage = 'TutorialPage';
                          }
                       }).catch(e => {
                          ////console.log ("RECORD DOES NOT EXISTS");
                          this.rootPage = 'TutorialPage';//for_prod
                       });

               }//endif
           })
           .catch(e => {
               //Error in operation
               ////console.log ("ERROR11:");
               ////console.log (e);
           });

       })
       .catch(e => {
           //Error in operation
           ////console.log ("ERROR12:");
           ////console.log (e);
       });//create table

     });//create database
}//end function

    goStraightToRoot() {
       this.nav.setRoot("TabsPage");
    }
    openPage(page) {
      // Reset the content nav to have just this page
      // we wouldn't want the back button to show in this scenario
      if(page.component == "TabsPage"){
        this.nav.setRoot(page.component);
      } else {
        if(page.component == "RegisterbusinessPage"){
          this.nav.push(page.component, {treatNewInstall : 0});
        } else {
          this.nav.push(page.component);
        }
      }

    }
}
