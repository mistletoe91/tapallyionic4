import { Injectable } from '@angular/core';
import { SMS } from '@ionic-native/sms';
import { Platform } from 'ionic-angular';
import { TpstorageProvider } from '../tpstorage/tpstorage'; //mytodo: this should not come here. remove it and put it in parent
@Injectable()
export class SmsProvider {

  simulateSMS;
  options;
  iosDelay = 300;

  constructor(public platform: Platform,  public sms: SMS,public tpStorageService: TpstorageProvider) {
      this.simulateSMS = false;
      this.options =   {
            replaceLineBreaks: false, // true to replace \n by a new line, false by default
            android: {
                intent: 'INTENT'  // send SMS with the native android SMS messaging
                //intent: '' // send SMS without opening any other app
            }
        };
  }


  sendSmsCustomMsg(mobile,msg){
    return new Promise((resolve,reject)=>{
      if(this.simulateSMS){
          resolve(true);
      } else {
          try{


            //timeout to prevent issue we are getting on IOS device. Keyboard dissapear : https://github.com/cordova-sms/cordova-sms-plugin/issues/22
            if (this.platform.is('ios')) {
              setTimeout(function(){
                this.sms.send(mobile,msg,this.options);
              }, this.iosDelay);
              resolve(true);
            } else {
              this.sms.send(mobile,msg,this.options).then((res)=>{
                resolve(true)
              }).catch((err)=>{
                reject(err);
              })
            }//endif
              

          }catch(e){
          }
      }//endif
    })
  }//end function

  sendSms(mobileIn){
    let mobile = mobileIn.toString();
    return new Promise((resolve,reject)=>{
        this.tpStorageService.getItem('business_created').then((business_created_raw: any) => {
              //business created
              let msg = "Hi, I'm offering referral incentives. Please download my app at tapally.com and start earning";

              //timeout to prevent issue we are getting on IOS device. Keyboard dissapear : https://github.com/cordova-sms/cordova-sms-plugin/issues/22
              if (this.platform.is('ios')) {
                setTimeout(function(){
                  this.sms.send(mobile,msg,this.options);
                }, this.iosDelay);
                resolve(true);
              } else {
                this.sms.send(mobile,msg,this.options).then((res)=>{
                  resolve(true)
                }).catch((err)=>{
                  reject(err);
                })
              }//endif

        }).catch(e => {
              //no business registered for this user
              let msg = "Hi, please download app at TapAlly.com and start earning";

              //timeout to prevent issue we are getting on IOS device. Keyboard dissapear : https://github.com/cordova-sms/cordova-sms-plugin/issues/22
              if (this.platform.is('ios')) {
                setTimeout(function(){
                  this.sms.send(mobile,msg,this.options);
                }, this.iosDelay);
                resolve(true);
              } else {
                this.sms.send(mobile,msg,this.options).then((res)=>{
                  resolve(true)
                }).catch((err)=>{
                  reject(err);
                })
              }//endif
        });
  });
}//end function

}//end class
