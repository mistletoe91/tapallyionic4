import { Injectable } from '@angular/core';
import { LoadingController, ActionSheetController, ToastController, Platform } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import firebase from 'firebase';
import { LoadingProvider } from '../../providers/loading/loading';
import { ImagePicker } from '@ionic-native/image-picker';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { MediaCapture, MediaFile } from '@ionic-native/media-capture';

import { TpstorageProvider } from '../../providers/tpstorage/tpstorage';
import { Chooser } from '@ionic-native/chooser';


@Injectable()
export class ImagehandlerProvider {
  nativepath: any;
  firestore = firebase.storage();
  userId;
  constructor(

    public loadingCtrl: LoadingController,
    public loadingProvider: LoadingProvider,
    private camera: Camera,
    public actionSheetCtrl: ActionSheetController,
    public toastCtrl: ToastController,
    private imagePicker: ImagePicker,
    public pf: Platform,
    private iab: InAppBrowser,
    private mediaCapture: MediaCapture,
    private chooser: Chooser,
    public tpStorageService: TpstorageProvider
  ) {

    this.tpStorageService.getItem('userUID').then((res: any) => {
			 if(res){
				 this.userId = res;
			 } else {
         this.userId = firebase.auth().currentUser.uid;
       }
		}).catch(e => { });

  }
  uploadimage() {
    return new Promise((resolve, reject) => {
      let actionSheet = this.actionSheetCtrl.create({
        title: 'Select Image From',
        buttons: [{
          text: 'Load from Gallery',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY, this.camera.DestinationType.FILE_URI).then((data) => {
              resolve(data)
            }).catch((err) => {
              reject(err)
            })
          }
        }, {
          text: 'Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA, this.camera.DestinationType.FILE_URI).then((data) => {
              resolve(data)
            }).catch((err) => {
              reject(err)
            })
          }
        }, {
          text: 'Cancel',
          role: 'cancel'
        }
        ]
      });
      actionSheet.present();
    })
  }
  takePicture(sourceType, destinationType) {
    let options = {
      quality: 100,
      sourceType: sourceType,
      destinationType: destinationType,
      saveToPhotoAlbum: false,
      allowEdit: true,
      targetWidth: 500,
      targetHeight: 500,
      correctOrientation: true
    };
    return new Promise((resolve, reject) => {
      this.camera.getPicture(options).then((url) => {
        this.loadingProvider.presentLoading();


        (<any>window).resolveLocalFileSystemURL(url, (res) => {
          res.file((resFile) => {
            let reader = new FileReader();
            reader.readAsArrayBuffer(resFile);
            reader.onloadend = (evt: any) => {
              var imgBlob = new Blob([evt.target.result], { type: 'image/jpeg' });
              var imageStore = this.firestore.ref('/profileimages').child(this.userId);
              imageStore.put(imgBlob).then((res) => {
                this.firestore.ref('/profileimages').child(this.userId).getDownloadURL().then((url) => {
                  resolve(url);
                }).catch((err) => {
                  reject(err);
                })
              }).catch((err) => {
                reject(err);
              })
            }
          })
        })
      }, (err) => {
        this.presentToast('Error while selecting image.');
        reject(err);
      });
    })
  }
  getAllIomage() {
    return new Promise((resolve, reject) => {
      this.firestore.ref('/picmsgs').child(this.userId).getDownloadURL().then((url) => {
      })
    })
  }
  picmsgstore() {
    return new Promise((resolve, reject) => {
      var options = {
        quality: 100,
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        destinationType: this.camera.DestinationType.FILE_URI,
        saveToPhotoAlbum: false,
        allowEdit: true,
        targetWidth: 500,
        targetHeight: 500,
        correctOrientation: true
      };
      this.imagePicker.getPictures(options).then((results) => {
        var imageName = '';
        let imageCounter = 0;
        let responseArr = [];
        if (results.length > 0) {
          this.loadingProvider.presentLoading();
          for (var i = 0; i < results.length; i++) {
            if (this.pf.is('ios')) {
              imageName = 'file://' + results[i];
            } else {
              imageName = results[i];
            }
            (<any>window).resolveLocalFileSystemURL(imageName, (res) => {
              res.file((resFile) => {
                var reader = new FileReader();
                reader.readAsArrayBuffer(resFile);
                reader.onloadend = (evt: any) => {
                  var imgBlob = new Blob([evt.target.result], { type: 'image/jpeg' });
                  var uuid = this.guid();
                  var imageStore = this.firestore.ref('/picmsgs').child(this.userId).child('picmsg' + uuid);
                  imageStore.put(imgBlob).then((res) => {
                    imageCounter++;
                    responseArr.push(res.downloadURL);
                    if (results.length == imageCounter) {
                      this.loadingProvider.dismissMyLoading();
                      resolve(responseArr);
                    }
                  }).catch((err) => {
                    this.loadingProvider.dismissMyLoading();
                    reject(err);
                  }).catch((err) => {
                    this.loadingProvider.dismissMyLoading();
                    reject(err);
                  })
                }
              })
            }, (err) => {
              this.loadingProvider.dismissMyLoading();
            })
          }
        } else {
          resolve(true)
        }
      }).catch((err) => {
        this.loadingProvider.dismissMyLoading();
      })
    }).catch((err) => {
    })
  }
  cameraPicmsgStore() {
    return new Promise((resolve, reject) => {
      // Create options for the Camera Dialog
      var options = {
        quality: 100,
        sourceType: this.camera.PictureSourceType.CAMERA,
        destinationType: this.camera.DestinationType.FILE_URI,
        saveToPhotoAlbum: false,
        allowEdit: true,
        targetWidth: 500,
        targetHeight: 500,
        correctOrientation: true
      };
      this.camera.getPicture(options).then((url) => {
        this.loadingProvider.presentLoading();
        (<any>window).resolveLocalFileSystemURL(url, (res) => {
          res.file((resFile) => {
            var reader = new FileReader();
            reader.readAsArrayBuffer(resFile);
            reader.onloadend = (evt: any) => {
              var imgBlob = new Blob([evt.target.result], { type: 'image/jpeg' });
              var uuid = this.guid();
              var imageStore = this.firestore.ref('/picmsgs').child(this.userId).child('picmsg' + uuid);
              imageStore.put(imgBlob).then((res) => {
                this.loadingProvider.dismissMyLoading();
                resolve(res.downloadURL);
              }).catch((err) => {
                this.loadingProvider.dismissMyLoading();
                reject(err);
              }).catch((err) => {
                this.loadingProvider.dismissMyLoading();
                reject(err);
              })
            }
          })
        })
      })
      this.loadingProvider.dismissMyLoading();
    })

  }
  guid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
  }
  presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }
  gpuploadimage(groupname) {
    return new Promise((resolve, reject) => {
      let actionSheet = this.actionSheetCtrl.create({
        title: 'Select Image From',
        buttons: [{
          text: 'Load from Gallery',
          handler: () => {
            this.gptakePicture(this.camera.PictureSourceType.PHOTOLIBRARY, this.camera.DestinationType.FILE_URI, groupname).then((data) => {
              resolve(data)
            }).catch((err) => {
              reject(err)
            })
          }
        }, {
          text: 'Camera',
          handler: () => {
            this.gptakePicture(this.camera.PictureSourceType.CAMERA, this.camera.DestinationType.FILE_URI, groupname).then((data) => {
              resolve(data)
            }).catch((err) => {
              reject(err)
            })
          }
        }, {
          text: 'Cancel',
          role: 'cancel'
        }
        ]
      });
      actionSheet.present();
    })
  }
  gptakePicture(sourceType, destinationType, groupname) {
    // Create options for the Camera Dialog
    var options = {
      quality: 100,
      sourceType: sourceType,
      destinationType: destinationType,
      saveToPhotoAlbum: false,
      allowEdit: true,
      targetWidth: 500,
      targetHeight: 500,
      correctOrientation: true
    };

    return new Promise((resolve, reject) => {
      this.camera.getPicture(options).then((url) => {
        this.loadingProvider.presentLoading();
        (<any>window).resolveLocalFileSystemURL(url, (res) => {
          res.file((resFile) => {
            var reader = new FileReader();
            reader.readAsArrayBuffer(resFile);
            reader.onloadend = (evt: any) => {
              var imgBlob = new Blob([evt.target.result], { type: 'image/jpeg' });
              var imageStore = this.firestore.ref('/groupimages').child(this.userId).child(groupname);
              imageStore.put(imgBlob).then((res) => {
                this.firestore.ref('/groupimages').child(this.userId).child(groupname).getDownloadURL().then((url) => {
                  resolve(url);
                }).catch((err) => {
                  this.loadingProvider.dismissMyLoading();
                  reject(err);
                })
              }).catch((err) => {
                this.loadingProvider.dismissMyLoading();
                reject(err);
              })
            }
          })
        })
      }, (err) => {
        reject(err);
      });
    })
  }
  get_url_extension(url) {
    return url.split(/\#|\?/)[0].split('.').pop().trim();
  }
  selectDocument() {
    return new Promise((resolve, reject) => {
      this.chooser.getFile('application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet').then((url: any) => {
        if ((url.mediaType == 'application/octet-stream' && this.get_url_extension(url.uri) == 'txt') || (url.mediaType == 'application/octet-stream' && this.get_url_extension(url.uri) == 'xls') || (url.mediaType == 'application/octet-stream' && this.get_url_extension(url.uri) == 'xlsx') ||
        (url.mediaType == 'application/octet-stream' && this.get_url_extension(url.uri) == 'doc') ||
        (url.mediaType == 'application/octet-stream' && this.get_url_extension(url.uri) == 'docx') ||
        (url.mediaType == 'application/octet-stream' && this.get_url_extension(url.uri) == 'pdf') ||    url.mediaType == 'application/pdf' || url.mediaType == 'application/msword'|| url.mediaType == 'application/vnd.ms-excel' || url.mediaType == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' || url.mediaType == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
          this.loadingProvider.presentLoading();
          (<any>window).resolveLocalFileSystemURL(url.uri, (res) => {
            res.file((resFile) => {
              var reader = new FileReader();
              reader.readAsArrayBuffer(resFile);
              reader.onloadend = (evt: any) => {
                var imgBlob = new Blob([evt.target.result], { type: 'document' });
                var uuid = this.guid();
                var imageStore = this.firestore.ref('/docs').child(this.userId).child('doc' + uuid);
                imageStore.put(imgBlob).then((res) => {
                  this.loadingProvider.dismissMyLoading();
                  resolve(res.downloadURL);
                }).catch((err) => {
                  this.loadingProvider.dismissMyLoading();
                  reject(err);
                }).catch((err) => {
                  this.loadingProvider.dismissMyLoading();
                  reject(err);
                })
              }
            })
          })
        } else {
          this.presentToast('Upload only document files.');
        }
      }).catch((err) => {
        this.loadingProvider.dismissMyLoading();

      })
    })
  }
  openDocument(path) {
    return new Promise((resolve, reject) => {
      const browser = this.iab.create(path, "_system", { location: "yes" });
      browser.on('loadstop').subscribe(event => {
        resolve("File opened");
      });

    })
  }

  recordAudio() {
    return new Promise((resolve, reject) => {
      this.mediaCapture.captureAudio().then((data: MediaFile[]) => {
        this.loadingProvider.presentLoading();
        (<any>window).resolveLocalFileSystemURL(data[0]["localURL"], (res) => {
          res.file((resFile) => {
            var reader = new FileReader();
            reader.readAsArrayBuffer(resFile);
            reader.onloadend = (evt: any) => {
              var imgBlob = new Blob([evt.target.result], { type: 'audio' });
              var uuid = this.guid();
              var imageStore = this.firestore.ref('/audios').child(this.userId).child('audio' + uuid);
              imageStore.put(imgBlob).then((res) => {
                this.loadingProvider.dismissMyLoading();
                resolve(res.downloadURL);

              }).catch((err) => {
                this.loadingProvider.dismissMyLoading();
                reject(err);
              }).catch((err) => {
                this.loadingProvider.dismissMyLoading();
                reject(err);
              })
            }
          })
        })
      })
      this.loadingProvider.dismissMyLoading();
    })
  }
}
