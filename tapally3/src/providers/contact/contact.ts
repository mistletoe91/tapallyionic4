import { Injectable } from '@angular/core';
import { Contacts, Contact, ContactField } from '@ionic-native/contacts';
import {   Events   } from 'ionic-angular';
import { SQLite,SQLiteObject } from '@ionic-native/sqlite';
import { CONST} from '../../app/const';
@Injectable()
export class ContactProvider {
    contactInsertCounter = 0;
    contactErrorCounter = 0;
    contactTotalCounter = 0;
    contactInsertingDone = false;
    ContactCounterMargin = 40; //margin of error when calculating insert/error and total counter
    constructor(public events: Events,public contacts: Contacts,private sqlite: SQLite) {
         //this.deleteFromTable ();
    }

    deleteFromTable() {
        this.sqlite.create({
          name: 'gr.db',
          location: 'default'
        }).then((db: SQLiteObject) => {
          db.executeSql(CONST.create_table_statement_tapally_contacts, {})
          .then(res => {
              db.executeSql("DELETE FROM cache_tapally_contacts", {})
              .then(res => {
                 console.log ("table cache_tapally_contacts dropped ");
              })
              .catch(e => {
                  //Error in operation
                  //console.log ("ERROR 101");
                  //console.log (e);
              });
          })
          .catch(e => {
              //Error in operation
              //console.log ("ERROR 102");
              //console.log (e);
          });//create table

        });//create database
   }//end function

  getSimJsonContacts() {
    var options = {
        filter: "",
        multiple: true,
        hasPhoneNumber: true
    };
    return new Promise((resolve, reject) => {
        this.sqlite.create({
            name: 'gr.db',
            location: 'default'
        }).then((db: SQLiteObject) => {
            db.executeSql(CONST.create_table_statement_tapally_contacts, {})
            .then(res => {
                db.executeSql("SELECT *  FROM cache_tapally_contacts order by displayName", {})
                .then(res => {
                       if(res.rows.length>0){
                            let mainArray = {};
                            for (var i = 0; i < res.rows.length; i++) {
                              mainArray[res.rows.item(i).displayName+'_'+res.rows.item(i).mobile] = res.rows.item(i);
                            }//end for
                            resolve({contacts:mainArray, fromcache:true});


                    } else {
                        //No contact found. LEts get from contact list
                        this.contacts.find(["phoneNumbers", "displayName"], options).then((phone_contacts) => {
                            if (phone_contacts != null) {
                                 resolve(this.onSuccessSqlLite(phone_contacts,db));
                            }
                        }).catch((err) => {
                            reject(err);
                        });
                    }//endif
                }).catch(e => {reject();});
            }).catch(e => {reject();});
        }).catch(e => {reject();});

      }).catch((error) => {
        console.log ("Error 81273 " ) ;
    });
    }//end function

    CheckForUpdatedContacts (){
      var options = {
          filter: "",
          multiple: true,
          hasPhoneNumber: true
      };
      return new Promise((resolve, reject) => {
      this.sqlite.create({
          name: 'gr.db',
          location: 'default'
      }).then((db: SQLiteObject) => {
          db.executeSql(CONST.create_table_statement_tapally_contacts, {})
          .then(res => {
              db.executeSql("SELECT *  FROM cache_tapally_contacts", {})
              .then(res => {
                  if(res.rows.length>0){

                    ////console.log ("Finding contacts in phone ")
                    this.contacts.find(["phoneNumbers", "displayName"], options).then((phone_contacts) => {
                        if (phone_contacts.length>res.rows.length) {
                             //If contact on phone are more than what in our DB
                             //console.log (">>>>contact on phone are more than what in our DB");
                             //console.log (phone_contacts.length+"::::::"+res.rows.length);
                             this.onSuccessSqlLite(phone_contacts,db);
                             //Uncommend following if returning value
                             //resolve(this.onSuccessSqlLite(phone_contacts,db));
                        } else {
                             console.log ("Resolve : No new contacts found  ")
                             resolve(true);
                             //Uncommend following if returning value
                             //resolve({contacts: phoneContactsJson});
                        }
                    }).catch((err) => {
                        resolve(true);
                        //Uncommend following if returning value
                        //resolve({contacts: phoneContactsJson});
                    });

                  }//endif
              }).catch(e => {});
          }).catch(e => {});
      }).catch(e => {});
    }).catch((error) => { console.log ("Error 2371"); });

  }//end function


  formatPhoneNumber(phoneNumberString) {
		var cleaned = ('' + phoneNumberString).replace(/\D/g, '')
		var match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/)
		if (match) {
			var intlCode = (match[1] ? '+1 ' : '')
			return [intlCode, '(', match[2], ') ', match[3], '-', match[4]].join('')
		}
		return ""
	}


    // Call   contact res
    onSuccessSqlLite(contacts,db) {
        //console.log ("Came hereee : " + contacts.length);
        var phoneContactsJson = {};
        //sort by name
        /*
        contacts = contacts.sort(function (a, b) {
          if (a.displayName != undefined) {
            var nameA = a.displayName.toUpperCase(); // ignore upper and lowercase
          }
          if (b.displayName != undefined) {
            var nameB = b.displayName.toUpperCase(); // ignore upper and lowercase
          }
          if (nameA < nameB) {
            return -1;
          }
          if (nameA > nameB) {
            return 1;
          }
          // names must be equal
          return 0;
        });
        */

        for (var i = 0; i < contacts.length; i++) {
            ////console.log ("Itetrating "+i);
            let no = "";
            if (contacts[i].name && contacts[i].name != null && contacts[i].name != undefined) {
                no = contacts[i].name.formatted;
            }
            var phonenumber = contacts[i].phoneNumbers;
            if (phonenumber != null) {

                for (var x = 0; x < phonenumber.length; x++) {
                  if (true) {

                      let phone = phonenumber[x].value;
                      let tempMobile;
                      tempMobile = phone.replace(/[^0-9]/g, "");

                      let mobile;
                      if (tempMobile.length > 10) {
                          mobile = tempMobile.substr(tempMobile.length - 10);
                      } else {
                          mobile = tempMobile;
                      }
                      var contactData = {
                          "_id": mobile,
                          "displayName": no,
                          "mobile": mobile,
          			 					"mobile_formatted": this.formatPhoneNumber(mobile),
          			 					"status": "",
          			 					"isBlock":false,
          			 					"isUser":0,
          			 					"isdisable":false,
          			 					"photoURL":"",
                          "uid":""
                      }

                      if(contactData.displayName && contactData.displayName != "undefined" && contactData.mobile && contactData.mobile != "undefined"  ){
                        phoneContactsJson [mobile] = contactData;
                      }
                  }//endif
                }//end for
            }//endif
        }//end for

        //Now insert in db
        this.contactInsertCounter = 0;
        this.contactTotalCounter= 0;
        this.contactErrorCounter = 0;
        this.contactInsertingDone = false;
        Object.keys(phoneContactsJson).forEach(element => {
                  phoneContactsJson[element].mobile = phoneContactsJson[element].mobile.replace(/'/g, "`");//replace the '
                  db.executeSql("INSERT INTO cache_tapally_contacts (id,displayName,mobile,mobile_formatted,isUser) VALUES('"+phoneContactsJson[element].mobile+"','"+phoneContactsJson[element].displayName+"','"+phoneContactsJson[element].mobile+"','"+phoneContactsJson[element].mobile_formatted+"',0)", {})
                  .then(res1 => {
                    this.contactInsertCounter++;
                    this.calculateAndMarkDone ();
                  }).catch((err) => {
                    this.contactErrorCounter++;
                    this.calculateAndMarkDone ();
                  });
              this.contactTotalCounter++;
        });//end loop
        this.calculateAndMarkDone ();

        var collection = {
            contacts: phoneContactsJson,
            fromcache:false
        };
        return collection;
    }

    calculateAndMarkDone (){
      if(this.contactInsertCounter+this.contactErrorCounter+this.ContactCounterMargin>=this.contactTotalCounter){
            this.contactInsertingDone = true;
            //console.log ("Going to publish contactInsertingDone : "+this.contactTotalCounter);
            //console.log (this.contactInsertCounter+this.contactErrorCounter+this.ContactCounterMargin);
            //console.log ("----------------");
            this.events.publish('contactInsertingDone');
      }
    }

    removeDuplicates(originalArray: any[], prop) {
  		let newArray = [];
  		let lookupObject = {};

  		originalArray.forEach((item, index) => {
  			lookupObject[originalArray[index][prop]] = originalArray[index];
  		});

  		Object.keys(lookupObject).forEach(element => {
  			newArray.push(lookupObject[element]);
  		});
  		return newArray;
  	}

    // Call onSuccess contact res : might be unused
    onSuccess(contacts) {
        var phoneContactsJson = [];
        for (var i = 0; i < contacts.length; i++) {
            let no;
            if (contacts[i].name != null) {
                no = contacts[i].name.formatted;

            }
            var phonenumber = contacts[i].phoneNumbers;
            if (phonenumber != null) {

                var type = phonenumber[0].type;
                if (type == 'mobile' || type == 'work' || type == 'home') {

                    let phone = phonenumber[0].value;
                    let tempMobile;
                    tempMobile = phone.replace(/[^0-9]/g, "");

                    let mobile;
                    if (tempMobile.length > 10) {
                        mobile = tempMobile.substr(tempMobile.length - 10);
                    } else {
                        mobile = tempMobile;
                    }
                    var contactData = {
                        "_id": mobile,
                        "displayName": no,
                        "mobile": mobile,
                        "isUser": '0'
                    }
                    phoneContactsJson.push(contactData);
                }
            }
        }
        var collection = {
            contacts: phoneContactsJson
        };

        return collection;
    }
    addContact(newContact: any) {
        return new Promise((resolve, reject) => {
            let contact: Contact = this.contacts.create();
            contact.displayName = newContact.displayName;
            let field = new ContactField();
            field.type = 'mobile';
            field.value = newContact.phoneNumber;
            field.pref = true;
            var numberSection = [];
            numberSection.push(field);
            contact.phoneNumbers = numberSection;
            contact.save().then((value) => {
                resolve(true);
            }, (error) => {
                reject(error);
            })
        }).catch((error) => {});
    }
}
