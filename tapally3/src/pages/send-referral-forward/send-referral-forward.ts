import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TpstorageProvider } from '../../providers/tpstorage/tpstorage';
@IonicPage()
@Component({
  selector: 'page-send-referral-forward',
  templateUrl: 'send-referral-forward.html',
})
export class SendReferralForwardPage {

  buddy_displayName;
  buddy_uid;
  sourcePage;
  showheader:boolean = true;
  thisIsMe:boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams,public tpStorageService: TpstorageProvider) {

    this.tpStorageService.get('userUID').then((myUID: any) => {
      this.buddy_displayName = navParams.get('buddy_displayName');
      this.buddy_uid = navParams.get('buddy_uid');
      this.sourcePage = navParams.get('source');

      if(myUID == this.buddy_uid){
        this.thisIsMe = true;
      }//endif
    }).catch(e => {
    });

  }
  ionViewWillLeave() {

  }
  forwardContact (){
    this.navCtrl.push("PickContactToPage", {source : this.sourcePage,friend_to_forward_uid : this.buddy_uid,friend_to_forward_displayName : this.buddy_displayName, friend_to_forward_catId:"", friend_to_forward_catName : ""});
  }


  backButtonClick() {
      console.log ("BACK BUTTON CLICK" );
      //this.navCtrl.setRoot('TabsPage');
      //This does not work : dont know why
      this.navCtrl.pop({animate: true, direction: "forward"});
  }
  ionViewDidLoad() {
  }

}
