import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, Platform, NavParams, AlertController, Events, ModalController, ToastController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { ContactProvider } from '../../providers/contact/contact';
import { RequestsProvider } from '../../providers/requests/requests';
import { connreq } from '../../models/interfaces/request';
import { LoadingProvider } from '../../providers/loading/loading';
import { ChatProvider } from '../../providers/chat/chat';
import { SmsProvider } from '../../providers/sms/sms';
import { FcmProvider } from '../../providers/fcm/fcm';
//import { GroupsProvider } from '../../providers/groups/groups';
import { env } from '../../app/app.angularfireconfig';
import { TpstorageProvider } from '../../providers/tpstorage/tpstorage';

@IonicPage()
@Component({
    selector: 'page-pick-contact-from',
    templateUrl: 'pick-contact-from.html',
})
export class PickContactFromPage {
    friend_asking_referral_uid;
    friend_asking_referral_displayName;
    friend_asking_referral_catId;
    friend_asking_referral_catName;
    showheader: boolean = true;
    cntCounter;
    contactList: any = [];
    allregisteUserData: any = [];
    metchContact: any = [];
    mainArray: any = [];
    tempArray: any = [];
    myfriends;
    userpic = env.userPic;
    isData: boolean = false;
    reqStatus = false;
    isInviteArray: any = [];
    newmessage;
    updatedRef:String;
    source;
    newrequest = {}   as connreq;
    temparr = [];
    filteredusers = [];
    userId;
    constructor(
        public events: Events,
        private platform: Platform,
        public loadingProvider: LoadingProvider,
        public contactProvider: ContactProvider,
        public userservice: UserProvider,
        public navCtrl: NavController,
        public navParams: NavParams,
        public alertCtrl: AlertController,
        public requestservice: RequestsProvider,
        public chatservice: ChatProvider,
        public zone: NgZone,
        public smsservice: SmsProvider,
        public fcm: FcmProvider,
        public modalCtrl: ModalController,
        private toastCtrl: ToastController,
        public tpStorageService: TpstorageProvider
    ) {
        this.friend_asking_referral_uid = navParams.get('friend_asking_referral_uid');
        this.friend_asking_referral_displayName = navParams.get('friend_asking_referral_displayName');
        this.friend_asking_referral_catId = navParams.get('friend_asking_referral_catId');
        this.friend_asking_referral_catName = navParams.get('friend_asking_referral_catName');
        this.source = navParams.get('source');
        this.cntCounter = 0;
    }
    tpInitilizeFromStorage (){
      this.tpStorageService.getItem('userUID').then((res: any) => {
         if(res){
           this.userId = res;
         }
      }).catch(e => { });
      this.tpStorageService.getItem('updated').then((res: any) => {
         if(res){
           this.updatedRef = res;
         }
      }).catch(e => { });
    }
    backButtonClick() {
        this.navCtrl.pop({
            animate: true,
            direction: "forward"
        });
    }

    pickContactExecute(buddy, isTapAllyMember) {
        //console.log ("pickContactExecutepickContactExecute");
        //console.log (buddy);
        //console.log (this.myfriends);
        //console.log (this.friend_asking_referral_uid);
        //Tapally Member so send push notification
        /*
           - First Get all buddies
           - Then select send to. It should be this.friend_asking_referral_uid
           - Then select message with buddy information
           - Then send push notification
           - Also send chat messages
           - Redirect back to buddypage
        */
        for (var key = 0; key < this.myfriends.length; key++) {
            ////console.log (this.myfriends[key].displayName)
            //Find out who is receipient
            if (this.friend_asking_referral_uid == this.myfriends[key].uid) {
                //send to the one who asked for referral

                let catStr = '';
                if (this.friend_asking_referral_catName) {
                    catStr = '(' + this.friend_asking_referral_catName + ')';
                }


                this.newmessage = 'Hi I am sending ' + catStr + ' referral to you : ' + buddy.displayName + " (" + buddy.mobile + ")";
                this.addmessage(this.myfriends[key], buddy, this.myfriends[key]);

                //also tell the one who got referred
                if (isTapAllyMember) {
                    //referral is member of Tapally
                    this.newmessage = 'Hi I send your referral to ' + this.myfriends[key].displayName + " (" + this.myfriends[key].mobile + ")";
                    if (catStr) {
                        this.newmessage += '. ' + this.myfriends[key].displayName + ' is asking a referral for ' + catStr;
                    } //endif

                    this.addmessage(buddy, this.myfriends[key], this.myfriends[key]);
                } else {
                    //referral is not member of Tapally
                    console.log ("this.myfriends[key]");
                    console.log (this.myfriends[key]);
                    let msg = 'Hi, I send your referral to ' + this.myfriends[key].displayName + " (" + this.myfriends[key].mobile  + "). Sign up & download app at https://tapally.com ";
                    this.sendSMSCustomMsg(buddy, msg, this.myfriends[key]);
                } //endif
                break;
            }
        } //end for
    }

    sendToNextPage(originalChat) {
        this.cntCounter++;
        //console.log ("cntCounter:" + this.cntCounter);
        if (this.cntCounter >= 2) {
            this.cntCounter = 0;
            this.chatservice.initializebuddy(originalChat);
            if(this.source == "buddypage"){
              //mytodo : this should go to buddychatpage as well but it was not working properly so i temporarliy sending to tabspage with a toast
              this.navCtrl.setRoot('TabsPage');
      				const toast = this.toastCtrl.create({
      		      message: 'Your referral sent successfully. You can see status by clicking on your contact`s icon',
      		      duration: 5000
      		    });
      		    toast.present();
            } else {
              this.navCtrl.setRoot('BuddychatPage', {back_button_pop_on : false});
            }
        }
    }

    //here buddy is receipient and referral is contact being sent
    addmessage(buddy, referral, originalChat) {

        //Send Push notification : mytodo check if the buddy is online or not. if online then no need to send push notification
        let newMessage = 'You have received a message';
        this.fcm.sendNotification(buddy, newMessage, 'chatpage');

        //Send chat message
        this.chatservice.addnewmessageDirectReferral(this.newmessage, 'message', buddy,referral, 'backward').then(() => {
            this.newmessage = '';
            this.sendToNextPage(originalChat);
        });
    }
    ionViewWillEnter() {
        this.cntCounter = 0;
			  this.tpInitilizeFromStorage ();
	      this.requestservice.getmyfriends();
	      this.myfriends = [];
	      this.events.subscribe('friends', () => {
					this.events.unsubscribe('friends');
	        this.myfriends = [];
	        this.myfriends = this.requestservice.myfriends;
	      });
	  }
    ionViewDidLoad() {
  				this.loadingProvider.presentLoading();
  				this.contactProvider.getSimJsonContacts()
  				.then(res => {
  					console.log ("asdjasdas");
  					console.log (res);
  						Object.keys(res).forEach(ele => {
  						   if(ele == "contacts"){
  								 this.mainArray = [];
  								 	Object.keys(res["contacts"] as any).forEach(key  => {
  										this.mainArray.push ({
  											"displayName": res["contacts"][key].displayName,
  											"mobile":  res["contacts"][key].mobile,
  											"mobile_formatted":  res["contacts"][key].mobile_formatted,
  											"status": "",
  											"isBlock":false,
  											"isUser":0,
  											"isdisable":false,
  											"photoURL":res["contacts"][key].photoURL,
  											"uid":res["contacts"][key].uid
  										});
  									});
  							 }//endif
  					  });
  						this.loadingProvider.dismissMyLoading();
  				}).catch(e => { });
  	}//end function

    removeDuplicates(originalArray: any[], prop) {
        let newArray = [];
        let lookupObject = {};

        originalArray.forEach((item, index) => {
            lookupObject[originalArray[index][prop]] = originalArray[index];
        });

        Object.keys(lookupObject).forEach(element => {
            newArray.push(lookupObject[element]);
        });
        return newArray;
    }

    ionViewWillLeave() {
        //this.groupservice.getmygroups();
        //this.requestservice.getmyfriends();
        this.loadingProvider.dismissMyLoading();
    }
    initializeContacts() {
        this.mainArray = this.tempArray;
    }
    refreshPage() {
  		const confirm = this.alertCtrl.create({
        title: 'Sync All Contacts',
        message: 'It may take few minutes to re-sync all contacts from your device. Do you agree ? ',
        buttons: [
          {
            text: 'Cancel',
            handler: () => {
              //console.log('Disagree clicked');
            }
          },
          {
            text: 'Agree',
            handler: () => {
              //console.log('Agree clicked');
  						this.contactProvider.deleteFromTable() ;
  						this.navCtrl.setRoot('ContactimportprogressPage', {source : 'PickContactFromPage'});
            }
          }
        ]
      });
      confirm.present();
  	}

    searchuser(ev: any) {

  		//initize the array
  		if(this.tempArray.length<=0){
  			this.tempArray = this.mainArray
  		} else {
  			this.mainArray = this.tempArray;
  		}//endif

  		let val = ev.target.value;
  		if (val && val.trim() != '') {
  			this.mainArray = this.mainArray.filter((item) => {
  				if (item.displayName != undefined) {
  					return (item.displayName.toLowerCase().indexOf(val.toLowerCase()) > -1);
  				}
  			});
  		}
  	}

    inviteReq(recipient) {
        /*let alert = this.alertCtrl.create({
            title: 'Invitation',
            subTitle: 'Invitation already sent to ' + recipient.displayName + '.',
            buttons: ['Ok']
        });
        alert.present();
        */
    }

    sendreq(recipient) {
        this.reqStatus = true;
        this.newrequest.sender = this.userId;
        this.newrequest.recipient = recipient.uid;
        if (this.newrequest.sender === this.newrequest.recipient)
            alert('You are your friend always');
        else {
            let successalert = this.alertCtrl.create({
                title: 'Request sent',
                subTitle: 'Your request has been sent to ' + recipient.displayName + '.',
                buttons: ['ok']
            });

            let userName: any;

            this.userservice.getuserdetails().then((res: any) => {
                userName = res.displayName;

                let newMessage = userName + " has sent you friend request.";

                this.fcm.sendNotification(recipient, newMessage, 'sendreq');
            });

            this.requestservice.sendrequest(this.newrequest).then((res: any) => {
                if (res.success) {
                    this.reqStatus = false;
                    successalert.present();
                    let sentuser = this.mainArray.indexOf(recipient);
                    this.mainArray[sentuser].status = "pending";
                }
            }).catch((err) => {})
        }
    }

    buddychat(buddy) {
            this.chatservice.initializebuddy(buddy);
            this.navCtrl.setRoot('BuddychatPage', {back_button_pop_on : false});
    }

    sendSMSCustomMsg(item, msg, originalChat) {
        this.loadingProvider.presentLoading();
        this.smsservice.sendSmsCustomMsg(item.mobile, msg).then((res) => {
            this.loadingProvider.dismissMyLoading();
            this.sendToNextPage(originalChat);
        }).catch(err => {
            //console.log (err);
            this.loadingProvider.dismissMyLoading();
            this.sendToNextPage(originalChat);
            /*let alert = this.alertCtrl.create({
                title: 'Message Send',
                subTitle: 'Error for sending message to ' + item.displayName + '.',
                buttons: ['Ok']
            });
            alert.present();
            */
        });
    } //end function

    sendSMS(item) {

        this.loadingProvider.presentLoading();

        this.smsservice.sendSms(item.mobile).then((res) => {
            this.loadingProvider.dismissMyLoading();
            if (res) {
            } else {
            }
        }).catch(err => {
            this.loadingProvider.dismissMyLoading();
        });
    }
    addnewContact(myEvent) {
        let profileModal = this.modalCtrl.create("AddContactPage");
        profileModal.present({
            ev: myEvent
        });
        profileModal.onDidDismiss((pages) => {})
    }
    goBack() {
        this.navCtrl.setRoot('TabsPage');
    }
}
