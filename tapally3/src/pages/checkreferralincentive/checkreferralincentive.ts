import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,App } from 'ionic-angular';
import { ReferralincentiveTypeProvider } from '../../providers/referralincentive-type/referralincentive-type';
import { UserProvider } from '../../providers/user/user';
import { TpstorageProvider } from '../../providers/tpstorage/tpstorage';
@IonicPage()
@Component({
  selector: 'page-checkreferralincentive',
  templateUrl: 'checkreferralincentive.html',
})
export class CheckreferralincentivePage {
  whitelabelObj = {'buddy_uid':0,'displayName':''};
  myIncentive;
  myIncentiveName;
  businessOwnerCheckingHisOwnPage:boolean = false;
  businessOwnerCheckingHisOwnPage_Checked:boolean = false;
  hasnotLoadedYet:boolean = false;
  rtlistComplete;
  constructor(public tpStorageService: TpstorageProvider,public userservice: UserProvider,public navCtrl: NavController, public navParams: NavParams,public rtService : ReferralincentiveTypeProvider,public app: App) {
    this.rtlistComplete = this.rtService.getList();

  }



    referralSendBackward (){
          this.app.getRootNav().push("SendReferralBackwardPage", {source :"checkreferralpage",buddy_uid : this.whitelabelObj.buddy_uid, buddy_displayName : this.whitelabelObj.displayName});//
    }


  ionViewWillLoad (){
    this.myIncentive = '';
    this.myIncentiveName = '';
    this.whitelabelObj = this.navParams.get('whitelabelObj');


    this.tpStorageService.get('userUID').then((myUID: any) => {
      this.businessOwnerCheckingHisOwnPage_Checked = true;
      if(myUID == this.whitelabelObj.buddy_uid) {
        this.businessOwnerCheckingHisOwnPage = true;
      }
    }).catch(e => {
      this.businessOwnerCheckingHisOwnPage_Checked = true;
    });

   this.userservice.getBusinessDetailsOther(this.whitelabelObj.buddy_uid).then((res)=>{
          this.hasnotLoadedYet = true;
          //jaswinder
          if(res){

            for(let x=0;x<this.rtlistComplete.length;x++){
              if(this.rtlistComplete[x].id == res['referral_type_id']){
                this.myIncentiveName   = this.rtlistComplete[x].name;
                break;
              }
            }//end for

            if(res['referral_type_id'] && res['referral_type_id']>0 && res['referral_detail']){

              let referral_detail = this.getDetail ( res['referral_type_id'] ,res['referral_detail']);
              this.myIncentive  = this.myIncentiveName + " ( "+ referral_detail +" ) ";
            } else {
              //No Incentive
              this.myIncentive = '';
            }


          }else{
            this.myIncentive = '';
          }
   }).catch(err=>{
   });




  }

  getDetail (referral_type_id, referral_detail){
    switch (referral_type_id) {
        case '1':
        case '2':
            //cash
            //Gift Card
            let onlyNumber = referral_detail.replace(/\D/g,'');
            ////console.log ("onlyNumber");
            ////console.log (onlyNumber);
            return "Amount "+onlyNumber;
        case '3':
        case '4':
        default:
            return referral_detail;
    }//end switch
  }

  backButtonClick (){
    this.navCtrl.pop({animate: true, direction: "forward"});
  }


  sendToBusinessReg (){
    //this.app.getRootNav().push("RegisterbusinessPage", {treatNewInstall : 0});
    this.navCtrl.push("RegisterbusinessPage", {treatNewInstall : 0});
  }

}
