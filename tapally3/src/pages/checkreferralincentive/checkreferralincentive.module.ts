import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CheckreferralincentivePage } from './checkreferralincentive';

@NgModule({
  declarations: [
    CheckreferralincentivePage,
  ],
  imports: [
    IonicPageModule.forChild(CheckreferralincentivePage),
  ],
})
export class CheckreferralincentivePageModule {}
