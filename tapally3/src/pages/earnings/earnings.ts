import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,App } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { SQLite,SQLiteObject } from '@ionic-native/sqlite';
import { CONST} from '../../app/const';
import { ReferralincentiveTypeProvider } from '../../providers/referralincentive-type/referralincentive-type';
@IonicPage()
@Component({
  selector: 'page-earnings',
  templateUrl: 'earnings.html',
})
export class EarningsPage {
  pageDoneLoading:boolean = false;
  recordExist:boolean = false;
  allChatListing_HTML = [];
  allReferrals_HTML = [];
  isNoRecord:boolean = false;
  rtlistComplete;
  constructor(private sqlite: SQLite,public navCtrl: NavController, public navParams: NavParams,public app: App,public userservice: UserProvider,public rtService : ReferralincentiveTypeProvider) {
  }

  addbuddy() {
        this.app.getRootNav().push('ContactPage', {source:"ImportpPage"});
  }
  ionViewDidLoad() {
     this.rtlistComplete = this.rtService.getList();
     this.fnGetLocalCache ();
  }

  backButtonClick() {
    this.navCtrl.pop({animate: true, direction: "right"});
  }

  goToIncentiveDeailsPage (item){
    if(null != item){
      this.navCtrl.push("IncentivedetailPage", { item: JSON.stringify(item) });
    }
  }

  //this is in multple places
  getDetail (referral_type_id, referral_detail){
    switch (referral_type_id) {
        case '1':
        case '2':
            //cash
            //Gift Card
            let onlyNumber = referral_detail.replace(/\D/g,'');
            return "Amount "+onlyNumber;
        case '3':
        case '4':
        default:
            return referral_detail;
    }//end switch
  }

  getReferralRecieved (){
    this.allReferrals_HTML = [];
    this.userservice.getReferralRecieved().then((referralRecieved)=>{
      for (let i = 0; i < this.allChatListing_HTML.length; i++) {
        for (var friendUid in referralRecieved) {
           this.recordExist = true;
           if(friendUid == this.allChatListing_HTML [i].uid){
                  for (var myKey in referralRecieved[friendUid]) {

                      let referalType = '';
                      for(var myReferType in this.rtlistComplete){
                          if(referralRecieved[friendUid][myKey].business_referral_type_id == this.rtlistComplete [myReferType].id ) {
                             referalType = this.rtlistComplete [myReferType].name;
                             break;
                          }
                      }//end for

                      let record = {
                        displayName : this.allChatListing_HTML [i].displayName,
                        photoURL : this.allChatListing_HTML [i].photoURL,
                        referral_displayName : referralRecieved[friendUid][myKey].referral.displayName,
                        redeemStatus : referralRecieved[friendUid][myKey].redeemStatus,
                        myKey :myKey,
                        friendUid : friendUid,
                        received_by: referralRecieved[friendUid][myKey].received_by,
                        sent_by: referralRecieved[friendUid][myKey].sent_by,
                        isBlock : this.allChatListing_HTML [i].isBlock,
                        business_referral_detail : this.getDetail (referralRecieved[friendUid][myKey].business_referral_type_id,referralRecieved[friendUid][myKey].business_referral_detail) ,
                        business_referral_type_id : referralRecieved[friendUid][myKey].business_referral_type_id,
                        business_referral_type_name : referalType,
                        business_referral_type_detail : referralRecieved[friendUid][myKey].business_referral_detail,
                        dateofmsg:referralRecieved[friendUid][myKey].dateofmsg,
                        timeofmsg: referralRecieved[friendUid][myKey].timeofmsg
                      };

                      //try to find unique record
                      let recJson = JSON.stringify(record);
                      let thisRecordExist = false;
                      for(var l in this.allReferrals_HTML){
                          let tmpJson = JSON.stringify(this.allReferrals_HTML[l]);
                          if(recJson == tmpJson){
                            thisRecordExist = true;
                            break;
                          }//endif
                      }//endif
                      if(!thisRecordExist){
                        this.allReferrals_HTML.push (record);
                      }//endif
                  }
           }//endif
        }//end for
      }//end for
    }).catch(err=>{
    });

  }//end for

  fnGetLocalCache() {
            this.sqlite.create({
              name: 'gr.db',
              location: 'default'
            }).then((db: SQLiteObject) => {
              //Get Friends
              db.executeSql(CONST.create_table_statement_tapally_friends, {})
              .then(res => {
                  db.executeSql("SELECT * FROM cache_tapally_friends", {})
                  .then(res => {
                            this.pageDoneLoading = true;
                            if(res.rows.length>0){
                                   //if row exists
                                   this.allChatListing_HTML = [];
                                   this.isNoRecord = false;
                                   for (var i = 0; i < res.rows.length; i++) {
                                              //////console.log (res.rows.item(i));
                                             let isBlock_ = false ;
                                             if(res.rows.item(i).isBlock){
                                               isBlock_ = true ;
                                             }
                                             let isActive_ = true ;
                                             if(!res.rows.item(i).isactive){
                                               isActive_ = false ;
                                             }
                                             let  record = {};
                                             if(res.rows.item(i).gpflag>0){
                                             } else {
                                               ////////console.log ("gpflaggpflaggpflaggpflag= 0");
                                                //Individual
                                                let lastMssg = '';
                                                if(res.rows.item(i).lastMessage_message && res.rows.item(i).lastMessage_message!= "undefined"){
                                                  lastMssg = res.rows.item(i).lastMessage_message;
                                                }
                                                let tmofmsg = '';
                                                if(res.rows.item(i).timeofmsg){
                                                  tmofmsg = res.rows.item(i).timeofmsg;
                                                }
                                                record = {
                                                    uid : res.rows.item(i).uid,
                                                    displayName : res.rows.item(i).displayName,
                                                    gpflag : res.rows.item(i).gpflag,
                                                    isActive : isActive_,
                                                    isBlock : isBlock_,
                                                    mobile : res.rows.item(i).mobile,
                                                    selection : res.rows.item(i).selection,
                                                    unreadmessage : 0,
                                                    photoURL : res.rows.item(i).photoURL,
                                                    groupimage : res.rows.item(i).groupimage,
                                                    groupName : res.rows.item(i).groupName
                                                };
                                                //mytodo : there is some bug causing multiple duplciate entries being inserted in this.allChatListing_HTML. Please fix . I have been mitigating them but good if we can fix them
                                                this.allChatListing_HTML.push ( record ) ;
                                             }//endif
                                   }//end for

                                   if (this.allChatListing_HTML.length > 0) {
                                       this.isNoRecord = true;
                                   }

                                   this.getReferralRecieved ();
                            }
                  })
                  .catch(e => {
                      //Error in operation
                  });
              })
              .catch(e => {
              });//create table

            }).catch(e => {
            });//create database
       }//end function


}
