import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FriendAskingReferralPage } from './friend-asking-referral';

@NgModule({
  declarations: [
    FriendAskingReferralPage,
  ],
  imports: [
    IonicPageModule.forChild(FriendAskingReferralPage),
  ],
})
export class FriendAskingReferralPageModule {}
