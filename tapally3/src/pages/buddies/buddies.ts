import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { RequestsProvider } from '../../providers/requests/requests';
import { connreq } from '../../models/interfaces/request';
import firebase from 'firebase';


@IonicPage()
@Component({
  selector: 'page-buddies',
  templateUrl: 'buddies.html',
})
export class BuddiesPage {
    newrequest = {} as connreq;
    temparr = [];
    filteredusers = [];
    showheader:boolean = true;
    constructor(public navCtrl: NavController, public navParams: NavParams,public userservice: UserProvider,public alertCtrl : AlertController, public requestservice : RequestsProvider) {
      this.userservice.getallusers().then((res: any) => {
        this.filteredusers = res;
        this.temparr = res;
     })
    }

  ionViewWillLeave() {
       this.showheader = false;
 }
  ionViewDidLoad() {
  }
  searchuser(searchbar) {
     this.filteredusers = this.temparr;
     var q = searchbar.target.value;
     if (q.trim() == '') {
       return;
     }
     this.filteredusers = this.filteredusers.filter((v) => {
       if (v.displayName.toLowerCase().indexOf(q.toLowerCase()) > -1) {
         return true;
       }
       return false;
     })
  }

  backButtonClick() {
      this.navCtrl.pop({animate: true, direction: "forward"});
  }
  sendreq(recipient) {
    this.newrequest.sender = firebase.auth().currentUser.uid;
    this.newrequest.recipient = recipient.uid;
    if (this.newrequest.sender === this.newrequest.recipient)
      alert('You are your friend always');
    else {
      let successalert = this.alertCtrl.create({
        title: 'Request sent',
        subTitle: 'Your request was sent to ' + recipient.displayName,
        buttons: ['ok']
      });

      this.requestservice.sendrequest(this.newrequest).then((res: any) => {
        if (res.success) {
          successalert.present();
          let sentuser = this.filteredusers.indexOf(recipient);

          this.filteredusers[sentuser].status = "pending";
        }
      }).catch((err) => {
        alert(err);
      })
    }
  }
}
