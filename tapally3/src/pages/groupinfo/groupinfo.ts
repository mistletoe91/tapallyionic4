import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, AlertController, ActionSheetController } from 'ionic-angular';
import { GroupsProvider } from '../../providers/groups/groups';
import { UserProvider } from '../../providers/user/user';
import { ImagehandlerProvider } from '../../providers/imagehandler/imagehandler';
import { LoadingProvider } from '../../providers/loading/loading';
import {env} from '../../app/app.angularfireconfig';
import { TpstorageProvider } from '../../providers/tpstorage/tpstorage';

@IonicPage()
@Component({
    selector: 'page-groupinfo',
    templateUrl: 'groupinfo.html',
})
export class GroupinfoPage {
    owner: boolean = false;
    groupmembers = [];
    groupImage;
    userpic=env.userPic;
    groupName;
    userId;
    currentgroup = [];
    ownergroup = [];
    allMembers = [];
    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public groupservice: GroupsProvider,
        public events: Events,
        public userserivce: UserProvider,
        public alertCtrl: AlertController,
        public zone: NgZone,
        public actionSheet: ActionSheetController,
        public imghandler: ImagehandlerProvider,
        public loadingProvider: LoadingProvider,
        public tpStorageService: TpstorageProvider
    ) {
      this.tpStorageService.getItem('userUID').then((userID: any) => {
        if (userID != undefined) {
          this.userId = userID;
        }
        this.doInitialize ();
      }).catch(e => { });
    }

    doInitialize () {
      if (this.groupservice.currentgroupname != undefined) {
          this.groupName = this.groupservice.currentgroupname;
      } else {
          this.groupName = this.navParams.get('groupName');
          this.groupservice.currentgroupname = this.groupName;

      }
      this.events.subscribe('gotmembers', () => {
          this.events.unsubscribe('gotmembers');
          this.zone.run(() => {
              this.groupmembers = this.groupservice.currentgroup;
              this.getGroupOwner();
          })
      });
      this.groupservice.getgroupimage();
      this.groupservice.getownership(this.groupName).then((res) => {
          if (res) {
              this.owner = true;
              this.groupmembers = this.groupservice.currentgroup;
              this.getGroupOwner();
          } else {
              this.groupservice.getgroupmembers();
          }
      });
      this.groupImage = this.groupservice.grouppic;
      if (this.groupName != undefined) {
          this.groupservice.getintogroup(this.groupName);
      }
    }

    ionViewCanEnter() {
        this.zone.run(() => {
            this.groupservice.getgroupInfo(this.groupName).then((res: any) => {
                this.groupImage = res.groupimage;
            })
        });
    }

    backButtonClick() {
        this.navCtrl.setRoot('TabsPage');
        //this.navCtrl.pop({animate: true, direction: "forward"});
    }
    getGroupOwner() {

        if (this.groupservice.currentgroupname != undefined) {
            this.groupName = this.groupservice.currentgroupname;
        } else {
            this.groupName = this.navParams.get('groupName');
            this.groupservice.currentgroupname = this.groupName;
        }
        this.groupservice.getgroupInfo(this.groupName).then((ress: any) => {
            let owners = this.groupservice.converanobj(ress.owner)
            this.ownergroup = owners;
            let currentuser = this.userId;
            if (this.groupmembers.length > 0 && this.ownergroup.length > 0) {
                let Allmember = [];
                for (let i = 0; i < this.groupmembers.length; i++) {
                    if (this.groupmembers[i].uid != currentuser) {
                        this.userserivce.getuserblock(this.groupmembers[i]).then((res) => {
                            this.groupmembers[i].blockby = res;
                        })
                    }
                    else {
                        this.groupmembers[i].blockby = false;
                    }
                    let flag = 0;
                    for (let q = 0; q < this.ownergroup.length; q++) {
                        if (this.groupmembers[i].uid == this.ownergroup[q].uid) {
                            this.groupmembers[i].admin = true;
                            Allmember.push(this.groupmembers[i]);
                            flag = 1;
                            break;
                        } else {
                            flag = 0
                        }
                    }
                    if (flag == 0) {
                        this.groupmembers[i].admin = false;
                        Allmember.push(this.groupmembers[i])
                    }

                }
                this.zone.run(() => {
                    this.allMembers = [];
                    this.allMembers = Allmember;
                })
            }
            this.currentgroup = ress;
        })
    }

    editgroupname() {
        let alert = this.alertCtrl.create({
            title: 'Edit Group Name',
            inputs: [{
                name: 'groupname',
                placeholder: 'Give a new groupname',
                value: this.groupName
            }],
            buttons: [{
                text: 'Cancel',
                role: 'cancel',
                handler: data => {
                }
            }, {
                text: 'Set',
                handler: data => {
                    if (data.groupname) {
                        if (data.groupname == "") {
                            let namealert = this.alertCtrl.create({
                                buttons: ['okay'],
                                message: 'Please enter the groupname proper!'
                            });
                            namealert.present();
                        } else {
                            if (data.groupname.length < 26) {
                                if (data.groupname.trim()) {
                                    this.groupservice.updategroupname(this.groupName, data.groupname);
                                    this.groupservice.getintogroup(data.groupname);
                                    this.groupName = data.groupname;
                                } else {
                                    let namealert = this.alertCtrl.create({
                                        buttons: ['okay'],
                                        message: 'Please Enter Valid Groupname!'
                                    });
                                    namealert.present();
                                }
                            } else {
                                let namealert = this.alertCtrl.create({
                                    buttons: ['okay'],
                                    message: 'Group name maximum length is 25 characters'
                                });
                                namealert.present();

                            }

                        }
                    }

                }
            }]
        });
        alert.present();
    }
    addMemner() {
        this.navCtrl.push('GroupbuddiesaddPage');
    }
    openpopover(buddy) {
        if (this.owner) {
            let sheet = this.actionSheet.create({
                title: 'Modification',
                buttons: [
                    {
                        text: 'Remove ' + buddy.displayName,
                        icon: 'trash',
                        handler: () => {
                            this.removemember(buddy);
                        }
                    }, {
                        text: 'Make group admin',
                        icon: 'person-add',
                        handler: () => {
                            this.groupservice.addGroupAdmin(buddy, this.groupName);
                            this.groupservice.getgroupmembers();
                        }
                    }, {
                        text: 'Cancel',
                        role: 'cancel',
                        icon: 'cancel',
                        handler: () => {
                        }
                    }
                ]
            })
            sheet.present();
        }
    }
    removemember(member) {
        var index = this.allMembers.findIndex(function (o) {
            return o.uid === member.uid;
        });
        if (index !== -1) { this.allMembers.splice(index, 1); }
        if (this.allMembers.length == 0) {
            this.groupservice.deletegroup().then(() => {
                this.navCtrl.setRoot('TabsPage');
            }).catch((err) => {
            });
        } else {
            this.groupservice.deletemember(member);
        }
    }
    updategroupimage() {
        this.imghandler.gpuploadimage(this.groupName).then((res: any) => {
            this.loadingProvider.dismissMyLoading();
            if (res) {
                this.groupservice.updategroupImage(res, this.groupName);
                this.groupImage = res;
            } else {
                this.loadingProvider.dismissMyLoading();
            }
        }).catch((err) => {
            if (err == 20) {
                this.navCtrl.pop();
            }
        })
    }
    leaveGroup() {
        let alert = this.alertCtrl.create({
            title: 'Leave Group',
            message: 'Do you want to leave this group?',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: () => {
                    }
                },
                {
                    text: 'Yes',
                    handler: () => {
                        this.groupservice.leavegroup().then((res) => {
                            if (res) {
                                this.navCtrl.setRoot('TabsPage');
                            }
                        }).catch((err) => {
                        })
                    }
                }
            ]
        });
        alert.present().then(() => {
        });

    }
    deleteGroup() {
        let alert = this.alertCtrl.create({
            title: 'Delete Group',
            message: 'Do you want to delete this group?',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: () => {
                    }
                },
                {
                    text: 'Yes',
                    handler: () => {
                        this.groupservice.deletegroup().then(() => {
                            this.navCtrl.setRoot('TabsPage');
                        }).catch((err) => {
                        });
                    }
                }
            ]
        });
        alert.present().then(() => {
        });
    }

    openpopoverforadmin(buddy) {

        if (this.owner && this.groupservice.userId != buddy.uid) {
            let sheet = this.actionSheet.create({
                title: 'Modification',
                buttons: [
                    {
                        text: 'Dismiss as Admin',
                        icon: 'trash',
                        handler: () => {
                            this.groupservice.removefromAdmin(buddy);
                        }
                    }, {
                        text: 'Remove ' + buddy.displayName,
                        icon: 'person-add',
                        handler: () => {
                            this.groupservice.removeAdmin(buddy);
                        }
                    }, {
                        text: 'Cancel',
                        role: 'cancel',
                        icon: 'cancel',
                        handler: () => {
                        }
                    }
                ]
            })
            sheet.present();
        }
    }
    goBack() {
        this.navCtrl.pop()
    }
}
