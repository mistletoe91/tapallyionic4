import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-outreach-for-business',
  templateUrl: 'outreach-for-business.html',
})
export class OutreachForBusinessPage {
  showheader:boolean = true;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  backButtonClick() {
      this.navCtrl.pop({animate: true, direction: "forward"});
  }
  ionViewWillLeave() {
     this.showheader = false;
  }
  senToInvite (){
    this.navCtrl.push("ContactPage", {source:"OutreachForBusinessPage"});
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad OutreachForBusinessPage');
  }

}
