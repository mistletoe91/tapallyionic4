import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the RequestAfterSelectedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-request-after-selected',
  templateUrl: 'request-after-selected.html',
})
export class RequestAfterSelectedPage {
  showheader:boolean = true;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  ionViewWillLeave() {
     this.showheader = false;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RequestAfterSelectedPage');
  }

}
