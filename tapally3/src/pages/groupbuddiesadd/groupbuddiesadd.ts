import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Platform } from 'ionic-angular';
import { RequestsProvider } from '../../providers/requests/requests';
import { GroupsProvider } from '../../providers/groups/groups';

@IonicPage()
@Component({
  selector: 'page-groupbuddiesadd',
  templateUrl: 'groupbuddiesadd.html',
})
export class GroupbuddiesaddPage {

	myfriends = [];
	groupmembers = [];
	tempmyfriends = [];
	selectedMember:any=[];
  showheader:boolean = true;

  constructor(public navCtrl: NavController, public navParams: NavParams, public requestservice: RequestsProvider, public groupservice: GroupsProvider,public platform:Platform) {

  }
  backButtonClick() {
      this.navCtrl.pop({animate: true, direction: "forward"});
  }
  ionViewDidLoad() {
  }
  ionViewWillLeave() { 
  }
  ionViewWillEnter() {
    this.myfriends = this.requestservice.myfriends;
    this.groupmembers = this.groupservice.currentgroup;

		for (var key in this.groupmembers){
			for (var friend in this.myfriends) {
				if (this.groupmembers[key].uid === this.myfriends[friend].uid)
					this.myfriends.splice(this.myfriends.indexOf(this.myfriends[friend]), 1);
				}
		}

		 this.tempmyfriends = this.myfriends;

  }

  addbuddy(buddy) {
    if(this.selectedMember.length == 0){
      this.selectedMember.push(buddy);
    }else{
      let flagofbuddy = false;
      let posionofbuddy = 0;
      for(let i=0;i<this.selectedMember.length;i++){
        if(this.selectedMember[i].uid == buddy.uid){
          flagofbuddy = true;
          posionofbuddy = i;
          break;
        }else{
          flagofbuddy = false;
        }
      }
      if(flagofbuddy == true){
          this.selectedMember.splice(posionofbuddy,1);
      }else{
          this.selectedMember.push(buddy);
      }
    }
  }

  initializeItems(){
    this.myfriends = this.tempmyfriends;
  }

  searchuser(ev: any){
    this.initializeItems();

    const val = ev.target.value;

    if (val && val.trim() != '') {
      this.myfriends = this.myfriends.filter((item) => {
        return (item.displayName.toLowerCase().indexOf(val.toLowerCase().trim()) > -1);
      });
    }
  }

  goBack(){
    this.navCtrl.pop();
  }

  goNext(){
    this.groupservice.tempAddMembers(this.selectedMember,this.groupservice.currentgroupname);
    this.groupservice.getintogroup(this.groupservice.currentgroupname);
    this.groupservice.getgroupmembers();
    this.navCtrl.pop();
  }

}
