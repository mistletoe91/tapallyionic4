import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SQLite,SQLiteObject } from '@ionic-native/sqlite';
import { CONST} from '../../app/const';
import { TpstorageProvider } from '../../providers/tpstorage/tpstorage';
@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {

  //tab1Root: string = 'HomePage';
  tab2Root: string = 'ChatsPage';
  tab3Root: string = 'ReferralPage';
  //tab4Root: string = 'BroadcastPage';

  constructor(public navCtrl: NavController, public navParams: NavParams,private sqlite: SQLite,public tpStorageService: TpstorageProvider) {
  }

  ionViewWillEnter() {
    this.tpStorageService.getItem('ever_referral_sent').then((res: any) => {
			   if(res){
           this.tab3Root = "ReferralPage";
         } else {
           this.CheckFlagForRedirection();
           this.tab3Root = "ReferralInfopagePage";
         }
		}).catch(e => {
      this.CheckFlagForRedirection();
      this.tab3Root = "ReferralInfopagePage";
    });
  }

  CheckFlagForRedirection() {
       this.sqlite.create({
         name: 'gr.db',
         location: 'default'
       }).then((db: SQLiteObject) => {
         db.executeSql(CONST.create_table_statement_key_val, {})
         .then(res => {
             db.executeSql("SELECT val FROM key_val where key like 'ever_referral_sent'", {})
             .then(res => {
                 if(res.rows.length>0){
                         //Record exists
                         //console.log ("Referral Record exists : so setting tab to ReferralPage ");
                         this.tab3Root = "ReferralPage";
                         this.tpStorageService.setItem('ever_referral_sent', "1");
                 } else {
                         //Record does not exist
                         //console.log ("Referral Record DOES NOT exists : so setting tab to ReferralInfopagePage ");
                         this.tab3Root = "ReferralInfopagePage";
                 }//endif
             })
             .catch(e => {
             });
         })
         .catch(e => {
         });//create table
       });//create database
  }//end function



  ionViewDidLoad() {
  }

}
