import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AllbuddyContactsPage } from './allbuddy-contacts';

@NgModule({
  declarations: [
    AllbuddyContactsPage,
  ],
  imports: [
    IonicPageModule.forChild(AllbuddyContactsPage),
  ],
})
export class AllbuddyContactsPageModule {}
