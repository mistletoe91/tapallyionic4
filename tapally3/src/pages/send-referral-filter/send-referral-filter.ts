import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the SendReferralFilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-send-referral-filter',
  templateUrl: 'send-referral-filter.html',
})
export class SendReferralFilterPage {
  showheader:boolean = true;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  backButtonClick() {
      this.navCtrl.pop({animate: true, direction: "forward"});
  }  
  ionViewWillLeave() {
     this.showheader = false;
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SendReferralFilterPage');
  }

}
