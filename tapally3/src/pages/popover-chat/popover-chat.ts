import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Events } from 'ionic-angular';
import { App } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-popover-chat',
  templateUrl: 'popover-chat.html',
})
export class PopoverChatPage {
  constructor(public viewCtrl: ViewController, public navCtrl: NavController, public navParams: NavParams,
    public app: App, public event: Events) {
  }

  userProfile() {
    this.viewCtrl.dismiss({page:"ProfilepicPage"});
  }
  SettingPage() {
    this.viewCtrl.dismiss({page:"SettingPage"});
  }
  userGroups() {
    this.viewCtrl.dismiss({page:"GroupbuddiesPage"});
  }

  ionViewWillLeave() {
  }
}
