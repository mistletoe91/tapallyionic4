import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ImportcontactsPage } from './importcontacts';

@NgModule({
  declarations: [
    ImportcontactsPage,
  ],
  imports: [
    IonicPageModule.forChild(ImportcontactsPage),
  ],
})
export class ImportcontactsPageModule {}
