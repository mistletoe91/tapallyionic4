import { Component } from '@angular/core';
import { NavController, NavParams,Platform,Events,ViewController ,App} from 'ionic-angular';
import { CatProvider } from '../../providers/cats/cats';
import { RequestsProvider } from '../../providers/requests/requests';
@IonicPage()
@Component({
  selector: 'page-referral',
  templateUrl: 'referral.html',
})
export class ReferralPage {
  myrequests = [];
  requestcounter = null;
  items;
  showheader:boolean = true;
  isVirgin:boolean = false;
  constructor(platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public catservice: CatProvider,
    public events: Events,
    public app: App,
    public requestservice: RequestsProvider,
    public viewCtrl: ViewController
  ) {
    platform.ready().then(() => {
         this.initializeItems();
    });
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
  sendToBroadcastPage (){
    this.app.getRootNav().push("BroadcastPage", {broadcast_retry : false});
  }
  presentPopover(myEvent) {
      this.navCtrl.push("NotificationPage");
      //this.requestcounter = this.myrequests.length;
  }
  ionViewWillLeave() {
  }
  ionViewWillEnter() {
      /*this.requestservice.getmyrequests();
      this.events.subscribe('gotrequests', () => {
          this.myrequests = [];
          this.myrequests = this.requestservice.userdetails;
          if (this.myrequests) {
              this.requestcounter = this.myrequests.length;
          }
      })
      */
  }
  sendreferraldirectly (){
    this.navCtrl.push("SearchNextPage", {src : 1});
  }

  getItems(ev) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the ev target
    var val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }
  sendToEarningsPage (){
    this.app.getRootNav().push("EarningsPage");
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ReferralPage');
  }
  //not used
  fnGoToSearchNextPage() {
      this.navCtrl.push("SearchNextPage", {src : 1});
  }
  fnGoToHomeAfterINeedTodayPage(item_id) {
      this.navCtrl.push("HomeAfterINeedTodayPage", {cat_id : item_id});
  }

  initializeItems() {
      this.items = this.catservice.getCats();
  }
}
