import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,App} from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserProvider } from '../../providers/user/user';
import firebase from 'firebase';
import { TpstorageProvider } from '../../providers/tpstorage/tpstorage';
@IonicPage()
@Component({
  selector: 'page-displayname',
  templateUrl: 'displayname.html',
})
export class DisplaynamePage {
  authForm : FormGroup;
  username;
  submitted:boolean=false;
  msgstatus: boolean = false;
  showheader:boolean = true;
  userId;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public fb: FormBuilder,
    public app: App,
    public userservice: UserProvider
    , public tpStorageService: TpstorageProvider
    ) {

    this.authForm=this.fb.group({
      username:['',Validators.compose([Validators.required])]
    })
  }
  ionViewWillLeave() {
     this.showheader = false;
  }
  ionViewDidLoad() {
    this.loadusername();
  }
  loadusername(){
    let userId;
    //console.log ("firebase.auth().currentUser");
    //console.log (firebase.auth().currentUser);
    if (firebase.auth().currentUser) {
      userId = firebase.auth().currentUser.uid;
   } else {
      //console.log ("..Trying to get userUID from storage service");
      this.tpStorageService.getItem('userUID').then((res: any) => {
         //console.log ("res +++ res");
         //console.log (res);
         if(res){
           this.userId = res;
         }
      }).catch(e => {
          this.tpStorageService.setItem('userUID',userId );
          this.userservice.saveUserIdInSqlLite (userId);
      });
  }
  if (userId != undefined) {
    this.userservice.getuserdetails().then((res)=>{
      if(res){
        this.msgstatus=true;
        this.username=res['displayName'];
      }else{
        this.username='';
      }
    }).catch(err=>{
    });
  }else{
    this.username='';
  }

  }
  onChange(e) {
    let usnr = this.username;
    if (usnr.trim()) {
        this.msgstatus = true;
    }
    else {
        this.msgstatus = false;
    }
  }
  save(){
    this.submitted=true;
    if(this.authForm.valid){
      if(this.username.trim()!=null || this.username.trim()!= ""){
     this.username=this.username.trim();
        this.userservice.updatedisplayname(this.username).then((res: any) => {
          if (res.success) {
            this.app.getRootNav().push ("RegisterbusinessPage", {treatNewInstall : 1})
          }
        })
        } else {

        }
    }

    }

}
