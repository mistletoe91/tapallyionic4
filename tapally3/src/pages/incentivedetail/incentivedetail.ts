import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,App,ToastController ,Platform } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { SQLite,SQLiteObject } from '@ionic-native/sqlite';
import { CONST} from '../../app/const';
import { ChatProvider } from '../../providers/chat/chat';
import { FcmProvider } from '../../providers/fcm/fcm';
@IonicPage()
@Component({
  selector: 'page-incentivedetail',
  templateUrl: 'incentivedetail.html',
})
export class IncentivedetailPage {
  pageDoneLoading:boolean = false;
  recordExist:boolean = true;
  item = {
    "received_by" : "",
    "myKey" : "",
    "sent_by" : ""
  } ;
  constructor(public fcm: FcmProvider,public platform: Platform,public toastCtrl: ToastController,public chatservice: ChatProvider,public userservice: UserProvider,private sqlite: SQLite,public navCtrl: NavController, public navParams: NavParams,public app: App) {

  }

  buddychat(buddy) {
      //mytodo : it should go to buddychatpage but it is not working on IOS
      //this.navCtrl.setRoot('BuddychatPage', {back_button_pop_on : false});
       if (this.platform.is('ios')) {
           //ios
           this.navCtrl.setRoot('TabsPage');
           const toast = this.toastCtrl.create({
                 message: 'Operation performed successfully',
                 duration: 3000,
                 position: 'top'
           });
           toast.present();
       } else {
          //android
          this.navCtrl.setRoot('BuddychatPage', {back_button_pop_on : false});
       }


	}

  sendBuddyMessage_RedeemRequest (buddy){
    let msg = "Request For Incentives";
    this.chatservice.addnewmessageRedeemRequest(msg, 'message', '', this.item.myKey, this.item.sent_by ,this.item.received_by).then(() => {
        if(buddy){

            let newMessage = 'You received a request to redeem incentives';
            this.fcm.sendNotification(buddy, newMessage, 'chatpage');

            this.buddychat (buddy);
        }
    });
  }

  fnOpenChatPage ( ){
    this.userservice.getbuddydetails(this.item.received_by).then((buddy) => {
        this.chatservice.initializebuddy(buddy);
        this.buddychat (buddy);
    })
  }
  fnAskBuddyToAddSomeIncentives (){
    //Send request to add some incentive and register business
    this.userservice.getbuddydetails(this.item.received_by).then((buddy) => {
        this.chatservice.initializebuddy(buddy);
        this.sendBuddyMessage_RedeemRequest (buddy);
    })
  }

  fnSendRedeemIncentiveRequest (){
    //Send requet to redeem Incentive
    this.userservice.getbuddydetails(this.item.received_by).then((buddy) => {
        //this.chatservice.initializebuddy(buddy);
        this.sendBuddyMessage_RedeemRequest (buddy);
        this.navCtrl.pop({animate: true, direction: "right"});

        const toast = this.toastCtrl.create({
              message: 'Request has been posted',
              duration: 3000,
              position: 'top'
        });
        toast.present();


    })
  }

  ionViewDidLoad() {
     //this.rtlistComplete = this.rtService.getList();
     if(this.navParams.get('item')){
       this.item = JSON.parse(this.navParams.get('item'));
       this.pageDoneLoading = true;
     }
  }

  backButtonClick() {
    this.navCtrl.pop({animate: true, direction: "right"});
  }
}
