import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Events, ItemSliding ,Platform  } from 'ionic-angular';
import { RequestsProvider } from '../../providers/requests/requests';
import { ContactProvider } from '../../providers/contact/contact';
import { LoadingProvider } from '../../providers/loading/loading';
import { Storage } from '@ionic/storage';
import { FcmProvider } from '../../providers/fcm/fcm';
import { UserProvider } from '../../providers/user/user';
import { GroupsProvider } from '../../providers/groups/groups';
@IonicPage()
@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html',
})
export class NotificationPage {
	myrequests;
	isData:boolean = false;
  notificationList:any;
  messageList:any =[];
  messages=0;
  acceptStatus=false;
  showheader:boolean = true;
  constructor(public userservice: UserProvider,public platform:Platform, public fcm: FcmProvider, public loadingProvider: LoadingProvider, public contactProvider:ContactProvider, public storage: Storage, public events: Events, public navCtrl: NavController, public navParams: NavParams, public requestservice: RequestsProvider, public alertCtrl : AlertController,public groupservice: GroupsProvider) {
  }
  ionViewDidLoad() {
    //this.loadingProvider.presentLoading();

    this.requestservice.getmyrequests_promise().then((res) => {
      this.notificationList = res;
      if (this.notificationList.length > 0) {
         this.isData = false;
      }else{
        this.notificationList = [];
      	this.isData = true;
      }
      //this.loadingProvider.dismissMyLoading();
    })
  }//end function

  ionViewWillLeave() {
    this.showheader = false;
    this.requestservice.getmyfriends();
    this.groupservice.getmygroups();
    this.loadingProvider.dismissMyLoading();
    }

    backButtonClick() {
        this.navCtrl.pop({animate: true, direction: "forward"});
    }
  getallNotification(myrequests){
  	 this.contactProvider.getSimJsonContacts().then(res => {
       this.notificationList = myrequests;
       this.loadingProvider.dismissMyLoading();
       /*
  	 	if (res['contacts'].length > 0) {
        for (let j = 0; j < myrequests.length; j++) {
  	 		  for (let i = 0; i < res['contacts'].length; i++) {
  	 				if ( res['contacts'][i].mobile == myrequests[j].mobile) {
  	 					 myrequests[j].displayName = res['contacts'][i].displayName
               break;
    	 				}else{
                 myrequests[j].displayName = myrequests[j].mobile
               }
  	 			}
  	 		}
  	 		this.notificationList = myrequests;
        this.loadingProvider.dismissMyLoading();
  	 	}else{
         for (let i = 0; i < myrequests.length; ++i) {
           myrequests[i].displayName = myrequests[i].mobile
         }
  	 		 this.notificationList = myrequests;
  	 	}
      */
  	 }).catch(err => {
      this.loadingProvider.dismissMyLoading();
      if(err == 20){
        this.platform.exitApp();
      }
  	 })
  }

  accept(item,slidingItem: ItemSliding) {
    let userName:any;

    this.userservice.getuserdetails().then((res: any) => {
       userName = res.displayName;
       let newMessage = userName+" has accepted your friend request.";

       this.fcm.sendNotification(item,newMessage,'sendreq');
    });
    console.log ("NOTIFICATIONS");
    this.requestservice.acceptrequest(item).then(() => {
      console.log ("[[[[[[[[acceptrequest done");
      let newalert = this.alertCtrl.create({
        title: 'Friend added',
        subTitle: 'Tap on the friend to chat with him/her',
        buttons: ['Ok']
      });
      newalert.present();
    }).catch((err) => {
      console.log ("Error 4011");

    });
    console.log ("Async Called");
  }

  ignore(item,slidingItem : ItemSliding) {
    this.requestservice.deleterequest(item).then(() => {
      let newalert = this.alertCtrl.create({
        title: 'Delete',
        subTitle: 'Request Successfully Deleted',
        buttons: ['Ok']
        });
        newalert.present();
    }).catch((err) => {
      alert(err);
    });
  }

 blockUser(item,slidingItem: ItemSliding){
    this.requestservice.blockuserprof(item).then((res)=>{

 })

 }

  refreshPage(refresher) {
      this.ionViewDidLoad();
      setTimeout(() => {
        refresher.complete();
      }, 500);

    }

  goBack(){
    this.navCtrl.setRoot('TabsPage');
  }

}
