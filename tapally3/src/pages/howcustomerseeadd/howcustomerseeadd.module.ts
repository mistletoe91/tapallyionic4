import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HowcustomerseeaddPage } from './howcustomerseeadd';

@NgModule({
  declarations: [
    HowcustomerseeaddPage,
  ],
  imports: [
    IonicPageModule.forChild(HowcustomerseeaddPage),
  ],
})
export class HowcustomerseeaddPageModule {}
