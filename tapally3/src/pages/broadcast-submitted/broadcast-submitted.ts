import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-broadcast-submitted',
  templateUrl: 'broadcast-submitted.html',
})
export class BroadcastSubmittedPage {

  showheader:boolean = true;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  gotBackToBroadcastPage() {
      this.navCtrl.setRoot("BroadcastPage", {broadcast_retry : true});
  }

  gotoChatsPage() {
      this.navCtrl.setRoot("TabsPage");
  }

  ionViewDidLoad() {
  }

  ionViewWillLeave() {
     this.showheader = false;
  }

}
