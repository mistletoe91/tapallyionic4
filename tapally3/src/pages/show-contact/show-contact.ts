import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,ModalController,Platform} from 'ionic-angular';
import { ContactProvider } from '../../providers/contact/contact';
@IonicPage()
@Component({
  selector: 'page-show-contact',
  templateUrl: 'show-contact.html',
})
export class ShowContactPage {
  contacts:any=[];
  showheader:boolean = true;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public modalCtrl: ModalController,platform:Platform,
    public contactProvider: ContactProvider,) {
  }

  ionViewDidLoad() {

    this.getAllDeviceContacts();
  }
  ionViewWillLeave() {
     this.showheader = false;
  }
  getAllDeviceContacts(){
    let deviceContacts:any=[];
    let tmpcontacts:any=[];
    tmpcontacts =this.navParams.get('contacts');
    this.contactProvider.getSimJsonContacts().then((res)=>{
      deviceContacts =res['contacts'];
      deviceContacts.forEach(deviceelement => {
        tmpcontacts.forEach(elements => {

            if(deviceelement['mobile'] == elements['mobile']){
              elements['displayName']=deviceelement['displayName'];
              elements.isSave=true;
            }

        });
      });
      this.contacts=tmpcontacts;
    }).catch(e => { });
  }
  addContacts(contacts,myEvent){
    let profileModal = this.modalCtrl.create("AddContactPage",{contacts:contacts});
		profileModal.present({
			ev: myEvent
		});
		profileModal.onDidDismiss((pages)=>{
		this.ionViewDidLoad();
		})

  }
  backButtonClick() {
    this.navCtrl.pop();
  }

}
