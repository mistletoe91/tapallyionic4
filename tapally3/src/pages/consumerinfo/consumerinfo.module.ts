import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConsumerinfoPage } from './consumerinfo';

@NgModule({
  declarations: [
    ConsumerinfoPage,
  ],
  imports: [
    IonicPageModule.forChild(ConsumerinfoPage),
  ],
})
export class ConsumerinfoPageModule {}
