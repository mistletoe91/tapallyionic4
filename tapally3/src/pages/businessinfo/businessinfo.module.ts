import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BusinessinfoPage } from './businessinfo';

@NgModule({
  declarations: [
    BusinessinfoPage,
  ],
  imports: [
    IonicPageModule.forChild(BusinessinfoPage),
  ],
})
export class BusinessinfoPageModule {}
