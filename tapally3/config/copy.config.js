
/*
module.exports = {
    copyMaterialThemeCSS: {
        src: ['{{ROOT}}/node_modules/@angular/material/prebuilt-themes/indigo-pink.css'],
        dest: '{{WWW}}/assets'
    }
}
*/
// cross verify with node_modules/@ionic/app-scripts/config/sass.config.js

module.exports = {

  includePaths: [
  'node_modules/ionic-angular/themes',
  'node_modules/ionicons/dist/scss',
  'node_modules/ionic-angular/fonts',
  'node_modules/@angular/material/prebuilt-themes'
  ],

  includeFiles: [
    /\.(s?(c|a)ss)$/i
  ],

  excludeFiles: [
    // /\.(wp|ios|md).(scss)$/i
  ],

};
*/
